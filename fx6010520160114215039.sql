-- MySQL dump 10.13  Distrib 5.5.40, for Win32 (x86)
--
-- Host: localhost    Database: fx60105
-- ------------------------------------------------------
-- Server version	5.5.40

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `xt_access`
--

DROP TABLE IF EXISTS `xt_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `pid` smallint(6) NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `groupId` (`role_id`) USING BTREE,
  KEY `nodeId` (`node_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_access`
--

LOCK TABLES `xt_access` WRITE;
/*!40000 ALTER TABLE `xt_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_address`
--

DROP TABLE IF EXISTS `xt_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `moren` int(11) NOT NULL COMMENT '是否为默认地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_address`
--

LOCK TABLES `xt_address` WRITE;
/*!40000 ALTER TABLE `xt_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_blist`
--

DROP TABLE IF EXISTS `xt_blist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_blist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `rdt` int(11) NOT NULL,
  `nums` int(11) NOT NULL,
  `cj_nums` int(11) NOT NULL,
  `cj_time` int(11) NOT NULL,
  `is_cj` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `prices` decimal(12,2) NOT NULL,
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_blist`
--

LOCK TABLES `xt_blist` WRITE;
/*!40000 ALTER TABLE `xt_blist` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_blist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_bonus`
--

DROP TABLE IF EXISTS `xt_bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_bonus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `did` int(11) NOT NULL,
  `s_date` int(11) NOT NULL,
  `e_date` int(11) NOT NULL,
  `b0` decimal(12,2) NOT NULL,
  `b1` decimal(12,2) NOT NULL,
  `b2` decimal(12,2) NOT NULL,
  `b3` decimal(12,2) NOT NULL,
  `b4` decimal(12,2) NOT NULL,
  `b5` decimal(12,2) NOT NULL,
  `b6` decimal(12,2) NOT NULL,
  `b7` decimal(12,2) NOT NULL,
  `b8` decimal(12,2) NOT NULL,
  `b9` decimal(12,2) NOT NULL,
  `b11` decimal(12,2) NOT NULL,
  `b12` decimal(12,2) NOT NULL,
  `b10` decimal(12,2) NOT NULL,
  `encash_l` int(11) NOT NULL,
  `encash_r` int(11) NOT NULL,
  `encash` int(11) NOT NULL,
  `is_count_b` int(11) NOT NULL,
  `is_count_c` int(11) NOT NULL,
  `is_pay` int(11) NOT NULL,
  `u_level` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  `additional` varchar(50) NOT NULL COMMENT '额外奖',
  `encourage` varchar(50) NOT NULL COMMENT '阶段鼓励奖',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_bonus`
--

LOCK TABLES `xt_bonus` WRITE;
/*!40000 ALTER TABLE `xt_bonus` DISABLE KEYS */;
INSERT INTO `xt_bonus` VALUES (201,320,'7819213',102,1262275200,1452527999,830.00,1000.00,0.00,0.00,0.00,0.00,-50.00,-120.00,0.00,0.00,0.00,0.00,0.00,0,0,0,0,0,0,0,0,'',''),(200,1,'100000',102,1262275200,1452527999,469.30,0.00,50.00,0.00,444.00,0.00,-24.70,0.00,0.00,0.00,0.00,0.00,0.00,0,0,0,0,0,0,0,0,'','');
/*!40000 ALTER TABLE `xt_bonus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_card`
--

DROP TABLE IF EXISTS `xt_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL DEFAULT '0',
  `buser_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_no` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `card_pw` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `c_time` int(11) NOT NULL DEFAULT '0',
  `f_time` int(11) NOT NULL DEFAULT '0',
  `l_time` int(11) NOT NULL DEFAULT '0',
  `b_time` int(11) NOT NULL DEFAULT '0',
  `is_sell` int(3) NOT NULL DEFAULT '0',
  `is_use` int(3) NOT NULL DEFAULT '0',
  `money` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_card`
--

LOCK TABLES `xt_card` WRITE;
/*!40000 ALTER TABLE `xt_card` DISABLE KEYS */;
INSERT INTO `xt_card` VALUES (149,0,'','8181311080012','364399',1388974786,1383840000,1415376000,0,0,30,90.00),(148,0,'','8181311080011','364398',1388974786,1383840000,1415376000,0,0,30,90.00),(147,0,'','8181311080010','364397',1388974786,1383840000,1415376000,0,0,30,90.00),(146,0,'','8181311080009','364396',1388974786,1383840000,1415376000,0,0,30,90.00),(145,0,'','8181311080008','364395',1388974786,1383840000,1415376000,0,0,30,90.00),(143,0,'','8181311080006','364393',1388974786,1383840000,1415376000,0,0,30,90.00),(144,0,'','8181311080007','364394',1388974786,1383840000,1415376000,0,0,30,90.00),(142,0,'','8181311080005','364392',1388974786,1383840000,1415376000,0,0,30,90.00),(141,0,'','8181311080004','364391',1388974786,1383840000,1415376000,0,0,30,90.00),(140,0,'','8181311080003','364390',1388974786,1383840000,1415376000,0,0,30,90.00),(139,0,'','8181311080002','364389',1388974786,1383840000,1415376000,0,0,30,90.00),(138,0,'','8181311080001','364388',1388974786,1383840000,1415376000,0,0,30,90.00),(137,0,'','8181311080000','364387',1388974786,1383840000,1415376000,0,0,30,90.00);
/*!40000 ALTER TABLE `xt_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_cash`
--

DROP TABLE IF EXISTS `xt_cash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bid` int(11) NOT NULL DEFAULT '0',
  `b_user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `rdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `epoint` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `x1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sellbz` text COLLATE utf8_unicode_ci NOT NULL,
  `s_type` smallint(3) NOT NULL DEFAULT '0',
  `is_buy` int(11) NOT NULL DEFAULT '0',
  `bdt` int(11) NOT NULL DEFAULT '0',
  `ldt` int(11) NOT NULL DEFAULT '0',
  `okdt` int(11) NOT NULL DEFAULT '0',
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `is_sh` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_cash`
--

LOCK TABLES `xt_cash` WRITE;
/*!40000 ALTER TABLE `xt_cash` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_cash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_chongzhi`
--

DROP TABLE IF EXISTS `xt_chongzhi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_chongzhi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_bin NOT NULL,
  `epoint` decimal(12,2) NOT NULL,
  `huikuan` decimal(12,2) NOT NULL,
  `zhuanghao` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rdt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL DEFAULT '0',
  `is_pay` smallint(2) NOT NULL,
  `stype` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_chongzhi`
--

LOCK TABLES `xt_chongzhi` WRITE;
/*!40000 ALTER TABLE `xt_chongzhi` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_chongzhi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_cody`
--

DROP TABLE IF EXISTS `xt_cody`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_cody` (
  `c_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `cody_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_cody`
--

LOCK TABLES `xt_cody` WRITE;
/*!40000 ALTER TABLE `xt_cody` DISABLE KEYS */;
INSERT INTO `xt_cody` VALUES (1,'profile'),(2,'password'),(3,'Jj_FA'),(4,'4'),(5,'5'),(6,'6'),(7,'7'),(8,'8'),(9,'9'),(10,'10'),(11,'11'),(12,'12'),(13,'13'),(14,'14'),(15,'15'),(16,'16'),(17,'17'),(18,'18'),(19,'19'),(20,'20'),(21,'21'),(22,'22'),(23,'23'),(24,'24'),(25,'25'),(26,'26'),(27,'27'),(28,'28'),(29,'29'),(30,'30');
/*!40000 ALTER TABLE `xt_cody` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_cptype`
--

DROP TABLE IF EXISTS `xt_cptype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_cptype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tpname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `b_id` int(11) NOT NULL DEFAULT '0',
  `s_id` int(11) NOT NULL DEFAULT '0',
  `t_pai` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_cptype`
--

LOCK TABLES `xt_cptype` WRITE;
/*!40000 ALTER TABLE `xt_cptype` DISABLE KEYS */;
INSERT INTO `xt_cptype` VALUES (1,'家用电器',0,0,0,0),(2,'食品',0,0,0,0),(3,'生活用品',0,0,0,0);
/*!40000 ALTER TABLE `xt_cptype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fck`
--

DROP TABLE IF EXISTS `xt_fck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fck` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(64) DEFAULT NULL,
  `bind_account` varchar(50) DEFAULT NULL,
  `new_login_time` int(11) NOT NULL DEFAULT '0',
  `new_login_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_login_time` int(11) unsigned DEFAULT '0',
  `last_login_ip` varchar(40) DEFAULT NULL,
  `login_count` mediumint(8) unsigned DEFAULT '0',
  `verify` varchar(32) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `type_id` tinyint(2) unsigned DEFAULT '0',
  `info` text,
  `name` varchar(25) DEFAULT NULL,
  `dept_id` smallint(3) DEFAULT NULL,
  `user_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '用户编号',
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '银行开户名',
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '一级密码',
  `pwd1` varchar(50) DEFAULT NULL COMMENT '一级密码不加密',
  `passopen` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '二级密码',
  `pwd2` varchar(50) DEFAULT NULL COMMENT '二级密码不加密',
  `passopentwo` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '三级密码',
  `pwd3` varchar(50) DEFAULT NULL COMMENT '三级密码不加密',
  `nickname` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '昵称',
  `qq` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'QQ',
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行',
  `bank_card` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '银行卡号',
  `bank_province` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行所在省',
  `bank_city` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行所在城市',
  `bank_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '支行地址',
  `user_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '身份证',
  `user_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '联系地址',
  `user_post` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '联系方式',
  `user_tel` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '电话',
  `user_phone` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '手机',
  `rdt` int(11) NOT NULL COMMENT '注册时间',
  `treeplace` int(11) DEFAULT NULL COMMENT '区分左(中)右',
  `father_id` int(11) NOT NULL COMMENT '父节点',
  `father_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父名',
  `re_id` int(11) NOT NULL COMMENT '推荐ID',
  `re_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '推荐人名称',
  `is_pay` int(11) NOT NULL COMMENT '是否开通(0,1)',
  `is_lock` int(11) NOT NULL COMMENT '是否锁定(0,1)',
  `is_lock_ok` int(3) NOT NULL DEFAULT '0',
  `shoplx` int(11) NOT NULL COMMENT '报单中心ID',
  `shop_a` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '//中心所在省',
  `shop_b` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '//中心所在县',
  `is_agent` int(11) NOT NULL COMMENT '报单中心(0,1,2)',
  `agent_max` decimal(12,2) NOT NULL COMMENT '申请报单总金额',
  `agent_use` decimal(12,2) NOT NULL COMMENT '奖金币',
  `agent_cash` decimal(12,2) NOT NULL COMMENT '报单币',
  `agent_kt` decimal(12,2) NOT NULL DEFAULT '0.00',
  `agent_xf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `agent_cf` decimal(12,2) NOT NULL DEFAULT '0.00',
  `agent_gp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gp_num` int(11) NOT NULL DEFAULT '0',
  `agent_lock` decimal(12,2) NOT NULL DEFAULT '0.00',
  `live_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_in_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `p_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `no_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `ok_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `yuan_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_gupiao` int(11) NOT NULL DEFAULT '0',
  `tx_num` int(3) NOT NULL DEFAULT '0',
  `lssq` decimal(12,2) NOT NULL,
  `zsq` decimal(12,2) NOT NULL,
  `adt` int(11) NOT NULL COMMENT '申请成报单中心时间',
  `l` int(11) NOT NULL COMMENT '左边总人数',
  `r` int(11) NOT NULL COMMENT '右边总人数',
  `benqi_l` int(11) NOT NULL COMMENT '本期左区新增',
  `benqi_r` int(11) NOT NULL COMMENT '本期右区新增',
  `shangqi_l` int(11) NOT NULL COMMENT '上期左区剩余',
  `shangqi_r` int(11) NOT NULL COMMENT '上期右区剩余',
  `peng_num` int(11) NOT NULL DEFAULT '0',
  `u_level` int(11) NOT NULL COMMENT '等级(会员级别)',
  `is_boss` int(11) NOT NULL COMMENT '管理人为1,其它为0',
  `idt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL COMMENT '开通时间',
  `re_level` int(11) NOT NULL COMMENT '相对于推的代数',
  `p_level` int(11) NOT NULL COMMENT '绝对层数',
  `re_path` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '推荐的路径',
  `p_path` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '自已的路径',
  `tp_path` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_del` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL COMMENT '隶属报单ID',
  `shop_name` varchar(50) NOT NULL,
  `b0` decimal(12,2) NOT NULL COMMENT '每期总资金',
  `b1` decimal(12,2) NOT NULL COMMENT '奖1',
  `b2` decimal(12,2) NOT NULL COMMENT '奖2',
  `b3` decimal(12,2) NOT NULL COMMENT '奖3',
  `b4` decimal(12,2) NOT NULL COMMENT '奖4',
  `b5` decimal(12,2) NOT NULL COMMENT '奖5',
  `b6` decimal(12,2) NOT NULL COMMENT '奖6',
  `b7` decimal(12,2) NOT NULL COMMENT '奖7',
  `b8` decimal(12,2) NOT NULL COMMENT '奖8',
  `b9` decimal(12,2) NOT NULL COMMENT '奖9',
  `b12` decimal(12,2) NOT NULL COMMENT '奖12',
  `b11` decimal(12,2) NOT NULL COMMENT '奖11',
  `b10` decimal(12,2) NOT NULL COMMENT '奖10',
  `wlf` int(11) NOT NULL COMMENT '网络费',
  `wlf_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `cpzj` decimal(12,2) NOT NULL COMMENT '注册金额',
  `zjj` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '总奖金',
  `re_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '推荐总注册金额',
  `cz_epoint` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '冲值总金额',
  `lr` int(11) NOT NULL COMMENT '中间总单数',
  `shangqi_lr` int(11) NOT NULL COMMENT '中间上期剩余单数',
  `benqi_lr` int(11) NOT NULL COMMENT '中间本期单数',
  `user_type` varchar(200) NOT NULL COMMENT '多线登录限制',
  `re_peat_money` decimal(12,2) NOT NULL COMMENT 'x',
  `re_nums` smallint(4) NOT NULL DEFAULT '0' COMMENT 'x',
  `re_nums_b` int(11) NOT NULL DEFAULT '0',
  `re_nums_l` int(11) NOT NULL DEFAULT '0',
  `re_nums_r` int(11) NOT NULL DEFAULT '0',
  `duipeng` decimal(12,2) NOT NULL,
  `_times` int(11) NOT NULL,
  `fanli` int(11) NOT NULL,
  `fanli_time` int(11) NOT NULL,
  `fanli_num` int(11) NOT NULL,
  `fanli_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_fenh` smallint(2) NOT NULL,
  `open` smallint(2) NOT NULL,
  `f4` int(11) NOT NULL DEFAULT '0',
  `new_agent` smallint(1) NOT NULL DEFAULT '0' COMMENT '//是否新服务中心',
  `day_feng` decimal(12,2) NOT NULL DEFAULT '0.00',
  `get_date` int(11) DEFAULT '0',
  `get_numb` int(11) DEFAULT '0',
  `is_jb` int(11) DEFAULT '0',
  `sq_jb` int(11) DEFAULT '0',
  `jb_sdate` int(11) DEFAULT '0',
  `jb_idate` int(11) DEFAULT '0',
  `man_ceng` int(11) NOT NULL DEFAULT '0' COMMENT '//满层数',
  `prem` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '//权限',
  `wang_j` smallint(1) NOT NULL DEFAULT '0' COMMENT '//结构图',
  `wang_t` smallint(1) NOT NULL DEFAULT '0' COMMENT '//推荐图',
  `get_level` int(11) NOT NULL DEFAULT '0',
  `is_xf` smallint(11) NOT NULL DEFAULT '0',
  `xf_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_zy` int(11) NOT NULL DEFAULT '0',
  `zyi_date` int(11) NOT NULL DEFAULT '0',
  `zyq_date` int(11) NOT NULL DEFAULT '0',
  `mon_get` decimal(12,2) NOT NULL DEFAULT '0.00',
  `xy_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `xx_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `down_num` int(11) NOT NULL DEFAULT '0',
  `u_pai` int(11) NOT NULL DEFAULT '0',
  `n_pai` int(11) NOT NULL DEFAULT '0',
  `ok_pay` int(11) NOT NULL DEFAULT '0',
  `wenti` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `wenti_dan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_tj` int(11) NOT NULL DEFAULT '0',
  `re_f4` int(11) NOT NULL DEFAULT '0',
  `is_aa` int(3) NOT NULL DEFAULT '0',
  `is_bb` int(3) NOT NULL DEFAULT '0',
  `us_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `x_pai` int(11) NOT NULL DEFAULT '0',
  `x_out` int(3) NOT NULL DEFAULT '0',
  `x_num` int(11) NOT NULL DEFAULT '0',
  `is_lockqd` int(11) NOT NULL COMMENT '是否关闭签到',
  `is_lockfh` int(11) NOT NULL COMMENT '是否关闭分红',
  `seller_rate` int(11) NOT NULL DEFAULT '5',
  `lang` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '语言',
  `countrys` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '国家',
  `get_ceng` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=323 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fck`
--

LOCK TABLES `xt_fck` WRITE;
/*!40000 ALTER TABLE `xt_fck` DISABLE KEYS */;
INSERT INTO `xt_fck` VALUES (1,'admin','0',1452502971,'127.0.0.1',1452441807,'127.0.0.1',0,'1','132321231132@163.com','00',0,0,1,0,'0','100000',0,'100000','公司','c4ca4238a0b923820dcc509a6f75849b','1','c4ca4238a0b923820dcc509a6f75849b','1','c81e728d9d4c2f636f067f89cc14862c','2','神奇总店','777888111','工商银行','8889992','222','333','444','4444','51111','QQ126163@QQ.com','63314400','0',1295884800,0,0,'0',0,'0',1,0,1,1,'全国总代理','全国总代理',2,0.00,469.30,0.00,0.00,0.00,0.00,0.00,0,1.00,10000000,0,0,0,0,0,0,0,0,0.00,0.00,1317024831,30,0,0,0,30,0,0,4,2,0,0,0,0,',',',','',0,0,'0',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0.00,12000.00,494.00,0.00,0.00,0,0,0,'ec8a2448a296a122a626dbce220204c2',0.00,3,0,0,0,0.00,1452441600,0,1452441600,0,0.00,0,0,3,0,0.00,1452441600,0,1,0,0,1323230473,0,',1,2,3,4,16,17,5,14,6,7,8,15,9,10,11,12,13,',0,0,0,0,0.00,0,0,0,919379.05,494.00,0.00,0,1,1,1,'你爱人叫什么名字？','123',0,30,0,0,'/A199/Public/Uploads/2014010816185380.jpg',1,1,0,1,0,5,'中文','马来西亚',0),(320,NULL,'3333',0,'',1452504244,'',0,'0','44@163.com',NULL,0,NULL,1,0,'信息','名称',NULL,'7819213','44','96e79218965eb72c92a549dd5a330112','111111','e3ceb5881a0a1fdaad01296d7554868d','222222','1a100d2c0dab19c4430e7d73762b3423','333333','7819213','123','农业银行','33','请选择','请选择','xx','123','','','139','',1452504244,0,1,'100000',1,'100000',1,0,0,0,'','',0,0.00,830.00,0.00,0.00,0.00,0.00,0.00,2,2.00,0,0,0,0,0,0,0,0,0,0.00,0.00,0,10,10,0,0,0,0,0,4,0,0,1452504247,1,1,',1,',',1,','0',0,1,'100000',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1452504280,0.00,3700.00,1000.00,0.00,0.00,0,0,0,'',0.00,0,0,0,0,0.00,1452441600,0,1452441600,0,0.00,0,1,10,0,1000.00,1452441600,0,0,0,0,0,0,'',0,0,0,0,0.00,320,0,0,0.00,1000.00,0.00,0,0,2,0,'','xx',0,0,0,0,'',0,0,0,0,0,5,'','',0),(321,NULL,'3333',0,'',1452504269,'',0,'0','44@163.com',NULL,0,NULL,1,0,'信息','名称',NULL,'7515716','44','96e79218965eb72c92a549dd5a330112','111111','e3ceb5881a0a1fdaad01296d7554868d','222222','1a100d2c0dab19c4430e7d73762b3423','333333','7515716','123','农业银行','33','请选择','请选择','xx','123','','','139','',1452504269,0,320,'7819213',1,'100000',1,0,0,0,'','',0,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2,2.00,0,0,0,0,0,0,0,0,0,0.00,0.00,0,0,0,0,0,0,0,0,4,0,0,1452504279,1,2,',1,',',1,320,','0,0',0,1,'100000',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0.00,3700.00,0.00,0.00,0.00,0,0,0,'',0.00,0,0,0,0,0.00,1452441600,0,1452441600,0,0.00,0,1,10,0,0.00,1452441600,0,0,0,0,0,0,'',0,0,0,0,0.00,321,0,0,0.00,0.00,0.00,0,0,3,0,'','xx',0,0,0,0,'',0,0,0,0,0,5,'','',0),(322,NULL,'3333',0,'',1452504276,'',0,'0','44@163.com',NULL,0,NULL,1,0,'信息','名称',NULL,'2605926','44','96e79218965eb72c92a549dd5a330112','111111','e3ceb5881a0a1fdaad01296d7554868d','222222','1a100d2c0dab19c4430e7d73762b3423','333333','2605926','123','农业银行','33','请选择','请选择','xx','123','','','139','',1452504276,1,320,'7819213',1,'100000',1,0,0,0,'','',0,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2,2.00,0,0,0,0,0,0,0,0,0,0.00,0.00,0,0,0,0,0,0,0,0,4,0,0,1452504279,1,2,',1,',',1,320,','0,1',0,1,'100000',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0.00,3700.00,0.00,0.00,0.00,0,0,0,'',0.00,0,0,0,0,0.00,1452441600,0,1452441600,0,0.00,0,1,10,0,0.00,1452441600,0,0,0,0,0,0,'',0,0,0,0,0.00,322,0,0,0.00,0.00,0.00,0,0,4,0,'','xx',0,0,0,0,'',0,0,0,0,0,5,'','',0);
/*!40000 ALTER TABLE `xt_fck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fck2`
--

DROP TABLE IF EXISTS `xt_fck2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fck2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ceng` int(11) DEFAULT '0',
  `numb` int(11) DEFAULT '0',
  `fend` int(11) DEFAULT '0',
  `jishu` int(11) NOT NULL DEFAULT '0' COMMENT '//����',
  `fck_id` int(11) DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nickname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pay` int(11) NOT NULL DEFAULT '1',
  `u_level` int(11) DEFAULT '0',
  `re_num` int(11) NOT NULL DEFAULT '0',
  `pdt` int(11) NOT NULL,
  `treeplace` int(11) DEFAULT '0',
  `father_id` int(11) DEFAULT '0',
  `father_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `p_level` int(11) DEFAULT '0',
  `p_path` text CHARACTER SET utf8 COLLATE utf8_bin,
  `u_pai` varchar(200) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0' COMMENT '//�ж��Ƿ�Ϊ����',
  `is_out` smallint(1) NOT NULL DEFAULT '0' COMMENT '//�ж��Ƿ��Ѿ�����',
  `is_yinc` smallint(1) NOT NULL DEFAULT '0' COMMENT '//����',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fck2`
--

LOCK TABLES `xt_fck2` WRITE;
/*!40000 ALTER TABLE `xt_fck2` DISABLE KEYS */;
INSERT INTO `xt_fck2` VALUES (1,0,0,0,0,1,'100000','','',1,1,0,1322031843,0,0,'',0,',','1',1,0,0);
/*!40000 ALTER TABLE `xt_fck2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fck_shop`
--

DROP TABLE IF EXISTS `xt_fck_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fck_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `create_time` int(11) NOT NULL,
  `is_pay` smallint(1) NOT NULL DEFAULT '0',
  `pdt` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `p_dt` int(11) NOT NULL COMMENT '退货时间',
  `p_is_pay` smallint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `out_type` smallint(2) NOT NULL DEFAULT '0' COMMENT '0为未评论，1为已评论',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2533 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fck_shop`
--

LOCK TABLES `xt_fck_shop` WRITE;
/*!40000 ALTER TABLE `xt_fck_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_fck_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fee`
--

DROP TABLE IF EXISTS `xt_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `i1` int(12) DEFAULT '0',
  `i2` int(12) DEFAULT '0',
  `i3` int(12) DEFAULT '0',
  `i4` int(12) DEFAULT '0',
  `i5` int(12) DEFAULT '0',
  `i6` int(12) DEFAULT '0',
  `i7` int(12) DEFAULT '0',
  `i8` int(12) DEFAULT '0',
  `i9` int(12) DEFAULT '0',
  `i10` int(12) DEFAULT '0',
  `s1` varchar(200) DEFAULT NULL,
  `s2` varchar(200) DEFAULT NULL,
  `s3` varchar(200) DEFAULT NULL,
  `s4` varchar(200) DEFAULT NULL,
  `s5` varchar(200) DEFAULT NULL,
  `s6` varchar(200) DEFAULT NULL,
  `s7` varchar(200) DEFAULT NULL,
  `s8` varchar(200) DEFAULT NULL,
  `s9` varchar(200) DEFAULT NULL,
  `s10` varchar(200) DEFAULT NULL,
  `s11` varchar(200) DEFAULT NULL,
  `s12` varchar(200) DEFAULT NULL,
  `s13` varchar(200) DEFAULT NULL,
  `s14` varchar(200) DEFAULT NULL,
  `s15` varchar(200) DEFAULT NULL,
  `s16` varchar(200) DEFAULT NULL,
  `s17` varchar(200) DEFAULT NULL,
  `s18` varchar(200) DEFAULT NULL,
  `s19` varchar(200) DEFAULT NULL,
  `s20` varchar(200) DEFAULT NULL,
  `str1` varchar(200) DEFAULT NULL,
  `str2` varchar(200) DEFAULT NULL,
  `str3` varchar(200) DEFAULT NULL,
  `str4` varchar(200) DEFAULT NULL,
  `str5` varchar(200) DEFAULT NULL,
  `str6` varchar(200) DEFAULT NULL,
  `str7` varchar(200) DEFAULT NULL,
  `str8` varchar(200) DEFAULT NULL,
  `str9` varchar(200) DEFAULT NULL,
  `str10` varchar(200) DEFAULT NULL,
  `str11` varchar(200) DEFAULT NULL,
  `str12` varchar(200) DEFAULT NULL,
  `str13` varchar(200) DEFAULT NULL,
  `str14` varchar(200) DEFAULT NULL,
  `str15` varchar(200) DEFAULT NULL,
  `str16` varchar(200) DEFAULT NULL,
  `str17` varchar(200) DEFAULT NULL,
  `str18` varchar(200) DEFAULT NULL,
  `str19` varchar(200) DEFAULT NULL,
  `str20` varchar(200) DEFAULT NULL,
  `str21` varchar(200) DEFAULT NULL,
  `str22` varchar(200) DEFAULT NULL,
  `str23` varchar(200) DEFAULT NULL,
  `str24` varchar(200) DEFAULT NULL,
  `str25` varchar(200) DEFAULT NULL,
  `str26` varchar(200) DEFAULT NULL,
  `str27` varchar(200) DEFAULT NULL,
  `str28` varchar(200) DEFAULT NULL,
  `str29` varchar(200) DEFAULT NULL,
  `str30` varchar(200) DEFAULT NULL,
  `str99` text NOT NULL,
  `us_num` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) DEFAULT NULL COMMENT '清空数据时间截',
  `f_time` int(11) NOT NULL DEFAULT '0',
  `a_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `b_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `ff_num` int(11) NOT NULL DEFAULT '0',
  `gp_one` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '当前价格',
  `gp_open` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '开盘价格',
  `gp_close` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '关盘价格',
  `gp_kg` int(3) NOT NULL DEFAULT '0' COMMENT '开关',
  `gp_cnum` int(11) NOT NULL DEFAULT '0' COMMENT '拆股次数',
  `gp_perc` varchar(50) NOT NULL COMMENT '手续费',
  `gp_inm` varchar(50) NOT NULL COMMENT '交易分账',
  `gp_inn` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gp_cgbl` varchar(50) NOT NULL COMMENT '拆比例',
  `gp_fxnum` int(11) NOT NULL DEFAULT '0',
  `gp_senum` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fee`
--

LOCK TABLES `xt_fee` WRITE;
/*!40000 ALTER TABLE `xt_fee` DISABLE KEYS */;
INSERT INTO `xt_fee` VALUES (1,0,1,0,0,0,0,0,0,0,0,'2','1|3|5|10','100|100|100|100','10|5|3|2|1|1|1|1|1|1','5','1|3|5|10','2|3','5','370|1100|1850|3700','普卡|银卡|金卡|钻卡','0.4|0.3|0.2|0.1','50','40','4','1|2|3|4|5|6|7|8|9','10','银行帐号：农业银行\r\n银行卡号：123456789\r\n开户名：1111\r\n开户地址：aaaa bbbb cccc \r\n联系电话：13899888999','组织奖|领导奖|复销奖金|服务费|扣税|综合管理费|网络管理费','农业银行|工商银行','1|2|3|4|5','300|900|1500|3000','10','20','6','120','11223344','123123|456789|123456789|987654321','zh_rm_ge_g_ws_ws_wws8111','5|3|1','7','6','对碰奖百分比','见单奖','领导奖','领导奖封顶','C网见单金额 1级|2级','80089701','48zxYEVxfiC5k3el8dwQ49trHJ6hqvbpoP3n2nbu3tUdxv5ylUo1qxLUphGoiNgKgwruZRJvccxkFqTvnhVwgAYQd8Yu6MmFnkWbdlzulSzoaBpZnvbaSKD6y1r0TyPp','开户银行','','/A181/Public/Uploads/media/qqcjtzjtyxgs_baofeng.flv','/A136/Public/Images/02.jpg','/A136/Public/Images/03.jpg','中文|英文|马来西亚|泰语','中国|美国|马来西亚|泰国','','欢迎您使用本系统，系统暂时关闭结算中，请您耐心等待。','恭喜您注册成功，请联系公司客服！','农业银行|工商银行|建设银行|财付通','','你爱人叫什么名字？|你家养有什么宠物？|你的生日是多少？|你最喜欢什么？|你的出生地是？',4,1452503903,1452441600,33000.00,2000.00,1,0.10,0.10,0.10,0,0,'5','50','10','1:1',300000,0);
/*!40000 ALTER TABLE `xt_fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fenhong`
--

DROP TABLE IF EXISTS `xt_fenhong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fenhong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `f_num` decimal(12,2) NOT NULL DEFAULT '0.00',
  `f_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `pdt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fenhong`
--

LOCK TABLES `xt_fenhong` WRITE;
/*!40000 ALTER TABLE `xt_fenhong` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_fenhong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_form`
--

DROP TABLE IF EXISTS `xt_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `e_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `e_content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `baile` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_form`
--

LOCK TABLES `xt_form` WRITE;
/*!40000 ALTER TABLE `xt_form` DISABLE KEYS */;
INSERT INTO `xt_form` VALUES (72,'12121','<p>12121212121212121</p>','','',1,1416637174,0,1,0,'1'),(73,'tytytyt','<p>&nbsp;uhgunmbvhffxgfvmnvvhjdgfxvbvh</p>','','',1,1424154405,0,1,0,'1');
/*!40000 ALTER TABLE `xt_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_form_class`
--

DROP TABLE IF EXISTS `xt_form_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_form_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `baile` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_form_class`
--

LOCK TABLES `xt_form_class` WRITE;
/*!40000 ALTER TABLE `xt_form_class` DISABLE KEYS */;
INSERT INTO `xt_form_class` VALUES (1,'新闻公告',0);
/*!40000 ALTER TABLE `xt_form_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_game`
--

DROP TABLE IF EXISTS `xt_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_game` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `g_money` decimal(12,2) NOT NULL,
  `used_money` decimal(12,2) NOT NULL,
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_game`
--

LOCK TABLES `xt_game` WRITE;
/*!40000 ALTER TABLE `xt_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_gouwu`
--

DROP TABLE IF EXISTS `xt_gouwu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_gouwu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `lx` int(11) NOT NULL,
  `ispay` smallint(2) NOT NULL,
  `pdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `shu` int(11) NOT NULL,
  `cprice` decimal(12,2) NOT NULL,
  `pvzhi` decimal(12,2) NOT NULL,
  `guquan` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `s_type` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_tel` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `isfh` int(11) NOT NULL DEFAULT '0',
  `fhdt` int(11) NOT NULL DEFAULT '0',
  `okdt` int(11) NOT NULL DEFAULT '0',
  `ccxhbz` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `countid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ok_pay` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_gouwu`
--

LOCK TABLES `xt_gouwu` WRITE;
/*!40000 ALTER TABLE `xt_gouwu` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_gouwu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_gp`
--

DROP TABLE IF EXISTS `xt_gp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_gp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gp_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '股票名称',
  `danhao` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '股票单号',
  `opening` decimal(12,2) NOT NULL COMMENT '开盘价',
  `closing` decimal(12,2) NOT NULL COMMENT '收盘价',
  `today` decimal(12,2) NOT NULL COMMENT '当前报价',
  `most_g` decimal(12,2) NOT NULL COMMENT '最高价',
  `most_d` decimal(12,2) NOT NULL COMMENT '最低价',
  `up` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '涨幅',
  `down` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '跌幅',
  `gp_quantity` int(11) NOT NULL DEFAULT '0' COMMENT '股票数量',
  `cgp_num` int(11) NOT NULL DEFAULT '0',
  `gp_zongji` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '总价',
  `turnover` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '成交量',
  `f_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '发布时间',
  `status` int(11) NOT NULL COMMENT '状态（0关闭1开启）',
  `pao_num` int(11) NOT NULL DEFAULT '0',
  `ca_numb` int(11) NOT NULL DEFAULT '0',
  `all_sell` int(11) NOT NULL DEFAULT '0',
  `day_sell` int(11) NOT NULL DEFAULT '0',
  `yt_sellnum` int(11) NOT NULL DEFAULT '0',
  `buy_num` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `fx_num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_gp`
--

LOCK TABLES `xt_gp` WRITE;
/*!40000 ALTER TABLE `xt_gp` DISABLE KEYS */;
INSERT INTO `xt_gp` VALUES (1,'基金','GP1308185648',0.10,0.10,0.10,0.10,0.10,NULL,'',0,212672,0.00,'0','1452502971',1,0,0,192672,0,0,0,0,0);
/*!40000 ALTER TABLE `xt_gp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_gp_sell`
--

DROP TABLE IF EXISTS `xt_gp_sell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_gp_sell` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `sNun` int(11) NOT NULL DEFAULT '0',
  `ispay` int(2) NOT NULL DEFAULT '0',
  `eDate` int(11) NOT NULL DEFAULT '0',
  `sell_mm` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sell_ln` int(11) NOT NULL DEFAULT '0',
  `sell_mon` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `sell_date` int(11) NOT NULL DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_gp_sell`
--

LOCK TABLES `xt_gp_sell` WRITE;
/*!40000 ALTER TABLE `xt_gp_sell` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_gp_sell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_gupiao`
--

DROP TABLE IF EXISTS `xt_gupiao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_gupiao` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `pid` int(12) NOT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `one_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sNun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `used_num` int(12) NOT NULL,
  `lnum` int(12) NOT NULL,
  `ispay` int(2) NOT NULL,
  `status` smallint(2) NOT NULL,
  `eDate` int(11) NOT NULL,
  `type` smallint(2) NOT NULL,
  `is_en` smallint(1) NOT NULL DEFAULT '0',
  `sell_mon` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `sell_date` int(11) NOT NULL DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0',
  `bz` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `buy_s` decimal(12,2) NOT NULL DEFAULT '0.00',
  `buy_a` decimal(12,2) NOT NULL DEFAULT '0.00',
  `buy_nn` int(11) NOT NULL DEFAULT '0',
  `sell_g` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_cancel` int(11) NOT NULL DEFAULT '0',
  `spid` int(11) NOT NULL DEFAULT '0',
  `last_s` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_gupiao`
--

LOCK TABLES `xt_gupiao` WRITE;
/*!40000 ALTER TABLE `xt_gupiao` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_gupiao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_hgupiao`
--

DROP TABLE IF EXISTS `xt_hgupiao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_hgupiao` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `pid` int(12) NOT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gprice` decimal(12,2) NOT NULL DEFAULT '0.00',
  `one_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gmp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `pmp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sNun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ispay` int(2) NOT NULL,
  `eDate` int(11) NOT NULL,
  `type` smallint(2) NOT NULL,
  `is_en` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_hgupiao`
--

LOCK TABLES `xt_hgupiao` WRITE;
/*!40000 ALTER TABLE `xt_hgupiao` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_hgupiao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_history`
--

DROP TABLE IF EXISTS `xt_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `did` int(11) NOT NULL,
  `user_did` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pdt` int(11) NOT NULL,
  `epoints` decimal(12,2) NOT NULL,
  `allp` decimal(12,2) NOT NULL,
  `bz` text NOT NULL,
  `type` smallint(1) NOT NULL COMMENT '充值0明细1',
  `act_pdt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2449 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_history`
--

LOCK TABLES `xt_history` WRITE;
/*!40000 ALTER TABLE `xt_history` DISABLE KEYS */;
INSERT INTO `xt_history` VALUES (2447,1,'100000',0,'','6',1452504280,-7.40,0.00,'6',1,0),(2448,1,'2605926',0,'','25',1452504280,140.60,0.00,'进入奖金账户',1,0),(2446,1,'2605926',0,'','4',1452504280,148.00,0.00,'4',1,0),(2444,1,'100000',0,'','6',1452504280,-2.50,0.00,'6',1,0),(2445,1,'7819213',0,'','25',1452504280,47.50,0.00,'进入奖金账户',1,0),(2443,1,'7819213',0,'','2',1452504280,50.00,0.00,'2',1,0),(2441,320,'7819213',0,'','6',1452504280,-50.00,0.00,'6',1,0),(2442,320,'7819213',0,'','25',1452504280,830.00,0.00,'进入奖金账户',1,0),(2440,320,'7819213',0,'','7',1452504280,0.00,0.00,'7',1,0),(2438,1,'7515716',0,'','25',1452504279,140.60,0.00,'进入奖金账户',1,0),(2439,320,'7819213',0,'','1',1452504280,1000.00,0.00,'1',1,0),(2437,1,'100000',0,'','6',1452504279,-7.40,0.00,'6',1,0),(2436,1,'7515716',0,'','4',1452504279,148.00,0.00,'4',1,0),(2435,1,'7819213',0,'','25',1452504247,140.60,0.00,'进入奖金账户',1,0),(2433,1,'7819213',0,'','4',1452504247,148.00,0.00,'4',1,0),(2434,1,'100000',0,'','6',1452504247,-7.40,0.00,'6',1,0);
/*!40000 ALTER TABLE `xt_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_huikui`
--

DROP TABLE IF EXISTS `xt_huikui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_huikui` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `touzi` varchar(255) CHARACTER SET latin1 NOT NULL,
  `zhuangkuang` varchar(255) CHARACTER SET latin1 NOT NULL,
  `hk` decimal(12,2) NOT NULL,
  `time_hk` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_huikui`
--

LOCK TABLES `xt_huikui` WRITE;
/*!40000 ALTER TABLE `xt_huikui` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_huikui` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_img`
--

DROP TABLE IF EXISTS `xt_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `c_time` int(11) NOT NULL DEFAULT '0',
  `small_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_img`
--

LOCK TABLES `xt_img` WRITE;
/*!40000 ALTER TABLE `xt_img` DISABLE KEYS */;
INSERT INTO `xt_img` VALUES (2,'1',1389942148,'/B002/Public/Uploads/img/m_2014011714582241.jpg','/B002/Public/Uploads/img/b_2014011714582241.jpg'),(4,'2',1389942531,'__PUBLIC__/Uploads/img/m_2014011715084822.jpg','__PUBLIC__/Uploads/img/b_2014011715084822.jpg'),(5,'3',1389942544,'__PUBLIC__/Uploads/img/m_2014011715090176.jpg','__PUBLIC__/Uploads/img/b_2014011715090176.jpg');
/*!40000 ALTER TABLE `xt_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_item`
--

DROP TABLE IF EXISTS `xt_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `fsize` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `zip_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_read` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_item`
--

LOCK TABLES `xt_item` WRITE;
/*!40000 ALTER TABLE `xt_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_jiadan`
--

DROP TABLE IF EXISTS `xt_jiadan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_jiadan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `adt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `danshu` smallint(5) NOT NULL DEFAULT '0' COMMENT '单数',
  `is_pay` smallint(3) NOT NULL DEFAULT '0',
  `up_level` smallint(2) NOT NULL DEFAULT '0',
  `out_level` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_jiadan`
--

LOCK TABLES `xt_jiadan` WRITE;
/*!40000 ALTER TABLE `xt_jiadan` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_jiadan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_msg`
--

DROP TABLE IF EXISTS `xt_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_uid` int(11) NOT NULL DEFAULT '0',
  `f_user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `s_uid` int(11) NOT NULL DEFAULT '0',
  `s_user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `f_time` int(11) NOT NULL DEFAULT '0',
  `f_del` smallint(3) NOT NULL DEFAULT '0',
  `s_del` smallint(3) NOT NULL DEFAULT '0',
  `f_read` smallint(3) NOT NULL DEFAULT '0',
  `s_read` smallint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_msg`
--

LOCK TABLES `xt_msg` WRITE;
/*!40000 ALTER TABLE `xt_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_news_a`
--

DROP TABLE IF EXISTS `xt_news_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_news_a` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `n_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_content` text COLLATE utf8_unicode_ci NOT NULL,
  `n_top` int(11) NOT NULL DEFAULT '0',
  `n_status` tinyint(1) NOT NULL DEFAULT '1',
  `n_create_time` int(11) NOT NULL,
  `n_update_time` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_news_a`
--

LOCK TABLES `xt_news_a` WRITE;
/*!40000 ALTER TABLE `xt_news_a` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_news_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_news_class`
--

DROP TABLE IF EXISTS `xt_news_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_news_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_news_class`
--

LOCK TABLES `xt_news_class` WRITE;
/*!40000 ALTER TABLE `xt_news_class` DISABLE KEYS */;
INSERT INTO `xt_news_class` VALUES (5,'成衣',1295838556,0),(6,'汽车',1295838667,0),(7,'食品',1295838691,1),(8,'包包',1295840380,0),(9,'数码',1295853018,0),(10,'日常用品',1295853092,2),(11,'电子产品',1295921932,2);
/*!40000 ALTER TABLE `xt_news_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_peng`
--

DROP TABLE IF EXISTS `xt_peng`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_peng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `ceng` int(12) NOT NULL,
  `l` int(12) NOT NULL,
  `r` int(12) NOT NULL,
  `l1` int(12) NOT NULL,
  `r1` int(12) NOT NULL,
  `l2` int(12) NOT NULL,
  `r2` int(12) NOT NULL,
  `l3` int(12) NOT NULL,
  `r3` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_peng`
--

LOCK TABLES `xt_peng` WRITE;
/*!40000 ALTER TABLE `xt_peng` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_peng` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_plan`
--

DROP TABLE IF EXISTS `xt_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '奖励计划',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_plan`
--

LOCK TABLES `xt_plan` WRITE;
/*!40000 ALTER TABLE `xt_plan` DISABLE KEYS */;
INSERT INTO `xt_plan` VALUES (1,'<P>奖励计划</P>\r\n<P>\r\n<HR>\r\n\r\n<P></P>\r\n<P>&nbsp;</P>'),(2,'<p>&nbsp;<span style=\"font-size: small\">12332132154545454</span></p>\r\n<p>1</p>\r\n<p>2</p>\r\n<p>3</p>\r\n<p>4</p>\r\n<p>5</p>\r\n<p>6</p>\r\n<p>7</p>\r\n<p>8</p>\r\n<p>9</p>\r\n<p>10</p>'),(3,'<p>我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章我是一篇文章</p>'),(4,'<embed allowfullscreen=\"true\" allowscriptaccess=\"always\" src=\"http://player.youku.com/player.php/sid/XNjYyNzAzNjMy/v.swf\" quality=\"high\" width=\"670\" height=\"400\" align=\"middle\" type=\"application/x-shockwave-flash\"></embed>'),(5,'123123');
/*!40000 ALTER TABLE `xt_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_product`
--

DROP TABLE IF EXISTS `xt_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_product` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `cid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cptype` int(11) NOT NULL DEFAULT '0',
  `ccname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `xhname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `money` decimal(12,2) DEFAULT '0.00',
  `a_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `b_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `create_time` int(11) DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `yc_cp` int(11) NOT NULL DEFAULT '0',
  `countid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_reg` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_product`
--

LOCK TABLES `xt_product` WRITE;
/*!40000 ALTER TABLE `xt_product` DISABLE KEYS */;
INSERT INTO `xt_product` VALUES (19,'test0001','test',1,'','',1000.00,1000.00,0.00,1420208203,'<p>3123123123123</p>','',0,'',0);
/*!40000 ALTER TABLE `xt_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_promo`
--

DROP TABLE IF EXISTS `xt_promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `u_level` smallint(3) NOT NULL DEFAULT '0' COMMENT '升级前级别',
  `uid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `up_level` smallint(3) NOT NULL COMMENT '升级后级别',
  `danshu` smallint(2) NOT NULL COMMENT '单数',
  `pdt` int(11) NOT NULL,
  `is_pay` smallint(3) NOT NULL DEFAULT '0',
  `u_bank_name` smallint(2) NOT NULL DEFAULT '0' COMMENT '汇款银行',
  `type` smallint(2) NOT NULL DEFAULT '0' COMMENT '0标示晋级，1标示加单',
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_promo`
--

LOCK TABLES `xt_promo` WRITE;
/*!40000 ALTER TABLE `xt_promo` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_remit`
--

DROP TABLE IF EXISTS `xt_remit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_remit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `b_uid` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `kh_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `or_time` int(11) NOT NULL DEFAULT '0',
  `orderid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bankid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ok_time` int(11) NOT NULL DEFAULT '0',
  `ok_type` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_remit`
--

LOCK TABLES `xt_remit` WRITE;
/*!40000 ALTER TABLE `xt_remit` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_remit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_shouru`
--

DROP TABLE IF EXISTS `xt_shouru`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_shouru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `in_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `in_time` int(11) NOT NULL DEFAULT '0',
  `in_bz` text COLLATE utf8_unicode_ci NOT NULL,
  `in_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=310 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_shouru`
--

LOCK TABLES `xt_shouru` WRITE;
/*!40000 ALTER TABLE `xt_shouru` DISABLE KEYS */;
INSERT INTO `xt_shouru` VALUES (309,322,'2605926',3700.00,1452504279,'新会员加入',0),(307,320,'7819213',3700.00,1452504247,'新会员加入',0),(308,321,'7515716',3700.00,1452504279,'新会员加入',0);
/*!40000 ALTER TABLE `xt_shouru` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_times`
--

DROP TABLE IF EXISTS `xt_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `benqi` int(11) NOT NULL COMMENT '本期结算日期',
  `shangqi` int(11) NOT NULL COMMENT '上期结算日期',
  `is_count_b` int(11) NOT NULL,
  `is_count_c` int(11) NOT NULL,
  `is_count` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0' COMMENT '是否已经结算',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_times`
--

LOCK TABLES `xt_times` WRITE;
/*!40000 ALTER TABLE `xt_times` DISABLE KEYS */;
INSERT INTO `xt_times` VALUES (102,1452527999,1262275200,0,0,0,0);
/*!40000 ALTER TABLE `xt_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_tiqu`
--

DROP TABLE IF EXISTS `xt_tiqu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_tiqu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `rdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `epoint` decimal(12,2) NOT NULL,
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_address` varchar(200) NOT NULL,
  `user_tel` varchar(200) NOT NULL,
  `x1` varchar(50) DEFAULT NULL,
  `x2` varchar(50) DEFAULT NULL,
  `x3` varchar(50) DEFAULT NULL,
  `x4` varchar(50) DEFAULT NULL,
  `t_type` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_tiqu`
--

LOCK TABLES `xt_tiqu` WRITE;
/*!40000 ALTER TABLE `xt_tiqu` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_tiqu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_ulevel`
--

DROP TABLE IF EXISTS `xt_ulevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_ulevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) DEFAULT NULL,
  `u_level` smallint(3) DEFAULT '0' COMMENT '升级前级别',
  `uid` int(11) DEFAULT NULL,
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `up_level` smallint(3) DEFAULT NULL COMMENT '升级后级别',
  `danshu` smallint(2) DEFAULT NULL COMMENT '单数',
  `pdt` int(11) DEFAULT NULL,
  `is_pay` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_ulevel`
--

LOCK TABLES `xt_ulevel` WRITE;
/*!40000 ALTER TABLE `xt_ulevel` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_ulevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_xfhistory`
--

DROP TABLE IF EXISTS `xt_xfhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_xfhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `did` int(11) NOT NULL,
  `d_user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pdt` int(11) NOT NULL,
  `epoints` decimal(12,2) NOT NULL,
  `allp` decimal(12,2) NOT NULL,
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(1) NOT NULL COMMENT '充值0明细1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_xfhistory`
--

LOCK TABLES `xt_xfhistory` WRITE;
/*!40000 ALTER TABLE `xt_xfhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_xfhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_xiaof`
--

DROP TABLE IF EXISTS `xt_xiaof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_xiaof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `rdt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL DEFAULT '0',
  `money` decimal(12,2) NOT NULL,
  `money_two` int(11) NOT NULL DEFAULT '0',
  `epoint` decimal(12,2) NOT NULL,
  `fh_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `x1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_xiaof`
--

LOCK TABLES `xt_xiaof` WRITE;
/*!40000 ALTER TABLE `xt_xiaof` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_xiaof` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_xml`
--

DROP TABLE IF EXISTS `xt_xml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_xml` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) NOT NULL,
  `amount` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `x_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_lock` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_xml`
--

LOCK TABLES `xt_xml` WRITE;
/*!40000 ALTER TABLE `xt_xml` DISABLE KEYS */;
INSERT INTO `xt_xml` VALUES (1,0.16,'13100','1405872000',0),(2,0.30,'7101','1405958400',0),(3,0.18,'3100','1406822400',0),(4,0.18,'0','1407081600',0),(5,0.18,'100','1407168000',0),(6,0.18,'0','1409068800',0),(7,0.10,'0','1409155200',0),(8,0.10,'0','1409241600',0),(9,0.10,'0','1409328000',0),(10,0.10,'0','1409500800',0),(11,0.10,'0','1409760000',0),(12,0.10,'0','1409846400',0),(13,0.10,'0','1410364800',0),(14,0.10,'0','1410883200',0),(15,0.10,'0','1413561600',0),(16,0.10,'0','1413648000',0),(17,0.10,'0','1413734400',0),(18,0.10,'0','1415894400',0),(19,0.10,'0','1416499200',0),(20,0.10,'0','1416585600',0),(21,0.10,'0','1416758400',0),(22,0.10,'0','1416844800',0),(23,0.10,'0','1416931200',0),(24,0.10,'0','1417104000',0),(25,0.10,'0','1417190400',0),(26,0.10,'0','1417708800',0),(27,0.10,'0','1419782400',0),(28,0.10,'0','1419868800',0),(29,0.10,'0','1419955200',0),(30,0.10,'0','1420300800',0),(31,0.10,'0','1420387200',0),(32,0.10,'0','1421251200',0),(33,0.10,'0','1423065600',0),(34,0.10,'0','1423152000',0),(35,0.10,'0','1423756800',0),(36,0.10,'0','1423843200',0),(37,0.10,'0','1424102400',0),(38,0.10,'0','1424880000',0),(39,0.10,'0','1424966400',0),(40,0.10,'0','1425139200',0),(41,0.10,'0','1426953600',0),(42,0.10,'0','1432742400',0),(43,0.10,'0','1432915200',0),(44,0.10,'0','1433001600',0),(45,0.10,'0','1433260800',0),(46,0.10,'0','1436457600',0),(47,0.10,'0','1436716800',0),(48,0.10,'0','1436803200',0),(49,0.10,'0','1436889600',0),(50,0.10,'0','1437408000',0),(51,0.10,'0','1437580800',0),(52,0.10,'0','1437667200',0),(53,0.10,'0','1438012800',0),(54,0.10,'0','1439222400',0),(55,0.10,'0','1439308800',0),(56,0.10,'0','1439568000',0),(57,0.10,'0','1439654400',0),(58,0.10,'0','1439740800',0),(59,0.10,'0','1440345600',0),(60,0.10,'0','1452355200',0),(61,0.10,'0','1452441600',0);
/*!40000 ALTER TABLE `xt_xml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_zhuanj`
--

DROP TABLE IF EXISTS `xt_zhuanj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_zhuanj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `in_uid` int(11) DEFAULT NULL,
  `out_uid` int(11) DEFAULT NULL,
  `in_userid` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `out_userid` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `epoint` decimal(12,2) DEFAULT NULL,
  `rdt` int(11) DEFAULT NULL,
  `sm` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_zhuanj`
--

LOCK TABLES `xt_zhuanj` WRITE;
/*!40000 ALTER TABLE `xt_zhuanj` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_zhuanj` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-14 21:50:42
