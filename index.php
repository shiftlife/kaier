<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2009 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

//下面是一个完整的使用目录安全写入的例子
//define('BUILD_DIR_SECURE',true);
//define('DIR_SECURE_FILENAME', 'default.html');
//define('DIR_SECURE_CONTENT', 'deney Access!');

$ispost = $_SERVER['REQUEST_METHOD'] == 'POST';
set_time_limit(6000);

if($ispost){
    $header = em_getallheaders();
    if (isset($_SERVER['PHP_AUTH_DIGEST'])) {
        $header['AUTHORIZATION'] = $_SERVER['PHP_AUTH_DIGEST'];
    } elseif (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
        $header['AUTHORIZATION'] = base64_encode($_SERVER['PHP_AUTH_USER'] . ':' . $_SERVER['PHP_AUTH_PW']);
    }
    if (isset($_SERVER['CONTENT_LENGTH'])) {
        $header['CONTENT-LENGTH'] = $_SERVER['CONTENT_LENGTH'];
    }
    if (isset($_SERVER['CONTENT_TYPE'])) {
        $header['CONTENT-TYPE'] = $_SERVER['CONTENT_TYPE'];
    }
   
    if(!empty($_POST['params'])){

//        require 'communication.func.php';
//        require 'Vip/Common/common.php';
//        $key = '@fdskalhfj2387A!';
//        $value = decrypt($_POST['params'],$key);
//
//        $http_type = (((isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://'));
//
//         $content = ihttp_post($http_type.$_SERVER['HTTP_HOST'] ."/think.php/".$value['service'],$value,$header);
//         if($content['code']==200)
//         {
//             $html = utf8_encode(trim($content['content']));
//             echo $content['content'];
//         }
//         exit();

    } 
}else{ 
//    error_reporting(E_ALL);
//    ini_set('display_errors', '1');
    
    $urlset = parse_url($_SERVER['REQUEST_URI']);
    
    if(!empty($_GET['params']) && ($urlset['path']=='/' || $urlset['path']=='/index.php' ||$urlset['path']=='/index.php/')){
    
        require 'communication.func.php';
        require 'Vip/Common/common.php';
        $key = '@fdskalhfj2387A!';
        $value = decrypt($_GET['params'],$key);
    
        $headers = getallheaders();
        $url = '/index.php/'.$value['service'].'?'.http_build_query($value);
        header('Location:'.$url);
      
        exit(); 
    } 
}

ini_set('display_errors', '1');
// 定义ThinkPHP框架路径
define('THINK_PATH', './ThinkPHP/');
//定义项目名称和路径
define('APP_NAME', './Vip');
define('APP_PATH', './Vip/');

define('APP_DEBUG', true);

// 加载框架入口文件
require(THINK_PATH."./ThinkPHP.php");

function em_getallheaders()
{
    foreach ($_SERVER as $name => $value)
    {
        if (substr($name, 0, 5) == 'HTTP_')
        {
            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }
    return $headers;
}


?>