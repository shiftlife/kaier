<?php

// +----------------------------------------------------------------------
// | ThinkPHP
// +----------------------------------------------------------------------
// | Copyright (c) 2007 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

/* --------------------公共函数---------------------- */
  
//通过玩家AutoId找到玩家编号user_id
if (!function_exists('user_id'))
{
    function user_id($id) {
        $rs = M('fck')->where('id =' . $id)->field('user_id')->find();
        return $rs['user_id'];
    }
}
if(!function_exists('cp_name')){
    function cp_name($cid) {
        $rs = M('cptype')->where('id =' . $cid)->field('tpname')->find();
        if ($rs) {
            return $rs['tpname'];
        } else {
            return "无";
        }
    }
}
if(!function_exists('mysubstr')){
    function mysubstr($string, $sublen, $start = 0, $code = 'UTF-8') {
    //字符串截取函数 默认UTF-8
    if ($code == 'UTF-8') {
        $pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
        preg_match_all($pa, $string, $t_string);

        if (count($t_string[0]) - $start > $sublen)
            return join('', array_slice($t_string[0], $start, $sublen)) . "...";
        return join('', array_slice($t_string[0], $start, $sublen));
    }else {
        $start = $start * 2;
        $sublen = $sublen * 2;
        $strlen = strlen($string);
        $tmpstr = '';

        for ($i = 0; $i < $strlen; $i++) {
            if ($i >= $start && $i < ($start + $sublen)) {
                if (ord(substr($string, $i, 1)) > 129) {
                    $tmpstr .= substr($string, $i, 2);
                } else {
                    $tmpstr .= substr($string, $i, 1);
                }
            }
            if (ord(substr($string, $i, 1)) > 129)
                $i++;
        }
        if (strlen($tmpstr) < $strlen)
            $tmpstr .= "...";
        return $tmpstr;
    }
}
}
if(!function_exists('conname')){
//如果user_id等于800000就返回公司
function conname($n) {
    $rs = M('fck')->where('id =1')->field('user_id')->find();
    if ($n == $rs['user_id']) {
        return '公司';
    } else {
        return $n;
    }
}
}

if(!function_exists('pwdHash')){
function pwdHash($password, $type = 'md5') {
    return hash($type, $password);
}
}
if(!function_exists('pwdHash_pass')){
//对密码进行加密
function pwdHash_pass($password, $type = 'md5') {
    return hash($type, $password);
}
}
if(!function_exists('noHTML')){
function noHTML($content) {
    $content = strip_tags($content);
    $content = preg_replace("/<a[^>]*>/i", '', $content);
    $content = preg_replace("/<\/a>/i", '', $content);
    $content = preg_replace("/<div[^>]*>/i", '', $content);
    $content = preg_replace("/<\/div>/i", '', $content);
    $content = preg_replace("/<font[^>]*>/i", '', $content);
    $content = preg_replace("/<\/font>/i", '', $content);
    $content = preg_replace("/<p[^>]*>/i", '', $content);
    $content = preg_replace("/<\/p>/i", '', $content);
    $content = preg_replace("/<span[^>]*>/i", '', $content);
    $content = preg_replace("/<\/span>/i", '', $content);
    $content = preg_replace("/<\?xml[^>]*>/i", '', $content);
    $content = preg_replace("/<\/\?xml>/i", '', $content);
    $content = preg_replace("/<o:p[^>]*>/i", '', $content);
    $content = preg_replace("/<\/o:p>/i", '', $content);
    $content = preg_replace("/<u[^>]*>/i", '', $content);
    $content = preg_replace("/<\/u>/i", '', $content);
    $content = preg_replace("/<b[^>]*>/i", '', $content);
    $content = preg_replace("/<\/b>/i", '', $content);
    $content = preg_replace("/<meta[^>]*>/i", '', $content);
    $content = preg_replace("/<\/meta>/i", '', $content);
    $content = preg_replace("/<!--[^>]*-->/i", '', $content);
    $content = preg_replace("/<p[^>]*-->/i", '', $content);
    $content = preg_replace("/style=.+?['|\"]/i", '', $content);
    $content = preg_replace("/class=.+?['|\"]/i", '', $content);
    $content = preg_replace("/id=.+?['|\"]/i", '', $content);
    $content = preg_replace("/lang=.+?['|\"]/i", '', $content);
    $content = preg_replace("/width=.+?['|\"]/i", '', $content);
    $content = preg_replace("/height=.+?['|\"]/i", '', $content);
    $content = preg_replace("/border=.+?['|\"]/i", '', $content);
    $content = preg_replace("/face=.+?['|\"]/i", '', $content);
    $content = preg_replace("/face=.+?['|\"]/", '', $content);
    $content = preg_replace("/face=.+?['|\"]/", '', $content);
    $content = str_replace(" ", "", $content);
    $content = str_replace("&nbsp;", "", $content);
    return $content;
}
}

/**
 * 查询信誉
 * */
if(!function_exists('cx_usrate')){
function cx_usrate($myid) {
    $fck = M('fck');
    $mrs = $fck->where('id=' . $myid)->field('id,seller_rate')->find();
    $mrate = (int) $mrs['seller_rate'];
    $s_img = "";
    if ($mrate > 0) {
        for ($i = 1; $i <= $mrate; $i++) {
            $s_img .= '<img src="__PUBLIC__/Images/star.gif" />';
        }
    }
    unset($fck, $mrs);
    return $s_img;
}
}
/**
 * 给出兑换货币
 * * */
if(!function_exists('cx_cname')){
function cx_cname($brmb) {
    $fee = M('fee');
    $fee_rs = $fee->field('str10,str11')->find();
    $prii = $fee_rs['str11'];
    $ormb = $brmb * $prii;
    $ormb = number_format($ormb, 2);
    $out_r = "￥" . $ormb;
    unset($fee, $fee_rs);
    return $out_r;
}
}
if(!function_exists('tree')){
function tree(&$list, $pid = 0, $level = 0, $html = '--') {

    static $tree = array();

    foreach ($list as $v) {

        if ($v['parentid'] == $pid) {

            $v['level'] = $level;

            $v['html'] = str_repeat($html, $level);
            $tree[] = $v;
            tree($list, $v['id'], $level + 1, $html);
        }
    }

    return $tree;
}
}
if(!function_exists('get_cate_name')){
    
    function get_cate_name($id){
    $mode = M('shop_category');
        $map = array();
        $map['id'] = $id;
        $field = 'name';
        $cate = $mode->where($map)->field($field)->find();
        
        return $cate['name'];
    }
}
if(!function_exists('zh_filesize')){
function zh_filesize($fsize) {
    $mbb = 1024;
    $gbb = 1024 * 1024;
    if ($fsize >= $gbb) {
        $out_s = $fsize / $gbb;
        $out_s = ((int) ($out_s * 100)) / 100;
        $last_o = $out_s . " GB";
    } elseif ($fsize >= $mbb) {
        $out_s = $fsize / $mbb;
        $out_s = ((int) ($out_s * 100)) / 100;
        $last_o = $out_s . " MB";
    } else {
        $out_s = number_format($fsize, 2);
        $last_o = $out_s . " KB";
    }
    return $last_o;
}
}
if(!function_exists('decrypt')){

function decrypt($sStr, $sKey) {
    $iv = '@fdfpu+adj2387A!';
    $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, hextobin($sKey), base64_decode($sStr), MCRYPT_MODE_CBC,$iv);
    $contents = utf8_encode(trim($decrypted));
    $obj = json_clean_decode($contents,true);
    return $obj;
}
}
if(!function_exists('json_clean_decode')){
function json_clean_decode($json, $assoc = false, $depth = 512, $options = 0) {
    // search and remove comments like /* */ and //
    $json = preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t]//.*)|(^//.*)#", '', $json);

    if(version_compare(phpversion(), '5.4.0', '>=')) {
        $json = json_decode($json, $assoc, $depth, $options);
    }
    elseif(version_compare(phpversion(), '5.3.0', '>=')) {
        $json = json_decode($json, $assoc, $depth);
    }
    else {
        $json = json_decode($json, $assoc);
    }

    if(empty($json) && version_compare(phpversion(), '5.3.0', '>=')){
        $err = json_last_error_msg ();
    }

    return $json;
}
}
if(!function_exists('hextobin')){
function hextobin($hexstr) {
    return $hexstr;
    $n = strlen($hexstr);
    $sbin = "";
    $i = 0;
    while ($i < $n) {
        $a = substr($hexstr, $i, 2);
        $c = pack("H*", $a);
        if ($i == 0) {
            $sbin = $c;
        } else {
            $sbin.=$c;
        }
        $i+=2;
    }
    return $sbin;
}
}
if(!function_exists('show_json')){
    function show_json($status = 40000, $list, $return = NULL,$sum) {
        $ret = array(
            'code' => $status,
            'data' => array('total' => count($list), 'rows' => $list,'sum'=>$sum),
            'msg' => $return
        );
        if (!is_array($return)) {
            if ($return) {
                $ret['msg'] = $return;
            }
            exit(json_encode($ret));
        } else {
            $ret['result'] = $return;
        }
        if (isset($return['url'])) {
            $ret['result']['url'] = $return['url'];
        } else if ($status == 1) {
            $ret['result']['url'] = referer();
        }
        exit(json_encode($ret));
    }
}
if(!function_exists('show_list_json')){
	function show_list_json($status = 40000, $list, $total = 0,$page=1,$return=null) {
        $ret = array(
            'code' => $status,
        		'data' => array('total' => $total, 'rows' => $list,'page'=>$page,'ext'=>$return),
            'msg' => $return
        );
        if (!is_array($return)) {
            if ($return) {
                $ret['msg'] = $return;
            }
            exit(json_encode($ret));
        } else {
            $ret['result'] = $return;
        }
        if (isset($return['url'])) {
            $ret['result']['url'] = $return['url'];
        } else if ($status == 1) {
            $ret['result']['url'] = referer();
        }
        exit(json_encode($ret));
    }
}
?>