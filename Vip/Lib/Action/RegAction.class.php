<?php

class RegAction extends CommonAction {

    function _initialize()
    {
        $this->_inject_check(0); //调用过滤函数
        $this->_Config_name();
        header("Content-Type:text/html; charset=utf-8");
    }
    /**
     * 会员注册
     */
    public function users($Urlsz = 0) {
        if($_SESSION['administrator']==1 || $_SESSION['login_isAgent']>0||$_SESSION['is_shoper']==1) {
        }else{
            $this->error('不具有报单权限！');
            exit;
        }

        $this->_checkUser();
        $fck = M('fck');
        $fee = M('fee');
        $RID = (int) $_GET['RID'];
        $FID = (int) $_GET['FID'];
        $TP = (int) $_GET['TPL'];
        if (empty($TPL))
            $TPL = 0;
        $TPL = array();
        for ($i = 0; $i < 5; $i++) {
            $TPL[$i] = '';
        }
        $TPL[$TP] = 'selected="selected"';

        //===专卖店
        $zzc = array();
        $where = array();
        $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
        $field = 'user_id,is_agent,agent_cash,shop_name';
        $rs = $fck->where($where)->field($field)->find();
        $money = $rs['agent_cash'];
        $mmuserid = $rs['user_id'];
        if ($rs['is_agent'] >= 1) {
            $zzc[1] = $rs['user_id'];
        } else {
            $mrs = M('fck')->where('id=1')->field('id,user_id')->find();
            $zzc[1] = $mrs['user_id'];
        }
        $this->assign('myid', $_SESSION[C('USER_AUTH_KEY')]);
  
        //===销售人
        $where['id'] = $RID;
        $field = 'user_id,is_agent';
        $rs = $fck->where($where)->field($field)->find();
        if ($rs) {
            $zzc[2] = $rs['user_id'];
        } else {
            $zzc[2] = $mmuserid;
        }
        //$zzc[2] = $mmuserid;
        //===接点人
        $where['id'] = $FID;
        $field = 'user_id,is_agent';
        $rs = $fck->where($where)->field($field)->find();
        if ($rs) {
            $zzc[3] = $rs['user_id'];
        } else {
            $zzc[3] = $mmuserid;
        }
        $arr = array();
        $arr['UserID'] = $this->_getUserID();
        $this->assign('flist', $arr);
        $pwhere = array();
        $product = M('product');
        $pwhere['is_reg'] = array("eq", 1);
        $prs = $product->where($pwhere)->select();
        $this->assign('plist', $prs);
        $fee_s = $fee->field('*')->find();
        $s9 = $fee_s['s9'];
        $s9 = explode('|', $s9);

        $i4 = $fee_s['i4'];
        if ($i4 == 0) {
            $openm = 1;
        } else {
            $openm = 0;
        }
        //输出银行
        $bank = explode('|', $fee_s['str29']);
        //输出级别名称
        $Level = explode('|', C('Member_Level'));
        //输出注册单数
        $Single = explode('|', C('Member_Single'));
        //输出一单的金额
        $lang = explode('|', $fee_s['str24']);
        $countrys = explode('|', $fee_s['str25']);
        $wentilist = explode('|', $fee_s['str99']);
        $this->assign('s9', $s9);
        $this->assign('openm', $openm);
        $this->assign('bank', $bank);
        $this->assign('Level', $Level);
        $this->assign('Single', $Single);
        $this->assign('Money', $fee_s['s2']);
        $this->assign('Money1', $money);
        $this->assign('wentilist', $wentilist);
        $this->assign('lang', $lang);
        $this->assign('countrys', $countrys);
        unset($bank, $Level, $$Level);
        $this->assign('TPL', $TPL);
        $this->assign('zzc', $zzc);
        unset($fck, $TPL, $where, $field, $rs, $data_temp, $temp_rs, $rs);
        $this->display('users');
    }
    /**
     * 注册确认
     */
    public function usersConfirm() {
        $this->_checkUser();
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $fck = M('fck');
        $rs = $fck->field('is_pay,agent_cash')->find($id);
        if ($rs['is_pay'] == 0) {
            $this->error('临时会员不能注册会员！');
            exit;
        }
        if (strlen($_POST['UserID']) < 1) {
            $this->error('会员编号不能少！');
            exit;
        }
        $this->assign('UserID', $_POST['UserID']);

        // $data = array();  //创建数据对象
        // $shopid = trim($_POST['shopid']);  //所属专卖店帐号
        // if (empty($shopid)) {
        //     $this->error('请输入专卖店编号！');
        //     exit;
        // }
        // $smap = array();
        // $smap['user_id'] = $shopid;
        // $smap['is_agent'] = array('gt', 1);
        // $shop_rs = $fck->where($smap)->field('id,user_id')->find();
        // if (!$shop_rs) {
        //     $this->error('没有该专卖店！');
        //     exit;
        // }
        // $this->assign('shopid', $shopid);
        // unset($smap, $shop_rs, $shopid);

        //// @liuzhiheng  修改
        $data = array();  //创建数据对象
        $shopid = trim($_POST['re_id']);  //所属专卖店帐号
        if (empty($shopid)) {
            $this->error('请输入推荐人编号！');
            exit;
        }
        $smap = array();
        $smap['user_id'] = $shopid;
//        $smap['is_agent'] = array('gt', 1);
        $shop_rs = $fck->where($smap)->field('id,user_id')->find();
        if (!$shop_rs) {
            $this->error('没有该推荐人！');
            exit;
        }
        $this->assign('shopid', $shopid);
        unset($smap, $shop_rs, $shopid);

        //检测销售人    @liuzhiheng
        // $RID = trim($_POST['RID']);  //获取销售会员帐号
        // $mapp = array();
        // $mapp['user_id'] = $RID;
        // $mapp['is_pay'] = array('gt', 0);
        // $authInfoo = $fck->where($mapp)->field('id,user_id,re_level,re_path')->find();
        // if ($authInfoo) {
        //     $this->assign('RID', $RID);
        //     $data['re_id'] = $authInfoo['id'];
        // } else {
        //     $this->error('销售人不存在！');
        //     exit;
        // }
        // unset($authInfoo, $mapp);

        //检测上节点人
//        $FID = trim($_POST['FID']);  //上节点帐号
//        $mappp = array();
//        $mappp['user_id'] = $FID;
//        $authInfoo = $fck->where($mappp)->field('id,p_path,p_level,user_id,is_pay,tp_path')->find();
//        if ($authInfoo) {
//            $this->assign('FID', $FID);
//            $fatherispay = $authInfoo['is_pay'];
//            $data['father_id'] = $authInfoo['id'];                        //上节点ID
//            $tp_path = $authInfoo['tp_path'];
//        } else {
//            $this->error('上级会员不存在！');
//            exit;
//        }
//        unset($authInfoo, $mappp);
        $TPL = (int) $_POST['TPL'];
        /*
          $where = array();
          $where['father_id'] = $data['father_id'];
          $where['treeplace'] = $TPL;
          $rs = $fck->where($where)->field('id')->find();
          if ($rs){
          $this->error('该位置已经注册！');
          exit;
          } */
        if ($TPL == 0) {
            $zy_n = "1区";
        } elseif ($TPL == 1) {
            $zy_n = "2区";
        } elseif ($TPL == 2) {
            $zy_n = "3区";
        } else {
            $TPL = 0;
            $zy_n = "1区";
        }
        $this->assign('zy_n', $zy_n);
        $this->assign('TPL', $TPL);
        if ($fatherispay == 0 && $TPL > 0) {
            $this->error('接点人开通后才能在此位置注册！');
            exit;
        }
        unset($rs, $where, $TPL);
        $fwhere = array(); //检测帐号是否存在
        $fwhere['user_id'] = trim($_POST['UserID']);
        $frs = $fck->where($fwhere)->field('id')->find();
        if ($frs) {
            $this->error('该会员编号已存在！');
            exit;
        }
        $kk = stripos($fwhere['user_id'], '-');
        if ($kk) {
            $this->error('会员编号中不能有扛(-)符号！');
            exit;
        }
        unset($fwhere, $frs);
        $errmsg = "";
        if (empty($_POST['wenti_dan'])) {
            $errmsg .= "<li>密保答案不能为空！</li>";
        }
        $this->assign('wenti_dan', $_POST['wenti_dan']);
        $this->assign('countrys', $_POST['countrys']);
        if (empty($_POST['BankCard'])) {
            $errmsg .= "<li>银行卡号不能为空！</li>";
        }
        $this->assign('BankCard', $_POST['BankCard']);
        $huhu = trim($_POST['UserName']);
        if (empty($huhu)) {
            $errmsg .= "<li>请填写开户姓名！</li>";
        }
        $this->assign('UserName', $_POST['UserName']);
        if (empty($_POST['UserCode'])) {
            $errmsg .= "<li>请填写身份证号码！</li>";
        }
        $this->assign('UserCode', $_POST['UserCode']);
        if (empty($_POST['UserTel'])) {
            $errmsg .= "<li>请填写电话号码！</li>";
        }
        $this->assign('UserTel', $_POST['UserTel']);
        /*
        if (empty($_POST['qq'])) {
            $errmsg .= "<li>请填写QQ号码！</li>";
        }*/
        $this->assign('qq', $_POST['qq']);
        $this->assign('UserEmail', $_POST['UserEmail']);
        $usercc = trim($_POST['UserCode']);
        $this->assign('UserCode', $_POST['UserCode']);
        if (strlen($_POST['Password']) < 1 or strlen($_POST['Password']) > 16 or strlen($_POST['PassOpen']) < 1 or strlen($_POST['PassOpen']) > 16
        		/* or strlen($_POST['PassOpentwo']) < 1 or strlen($_POST['PassOpentwo']) > 16*/) {
            $this->error('密码应该是1-16位！');
            exit;
        }
        if ($_POST['Password'] != $_POST['rePassword']) {  //一级密码
            $this->error('一级密码两次输入不一致！');
            exit;
        }
        if ($_POST['PassOpen'] != $_POST['rePassOpen']) {  //二级密码
            $this->error('二级密码两次输入不一致！');
            exit;
        }
        /*
        if ($_POST['PassOpentwo'] != $_POST['rePassOpentwo']) {  //三级密码
            $this->error('三级密码两次输入不一致！');
            exit;
        }*/
        if ($_POST['Password'] == $_POST['PassOpen']) {  //二级密码
            $this->error('一级密码与二级密码不能相同！');
            exit;
        }
        /*
        if ($_POST['Password'] == $_POST['PassOpentwo']) {  //二级密码
            $this->error('一级密码与三级密码不能相同！');
            exit;
        }*/
        $this->assign('Password', $_POST['Password']);
        $this->assign('PassOpen', $_POST['PassOpen']);
//      $this->assign('PassOpentwo', $_POST['PassOpentwo']);

        $us_name = $_POST['us_name'];
        $us_address = $_POST['us_address'];
        $us_tel = $_POST['us_tel'];

        $this->assign('us_name', $_POST['us_name']);
        $this->assign('us_address', $_POST['us_address']);
        $this->assign('us_tel', $_POST['us_tel']);

        $s_err = "<ul>";
        $e_err = "</ul>";
        if (!empty($errmsg)) {
            $out_err = $s_err . $errmsg . $e_err;
            $this->error($out_err);
            exit;
        }
        $uLevel = $_POST['u_level'];
        $this->assign('u_level', $_POST['u_level']);
        $fee = M('fee')->find();
        $s = $fee['s9'];

        $s10 = explode('|', $fee['s10']);
        $this->assign('uarray', $s10);
        $s9 = explode('|', $fee['s9']);
        $s19 = explode('|', $fee['s19']);

        $u_money = $s9[$uLevel];  //这个是注册的金额  $uLevel为 0 1 2 3  $u_money为对应的金额
        $u_v = $s19[$uLevel];
        $this->assign('u_v',$u_v);


        $this->assign('u_level', $uLevel);
        $this->assign('s9', $s9);
        $this->assign('u_money', $u_money);
        // $agent_xf = floatval($u_money) * 10;
        // $this->assign('agent_xf', $agent_xf);

        $this->assign('BankName', $_POST['BankName']);
        $this->assign('BankProvince', $_POST['BankProvince']);
        $this->assign('BankCity', $_POST['BankCity']);
        $this->assign('BankAddress', $_POST['BankAddress']);
        $this->assign('UserAddress', $_POST['UserAddress']);
        $this->assign('qq', $_POST['qq']);
        $this->assign('nickname', $_POST['nickname']);
        $this->display();
    }

    /**
     * 注册处理
     * */
    public function usersAdd() {
        $this->_checkUser();
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $fck = M('fck');  //注册表

        $rs = $fck->field('is_pay,agent_cash')->find($id); 
        $m = $rs['agent_cash'];
        if ($rs['is_pay'] == 0) {
            $this->error('临时会员不能注册会员！');
            exit;
        }
        if (strlen($_POST['UserID']) < 1) {
            $this->error('会员编号不能少！');
            exit;
        }

        $data = array();  //创建数据对象
        //检测专卖店
        $re_id = trim($_POST['re_id']); 

        if (empty($re_id)) {
            $this->error('请输入推荐人编号！');
            exit;
        }
        $smap = array();
        $smap['user_id'] = $re_id;
//        $smap['is_agent'] = array('gt', 0);
        $shop_rs = $fck->where($smap)->field('id,user_id,is_agent')->find();

        if (!$shop_rs) {
            $this->error('没有该推荐人！');
            exit;
        } else {
            // $data['shop_id'] = $shop_rs['id'];      //隶属会员中心编号
            $data['re_id'] = $shop_rs['id'];
            $data['re_name'] = $shop_rs['user_id']; //隶属会员中心帐号
        }
        unset($smap, $shopid);

        //检测推荐人      @liuzhiheng
        // $RID = trim($_POST['RID']);  //获取销售会员帐号
        // $mapp = array();
        // $mapp['user_id'] = $RID;
        // $mapp['is_pay'] = array('gt', 0);
        // $authInfoo = $fck->where($mapp)->field('id,user_id,re_level,re_path')->find();
        // if ($authInfoo) {
        //     $data['re_path'] = $authInfoo['re_path'] . $authInfoo['id'] . ',';  //销售路径
        //     $data['re_id'] = $shopid;                             //销售人ID
        //     $data['re_name'] = $authInfoo['user_id'];                       //销售人帐号
        //     $data['re_level'] = $authInfoo['re_level'] + 1;                 //代数(绝对层数)
        // } else {
        //     $this->error('销售人不存在！');
        //     exit;
        // }
        // unset($authInfoo, $mapp);

//        检测上节点人
////        $FID = trim($_POST['FID']);  //上节点帐号
//        $mappp = array();
//        $mappp['user_id'] = $FID;
//// 		$mappp['is_pay']  = array('gt',0);
//        $authInfoo = $fck->where($mappp)->field('id,p_path,p_level,user_id,is_pay,tp_path')->find();
//        if ($authInfoo) {
//            $fatherispay = $authInfoo['is_pay'];
//            $data['p_path'] = $authInfoo['p_path'] . $authInfoo['id'] . ',';  //绝对路径
//            $data['father_id'] = $authInfoo['id'];                        //上节点ID
//            $data['father_name'] = $authInfoo['user_id'];                 //上节点帐号
//            $data['p_level'] = $authInfoo['p_level'] + 1;                 //上节点ID
//            $tp_path = $authInfoo['tp_path'];
//        } else {
//            $this->error('上级会员不存在！');
//            exit;
//        }
//        unset($authInfoo, $mappp);
        $TPL = (int) $_POST['TPL'];
        $where = array();
        $where['father_id'] = $data['father_id'];
        $where['treeplace'] = $TPL;
        $rs = null; //$fck->where($where)->field('id')->find();
        if ($rs) {
            $this->error('该位置已经注册！');
            exit;
        } else {
            $data['treeplace'] = $TPL;
            if (strlen($tp_path) == 0) {
                $data['tp_path'] = $TPL;
            } else {
                $data['tp_path'] = $tp_path . "," . $TPL;
            }
        }

        if ($fatherispay == 0 && $TPL > 0) {
            $this->error('接点人开通后才能在此位置注册！');
            exit;
        }
        unset($rs, $where, $TPL);

        $fwhere = array(); //检测帐号是否存在
        $fwhere['user_id'] = trim($_POST['UserID']);
        $frs = $fck->where($fwhere)->field('id')->find();
        if ($frs) {
            $this->error('该会员编号已存在！');
            exit;
        }
        $kk = stripos($fwhere['user_id'], '-');
        if ($kk) {
            $this->error('会员编号中不能有扛(-)符号！');
            exit;
        }
        unset($fwhere, $frs);

        $errmsg = "";
        if (empty($_POST['wenti_dan'])) {
            $errmsg .= "<li>密保答案不能为空！</li>";
        }
        if (empty($_POST['BankCard'])) {
            $errmsg .= "<li>银行卡号不能为空！</li>";
        }
        $huhu = trim($_POST['UserName']);
        if (empty($huhu)) {
            $errmsg .= "<li>请填写开户姓名！</li>";
        }
        if (empty($_POST['UserCode'])) {
            $errmsg .= "<li>请填写身份证号码！</li>";
        }
        if (empty($_POST['UserTel'])) {
            $errmsg .= "<li>请填写电话号码！</li>";
        }
        /*
        if (empty($_POST['qq'])) {
            $errmsg .= "<li>请填写QQ号码！</li>";
        }*/
        $usercc = trim($_POST['UserCode']);
        if (strlen($_POST['Password']) < 1 or strlen($_POST['Password']) > 16 or strlen($_POST['PassOpen']) < 1 or strlen($_POST['PassOpen']) > 16) {
                  $this->error('密码应该是1-16位！');
                  exit;
        }
        if ($_POST['Password'] == $_POST['PassOpen']) {  //二级密码
                  $this->error('一级密码与二级密码不能相同！');
                  exit;
        }
        $us_name = trim($_POST['us_name']);
        $us_address = trim($_POST['us_address']);
        $us_tel = trim($_POST['us_tel']);
        if (empty($us_name)) {
                 $errmsg .= "<li>请输入收货人姓名！</li>";
        }
        if (empty($us_address)) {
                 $errmsg .= "<li>请输入收货地址！</li>";
        }
        if (empty($us_tel)) {
                 $errmsg .= "<li>请输入收货人电话！</li>";
        }

        $this->assign('us_name', $_POST['us_name']);
        $this->assign('us_address', $_POST['us_address']);
        $this->assign('us_tel', $_POST['us_tel']);
        $s_err = "<ul>";
        $e_err = "</ul>";
        if (!empty($errmsg)) {
            $out_err = $s_err . $errmsg . $e_err;
            $this->error($out_err);
            exit;
        }
        $uLevel = $_POST['u_level'];
        $fee = M('fee')->find();
        $s = $fee['s9'];
        $s2 = explode('|', $fee['s2']);
        $s9 = explode('|', $fee['s9']);
        $F4 = $s2[$uLevel]; //认购单数
        $ul = $s9[$uLevel];
        $Money = explode('|', C('Member_Money'));  //注册金额数组
        $new_userid = $_POST['UserID'];
        $data['user_id'] = $new_userid;
  //      $data['bind_account'] = '3333';
        $data['last_login_ip'] = '';                            //最后登录IP
        $data['verify'] = '0';
        $data['status'] = 1;                             //状态(?)
        $data['type_id'] = '0';
        $data['last_login_time'] = time();                        //最后登录时间
        $data['login_count'] = 0;                             //登录次数
        $data['info'] = '信息';
        $data['name'] = '名称';
        $data['password'] = md5(trim($_POST['Password']));  //一级密码加密
        $data['passopen'] = md5(trim($_POST['PassOpen']));  //二级密码加密
 //       $data['passopentwo'] = md5(trim($_POST['PassOpentwo']));  //二级密码加密
        $data['pwd1'] = trim($_POST['Password']);       //一级密码不加密
        $data['pwd2'] = trim($_POST['PassOpen']);       //二级密码不加密
//        $data['pwd3'] = trim($_POST['PassOpentwo']);       //二级密码不加密
        $data['wenti'] = trim($_POST['wenti']);  //密保问题
        $data['wenti_dan'] = trim($_POST['wenti_dan']);  //密保答案
        $data['lang'] = $_POST['lang'];             //语言
        $data['countrys'] = $_POST['countrys']; //国家
        $data['bank_name'] = $_POST['BankName'];             //银行名称
        $data['bank_card'] = $_POST['BankCard'];             //帐户卡号
        $data['user_name'] = $_POST['UserName'];             //姓名
        $data['nickname'] = $_POST['nickname'];  //昵称
        $data['bank_province'] = $_POST['BankProvince'];  //省份
        $data['bank_city'] = $_POST['BankCity'];      //城市
        $data['bank_address'] = $_POST['BankAddress'];          //开户地址
        $data['user_code'] = $_POST['UserCode'];             //身份证号码
        $data['user_address'] = $_POST['UserAddress'];          //联系地址
        $data['email'] = $_POST['UserEmail'];            //电子邮箱
        $data['qq'] = $_POST['qq'];                //qq
        $data['user_tel'] = $_POST['UserTel'];              //联系电话
        $data['is_pay'] = 0;                              //是否开通
        $data['rdt'] = time();                         //注册时间
        $data['u_level'] = 0;//$uLevel + 1;                      //注册等级
        $data['cpzj'] = $ul;                          //注册金额
        $data['f4'] = $F4;       //单量
        $data['wlf'] = 0;                              //网络费
        $data['agent_xf'] = 0;
        $data['achievement'] = 0;         //获得的业绩
        $data['level'] = $_POST['u_v'];
        $data['shoper_id'] = $_SESSION[C('USER_AUTH_KEY')];

//        switch ($ul) {
//            case '2000':
//                $data['level'] = '会员';
//                break;
//            case '20000':
//                $data['level'] = 'VIP';
//                break;
//            case '50000':
//                $data['level'] = '社区体验店';
//                break;
//            case '400000':
//                $data['level'] = '专卖店';
//                break;
//        }

        $result = $fck->add($data);
        if ($result) {

        	$income = M('income');
        	$in = array(
        			'uid'=>$result,
        			'type'=>1,
        			'user_id'=>$new_userid,
        			'money'=>$ul,
        			'option' =>1,
        			'pv' =>$ul,
        			'act_pdt'=>time()
        	);
        	$inid = $income->add($in);
        	
        	$data['f4'] = $inid;
        	$data['id'] =$result;
        	
        	$fck->save($data);
            //@liuzhiheng   创建re_path推荐路径,方便以后计算级差奖和管理奖  $_POST['re_id']
            
            $path['user_id'] = trim($_POST['re_id']);
            $path_data = $fck->where($path)->find();
            if ( empty($path_data['re_path']) ){
                $datasss['id'] = $result;
                $datasss['re_path'] = $path_data['id'];
                $_b = $fck->save($datasss);
                unset($datasss,$_b,$path_data,$path);
            }else{
                $datasss['id'] = $result;
                $datasss['re_path'] = $path_data['re_path'].','.$path_data['id'];
                $_bb = $fck->save($datasss);
                unset($datasss,$_bb,$path_data,$path);
            }

            $adddata['uid'] = $result;
            $adddata['name'] = $us_name;
            $adddata['tel'] = $us_tel;
            $adddata['address'] = $us_address;
            M('address')->add($adddata);
            
            M('fee')->query("update __TABLE__ set us_num=us_num+1");
            
            $_SESSION['new_user_reg_id'] = $result;
            echo "<script>window.location='" . __URL__ . "/users_ok/';</script>";
            exit;
        } else {
            $this->error('会员注册失败！');
            exit;
        }
    }



    public function caljiangjin($a, $b) {
             
    }
    /**
     * 注册完成
     * * */
    public function users_ok() {
        $this->_checkUser();
        $gourl = __APP__ . "/Reg/users/";
        if (!empty($_SESSION['new_user_reg_id'])) {

            $fck = M('fck');
            $fee_rs = M('fee')->find();

//            $uLevel = $_POST['u_level'];
//            $s9 = explode('|', $fee['s9']);
//            $s19 = explode('|', $fee['s19']);
//            $u_money = $s9[$uLevel];  //这个是注册的金额  $uLevel为 0 1 2 3  $u_money为对应的金额
//            $u_v = $s19[$uLevel];
//            $this->assign('u_v',$u_v);

            $this->assign('s8', $fee_rs['s8']);
            $this->assign('alert_msg', $fee_rs['str28']);
            $this->assign('s17', $fee_rs['s17']);
            $myrs = $fck->where('id=' . $_SESSION['new_user_reg_id'])->find();
            $this->assign('myrs', $myrs);
            $this->assign('gourl', $gourl);
            unset($fck, $fee_rs);
            $this->display();
        } else {
            echo "<script>window.location='" . $gourl . "';</script>";
            exit;
        }
    }

    //前台注册
    public function us_reg() {
        $fck = M('fck');
        $fee = M('fee');
        $reid = (int) $_GET['rid'];

        $fee_rs = $fee->field('s9,str21,str27,str29,str99')->find();
        $this->assign('fflv', $fee_rs['str21']);
        $this->assign('str27', $fee_rs['str27']);
        $s9 = $fee_rs['s9'];
        $s9 = explode('|', $s9);
        $this->assign('s9', $s9);
        $bank = explode('|', $fee_rs['str29']);
        $this->assign('bank', $bank);
        $wentilist = explode('|', $fee_rs['str99']);
        $this->assign('wentilist', $wentilist);

        $arr = array();
        $arr['UserID'] = $this->_getUserID();
        $this->assign('flist', $arr);

        //检测销售人
        $where = array();
        $where['id'] = $reid;
        $where['is_pay'] = array('gt', 0);
        $field = 'id,user_id,nickname,us_img,is_agent,shop_name';
        $rs = $fck->where($where)->field($field)->find();
        if ($rs) { 
            if (empty($rs['us_img'])) {
                $rs['us_img'] = "__PUBLIC__/Images/tirns.jpg";
            }
            if ($rs['is_agent'] == 2) {
                $this->assign('shopname', $rs['user_id']);
            } else {
                $this->assign('shopname', $rs['shop_name']);
            }
            $this->assign('rs', $rs);
            $this->assign('reid', $reid);

        }
        $plan = M('plan');
        $prs = $plan->find(4);
        $this->assign('prs', $prs);
        $this->display();
    }

    //前台注册处理
    public function us_regAC() {
        $fck = M('fck');  //注册表

        if (strlen($_POST['UserID']) < 1) {
            $this->error('会员编号不能少！');
            exit;
        }

        $data = array();  //创建数据对象
        //检测专卖店
        $shopid = trim($_POST['shopid']);  //所属专卖店帐号
        if (empty($shopid)) {
            $this->error('请输入专卖店编号！');
            exit;
        }
        $smap = array();
        $smap['user_id'] = $shopid;
        $smap['is_agent'] = array('gt', 1);
        $shop_rs = $fck->where($smap)->field('id,user_id')->find();
        if (!$shop_rs) {
            $this->error('没有该专卖店！');
            exit;
        } else {
            $data['shop_id'] = $shop_rs['id'];      //隶属会员中心编号
            $data['shop_name'] = $shop_rs['user_id']; //隶属会员中心帐号
        }
        unset($smap, $shop_rs, $shopid);

        //检测销售人
        $RID = trim($_POST['RID']);  //获取销售会员帐号
        $mapp = array();
        $mapp['user_id'] = $RID;
        $mapp['is_pay'] = array('gt', 0);
        $authInfoo = $fck->where($mapp)->field('id,user_id,re_level,re_path')->find();
        if ($authInfoo) {
            $data['re_path'] = $authInfoo['re_path'] . $authInfoo['id'] . ',';  //销售路径
            $data['re_id'] = $authInfoo['id'];                              //销售人ID
            $data['re_name'] = $authInfoo['user_id'];                       //销售人帐号
            $data['re_level'] = $authInfoo['re_level'] + 1;                 //代数(绝对层数)
        } else {
            $this->error('销售人不存在！');
            exit;
        }
        unset($authInfoo, $mapp);

        //检测上节点人
        $FID = trim($_POST['FID']);  //上节点帐号
        $mappp = array();
        $mappp['user_id'] = $FID;
//		$mappp['is_pay']  = array('gt',0);
        $authInfoo = $fck->where($mappp)->field('id,p_path,p_level,user_id,is_pay,tp_path')->find();
        if ($authInfoo) {
            $fatherispay = $authInfoo['is_pay'];
            $data['p_path'] = $authInfoo['p_path'] . $authInfoo['id'] . ',';  //绝对路径
            $data['father_id'] = $authInfoo['id'];                        //上节点ID
            $data['father_name'] = $authInfoo['user_id'];                 //上节点帐号
            $data['p_level'] = $authInfoo['p_level'] + 1;                 //上节点ID
            $tp_path = $authInfoo['tp_path'];
        } else {
            $this->error('上级会员不存在！');
            exit;
        }
        unset($authInfoo, $mappp);
        $TPL = (int) $_POST['TPL'];
        $where = array();
        $where['father_id'] = $data['father_id'];
        $where['treeplace'] = $TPL;
        $rs = $fck->where($where)->field('id')->find();
        if ($rs) {
            $this->error('该位置已经注册！');
            exit;
        } else {
            $data['treeplace'] = $TPL;
            if (strlen($tp_path) == 0) {
                $data['tp_path'] = $TPL;
            } else {
                $data['tp_path'] = $tp_path . "," . $TPL;
            }
        }

        if ($fatherispay == 0 && $TPL > 0) {
            $this->error('接点人开通后才能在此位置注册！');
            exit;
        }
        unset($rs, $where, $TPL);

        $fwhere = array(); //检测帐号是否存在
        $fwhere['user_id'] = trim($_POST['UserID']);
        $frs = $fck->where($fwhere)->field('id')->find();
        if ($frs) {
            $this->error('该会员编号已存在！');
            exit;
        }
        $kk = stripos($fwhere['user_id'], '-');
        if ($kk) {
            $this->error('会员编号中不能有扛(-)符号！');
            exit;
        }
        unset($fwhere, $frs);

        $errmsg = "";
        if (empty($_POST['wenti_dan'])) {
            $errmsg .= "<li>密保答案不能为空！</li>";
        }
        if (empty($_POST['BankCard'])) {
            $errmsg .= "<li>银行卡号不能为空！</li>";
        }
        $huhu = trim($_POST['UserName']);
        if (empty($huhu)) {
            $errmsg .= "<li>请填写开户姓名！</li>";
        }
        if (empty($_POST['UserCode'])) {
            $errmsg .= "<li>请填写身份证号码！</li>";
        }
        if (empty($_POST['UserTel'])) {
            $errmsg .= "<li>请填写电话号码！</li>";
        }
        if (empty($_POST['qq'])) {
            $errmsg .= "<li>请填写QQ号码！</li>";
        }

        $usercc = trim($_POST['UserCode']);

        if (strlen($_POST['Password']) < 1 or strlen($_POST['Password']) > 16 or strlen($_POST['PassOpen']) < 1 or strlen($_POST['PassOpen']) > 16) {
            $this->error('密码应该是1-16位！');
            exit;
        }
        if ($_POST['Password'] == $_POST['PassOpen']) {  //二级密码
            $this->error('一级密码与二级密码不能相同！');
            exit;
        }

        $s_err = "<ul>";
        $e_err = "</ul>";
        if (!empty($errmsg)) {
            $out_err = $s_err . $errmsg . $e_err;
            $this->error($out_err);
            exit;
        }

        $uLevel = $_POST['u_level'];
        $fee = M('fee')->find();
        $s = $fee['s9'];
        $s2 = explode('|', $fee['s2']);
        $s9 = explode('|', $fee['s9']);
        $s15 = explode('|', $fee['s15']);

        $F4 = $s2[$uLevel]; //认购单数
        $ul = $s9[$uLevel];
        $gp = $s15[$uLevel];

        $Money = explode('|', C('Member_Money'));  //注册金额数组

        $new_userid = $_POST['UserID'];

        $data['user_id'] = $new_userid;
        $data['bind_account'] = '3333';
        $data['last_login_ip'] = '';                            //最后登录IP
        $data['verify'] = '0';
        $data['status'] = 1;                             //状态(?)
        $data['type_id'] = '0';
        $data['last_login_time'] = time();                        //最后登录时间
        $data['login_count'] = 0;                             //登录次数
        $data['info'] = '信息';
        $data['name'] = '名称';
        $data['password'] = md5(trim($_POST['Password']));  //一级密码加密
        $data['passopen'] = md5(trim($_POST['PassOpen']));  //二级密码加密
        $data['pwd1'] = trim($_POST['Password']);       //一级密码不加密
        $data['pwd2'] = trim($_POST['PassOpen']);       //二级密码不加密

        $data['wenti'] = trim($_POST['wenti']);  //密保问题
        $data['wenti_dan'] = trim($_POST['wenti_dan']);  //密保答案

        $data['bank_name'] = $_POST['BankName'];             //银行名称
        $data['bank_card'] = $_POST['BankCard'];             //帐户卡号
        $data['user_name'] = $_POST['UserName'];             //姓名
        $data['nickname'] = $_POST['UserID']; //$_POST['nickname'];  //昵称
        $data['bank_province'] = $_POST['BankProvince'];  //省份
        $data['bank_city'] = $_POST['BankCity'];      //城市
        $data['bank_address'] = $_POST['BankAddress'];          //开户地址
        //$data['user_post']           = $_POST['UserPost']; 		   //
        $data['user_code'] = $_POST['UserCode'];             //身份证号码
// 		$data['user_address']        = $_POST['UserAddress'];          //联系地址
// 		$data['email']               = $_POST['UserEmail'];            //电子邮箱
        $data['qq'] = $_POST['qq'];                //qq
        $data['user_tel'] = $_POST['UserTel'];              //联系电话
        $data['is_pay'] = 0;                              //是否开通
        $data['rdt'] = time();                         //注册时间
//		$data['pdt']                 = strtotime(date('Y-m-d'));
        $data['u_level'] = $uLevel + 1;                      //注册等级
        $data['cpzj'] = $ul;                          //注册金额
        $data['f4'] = $F4;       //单量
        $data['gp_num'] = $gp;       //原始股
        $data['wlf'] = 0;                              //网络费
      
        
        $result = $fck->add($data);
        
        $income = M('income');
        $in = array(
        		'uid'=>$result,
        		'user_id'=>$new_userid,
        		'money'=>$ul,
        		'pv' =>$ul,
        		'act_pdt'=>time()
        );
        $inid = $income->add($in);
        
        $data['f4'] = $inid;
        $data['id'] =$result;
        
        $fck->save($data);
        
        unset($data, $fck);
        if ($result) {

            echo "<script>";
            echo "alert('恭喜您注册成功，您的账户编号：" . $new_userid . "，请及时开通正式会员！');";
            echo "window.location='" . __APP__ . "/Public/login/';";
            echo "</script>";
            exit;
        } else {
            $this->error('会员注册失败！');
            exit;
        }
    }

    //生成会员编号
    private function _getUserID() {
        $fck = M('fck');
//		$fee = M('fee');
//		$fee_rs = $fee->field('us_num')->find(1);
//		$us_num = $fee_rs['us_num'];
//		$first_n = 800000000;
//		$mynn = $first_n+$us_num;

        $mynn = '' . rand(1000000, 9999999);

//		if($us_num<10){
//			$mynn = "00000".$us_num;
//		}elseif($us_num<100){
//			$mynn = "0000".$us_num;
//		}elseif($us_num<1000){
//			$mynn = "000".$us_num;
//		}elseif($us_num<10000){
//			$mynn = "00".$us_num;
//		}elseif($us_num<100000){
//			$mynn = "0".$us_num;
//		}else{
//			$mynn = $us_num;
//		}
        $fwhere['user_id'] = $mynn;
        $frss = $fck->where($fwhere)->field('id')->find();
        if ($frss) {
            return $this->_getUserID();
        } else {
            unset($fck, $fee);
            return $mynn;
        }
    }

    //判断最左区
    public function pd_left_us($uid, &$tp) {
        $fck = M('fck');
        $c_l = $fck->where('father_id=' . $uid . ' and treeplace=' . $tp . '')->field('id')->find();
        if ($c_l) {
            $n_id = $c_l['id'];
            $tp = 0;
            $ren_id = $this->pd_left_us($n_id, $tp);
        } else {
            $ren_id = $uid;
        }
        unset($fck, $c_l);
        return $ren_id;
    }

    //
    public function find_agent() {
        $fck = M('fck');
        $where = "is_agent=2 and is_pay>0";
        $s_echo = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tab1"><tr><td>';
        $e_echo = '</td></tr></table>';
        $m_echo = "";
        $c_l = $fck->where($where)->field('user_id,user_name,shop_a')->select();
        foreach ($c_l as $ll) {
            $m_echo .= "<li><b>" . $ll['user_id'] . "</b>(" . $ll['user_name'] . ")<br>" . $ll['shop_a'] . "</li>";
        }
        unset($fck, $c_l);
        echo $s_echo . $m_echo . $e_echo;
    }

    // 找回密码1
    public function find_pw() {
        $_SESSION['us_openemail'] = "";
        $this->display('find_pw');
    }

    // 找回密码2
    public function find_pw_s() {
        if (empty($_SESSION['us_openemail'])) {
            if (empty($_POST['us_name']) && empty($_POST['us_email'])) {
                $_SESSION = array();
                $this->display('../Public/LinkOut');
                return;
            }
            $ptname = $_POST['us_name'];
            $us_email = $_POST['us_email'];
            $fck = M('fck');
            $rs = $fck->where("user_id='" . $ptname . "'")->field('id,email,user_id,user_name,pwd1,pwd2')->find();
            if ($rs == false) {
                $errarry['err'] = '<font color=red>注：找不到此会员编号！</font>';
                $this->assign('errarry', $errarry);
                $this->display('find_pw');
            } else {
                if ($us_email <> $rs['email']) {
                    $errarry['err'] = '<font color=red>注：邮箱验证失败！</font>';
                    $this->assign('errarry', $errarry);
                    $this->display('find_pw');
                } else {

                    $passarr = array();
                    $passarr[0] = $rs['pwd1'];
                    $passarr[1] = $rs['pwd2'];

                    $title = '感谢您使用密码找回';

                    $body = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"font-size:12px; line-height:24px;\">";
                    $body = $body . "<tr>";
                    $body = $body . "<td height=\"30\">尊敬的客户:" . $rs['user_name'] . "</td>";
                    $body = $body . "</tr>";
                    $body = $body . "<tr>";
                    $body = $body . "<td height=\"30\">你的账户编号:" . $rs['user_id'] . "</td>";
                    $body = $body . "</tr>";
                    $body = $body . "<tr>";
                    $body = $body . "<td height=\"30\">一级密码为:" . $rs['pwd1'] . "</td>";
                    $body = $body . "</tr>";
                    $body = $body . "<tr>";
                    $body = $body . "<td height=\"30\">二级密码为:" . $rs['pwd2'] . "</td>";
                    $body = $body . "</tr>";
                    $body = $body . "此邮件由系统发出，请勿直接回复。<br>";
                    $body = $body . "</td></tr>";
                    $body = $body . "<tr>";
                    $body = $body . "<td height=\"30\" align=\"right\">" . date("Y-m-d H:i:s") . "</td>";
                    $body = $body . "</tr>";
                    $body = $body . "</table>";

                    $this->send_email($us_email, $title, $body);

                    $_SESSION['us_openemail'] = $us_email;
                    $this->find_pw_e($us_email);
                }
            }
        } else {
            $us_email = $_SESSION['us_openemail'];
            $this->find_pw_e($us_email);
        }
    }

    // 找回密码3
    public function find_pw_e($us_email) {
        $this->assign('myask', $us_email);
        $this->display('find_pw_s');
    }

    public function send_email($useremail, $title = '', $body = '') {

        require_once "stemp/class.phpmailer.php";
        require_once "stemp/class.smtp.php";

        $arra = array();

        $mail = new PHPMailer();
        $mail->IsSMTP();                  // send via SMTP
        $mail->Host = "smtp.163.com";   // SMTP servers
        $mail->SMTPAuth = true;           // turn on SMTP authentication
        $mail->Username = "yuyangtaoyecn";     // SMTP username     注意：普通邮件认证不需要加 @域名
        $mail->Password = "yuyangtaoyecn666";          // SMTP password
        $mail->From = "yuyangtaoyecn@163.com";        // 发件人邮箱
        $mail->FromName = "商务会员管理系统";    // 发件人
        $mail->CharSet = "utf-8";              // 这里指定字符集！
        $mail->Encoding = "base64";
        $mail->AddAddress("" . $useremail . "", "" . $useremail . "");    // 收件人邮箱和姓名
        //$mail->AddAddress("119515301@qq.com","text");    // 收件人邮箱和姓名
        $mail->AddReplyTo("" . $useremail . "", "163.com");
        $mail->IsHTML(true);    // send as HTML
        $mail->Subject = $title; // 邮件主题
        $mail->Body = "" . $body . ""; // 邮件内容
        $mail->AltBody = "text/html";
//		$mail->Send();

        if (!$mail->Send()) {
            echo "Message could not be sent. <p>";
            echo "Mailer Error: " . $mail->ErrorInfo;
            exit;
        }
        
    }

}

?>