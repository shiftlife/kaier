<?php
class UserAction extends CommonAction{
	public function _initialize() {
		header("Content-Type:text/html; charset=utf-8");
		$this->_inject_check(0);//调用过滤函数
		$this->_Config_name();//调用参数
		$this->_checkUser();
		$this->check_us_gq();
	}
	public function cody(){
		//===================================二级验证
		$UrlID = (int) $_GET['c_id'];
		if (empty($UrlID)){
			$this->error('二级密码错误!');
			exit;
		}
		if(!empty($_SESSION['user_pwd2'])){
			$url = __URL__."/codys/Urlsz/$UrlID";
			$this->_boxx($url);
			exit;
		}
		$cody   =  M ('cody');
		$list	=  $cody->where("c_id=$UrlID")->field('c_id')->find();
		if ($list){
			$this->assign('vo',$list);
			$this->display('../Public/cody');
			exit;
		}else{
			$this->error('二级密码错误!');
			exit;
		}
	}
	public function codys(){
		//=============================二级验证后调转页面
		$Urlsz = (int) $_POST['Urlsz'];
		if(empty($_SESSION['user_pwd2'])){
			$pass  = $_POST['oldpassword'];
			$fck   =  M ('fck');
			if (!$fck->autoCheckToken($_POST)){
				$this->error('页面过期请刷新页面!');
				exit();
			}
			if (empty($pass)){
				$this->error('二级密码错误!');
				exit();
			}
			$where = array();
			$where['id'] = $_SESSION[C('USER_AUTH_KEY')];
			$where['passopen'] = md5($pass);
			$list = $fck->where($where)->field('id,is_agent')->find();
			if($list == false){
				$this->error('二级密码错误!');
				exit();
			}
			$_SESSION['user_pwd2'] = 1;
		}else{
			$Urlsz = $_GET['Urlsz'];
		}
		switch ($Urlsz){
			case 1;
			$_SESSION['Urlszpass'] = 'MyssHuoLongGuo';
			$bUrl = __URL__.'/relations';
			$this->_boxx($bUrl);
			break;
			case 2;
			$_SESSION['Urlszpass'] = 'Myssmemberx';
			$bUrl = __URL__.'/member_x';
			$this->_boxx($bUrl);
			break;
			case 3;
			$_SESSION['Urlszpass'] = 'Myssmemberz';
			$bUrl = __URL__.'/member_z';
			$this->_boxx($bUrl);
			break;
			case 10;
			$_SESSION['UrlPTPass'] = 'admintongji';
			$bUrl = __URL__.'/adminalltongji';
			$this->_boxx($bUrl);
			break;
			case 4;
			$_SESSION['UrlPTPass'] = 'MyssShuiPuTao';
			$bUrl = __URL__.'/menber';
			$this->_boxx($bUrl);
			break;
			case 5; //升级
			$_SESSION['UrlPTPass'] = 'MyssShuiShj';
			$bUrl = __URL__.'/upgrade';
			$this->_boxx($bUrl);
			break;
			default;
			$this->error('二级密码错误!');
			exit;
		}
	}
	//销售表
	public function relations($Urlsz=0){
		//销售关系
		if ($_SESSION['Urlszpass'] == 'MyssHuoLongGuo'){
			$fck = M('fck');
			$UserID = $_REQUEST['UserID'];
			if (!empty($UserID)){
				$map['user_id'] = array('like',"%".$UserID."%");
			}
			$map['re_id'] = $_SESSION[C('USER_AUTH_KEY')];
			$map['is_pay'] = 1;

            $field  = '*';
            //=====================分页开始==============================================
            import ( "@.ORG.ZQPage" );  //导入分页类
            $count = $fck->where($map)->count();//总页数
    	    $listrows = C('ONE_PAGE_RE');//每页显示的记录数
			$page_where = 'UserID='.$UserID;//分页条件
            $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
            //===============(总页数,每页显示记录数,css样式 0-9)
            $show = $Page->show();//分页变量
            $this->assign('page',$show);//分页变量输出到模板
            $list = $fck->where($map)->field($field)->order('pdt desc')->page($Page->getPage().','.$listrows)->select();
            $HYJJ = '';
            $this->_levelConfirm($HYJJ,1);
            $this->assign('voo',$HYJJ);//会员级别
            $this->assign('list',$list);//数据输出到模板
            //=================================================
			$this->display ('relations');
			return;
		}else{
			$this->error('数据错误2!');
			exit;
		}
	}
	//前后5人
	public function member_x(){
		if ($_SESSION['Urlszpass'] == 'Myssmemberx'){
			$fck = M('fck');
			$id = $_SESSION[C('USER_AUTH_KEY')];
			$myrs = $fck->where('id='.$id)->field('id,user_id,n_pai')->find();
			$n_pai = $myrs['n_pai'];
			
			$field  = 'id,user_id,n_pai,pdt,user_tel,qq';
			//前面5个
    		$wherea = "is_pay>0 and n_pai<".$n_pai;
            $alist = $fck->where($wherea)->field($field)->order('n_pai desc')->limit(5)->select();
            $this->assign('alist',$alist);
            //后5个
    		$whereb = "is_pay>0 and n_pai>".$n_pai;
            $blist = $fck->where($whereb)->field($field)->order('n_pai asc')->limit(5)->select();
            $this->assign('blist',$blist);

			$this->display ('member_x');
			return;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}
	//一线排网
	public function member_z(){
		if ($_SESSION['Urlszpass'] == 'Myssmemberz'){
			$fck = M('fck');
			$id = $_SESSION[C('USER_AUTH_KEY')];
			$myrs = $fck->where('id='.$id)->field('id,user_id,x_pai')->find();
			$x_pai = $myrs['x_pai'];
			
			$field  = 'id,user_id,x_pai,pdt,user_tel,qq,x_num,x_out';

    		$wherea = "is_pay>0 and x_pai>=".$x_pai;
    		//=====================分页开始==============================================
            import ( "@.ORG.ZQPage" );  //导入分页类
            $count = $fck->where($wherea)->count();//总页数
       		$listrows = 20;//每页显示的记录数
            $page_where = '';//分页条件
            $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
            //===============(总页数,每页显示记录数,css样式 0-9)
            $show = $Page->show();//分页变量
            $this->assign('page',$show);//分页变量输出到模板
            $list = $fck->where($wherea)->field($field)->order('x_pai asc,id asc')->page($Page->getPage().','.$listrows)->select();
            $this->assign('list',$list);//数据输出到模板
            //=================================================
            
            $nn = $fck->where("is_pay>0 and x_pai<".$x_pai." and x_out=0")->count();
            $this->assign('nn',$nn);

			$this->display ('member_z');
			return;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}
	//统计
	public function adminalltongji(){
		$this->_Admin_checkUser();
		$fck = M ('fck');
		$msg = M ('msg');
		$chongzhi = M ('chongzhi');
		$tiqu = M ('tiqu');
		$tiqu = M ('tiqu');
		
		$now_day = strtotime(date("Y-m-d"));
		$end_day = $now_day+3600*24;
		
		$yes_day = $now_day-3600*24;
		
		$yes_c = $fck->where('is_pay>0 and pdt>='.$yes_day.' and pdt<'.$now_day)->count();//昨日新进
		$day_c = $fck->where('is_pay>0 and pdt>='.$now_day.' and pdt<'.$end_day)->count();//今日新进
		$not_c = $fck->where('is_pay=0')->count();//未开通
		$msg_c = $msg->where("s_uid=1 and s_read=0")->count(); //总记录数
		$chz_c = $chongzhi->where("is_pay=0")->count(); //充值
		$tix_c = $tiqu->where("is_pay=0")->count(); //提现
		$bad_c = $fck->where("is_agent=1 and is_pay>0")->count(); //专卖店
		
		$this->assign('yes_c',$yes_c);
		$this->assign('day_c',$day_c);
		$this->assign('not_c',$not_c);
		$this->assign('msg_c',$msg_c);
		$this->assign('chz_c',$chz_c);
		$this->assign('tix_c',$tix_c);
		$this->assign('upl_c',0);
		$this->assign('bad_c',$bad_c);
		$this->assign('did_c',0);
		
		$this->display();
	}
	
	
	//未开通会员
	public function menber($Urlsz=0){
		//列表过滤器，生成查询Map对象
		if ($_SESSION['UrlPTPass'] == 'MyssShuiPuTao'){
			$fck = M('fck');
			$map = array();
			$id = $_SESSION[C('USER_AUTH_KEY')];
			
			$rsss = $fck->where('id='.$id)->field('is_zy')->find();

			$map['is_pay'] = array('eq',1);
			$map['_string'] = "is_zy=".$id." or is_zy=".$rsss['is_zy'];
	
			//查询字段
			$field  = '*';
			//=====================分页开始==============================================
			import ( "@.ORG.ZQPage" );  //导入分页类
			$count = $fck->where($map)->count();//总页数
			$listrows = C('ONE_PAGE_RE');//每页显示的记录数
			$page_where = '';//分页条件
			$Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
			//===============(总页数,每页显示记录数,css样式 0-9)
			$show = $Page->show();//分页变量
			$this->assign('page',$show);//分页变量输出到模板
			$list = $fck->where($map)->field($field)->order('is_pay asc,pdt desc')->page($Page->getPage().','.$listrows)->select();
			$this->assign('list',$list);//数据输出到模板
			//=================================================
	
			$HYJJ = '';
			$this->_levelConfirm($HYJJ,1);
			$this->assign('voo',$HYJJ);//会员级别
			$where = array();
			$where['id'] = $id;
			$fck_rs = $fck->where($where)->field('*')->find();
			$this->assign('frs',$fck_rs);//电子币
			$this->display ('menber');
			exit;
		}else{
			$this->error('数据错误!');
			exit;
		}
	}
	
	public function upgrade(){
	    if ($_SESSION['UrlPTPass'] == 'MyssShuiShj'){
	        $fee = M('fee');
	        $id = $_SESSION[C('USER_AUTH_KEY')];
	        $fee = $fee->find(); 
	        $fck = M('fck');
	        $id = $_SESSION[C('USER_AUTH_KEY')];
	        $fck_rs = $fck->where('id='.$id)->find();
	        $this->assign('fck_rs',$fck_rs);
	        $this->assign('Aname',explode("|", $fee['s10']));
	        $this->assign('banklist',explode("|", $fee['str29']));
	    
	        $promo = M('promo');
	        $field  = '*';
	        $map['uid'] = $id;
	        $list = $promo->where($map)->field($field)->order('id desc')->select();
	        $this->assign('promo',$list[0]);
	        
	        $this->display ('upgrade');
	        return;
	    }else{
	        $this->error('数据错误!');
	        exit;
	    }
	}
	
	public function upgradeAC() {
	    //================================申请升级中转函数
	    $shop_a = $_POST['shop_a'];

	    $fee = M('fee');
	    $fee_rs = $fee->where('s9,s14')->find(1);
//	    $s14 = (int) $fee_rs['s14'];
	    $s9 = explode("|", $fee_rs['s9']);  //会员级别费用
	    //		$one_mm = $s9[0];
//	    $one_mm = 1;

               $fck = M('fck');
	    $id = $_SESSION[C('USER_AUTH_KEY')];
	    $where = array();
	    $where['id'] = $id;
	
	    $fck_rs = $fck->where($where)->find();
	
	    if ($fck_rs) {
	        if ($fck_rs['is_pay'] == 0) {
	            $this->error('临时会员不能申请!');
	            exit;
	        }
	        if (empty($_REQUEST['up_level'])) {
	            $this->error('申请级别不能为空!');
	        }
                   $money = $s9[$_REQUEST['up_level']-1]-$s9[$fck_rs['u_level']-1];
	        // 写入帐号数据
			$data['uid']				= $id;
			$data['user_id']			= $fck_rs['user_id'];
			$data['money']				= $money;//补差额
			$data['u_level']			= $fck_rs['u_level'];//旧的
			$data['up_level']			= $_REQUEST['up_level'];//新的
			$data['create_time']		= time();
			$data['pdt']				= 0;
			$data['danshu']				= 0;
			$data['is_pay']				= 0;
			$data['user_name']			= $fck_rs['user_name'];
			$data['u_bank_name']		= $_REQUEST['u_bank_name'];
			$data['bill_no']		= $_REQUEST['bill_no'];
			$data['type']				= 0;
			$promo = M('promo');
                   $result = $promo->add($data);        

	        $bUrl = __URL__ . '/upgrade';
	        $this->_box(1, '申请成功！', $bUrl, 2);
	    } else {
	        $this->error('非法操作');
	        exit;
	    }
	}
}
?>