<?php

class ShopAction extends CommonAction {

    public function _initialize() {
        header("Content-Type:text/html; charset=utf-8");
        $this->_inject_check(1); //调用过滤函数
        $this->_Config_name(); //调用参数
        //读出商品分类缓存 
        $file_path = "cate_menu.txt";
        $menu_list = file_get_contents($file_path);
        $menu_array = json_decode($menu_list, true);
        $children = $menu_array['b'];
        $goods_data = $menu_array['a'];
        $this->assign('children', $children);
        $this->assign('goods_data', $goods_data); //数据输出到模板
        $sum = $this->cart_number();
        $this->assign('sum', $sum);
        $fck_rs = $this->getUserInfo();
        $this->assign('fck_rs', $fck_rs);
    }

    //过滤查询字段
    function _filter(&$map) {
        $map['title'] = array('like', "%" . $_POST['name'] . "%");
    }

    // 顶部页面
    public function top() {
        C('SHOW_RUN_TIME', false);   // 运行时间显示
        C('SHOW_PAGE_TRACE', false);
        $this->display();
    }

    // 尾部页面
    public function footer() {
        C('SHOW_RUN_TIME', false);   // 运行时间显示
        C('SHOW_PAGE_TRACE', false);
        $this->display();
    }

    // 菜单页面
    public function menu() {
        $this->_checkUser();
        $map = array();
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $field = '*';

        $map = array();
        $map['s_uid'] = $id;   //会员ID
        $map['s_read'] = 0;     // 0 为未读
        $info_count = M('msg')->where($map)->count(); //总记录数
        $this->assign('info_count', $info_count);

        $fck = M('fck');
        $fwhere = array();
        $fwhere['ID'] = $_SESSION[C('USER_AUTH_KEY')];
        $frs = $fck->where($fwhere)->field('*')->find();
        //dump($frs);
        $HYJJ = '';
        $this->_levelConfirm($HYJJ, 1);
        $this->assign('voo', $HYJJ);

        $this->assign('fck_rs', $frs);
        $this->display('menu');
    }

    // 后台首页 查看系统信息
    public function main() {
        $this->_checkUser();
        $id = $_SESSION[C('USER_AUTH_KEY')];  //登录AutoId

        $bonus = M('bonus');  //奖金表
        $where = array();
        $where['uid'] = $id;
        $field = '*';
        $list = $bonus->where($where)->field($field)->order('id desc')->limit(10)->select();
        $this->assign('list', $list);

        $form = M('form');
        $map = array();
        $map['status'] = array('eq', 1);
        $field = '*';
        $nlist = $form->where($map)->field($field)->order('baile desc,id desc')->limit(10)->select();
        $this->assign('f_list', $nlist); //数据输出到模板

        $ntlist = $form->where($map)->field($field)->order('baile desc,id desc')->limit(3)->select();
        $this->assign('t_list', $ntlist); //数据输出到模板
        //销售人数
        $fck = M('fck');
        $map = array();
        $map['re_id'] = $id;
        $map['is_pay'] = 1;
        $re_count = $fck->where($map)->count();
        $this->assign('re_count', $re_count);

        $map = array();
        $map['s_uid'] = $id;   //会员ID
        $map['s_read'] = 0;     // 0 为未读
        $info_count = M('msg')->where($map)->count(); //总记录数
        $this->assign('info_count', $info_count);

        //会员级别
        $urs = $fck->where('id=' . $id)->field('*')->find();
        $HYJJ = '';
        $this->_levelConfirm($HYJJ, 1);
        $this->assign('voo', $HYJJ); //会员级别
        if (empty($urs['us_img'])) {
            $urs['us_img'] = "__PUBLIC__/Images/mctxico.jpg";
        }
        $this->assign('fck_rs', $urs); //总奖金

        $fee = M('fee');
        $fee_rs = $fee->field('str7,str21,str22,str23,a_money,b_money')->find();
        $str21 = $fee_rs['str21'];
        $str22 = $fee_rs['str22'];
        $str23 = $fee_rs['str23'];
        $all_img = $str21 . "|" . $str22 . "|" . $str23;
        $this->assign('all_img', $all_img);

        $a_money = $fee_rs['a_money'];
        $b_money = $fee_rs['b_money'];
        $all_money = $a_money + $b_money;
        $all_money = number_format($all_money, 2);
        $this->assign('all_money', $all_money);

        $str5 = explode("|", $fee_rs['str7']);
        $maxqq = 4;
        if (count($str5) > $maxqq) {
            $lenn = $maxqq;
        } else {
            $lenn = count($str5);
        }
        for ($i = 0; $i < $lenn; $i++) {
            $qqlist[$i] = $str5[$i];
        }
        $this->assign('qlist', $qqlist);

        $HYJJ = "";
        $this->_levelConfirm($HYJJ, 1);
        $this->assign('voo', $HYJJ); //会员级别

        $plan = M('plan');
        $svo = $plan->find(2);
        $this->assign('svo', $svo);
        $fvo = $plan->find(3);
        $this->assign('fvo', $fvo);

        $see = $_SERVER['HTTP_HOST'] . __APP__;
        $see = str_replace("//", "/", $see);
        $this->assign('server', $see);

//        sleep(5);

        $this->display();
    }

    // 用户登录页面
    public function login() {
        $fee = M('fee');
        $fee_rs = $fee->field('str21')->find();
        $this->assign('fflv', $fee_rs['str21']);
        unset($fee, $fee_rs);
        $this->display('login');
    }

    public function index() {
        //如果通过认证跳转到首页
        //	redirect(__APP__);
    /*
        $cp = M('product');
        $fck = M('fck');
        $map['id'] = $_SESSION[C('USER_AUTH_KEY')];
        $f_rs = $fck->where($map)->find();

        $where = array();
        $ss_type = (int) $_REQUEST['tp'];
        if ($ss_type > 0) {
            $where['cptype'] = array('eq', $ss_type);
        }
        $this->assign('tp', $ss_type);

        $where['yc_cp'] = array('eq', 0);

        $cptype = M('cptype');
        $tplist = $cptype->where('status=0')->order('id asc')->select();
        $this->assign('tplist', $tplist);

        $order = 'id asc';
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $cp->where($where)->count(); //总页数
        $listrows = 20; //每页显示的记录数
        $page_where = 'tp=' . $ss_type; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $cp->where($where)->field($field)->order('id asc')->page($Page->getPage() . ',' . $listrows)->select();
        //=================================================
        foreach ($list as $voo) {
            $w_money = $voo['a_money'];
            $cc[$voo['id']] = $w_money;
        }
        $this->assign('cc', $cc);
        $this->assign('list', $list); //数据输出到模板

        $this->assign('f_rs', $f_rs);
//	    $this->display('Buycp');
    */

        $this->display('index');
    }

    public function goods_list() {
        $cp = M('product');
        $where = array();
        $limit =4;
        $ss_type = (int) $_REQUEST['category_id'];
        if ($ss_type > 0) {
            $where['cptype'] = array('eq', $ss_type);
            $limit =20;
        }
        $keywords = $_REQUEST['keywords'];
        
        $this->assign('tp', $ss_type);
        $where['yc_cp'] = array('eq', 0);
        $where['is_del'] = 0;
        $field = '*';
        if(!empty($keywords)){
            $where['name']=array('like',"%{$keywords}%");
        }
        $category = M('shop_category');
        $catwhere = array();
        if ($ss_type > 0) {
            $catwhere['id'] = array('eq', $ss_type);
            $limit =20;
        }
        $catlist = $category->where($catwhere)->select();
        //=====================分页开始==============================================
        
        import("@.ORG.ZQPage");  //导入分页类
        $count = $cp->where($where)->count(); //总页数
        $listrows = 20; //每页显示的记录数
        $page_where = 'tp=' . $ss_type; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        //$this->assign('page', $show); //分页变量输出到模板

       // $list = $cp->where($where)->field($field)->order('id asc')->page($Page->getPage() . ',' . $listrows)->select();
       
        $list = array();
        //=================================================
        foreach ($catlist as $cat) {
            $where['cptype']= array('eq', $cat['id']);
            $glist = $cp->where($where)->field($field)->order('id asc')->limit(0,$limit)->select();
            foreach ($glist as &$voo) {
                //   $w_money = $voo['a_money'];
                //   $cc[$voo['id']] = $w_money;
                $voo['category_id'] = $voo['cptype'];
                $voo['goods_name'] = $voo['name'];
                $voo['goods_pics'] = $voo['img'];
                $voo['add_time'] = $voo['create_time'];
                $voo['memo'] = $voo['content'];
                $voo['price'] = $voo['a_money'];
                $voo['goods_option'] = '[]';
                $voo['option_title'] = '[]';
            }
            $list[] = array('cat' => $cat,'list'=> $glist);
        }
        //    $this->assign('cc',$cc);
        //    $this->assign('list',$list);//数据输出到模板
        /**
          {
          "id": "164",
          "category_id": "18",
          "goods_name": "【M164】日照红茶（花开富贵） 一芽一叶茶农原地直发  精品瓷罐礼盒包装250g",
          "goods_pics": "/upload/goods/201712/b466ea6f3a6cb647985e8695a2ad59fd.jpg",
          "add_time": "1513588966",
          "edit_time": "1515047924",
          "sort": "127",
          "is_rec": "0",
          "memo": "<p></p>",
          "market_price": "290.00",
          "price": "208.00",
          "stock": "50",
          "status": "1",
          "goods_option": "[]",
          "option_title": "[]",
          "has_option": "0",
          "weight": "0.00",
          "goods_sn": "",
          "product_sn": "",
          "buy_num": "0",
          "is_bd": "0",
          "category_name": "年货推荐"
          }
         */
        show_json(40000, $list, '');
    }

    // 用户登出
    public function LogOut() {
        $_SESSION = array();
        //unset($_SESSION);
        $this->assign('jumpUrl', __URL__ . '/login/');
        $this->success('退出成功！');
    }

    // 登录检测
    public function checkLogin() {
        if (empty($_POST['account'])) {
            $this->error('请输入帐号！');
        } elseif (empty($_POST['password'])) {
            $this->error('请输入密码！');
        } elseif (empty($_POST['verify'])) {
            $this->error('请输入验证码！');
        }
        $fee = M('fee');
//		$sel = (int) $_POST['radio'];
//		if($sel <=0 or $sel >=3){
//			$this->error('非法操作！');
//			exit;
//		}
//		if($sel != 1){
//			$this->error('暂时不支持英文版登录！');
//			exit;
//		}
        //生成认证条件
        $map = array();
        // 支持使用绑定帐号登录
        $map['user_id'] = $_POST['account'];
//		$map['nickname'] = $_POST['account'];   //用户名也可以登录
//		$map['_logic']    = 'or';
        //$map['_complex']    = $where;
        //$map["status"]	=	array('gt',0);
        if ($_SESSION['verify'] != md5($_POST['verify'])) {
            $this->error('验证码错误！');
        }

        import('@.ORG.RBAC');
        $fck = M('fck');
        $field = 'id,user_id,password,is_pay,is_lock,nickname,user_name,is_agent,user_type,last_login_time,login_count,is_boss';
        $authInfo = $fck->where($map)->field($field)->find();
        //使用用户名、密码和状态的方式进行认证
        if (false == $authInfo) {
            $this->error('帐号不存在或已禁用！');
        } else {
            if ($authInfo['password'] != md5($_POST['password'])) {
                $this->error('密码错误！');
                exit;
            }

            if ($_POST['lang'] == 1) {
                $this->error('英文版本暂时无法登陆，请选择中文版本！');
                exit;
            }

            if ($authInfo['is_pay'] < 1) {
                $this->error('用户尚未开通，暂时不能登录系统！');
                exit;
            }
            if ($authInfo['is_lock'] != 0) {
                $this->error('用户已锁定，请与管理员联系！');
                exit;
            }
            $_SESSION[C('USER_AUTH_KEY')] = $authInfo['id'];
            $_SESSION['loginUseracc'] = $authInfo['user_id']; //用户名
            $_SESSION['loginNickName'] = $authInfo['nickname']; //会员名
            $_SESSION['loginUserName'] = $authInfo['user_name']; //开户名
            $_SESSION['lastLoginTime'] = $authInfo['last_login_time'];
            //$_SESSION['login_count']	    =	$authInfo['login_count'];
            $_SESSION['login_isAgent'] = $authInfo['is_agent']; //是否专卖店
            $_SESSION['UserMktimes'] = mktime();
            //身份确认 = 用户名+识别字符+密码
            $_SESSION['login_sf_list_u'] = md5($authInfo['user_id'] . 'wodetp_new_1012!@#' . $authInfo['password'] . $_SERVER['HTTP_USER_AGENT']);

            //登录状态
            $user_type = md5($_SERVER['HTTP_USER_AGENT'] . 'wtp' . rand(0, 999999));
            $_SESSION['login_user_type'] = $user_type;
            $where['id'] = $authInfo['id'];
            $fck->where($where)->setField('user_type', $user_type);
//			$fck->where($where)->setField('last_login_time',mktime());
            //管理员

            $parmd = $this->_cheakPrem();
            if ($authInfo['id'] == 1 || $parmd[11] == 1) {
                $_SESSION['administrator'] = 1;
            } else {
                $_SESSION['administrator'] = 2;
            }

//			//管理员
//			if($authInfo['is_boss'] == 1) {
//            	$_SESSION['administrator'] =	1;
//            }elseif($authInfo['is_boss'] == 2){
//            	$_SESSION['administrator'] = 3;
//            }elseif($authInfo['is_boss'] == 3){
//                $_SESSION['administrator']  = 4;
//            }elseif($authInfo['is_boss'] == 4){
//                $_SESSION['administrator'] = 5;
//            }elseif($authInfo['is_boss'] == 5){
//                $_SESSION['administrator'] =   6;
//            }elseif($authInfo['is_boss'] == 6){
//                $_SESSION['administrator'] =   7;
//            }else{
//				$_SESSION['administrator'] = 2;
//			}

            $fck->execute("update __TABLE__ set last_login_time=new_login_time,last_login_ip=new_login_ip,new_login_time=" . time() . ",new_login_ip='" . $_SERVER['REMOTE_ADDR'] . "' where id=" . $authInfo['id']);

            // 缓存访问权限
            RBAC::saveAccessList();
            $this->success('登录成功！');
        }
    }

    //二级密码验证
    public function cody() {
        $UrlID = (int) $_GET['c_id'];
        if (empty($UrlID)) {
            $this->error('二级密码错误!');
            exit;
        }
        if (!empty($_SESSION['user_pwd2'])) {
            $url = __URL__ . "/codys/Urlsz/$UrlID";
            $this->_boxx($url);
            exit;
        }
        $fck = M('cody');
        $list = $fck->where("c_id=$UrlID")->getField('c_id');
        if (!empty($list)) {
            $this->assign('vo', $list);
            $this->display('../Public/cody');
            exit;
        } else {
            $this->error('二级密码错误!');
            exit;
        }
    }

    //二级验证后调转页面
    public function codys() {
        $Urlsz = $_POST['Urlsz'];
        if (empty($_SESSION['user_pwd2'])) {
            $pass = $_POST['oldpassword'];
            $fck = M('fck');
            if (!$fck->autoCheckToken($_POST)) {
                $this->error('页面过期请刷新页面!');
                exit();
            }
            if (empty($pass)) {
                $this->error('二级密码错误!');
                exit();
            }

            $where = array();
            $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
            $where['passopen'] = md5($pass);
            $list = $fck->where($where)->field('id')->find();
            if ($list == false) {
                $this->error('二级密码错误!');
                exit();
            }
            $_SESSION['user_pwd2'] = 1;
        } else {
            $Urlsz = $_GET['Urlsz'];
        }
        switch ($Urlsz) {
            case 1:
                $_SESSION['DLTZURL02'] = 'updateUserInfo';
                $bUrl = __URL__ . '/updateUserInfo'; //修改资料
                $this->_boxx($bUrl);
                break;
            case 2:
                $_SESSION['DLTZURL01'] = 'password';
                $bUrl = __URL__ . '/password'; //修改密码
                $this->_boxx($bUrl);
                break;
            case 3:
                $_SESSION['DLTZURL01'] = 'pprofile';
                $bUrl = __URL__ . '/pprofile'; //修改密码
                $this->_boxx($bUrl);
                break;
            case 4:
                $_SESSION['DLTZURL01'] = 'OURNEWS';
                $bUrl = __URL__ . '/News'; //修改密码
                $this->_boxx($bUrl);
                break;
            case 5:
                $_SESSION['DLTZURL01'] = 'Manager';
                $bUrl = __URL__ . '/Manager'; //商品管理
                $this->_boxx($bUrl);
                break;
 
            default;
                $this->error('二级密码错误!');
                break;
        }
    }

    public function verify() {
        ob_clean();
        $type = isset($_GET['type']) ? $_GET['type'] : 'gif';
        import("@.ORG.Image");
        Image::buildImageVerify();
    }

    public function category(){
    
    } 

    function Manager() {
        $mode = M('shop_category');
        $map = array();
        $map['id'] = array('gt', 0);
        $orderBy = 'id asc';
        $field = '*';
        $goods_data = $mode->where($map)->field($field)->order($orderBy)->select();
        $tmp = tree($goods_data, 0, 0, '--');
        $select .= "<select name='cptype' id='cptype' class='form-control'>";
        $select .= "<option value='0' >请选择</option>";
        foreach ($tmp as $v) {
            $select .= '<option value=' . $v['id'] . '>' . $v['html'] . $v['name'] . '</option>';
        }
        $select .= '</select>';

        $this->assign('select', $select);
        $this->display('manager');
    }

    public function GoodsManager() {
        $cp = M('product');
        $where['id'] = array('gt', 0);
        if (!empty($_GET['params'])) {
            //  require 'communication.func.php';
            $key = '@fdskalhfj2387A!';
            $value = decrypt($_GET['params'], $key);
            if( $value['is_del'] == "0" || $value['is_del'] == "1"){
                $where['is_del'] = intval($value['is_del']);
            }
            if( $value['status'] =="0" || $value['status'] =="1"){
                $where['status'] = intval($value['status']);
            }
            if($value['stock']=="0" || $value['stock']=="1"){
                if($value['stock'] == 1){
                    $where['stock'] = array('gt', 0);
                }else{
                    $where['stock'] = 0;
                }
            }
        }
        
        $search = trim($value['search']);
        $cptype = $value['cptype'];
        if ($cptype) {
            $where['cptype'] = $cptype;
        }
        if ($search) {
            $where['name|cid|money'] = array('like', $search);
        }

        $ss_type = 1;
        $count = $cp->where($where)->count(); //总页数
        $listrows = 20; //每页显示的记录数
        $page_where = 'tp=' . $ss_type; //分页条件
        import("@.ORG.ZQPage");
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $field = '*';
        $this->assign('page', $show); //分页变量输出到模板
        $list = $cp->where($where)->field($field)->order('id asc')->page($Page->getPage() . ',' . $listrows)->select();
        foreach ($list as $key => $vo) {
            $list[$key]['create_time'] = date('Y-m-d H:i:s', $vo['create_time']);
            $list[$key]['cptype'] = get_cate_name($vo['cptype']);
            $list[$key]['is_reg'] = $vo['is_reg'] == 1 ? '是' : '否';
        }
        show_json(40000, $list, '');
    }

    //回收站
    function recycle() {
        $mode = M('shop_category');
        $map = array();
        $map['id'] = array('gt', 0);
        $orderBy = 'id asc';
        $field = '*';
        $goods_data = $mode->where($map)->field($field)->order($orderBy)->select();
        $tmp = tree($goods_data, 0, 0, '--');
        $select .= "<select name='cptype' id='cptype' class='form-control'>";
        $select .= "<option value='0' >请选择</option>";
        foreach ($tmp as $v) {
            $select .= '<option value=' . $v['id'] . '>' . $v['html'] . $v['name'] . '</option>';
        }
        $select .= '</select>';

        $this->assign('select', $select);
        $this->display();
    }

    function recycleJson() {
        $cp = M('product');
        if (!empty($_GET['params'])) {
            //  require 'communication.func.php';
            $key = '@fdskalhfj2387A!';
            $value = decrypt($_GET['params'], $key);
        }

        $where['id'] = array('gt', 0);
        $where['is_del'] = 1;
        $search = trim($value['search']);
        $cptype = $value['cptype'];
        if ($cptype) {
            $where['cptype'] = $cptype;
        }
        if ($search) {
            $where['name|cid|money'] = array('like', $search);
        }

        $ss_type = 1;
        $count = $cp->where($where)->count(); //总页数
        $listrows = 20; //每页显示的记录数
        $page_where = 'tp=' . $ss_type; //分页条件
        import("@.ORG.ZQPage");
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $field = '*';
        $this->assign('page', $show); //分页变量输出到模板
        $list = $cp->where($where)->field($field)->order('id asc')->page($Page->getPage() . ',' . $listrows)->select();
        foreach ($list as $key => $vo) {
            $list[$key]['create_time'] = date('Y-m-d H:i:s', $vo['create_time']);
            $list[$key]['cptype'] = get_cate_name($vo['cptype']);
            $list[$key]['is_reg'] = $vo['is_reg'] == 1 ? '是' : '否';
        }
        $this->show_json(40000, $list, '');
    }
   
    //还原操作
    function restore() {
        $reid = $_GET['reid'];
        $product = M('product');
        $data['id'] = $reid;
        $data['is_del'] = 0;
        $rs = $product->save($data);

        if (!$rs) {
            $this->error('还原失败！');
            exit;
        }
        $bUrl = U('Shop/recycle');
        $this->_box(1, '操作成功', $bUrl, 1);
        exit;
    }

    //参数设置
    public function setParameter(){
        //		if ($_SESSION['UrlPTPass'] == 'MyssPingGuoCP'){
        $fee = M ('fee');
        $fee_rs = $fee -> find();
        $fee_s1  = $fee_rs['s1'];
        $fee_s2  = $fee_rs['s2'];
        $fee_s3  = $fee_rs['s3'];
        $fee_s4  = $fee_rs['s4'];
        $fee_s5  = $fee_rs['s5'];
        $fee_s6  = $fee_rs['s6'];
        $fee_s7  = $fee_rs['s7'];
        $fee_s8  = $fee_rs['s8'];
        $fee_s9  = $fee_rs['s9'];
        $fee_s10 = $fee_rs['s10'];
        $fee_s11 = $fee_rs['s11'];
        $fee_s12 = $fee_rs['s12'];
        $fee_s13 = $fee_rs['s13'];
        $fee_s14 = $fee_rs['s14'];
        $fee_s15 = $fee_rs['s15'];
        $fee_s16 = $fee_rs['s16'];
        $fee_s17 = $fee_rs['s17'];
        $fee_s18 = $fee_rs['s18'];
        	
        $fee_str1 = $fee_rs['str1'];
        $fee_str2 = $fee_rs['str2'];
        $fee_str3 = $fee_rs['str3'];
        $fee_str4 = $fee_rs['str4'];
        $fee_str5 = $fee_rs['str5'];
        $fee_str6 = $fee_rs['str6'];
        $fee_str7 = $fee_rs['str7'];
        $fee_str9 = $fee_rs['str9'];
        	
        $fee_str10 = $fee_rs['str10'];
        $fee_str11 = $fee_rs['str11'];
        	
        $fee_str17 = $fee_rs['str17'];
        $fee_str18 = $fee_rs['str18'];
    
        $fee_str21 = $fee_rs['str21'];
        $fee_str22 = $fee_rs['str22'];
        $fee_str23 = $fee_rs['str23'];
        $fee_str24 = $fee_rs['str24'];
        $fee_str25 = $fee_rs['str25'];
    
        $fee_str27 = $fee_rs['str27'];
        $fee_str28 = $fee_rs['str28'];
        $fee_str29 = $fee_rs['str29'];
    
        $fee_str99 = $fee_rs['str99'];
        	
        $a_money = $fee_rs['a_money'];
        $b_money = $fee_rs['b_money'];
    
        //			$fee_s20 = explode('|',$fee_rs['s20']);
        $this -> assign('fee_s1',$fee_s1);
        $this -> assign('fee_s2',$fee_s2);
        $this -> assign('fee_s3',$fee_s3);
        $this -> assign('fee_s4',$fee_s4);
        $this -> assign('fee_s5',$fee_s5);
        $this -> assign('fee_s6',$fee_s6);
        $this -> assign('fee_s7',$fee_s7);
        $this -> assign('fee_s8',$fee_s8);
        $this -> assign('fee_s9',$fee_s9);
        $this -> assign('fee_s10',$fee_s10);
        $this -> assign('fee_s11',$fee_s11);
        $this -> assign('fee_s12',$fee_s12);
        $this -> assign('fee_s13',$fee_s13);
        $this -> assign('fee_s14',$fee_s14);
        $this -> assign('fee_s15',$fee_s15);
        $this -> assign('fee_s16',$fee_s16);
        $this -> assign('fee_s17',$fee_s17);
        $this -> assign('fee_s18',$fee_s18);
        //			$this -> assign('fee_s20',$fee_s20);
        $this -> assign('fee_i1',$fee_rs['i1']);
        $this -> assign('fee_i2',$fee_rs['i2']);
        $this -> assign('fee_i3',$fee_rs['i3']);
        $this -> assign('fee_i4',$fee_rs['i4']);
        $this -> assign('fee_id',$fee_rs['id']);  //记录ID
        	
        $this -> assign('b_money',$fee_rs['b_money']);
        	
        $this -> assign('fee_str1',$fee_str1);
        $this -> assign('fee_str2',$fee_str2);
        $this -> assign('fee_str3',$fee_str3);
        $this -> assign('fee_str4',$fee_str4);
        $this -> assign('fee_str5',$fee_str5);
        $this -> assign('fee_str6',$fee_str6);
        $this -> assign('fee_str7',$fee_str7);
        $this -> assign('fee_str9',$fee_str9);
        	
        $this -> assign('fee_str10',$fee_str10);
        $this -> assign('fee_str11',$fee_str11);
        	
        $this -> assign('fee_str17',$fee_str17);
        $this -> assign('fee_str18',$fee_str18);
    
        $this -> assign('fee_str21',$fee_str21);
        $this -> assign('fee_str22',$fee_str22);
        $this -> assign('fee_str23',$fee_str23);
        $this -> assign('fee_str24',$fee_str24);
        $this -> assign('fee_str25',$fee_str25);
    
        $this -> assign('fee_str27',$fee_str27);
        $this -> assign('fee_str28',$fee_str28);
        $this -> assign('fee_str29',$fee_str29);
        $this -> assign('fee_str99',$fee_str99);
        	
        $this -> assign('a_money',$a_money);
        $this -> assign('b_money',$b_money);
    
        $this->display('setParameter');
        //		}else{
        //			$this->error('错误!');
        //			exit;
        //		}
    }
}

?>