<?php

//注册模块
class AgentAction extends CommonAction
{

    public function _initialize()
    {
        header("Content-Type:text/html; charset=utf-8");
        $this->_inject_check(0); //调用过滤函数
        $this->_Config_name(); //调用参数
        $this->_checkUser();
        $fck_rs = $this->getUserInfo();
        $this->assign('fck_rs', $fck_rs);
    }

    public function cody()
    {
        //===================================二级验证
        $UrlID = (int)$_GET['c_id'];
        if (empty($UrlID)) {
            $this->error('二级密码错误!');
            exit;
        }
        if (!empty($_SESSION['user_pwd2'])) {
            $url = __URL__ . "/codys/Urlsz/$UrlID";
            $this->_boxx($url);
            exit;
        }
        $cody = M('cody');
        $list = $cody->where("c_id=$UrlID")->field('c_id')->find();
        if ($list) {
            $this->assign('vo', $list);
            $this->display('../Public/cody');
            exit;
        } else {
            $this->error('二级密码错误!');
            exit;
        }
    }

    public function codys()
    {
        //=============================二级验证后调转页面
        $Urlsz = (int)$_POST['Urlsz'];
        if (empty($_SESSION['user_pwd2'])) {
            $pass = $_POST['oldpassword'];
            $fck = M('fck');
            if (!$fck->autoCheckToken($_POST)) {
                $this->error('页面过期请刷新页面!');
                exit();
            }
            if (empty($pass)) {
                $this->error('二级密码错误!');
                exit();
            }
            $where = array();
            $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
            $where['passopen'] = md5($pass);
            $list = $fck->where($where)->field('id,is_agent')->find();
            if ($list == false) {
                $this->error('二级密码错误!');
                exit();
            }
            $_SESSION['user_pwd2'] = 1;
        } else {
            $Urlsz = $_GET['Urlsz'];
        }
        switch ($Urlsz) {
            case 1;
                if ($list['is_agent'] >= 2) {
                    $this->error('您已经是专卖店!');
                    exit();
                }
                $_SESSION['Urlszpass'] = 'MyssXiGua';
                $bUrl = __URL__ . '/agents'; //申请代理
                $this->_boxx($bUrl);
                break;
            case 2;
                $_SESSION['Urlszpass'] = 'MyssShuiPuTao';
                $bUrl = __URL__ . '/menber'; //未开通会员
                $this->_boxx($bUrl);
                break;
            case 3;
                $_SESSION['Urlszpass'] = 'Myssmenberok';
                $bUrl = __URL__ . '/menberok'; //已开通会员
                $this->_boxx($bUrl);
                break;
            case 4;
                $_SESSION['UrlPTPass'] = 'MyssGuanXiGua';
                $bUrl = __URL__ . '/adminAgents'; //后台确认专卖店
                $this->_boxx($bUrl);
                break;
            case 5;
                $_SESSION['UrlPTPass'] = 'MyssXiGua';
                $bUrl = __URL__ . '/myagents';
                $this->_boxx($bUrl);
                break;
            case 6;
                $_SESSION['UrlPTPass'] = 'MyssGuanXiGua';
                $bUrl = __URL__ . '/adminAgentsOk'; //后台确认专卖店
                $this->_boxx($bUrl);
                break;
            case 7;
                $_SESSION['UrlPTPass'] = 'MyssGuanXiGua';
                $bUrl = __URL__ . '/adminApplygouwu'; //再次进货管理
                $this->_boxx($bUrl);
                break;
            case 8;
                $_SESSION['UrlPTPass'] = 'MyssGuanXiGua';
                $bUrl = __URL__ . '/applygouwulist'; //再次进货
                $this->_boxx($bUrl);
                break;
            case 9;
                $_SESSION['UrlPTPass'] = 'MyssGuanXiGua';
                $bUrl = __URL__ . '/applygouwu'; //再次进货
                $this->_boxx($bUrl);
                break;
            default;
                $this->error('二级密码错误!');
                exit;
        }
    }

    public function agents($Urlsz = 0)
    {
        //======================================申请会员中心/代理中心/专卖店
        if ($_SESSION['Urlszpass'] == 'MyssXiGua') {
            $fee_rs = M('fee')->find();
            $fck = M('fck');
            $where = array();
            //查询条件
            $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
            $field = '*';
            $fck_rs = $fck->where($where)->field($field)->find();
            if ($fck_rs) {
                //会员级别
                if ($fck_rs['is_agent'] == 0) {
                    $agent_status = '未申请专卖店!';
                } else {
                    if ($fck_rs['new_agent'] == 1) {
                        $agent_status = '申请正在审核中!';
                    } else {
                        $agent_status = '专卖店已开通!';
                    }
                }
                $this->assign('fee_s6', $fee_rs['i1']);
                $this->assign('agent_level', 0);
                $this->assign('agent_status', $agent_status);
                $this->assign('fck_rs', $fck_rs);
                $map['area_parent_id'] = 0;
                $provinceList = $this->getList($map);
                $this->assign("provinceList", $provinceList);
                $fee = M('fee');
                $fee_rs = $fee->find();
                $Agent_Us_Name = C('Agent_Us_Name');
                $Aname = explode("|", $fee_rs['s19']);
                $this->assign('Aname', $Aname);
                $this->display('agents');
            } else {
                $this->error('操作失败!');
                exit;
            }
        } else {
            $this->error('错误!');
            exit;
        }
    }

    /**
     * 申请专卖店提交处理
     */
    public function agentsAC()
    {
        //================================申请会员中心中转函数
        $content = $_POST['content'];
        $agentMax = $_POST['agentMax'];
        $shoplx = (int)$_POST['shoplx'];
        $shop_a = $_POST['shop_a'];
        $shop_b = $_POST['shop_b'];
        $shop_c = $_POST['shop_c'];
        $shop_d = trim($_POST['shop_d']);
        
        if ($shoplx == 1) {
            if (!$shop_a || !$shop_b || !$shop_c || !$shop_d) {
                $this->error('区域信息填写完整');
            }
        }
        if ($shoplx == 2) {
            if (!$shop_a || !$shop_b || !$shop_c) {
                $this->error('请选择区');
            }
        }
        if ($shoplx == 3) {
            if (!$shop_a || !$shop_b) {
                $this->error('请选择地级市');
            }
        }
        if ($shoplx == 4) {
            if (!$shop_a) {
                $this->error('请选择省');
            }
        }
        $fee = M('fee');
        $fee_rs = $fee->where('s9,s14')->find(1);
        $s14 = (int)$fee_rs['s14'];
        $s9 = explode("|", $fee_rs['s9']);  //会员级别费用
        $fck = M('fck');
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $where = array();
        $where['id'] = $id;
        $fck_rs = $fck->where($where)->field('*')->find();

        if ($fck_rs) {
            if ($fck_rs['is_pay'] == 0) {
                $this->error('临时会员不能申请!');
                exit;
            }
            if ($fck_rs['new_agent'] == 1) {
                $this->error('上次申请还没通过审核!');
                exit;
            }
            if (empty($content)) {
                $this->error('请输入备注!');
                exit;
            }
            $map['shoplx'] = $shoplx;
            $map['shop_a'] = $shop_a;
            $shop = $fck->where($map)->find();
            $map1['shoplx'] = $shoplx;
            $map1['shop_a'] = $shop_a;
            $map1['shop_b'] = $shop_b;
            $shop1 = $fck->where($map1)->find();
            $map2['shoplx'] = $shoplx;
            $map2['shop_a'] = $shop_a;
            $map2['shop_b'] = $shop_b;
            $map2['shop_c'] = $shop_c;
            $shop2 = $fck->where($map2)->find();
            if ($shoplx == 4 && $shop) {
                $this->error('该区域已有代理!');
            }
            if ($shoplx == 3 && $shop1) {
                $this->error('该区域已有代理!');
            }
            if ($shoplx == 2 && $shop2) {
                $this->error('该区域已有代理!');
            }
            if ($fck_rs['is_agent'] == 0) {
                $nowdate = time();
                $result = $fck->query("update __TABLE__ set verify='" . $content . "',is_agent=1,new_agent=1,shoplx=" . $shoplx . ",shop_a='" . $shop_a . "',shop_b='" . $shop_b . "',shop_c='" . $shop_c . "',shop_d='" . $shop_d . "',idt=$nowdate where id=" . $id);
            }
            $bUrl = __URL__ . '/agents';
            $this->_box(1, '申请成功！', $bUrl, 2);
        } else {
            $this->error('非法操作');
            exit;
        }
    }

    //未开通会员
    public function menber($Urlsz = 0)
    {
        //列表过滤器，生成查询Map对象
        if ($_SESSION['Urlszpass'] == 'MyssShuiPuTao') {

            $this->assign('isagent', $_SESSION['login_isAgent']);

            $this->assign('reg_id', $_SESSION['new_user_reg_id']);
            $this->display('menber');
            exit;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    /*
     * 管理员或者专卖店获取未激活会员列表
     * 管理员获取所有的
     * 专卖店获取自己的
     * 返回json数据
     */
    public function memberlist()
    {
        $fck = M('fck');
        $map = array();
        $id = $_SESSION[C('USER_AUTH_KEY')];

        $params = $this->getParams();
        $qvalue = trim($params['qvalue']);
        if (!empty($qvalue)) {
            $where['nickname'] = array('like', "%" . $qvalue . "%");
            $where['user_id'] = array('like', "%" . $qvalue . "%");
            $where['_logic'] = 'or';
            $map['_complex'] = $where;
        }
        if (!empty($params['s_time']) && empty($params['e_time'])) {
            $map['rdt'] = array('between', array(strtotime($params['s_time']), time()));
        }
        if (!empty($params['s_time']) && !empty($params['e_time'])) {
            $map['rdt'] = array('between', array(strtotime($params['s_time']), strtotime($params['e_time'])));
        }
        if ($id == 1) {
            $map['is_pay'] = array('eq', 0);
        } else {
            $map['is_pay'] = array('eq', 0);
            $map['_string'] = "shop_id=" . $id . " or re_id=" . $id . "";
        }
        //查询字段
        $field = '*';
        //=====================分页开始===========================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $fck->where($map)->count(); //总页数
        $listrows = $params['limit']; //每页显示的记录数
        $nowPage = $params['offset'] / $params['limit'] + 1;
        $page_where = ''; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $fck->where($map)->field($field)->order($params['sort'] . ' ' . $params['order'])->page($Page->getPage() . ',' . $listrows)->select();
        show_list_json(40000, $list, $count, $nowPage);
    }

    //已开通会员
    public function menberok($Urlsz = 0)
    {
        //列表过滤器，生成查询Map对象
        if ($_SESSION['Urlszpass'] == 'Myssmenberok') {
            $this->display('menberok');
            exit;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    /*     * *
     * 管理员或者专卖店获取已激活会员列表
     * 管理员获取所有的
     * 专卖店获取自己的
     * 返回json数据
     */
    public function memberoklist()
    {

        $fck = M('fck');
        $map = array();
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $params = $this->getParams();
        $gid = (int)$_GET['bj_id'];
        $qvalue = trim($params['qvalue']);
        if ($id == 1) {
            $map['is_pay'] = array('eq', 1);
        } else {
            $map['is_pay'] = array('eq', 1);
            $map['_string'] = "shop_id=" . $id . " or re_id=" . $id . "";
        }

        if (!empty($qvalue)) {
//            import("@.ORG.KuoZhan");  //导入扩展类
//            $KuoZhan = new KuoZhan();
//            if ($KuoZhan->is_utf8($UserID) == false) {
//                $UserID = iconv('GB2312', 'UTF-8', $UserID);
//            }
//            unset($KuoZhan);
            $where['nickname'] = array('like', "%" . $qvalue . "%");
            $where['user_id'] = array('like', "%" . $qvalue . "%");
            $where['_logic'] = 'or';
            $map['_complex'] = $where;
        }
        if (!empty($params['s_time']) && empty($params['e_time'])) {
            $map['rdt'] = array('between', array(strtotime($params['s_time']), time()));
        }
        if (!empty($params['s_time']) && !empty($params['e_time'])) {
            $map['rdt'] = array('between', array(strtotime($params['s_time']), strtotime($params['e_time'])));
        }
        //查询字段
        $field = '*';
        //=====================分页开始===========================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $fck->where($map)->count(); //总页数
        $listrows = $params['limit']; //每页显示的记录数
        $nowPage = $params['offset'] / $params['limit'] + 1;
        $page_where = ''; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $fck->where($map)->field($field)->order($params['sort'] . ' ' . $params['order'])->page($Page->getPage() . ',' . $listrows)->select();

        if ($list == null)
            $list = array();
        show_list_json(40000, $list, $count, $nowPage);
    }

    public function myagents()
    {
        if ($_SESSION['UrlPTPass'] == 'MyssXiGua') {
        	
        	$fee = M('fee');
        	$fee_rs = $fee->find();
        	$fee_s19 = $fee_rs['s19'];
        	$fee_s19 = $fee_rs['s20'];
        	
        	$fee_shj2 = explode('|', $fee_rs['s19']);
        	$fee_shj3 = explode('|', $fee_rs['s20']);
        	
        	$fee_shj = array_merge($fee_shj2,$fee_shj3);
        	$str_shj = json_encode($fee_shj);
        	
        	$this->assign('sjlist', $str_shj);
        	
            $this->assign('isagent', $_SESSION['login_isAgent']);
            $this->display('myagents');
            exit;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    public function myagentslist()
    {
        $fck = M('fck');
        $map = array();
        $id = $_SESSION[C('USER_AUTH_KEY')];

        $params = $this->getParams();
        $gid = (int)$params['bj_id'];
        $UserID = $params['UserID'];
        if (!empty($UserID)) {
            import("@.ORG.KuoZhan");  //导入扩展类
            $KuoZhan = new KuoZhan();
            if ($KuoZhan->is_utf8($UserID) == false) {
                $UserID = iconv('GB2312', 'UTF-8', $UserID);
            }
            unset($KuoZhan);
            $where['nickname'] = array('like', "%" . $UserID . "%");
            $where['user_id'] = array('like', "%" . $UserID . "%");
            $where['_logic'] = 'or';
            $map['_complex'] = $where;
            $UserID = urlencode($UserID);
        }
        //  $map['is_pay'] = array('eq', 0);
        $map['_string'] = "shop_id=" . $id . " or re_id=" . $id . "";

        //查询字段
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $fck->where($map)->count(); //总页数
        $listrows = $params['limit']; //每页显示的记录数
        $nowPage = $params['offset'] / $params['limit'] + 1;
        $page_where = 'UserID=' . $UserID; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $fck->where($map)->field($field)->order($params['sort'] . ' ' . $params['order'])->page($Page->getPage() . ',' . $listrows)->select();

        show_list_json(40000, $list, $count, $nowPage);
    }

    public function haveBounds($user_id, $type)
    {
        $map['user_id'] = $user_id;
        $map['action_type'] = $type;
        $h = M('history')->where($map)->find();
        if ($h) {
            return $h;
        } else {
            return false;
        }
    }

    /*
     * 删除会员申请及注册信息
     *
     */
    function delApply()
    {
    	if($_SESSION['administrator']!=1){
    		$this->ajaxReturn('', '无权审核',0);
    	}
    	
        $uid = $_POST['uid'];
        $fck = M('fck');
        $map['id'] = $uid;
        $user_data = $fck->where($map)->find();
        if ($user_data['is_pay'] == 1) {
            $msg = '该会员已通过！';
            $code = 0;
        }
        $result = $fck->where($map)->delete();
        if ($result) {
            $msg = '删除成功！';
            $code = 40000;
        } else {
            $msg = '删除失败！';
            $code = 0;
        }
        $this->ajaxReturn($data, $msg, $code);
    }
    
	public function getLastLeftPath($uid){
		$fck = M('fck');
		$left_path = $fck->query("select id,left_path from xt_fck where re_id = {$uid} and length(left_path)>0 order by length(left_path) desc limit 1");
		if(empty($left_path)){
			$left_path = $fck->query("select id,left_path from xt_fck where id = {$uid} and length(left_path)>0 order by length(left_path) desc limit 1");
		}
		if(!empty($left_path) && count($left_path)>0)
			return $left_path[0]['left_path'].','.$left_path[0]['id'];
		else 
			return $uid;
	}

    /**
     * 审核开通会员
     */
    function toExamine()
    {
    	if($_SESSION['administrator']!=1){
    		$this->ajaxReturn('', '无权审核',0);
    	}
        $uid = $_POST['uid'];
        $fck = M('fck');
        
        $sav = array();
        $user_data = $fck->where(array('id' => $uid))->find();
        if($user_data['cpzj'] == 2000){
        	$sav['u_level'] = 1;
        }
        if($user_data['cpzj'] == 20000){
        	$sav['u_level'] = 2;
        }
        $income = M('income');
        $ind = $income->where(array('id'=>$user_data['f4']))->find();
        
        if ($user_data['cpzj'] == 50000) {
            $sav['is_shoper'] = 1;
            $sav['is_agent'] = 2;
            $sav['u_level'] = 3;
            $user_data['cpzj'] = 20000;
            $ind['pv'] = $user_data['cpzj'];
        }
        if ($user_data['is_pay'] == 1) {
            $msg = '该会员已通过！';
            $code = 0;
            $this->ajaxReturn('', $msg, $code);
            return;
        }
        $fee = M('fee');
        $fee_rs = $fee->find();
        $fee_shj3 = explode('|', $fee_rs['s20']);
        $fee_name = explode('|', $fee_rs['prizename']);

        $re_user = $fck->where(array('id' => $user_data['re_id']))->find();
        
        //left_path 接点路径
        $left_path_str = $this->getLastLeftPath($user_data['re_id']);//$re_user['down_path'];
        
        $left_path_str = ltrim(rtrim($left_path_str,','),',');
        
        //re_pth   推荐路径
        $re_path = empty($re_user['re_path'])?$user_data['re_id']:$re_user['re_path'].','.$user_data['re_id'];
        $re_path = rtrim(ltrim($re_path,','),',');
        $fck->where(array('id' => $user_data['id']))->save(array('left_path' => $left_path_str,'re_path'=>$re_path));

        //down_path本级所有下级
        /*
        $down_path_str = $re_user['down_path'];
        $down_path_arr = explode(',', $down_path_str);
        array_push($down_path_arr, $user_data['id']);
        $new_down_path_str = implode(',', $down_path_arr);
        $new_down_path_str = trim($new_down_path_str, ',');
        $fck->where(array('id' => $re_user['id']))->save(array('down_path' => $new_down_path_str));
*/
        //级差奖
        $this->jicha($user_data['id']);

        //拓展奖
        $this->tuozhan($user_data['id']);


        //增加业绩升级
        $this->add_achievement($user_data['id']);

        //店补
        $ID = $user_data['shoper_id'];
        $us = $fck->where('id=' . $ID)->find();
        $fee_db = explode('|', $fee_rs['prize7']);
        $fee_shj = explode('|', $fee_rs['s20']);
        $b5 = $fck->where('id=' . $ID)->find();
        $bx['id'] = $ID;
        $bx['b5'] = $b5['b5'] + $user_data['cpzj'] * $fee_db[0] / 100;
        $fck->save($bx);
        $this->add_history($ID, time(), $fee_name[6], $user_data['cpzj'] * $fee_db[0] / 100, $uid);

        M('leader_achievement')->add(array('uid' => $user_data['id'], 'achievement' => $user_data['cpzj'], 'createtime' => time()));

        $prize1 = explode('|', $fee_rs['prize1']);
        
        $sav['id'] = $uid;
        $sav['is_pay'] = 1;
        $sav[lssq] = $prize1[$sav['u_level']-1];
        
        $result = $fck->save($sav);
        
        
        $ind['status'] =1;
        $income->save($ind);
        
        if ($result) {
            $msg = '审核成功！';
            $code = 40000;
        } else {
            $msg = '审核失败!';
            $code = 0;
        }
        $this->ajaxReturn('', $msg, $code);
    }

    
    
   
    public function add_achievement($uid)
    {
    	$fee = M('fee');
    	$fee_rs = $fee->find();
    	$fee_s10  = explode('|', $fee_rs['s10']);
    	$prize1 = explode('|', $fee_rs['prize1']);
    	
    	    	
        $fck = M('FckModel:Fck');
        $user_data = $fck->where(array('id' => $uid))->find();
        if ($user_data['cpzj'] == 50000) {
            $user_data['cpzj'] = 20000;
        }
        /*
        $line_arr = $this->path_line($uid);
        
        $line_str = implode(',', $line_arr);
        $line_arr = array_reverse($line_arr);
        */
        $line_str = $user_data['left_path'];
        //增加用户升级业绩
        $fck->query('update xt_fck set achievement = achievement + ' . $user_data['cpzj'] . ' where id in (' . $line_str . ') and cpzj>=20000 and id !=' . $uid);

        //升级用户等级
        $rise_user = $fck->query('select id,achievement from xt_fck  where id in (' . $line_str . ') and cpzj>=20000 and id !=' . $uid);
        foreach ($rise_user as $ru) {
        	//lssq 级差奖
        	
        	if ($ru['achievement'] >= $fee_s10[0] && $ru['achievement'] < $fee_s10[1] && $prize1['3']) {
        		$fck->query('update xt_fck set level = "区级代理商",u_level=4,lssq='.$prize1['3'].' where id = ' . $ru['id']);
            }
            if ($ru['achievement'] >= $fee_s10[1] && $ru['achievement'] < $fee_s10[2] && $prize1['4']) {
                $fck->query('update xt_fck set level = "市级代理商",u_level=5,lssq='.$prize1['4'].',is_hege = 0,sj_time = ' . time() . ' where id = ' . $ru['id'] . ' and level !="市级代理商"');
            }
            if ($ru['achievement'] >= $fee_s10[2] && $ru['achievement'] < $fee_s10[3] && $prize1['5']) {
            	$fck->query('update xt_fck set more_level = "省级代理" ,u_level=6 ,lssq='.$prize1['5'].'  where id = ' . $ru['id']);
            }
            if ($ru['achievement'] >= $fee_s10[3] && $prize1['6']) {
                $fck->query('update xt_fck set more_level = "全国代理",u_level=7 ,lssq='.$prize1['6'].',b10= where id = ' . $ru['id']);
            }
        }

        //增加用户市代审核业绩
        //根据推荐路径
        //领导奖只计算一级业绩
        $user = $user_data;
        $jia = M('jia');
        $leader_month = M('leader_month');
        $month = date('Y-m');
   
        
        while ($user['re_id']) {
        	
            $re_user = $fck->where(array('id' => $user['re_id']))->find();            
         
                //if上级升级为市代中断
            if ($re_user['u_level']>=5 ) {
            	$leader_month ->add(array('user_id' =>$re_user['id'] ,'user_no'=>$re_user['user_id'],'month'=>$month,'archieve'=>$user_data['cpzj'],'source_id'=>$user_data['re_id']));
                	$res = $fck->updateHege($re_user['id']);
                    break;
                }
                
            $user = $re_user;
        }

        return true;
    }

    

    public function path_line($uid)
    {
        $fck = M('fck');
        $user = $fck->where(array('id' => $uid))->find();
        /*
        $re_user = $fck->where(array('id' => $user['re_id']))->find();
        $str = ',' . $user['id'];
        if (!empty($user['left_path'])) {
            $str = ',' . $user['left_path'] . $str;
        }
        while ($re_user) {
            $str = ',' . $re_user['id'] . $str;
            if (!empty($re_user['left_path'])) {
                $str = ',' . $re_user['left_path'] . $str;
            }
            $re_user = $fck->where(array('id' => $re_user['re_id']))->find();
        }
        $array = explode(',', trim($str, ','));
        */
        
        $re_user = $fck->where(array('id' => $user['re_id']))->find();
        
        return $array;
    }

    public function jicha($uid)
    {
        $fee = M('fee');
        $fee_rs = $fee->find();
        $fee_name = explode('|', $fee_rs['prizename']);
        $fee_s10  = explode('|', $fee_rs['s10']);
        
        $fck = M('fck');
        $user = $fck->where(array('id' => $uid))->find();
        if ($user['cpzj'] == 50000) {
            $cpzj = 20000;
        } else {
            $cpzj = $user['cpzj'];
        }
        
        $re_id = $user['re_id'];
        if ($user['is_pay'] != 0) {
            $user_jicha = $this->level($user['level']);
        } else {
            $user_jicha = 0;
        }
        
        while ($user['re_id']) {
//            echo $user['re_id'];
            $re_user = $fck->where(array('id' => $user['re_id']))->find();
            /*
            if (empty($re_user['more_level'])) {
                $jicha = $this->level($re_user['level']);
            } else {
                $jicha = $this->level($re_user['more_level']);
            }*/
            $jicha = $re_user['lssq'];
            
            if (($jicha - $user_jicha) > 0) {
                $before_rale = '';
                $before_money = '';
                $after_rale = '';
                $after_money = '';
                
                $achievement = $re_user['achievement'];
                if($achievement >= $fee_s10[3]){
                	
                }
                if ($achievement < $fee_s10[3] && ($achievement + $cpzj) >= $fee_s10[3]) {
                    $before_rale = 11;
                    $before_money = $fee_s10[3] - $achievement;
                    $after_rale = 13;
                    $after_money = $cpzj - ($fee_s10[3] - $achievement);
                }
                if ($achievement < $fee_s10[2] && ($achievement + $cpzj) >= $fee_s10[2] && ($achievement + $cpzj) < $fee_s10[3]) {
                    $before_rale = 9;
                    $before_money = $fee_s10[2] - $achievement;
                    $after_rale = 11;
                    $after_money = $cpzj - ($fee_s10[2] - $achievement);
                }
                if ($achievement < $fee_s10[1] && ($achievement + $cpzj) >= $fee_s10[1] && ($achievement + $cpzj) < $fee_s10[2]) {
                    $before_rale = 7;
                    $before_money = $fee_s10[1] - $achievement;
                    $after_rale = 9;
                    $after_money = $cpzj - ($fee_s10[1] - $achievement);
                }
                if ($achievement < $fee_s10[0] && ($achievement + $cpzj) >= $fee_s10[0] && ($achievement + $cpzj) < $fee_s10[1]) {
                    $before_rale = 4;
                    $before_money = $fee_s10[0] - $achievement;
                    $after_rale = 7;
                    $after_money = $cpzj - ($fee_s10[0] - $achievement);
                }
                if ($before_money > 0 && $after_money > 0) {
                    $bonus = $before_money * ($before_rale - $user_jicha) / 100 + $after_money * ($after_rale - $user_jicha) / 100;
                    $fck->where(array('id' => $re_user['id']))->save(array('b2' => $re_user['b2'] + $bonus));
                    $this->add_history($re_user['id'], time(), $fee_name[0], $bonus, $user['id']);
                    //管理奖
                    $this->guanli($re_user['id'], $bonus);
                    //合作奖
                    $this->hezuo($re_user['id'], $bonus);
                    $user_jicha = $after_rale;
                } else {
                    $fck->where(array('id' => $re_user['id']))->save(array('b2' => $re_user['b2'] + ($cpzj * ($jicha - $user_jicha) / 100)));
                    $this->add_history($re_user['id'], time(), $fee_name[0], $cpzj * ($jicha - $user_jicha) / 100, $user['id']);
                    //管理奖
                    $this->guanli($re_user['id'], $cpzj * ($jicha - $user_jicha) / 100);
                    //合作奖
                    $this->hezuo($re_user['id'], $cpzj * ($jicha - $user_jicha) / 100);
                    $user_jicha = $jicha;
                }
            }
            if ($user_jicha == 13) {
                break;
            }
            $user = $re_user;
        }
        return true;
    }

    public function guanli($uid, $bonus)
    {
        $fee = M('fee');
        $fee_rs = $fee->find();
        $fee_name = explode('|', $fee_rs['prizename']);
        $fck = M('fck');
        $user = $fck->where(array('id' => $uid))->find();
        for ($i = 1; $i <= 10; $i++) {
            if (empty($user['re_id'])) {
                break;
            }
            $re_user = $fck->where(array('id' => $user['re_id']))->find();
            $persent = 1 / (pow(2, $i));
            $fck->where(array('id' => $re_user['id']))->save(array('b4' => $re_user['b4'] + ($bonus * $persent)));
            $this->add_history($re_user['id'], time(), $fee_name[2], $bonus * $persent, $user['id']);
            $user = $re_user;
        }
        return true;
    }

    public function tuozhan($uid)
    {
        $fee = M('fee');
        $fee_rs = $fee->find();
        $fee_name = explode('|', $fee_rs['prizename']);

        $fck = M('fck');
        $user = $fck->where(array('id' => $uid))->find();
        if ($user['cpzj'] == 50000) {
            $user['cpzj'] = 20000;
        }
        $re_user = $fck->where(array('id' => $user['re_id']))->find();
        $fck->where(array('id' => $re_user['id']))->save(array('b1' => $re_user['b1'] + $user['cpzj'] * 15 / 100));
        $this->add_history($re_user['id'], time(), $fee_name[1], $user['cpzj'] * 15 / 100, $user['id']);
    }

    public function hezuo($uid, $bonus)
    {
        $fee = M('fee');
        $fee_rs = $fee->find();
        $fee_name = explode('|', $fee_rs['prizename']);
        $fee_tz = explode('|', $fee_rs['prize4']);
        
        $fck = M('fck');
        /*
        $re_user = $fck->where(array('id' => $uid))->find();
        $down_path_arr = explode(',', $re_user['down_path']);
        $down_path_arr = array_reverse($down_path_arr);
        */
        
        $down_path_arr = $this->getLastHezuoUser($uid);
        
        $b1 = number_format($bonus * floatval($fee_tz[0]) / 100.0,2);
        $b2 = number_format($bonus * floatval($fee_tz[1]) / 100.0,2);
        $b3 = number_format($bonus * floatval($fee_tz[2]) / 100.0,2);
        
        $p1 = $down_path_arr[0];
        $p2 = $down_path_arr[1];
        $p3 = $down_path_arr[2];
        
        if(!empty($p1)){
        	$fck->where(array('id' =>$p1))->setInc('b3', $b1);        
        	$this->add_history($p1, time(), $fee_name[3], $b1, $uid);
        }
        
        if(!empty($p2)){
        	$fck->where(array('id' =>$p2))->setInc('b3', $b2);
        	$this->add_history($p2, time(), $fee_name[3], $b2, $uid);
        }
        
		if(!empty($p3)){
			$fck->where(array('id' => $p3))->setInc('b3', $b3);
			$this->add_history($p3, time(), $fee_name[3], $b3, $uid);
		}
    }

    public function getLastHezuoUser($uid){
    	$fck = M('fck');
    	$left_path = $fck->query("select id from xt_fck where re_id = {$uid} and length(left_path)>0 order by length(left_path) desc limit 3");
    	
    	$arr = array();
    	foreach ($left_path as $var){
    		$arr[] = $var['id'];
    	}
    	return  $arr;    
    }
    
//    public function lead_bonus($uid)
//    {
//        $fee = M('fee');
//        $fee_rs = $fee->find();
//        $fee_name = explode('|', $fee_rs['prizename']);
//
//        $fck = M('fck');
//        $user_data = $fck->where(array('id' => $uid))->find();
//        if ($user_data['cpzj'] == 50000) {
//            $cpzj = 20000;
//        } else {
//            $cpzj = $user_data['cpzj'];
//        }
//        if (empty($user_data['re_id'])) {
//            return true;
//        }
//        $user_info = $fck->where(array('id' => $user_data['re_id']))->find();
//
//        $n = 1;
//        while ($user_info && $n <= 9) {
//            if ($user_info['level'] == '市级代理商' && $user_info['is_hege'] == 1) {
//                $is_hege = $fck->query('select count(*) co from xt_fck where id in(' . $user_info['down_path'] . ') and level="市级代理商" and is_hege=1');
//                $count = $is_hege[0]['co'];
//                if ($count == 1) {
//                    $can = 3;
//                } elseif ($count == 2) {
//                    $can = 4;
//                } elseif ($count == 3) {
//                    $can = 5;
//                } elseif ($count >= 4 && $count < 6) {
//                    $can = 7;
//                } elseif ($count >= 6) {
//                    $can = 9;
//                } else {
//                    $can = 0;
//                }
//                if ($n == 1) {
//                    $rate = 5;
//                }
//                if ($n >= 2 && $n <= 8) {
//                    $rate = 2;
//                }
//                if ($n == 9) {
//                    $rate = 1;
//                }
//                if ($can >= $n) {
//                    $fck->query('update xt_fck set b6 = ' . ($user_info['b6'] + ($cpzj * $rate / 100)) . ' where id = ' . $user_info['id']);
//                    $this->add_history($user_info['id'], time(), $fee_name[4], $cpzj * $rate / 100, $uid);
//                }
//
//                $n++;
//            }
//            if (empty($user_info['re_id'])) {
//                break;
//            }
//            $user_info = $fck->where(array('id' => $user_info['re_id']))->find();
//        }
//
//    }


    public function level($level)
    {
        $fee_rs = M('fee')->find();
        $fee_jicha = explode('|', $fee_rs['prize1']);
        $fee_dej1 = explode('|', $fee_rs['s19']);
        $fee_shj1 = explode('|', $fee_rs['s20']);
        
        switch ($level) {
            case "$fee_dej1[0]":
                return $jicha = $fee_jicha['0'];
                break;
            case "$fee_dej1[1]":
                return $jicha = $fee_jicha['1'];
                break;
            case "$fee_dej1[2]":
                return $jicha = $fee_jicha['2'];
                break;
            case "$fee_shj1[0]":
                return $jicha = $fee_jicha['3'];
                break;
            case "$fee_shj1[1]":
                return $jicha = $fee_jicha['4'];
                break;
            case "$fee_shj1[2]":
                return $jicha = $fee_jicha['5'];
                break;
            case "$fee_shj1[3]":
                return $jicha = $fee_jicha['6'];
                break;
        }
        unset($fee_dej1, $fee_shj1);
    }
/*
    public function shell()
    {
        $fck = M('fck');
        $re_id = 1;
        $re_name = 100000;

        for ($i = 1; $i < 100; $i++) {
            $arr = array(2000, 20000, 50000, 400000);
            $mt_rand = rand(0, 3);
            $user_id = rand(1000001, 9999999);
            switch ($arr[$mt_rand]) {
                case "2000":
                    $level = '会员';
                    break;
                case "20000":
                    $level = 'VIP';
                    break;
                case "50000":
                    $level = '社区体验店';
                    break;
                case "400000":
                    $level = '专卖店';
                    break;
            }
            if ($i % 4 == 0) {
                $info = $fck->query('select * from  xt_fck order by rdt desc limit 0,1;');
                $re_id = $info[0]['id'];
                $re_name = $info[0]['user_id'];
            }
            $fck->query("INSERT INTO xt_fck VALUES ('','','0','1539873519','127.0.0.1','1539871269','127.0.0.1', '0', '1', '132321231132@163.com', '00', '0', '0', '1', '0', '0', '100000', '0', " . $user_id . ", '公司', 'c4ca4238a0b923820dcc509a6f75849b', '1', 'c4ca4238a0b923820dcc509a6f75849b', 
                  '1', 'c81e728d9d4c2f636f067f89cc14862c', '2', '', '', '工商银行', '8889992', '', '', '444', '4444',
                   '51111', 'QQ126163@QQ.com', '', '0', '" . time() . "', '0', '0', '0', '" . $re_id . "', '" . $re_name . "', '0', '0', '1', 
                   '1', '2', '0.00', '0.00', '0.00', '0.00', '75000.00', '1870002.36', '0.00', '0', '1.00', '10000000', 
                   '0', '0', '0', '0', '0', '0', '0', '0', '0.00', '0.00', '1317024831', '0', '0', '0', '0', '0', '0', 
                   '0', '3', '2', '0', '0', '0', '0', 0x2C, null, null, null, '', 0x2C, '', '0', '2108', '0', '0.00', 
                   '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', 
                   '0.00', '" . $arr[$mt_rand] . "', '0.00', '0.00', '0.00', '0', '0', '0', '34e088c99641e9dd78c3d1f14da9a310', '0.00', 
                   '0', '0', '0', '0', '0.00', '1539792000', '0', '1525363200', '0', '0.00', '0', '0', '3', '0', '0.00', 
                   '1525363200', '0', '1', '0', '0', '1323230473', '0', '', '0', '0', '0', '0', '0.00', '0', '0', '10', 
                   '919379.05', '0.00', '0.00', '0', '1', '1', '1', '你爱人叫什么名字？', '123', '0', '0', '0', '0', 
                   '/Public/Uploads/2014010816185380.jpg', '1', '1', '0', '1', '0', '5', '中文', '马来西亚', '0', null, 
                   '0', '" . $arr[$mt_rand] . "', '233746.58', '3', null, null, null, null, '" . $level . "', '0', '', '', '0')");
            sleep(1);
        }
    }

*/
    public function add_history($id, $pdt, $bz, $take_home, $link,$did=0)
    {
        /**
         * @param   $id         为获得奖金的人的主键id,fck表
         * @param   $pdt        生成奖金的时间
         * @param   $bz         奖金的类型
         * @param   $take_home  奖金金额
         * @param   $link       奖金关系人
         * @return  mixed       执行成功返回主键id,失败返回false
         */
        $fck = M('fck');
        $user_info = $fck->where('id=' . $id)->find();
        $history = M('history');
        $data['uid'] = $id;
        $data['user_id'] = $user_info['user_id'];
        $data['pdt'] = $pdt;
        $data['did'] = $did;
        $data['bz'] = $bz;
        $data['epoints'] = $take_home;
        $data['fain_fee'] =  number_format($take_home *0.1,2);
        $data['take_home'] = $take_home -  $data['fain_fee'];

        $link_info = $fck->where('id=' . $link)->find();

        $data['link'] = $link_info['user_id'];
        $history->add($data);
        $fck->query('update xt_fck set bonus = bonus+' .      $data['take_home'] . ' where id = ' . $id);
        $fck->query('update xt_fck set agent_cf = agent_cf+' . $data['take_home']. ' where id = ' . $id);
    }

    function i_array_column($input, $columnKey, $indexKey = null)
    {
        if (!function_exists('array_column')) {
            $columnKeyIsNumber = (is_numeric($columnKey)) ? true : false;
            $indexKeyIsNull = (is_null($indexKey)) ? true : false;
            $indexKeyIsNumber = (is_numeric($indexKey)) ? true : false;
            $result = array();
            foreach ((array)$input as $key => $row) {
                if ($columnKeyIsNumber) {
                    $tmp = array_slice($row, $columnKey, 1);
                    $tmp = (is_array($tmp) && !empty($tmp)) ? current($tmp) : null;
                } else {
                    $tmp = isset($row[$columnKey]) ? $row[$columnKey] : null;
                }
                if (!$indexKeyIsNull) {
                    if ($indexKeyIsNumber) {
                        $key = array_slice($row, $indexKey, 1);
                        $key = (is_array($key) && !empty($key)) ? current($key) : null;
                        $key = is_null($key) ? 0 : $key;
                    } else {
                        $key = isset($row[$indexKey]) ? $row[$indexKey] : 0;
                    }
                }
                $result[$key] = $tmp;
            }
            return $result;
        } else {
            return array_column($input, $columnKey, $indexKey);
        }
    }

    public function menberAC()
    {
        //处理提交按钮
        $action = $_POST['action'];
        //获取复选框的值
        $OpID = $_POST['tabledb'];
        if (!isset($OpID) || empty($OpID)) {
            $bUrl = __URL__ . '/menber';
            $this->_box(0, '没有该会员！', $bUrl, 1);
            exit;
        }
        switch ($action) {
            case '开通会员':
                $this->_menberOpenUse($OpID, 0);
                break;
            case '电子币开通会员':
                $this->_menberOpenUse($OpID, 0);
                break;
            case '电子币开通会员':
                $this->_menberOpenUse($OpID, 1);
                break;
            case '一进一出币开通会员':
                $this->_menberOpenUse($OpID, 2);
                break;
            case '删除会员':
                $this->_menberDelUse($OpID);
                break;
            default:
                $bUrl = __URL__ . '/menber';
                $this->_box(0, '没有该会员！', $bUrl, 1);
                break;
        }
    }

    private function _menberOpenUse($OpID = 0, $reg_money = 0)
    {
        //=============================================开通会员
        if ($_SESSION['Urlszpass'] == 'MyssShuiPuTao') {
            $fck = D('Fck');
            $fee = M('fee');
            $gouwu = M('gouwu');
            $shouru = M('shouru');
            $blist = M('blist');
            $Guzhi = A('Guzhi');
            if (!$fck->autoCheckToken($_POST)) {
                $this->error('页面过期，请刷新页面！');
                exit;
            }

            //被开通会员参数
            $where = array();
            $where['id'] = array('in', $OpID);  //被开通会员id数组
            $where['is_pay'] = 0;  //未开通的
            $field = '*';
            $vo = $fck->where($where)->field($field)->order('id asc')->select();
            $fee_rs = $fee->field('s4,s9')->find();
            $s4 = explode("|", $fee_rs['s4']);
            $s9 = explode("|", $fee_rs['s9']);

            //专卖店参数
            $where_two = array();
            $field_two = '*';
            $ID = $_SESSION[C('USER_AUTH_KEY')];
            $where_two['id'] = $ID;
//			$where_two['is_agent'] = array('gt',1);
            $nowdate = strtotime(date('c'));
            $nowday = strtotime(date('Y-m-d'));
            $nowmonth = date('m');
            $fck->emptyTime();

            foreach ($vo as $voo) {
                $rs = $fck->where($where_two)->field($field_two)->find();  //找出登录会员(必须为专卖店并且已经登录)
                if (!$rs) {
                    $this->error('会员错误！');
                    exit;
                }
                $ppath = $voo['p_path'];
                //上级未开通不能开通下级员工
                $frs_where['is_pay'] = array('eq', 0);
                $frs_where['id'] = $voo['father_id'];
                $frs = $fck->where($frs_where)->find();
                if ($frs) {
                    $this->error('开通失败，上级未开通');
                    exit;
                }
                $us_money = $rs['agent_cash'];
                $money_a = $voo['cpzj'];

                if ($us_money < $money_a) {
                    $bUrl = __URL__ . '/menber';
                    $this->_box(0, '电子币余额不足！', $bUrl, 1);
                    exit;
                }

                $result = $fck->execute("update __TABLE__ set `agent_cash`=agent_cash-" . $money_a . " where `id`=" . $ID);
                if ($result) {
                    if ($reg_money == 1) {
                        $kt_cont = "电子币开通会员";
                    } elseif ($reg_money == 2) {
                        $kt_cont = "一进一出币开通会员";
                    } else {
                        $kt_cont = "电子币开通会员";
                    }
                    $fck->addencAdd($rs['id'], $voo['user_id'], -$money_a, 19, 0, 0, 0, $kt_cont); //历史记录
                    //给销售人添加销售人数或单数
                    $fck->query("update __TABLE__ set `re_nums`=re_nums+1,re_f4=re_f4+" . $voo['f4'] . " where `id`=" . $voo['re_id']);
                    $fee->query("update __TABLE__ set `a_money`=a_money+" . $voo['cpzj'] . " where id=1");

                    $nnrs = $fck->where('is_pay>0')->field('n_pai')->order('n_pai desc')->find();
                    $mynpai = ((int)$nnrs['n_pai']) + 1;

// 					//接点人信息
// 					$arry = array();
// 					$arry = $this->gongpaixtsmall($voo['re_id']);
// 					$father_id      = $arry['father_id'];
// 					$father_name = $arry['father_name'];
// 					$TreePlace     = $arry['treeplace'];
// 					$p_level        = $arry['p_level'];
// 					$p_path        = $arry['p_path'];
// 					$u_pai          = $arry['u_pai'];

                    $in_gp = $s4[$voo['u_level'] - 1];
                    $in_gw = $s9[$voo['u_level'] - 1];
                    $data = array();
                    $data['is_pay'] = 1;
                    $data['pdt'] = $nowdate;
                    $data['open'] = 0;
                    $data['get_date'] = $nowday;
                    $data['fanli_time'] = $nowday; //当天没有分红奖
                    $data['agent_lock'] = $in_gp; //
                    $data['gp_num'] = $in_gp; //
                    $data['n_pai'] = $mynpai;
                    $data['is_zy'] = $voo['id'];

// 					$data['father_id'] = $father_id;
// 					$data['father_name'] = $father_name;
// 					$data['treeplace'] = $TreePlace;
// 					$data['p_level'] = $p_level;
// 					$data['p_path'] = $p_path;
// 					$data['u_pai'] = $u_pai;
                    //开通会员
                    $result = $fck->where('id=' . $voo['id'])->save($data);
                    unset($data, $varray);

                    $data = array();
                    $data['uid'] = $voo['id'];
                    $data['user_id'] = $voo['user_id'];
                    $data['in_money'] = $voo['cpzj'];
                    $data['in_time'] = time();
                    $data['in_bz'] = "新会员加入";
                    $shouru->add($data);
                    unset($data);

                    //统计单数
                    $fck->xiangJiao($voo['id'], $voo['f4']);

                    //算出奖金
                    $fck->getusjj($voo['id'], 1);
                }
//				//全部奖金结算
//				$this->_clearing();
            }
            unset($fck, $where, $where_two, $rs);
            if ($vo) {
                unset($vo);
                $bUrl = __URL__ . '/menber';
                $this->_box(1, '开通会员成功！', $bUrl, 2);
                exit;
            } else {
                unset($vo);
                $bUrl = __URL__ . '/menber';
                $this->_box(0, '开通会员失败！', $bUrl, 1);
                exit;
            }
        } else {
            $this->error('错误！');
            exit;
        }
    }

    private function _menberDelUse($OpID = 0)
    {
        //=========================================删除会员
        if ($_SESSION['Urlszpass'] == 'MyssShuiPuTao') {
            $fck = M('fck');
            $where['is_pay'] = 0;
            foreach ($OpID as $voo) {
                $rs = $fck->find($voo);
                if ($rs) {
                    $whe['father_name'] = $rs['user_id'];
                    $rss = $fck->where($whe)->field('id')->find();
                    if ($rss) {
                        $bUrl = __URL__ . '/menber';
                        $this->error('该 ' . $rs['user_id'] . ' 会员有下级会员，不能删除！');
                        exit;
                    } else {
                        $where['id'] = $voo;
                        $fck->where($where)->delete();
                    }
                } else {
                    $this->error('错误!');
                }
            }
            $bUrl = __URL__ . '/menber';
            $this->_box(1, '删除会员！', $bUrl, 1);
            exit;
        } else {
            $this->error('错误!');
        }
    }

    //已开通会员
    public function frontMenber($Urlsz = 0)
    {
        //列表过滤器，生成查询Map对象
        if ($_SESSION['Urlszpass'] == 'MyssDaShuiPuTao') {
            $fck = M('fck');
            $id = $_SESSION[C('USER_AUTH_KEY')];
            $map = array();
            $map['open'] = $id;
            $map['is_pay'] = array('gt', 0);
            $UserID = $_POST['UserID'];
            if (!empty($UserID)) {
                import("@.ORG.KuoZhan");  //导入扩展类
                $KuoZhan = new KuoZhan();
                if ($KuoZhan->is_utf8($UserID) == false) {
                    $UserID = iconv('GB2312', 'UTF-8', $UserID);
                }
                unset($KuoZhan);
                $where['nickname'] = array('like', "%" . $UserID . "%");
                $where['user_id'] = array('like', "%" . $UserID . "%");
                $where['_logic'] = 'or';
                $map['_complex'] = $where;
                $UserID = urlencode($UserID);
            }
            //查询字段
            $field = "*";
            //=====================分页开始==============================================
            import("@.ORG.ZQPage");  //导入分页类
            $count = $fck->where($map)->count(); //总页数
            $listrows = C('ONE_PAGE_RE'); //每页显示的记录数
            $page_where = 'UserID=' . $UserID; //分页条件
            $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
            //===============(总页数,每页显示记录数,css样式 0-9)
            $show = $Page->show(); //分页变量
            $this->assign('page', $show); //分页变量输出到模板
            $list = $fck->where($map)->field($field)->order('pdt desc')->page($Page->getPage() . ',' . $listrows)->select();

            $HYJJ = '';
            $this->_levelConfirm($HYJJ, 1);
            $this->assign('voo', $HYJJ); //会员级别
            $this->assign('list', $list); //数据输出到模板
            //=================================================

            $this->display('frontMenber');
            exit;
        } else {
            $this->error('数据错误2!');
            exit;
        }
    }

    public function adminAgents()
    {
        //=====================================后台专卖店管理
        $this->_Admin_checkUser();
        if (true)/* ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') */ {
            /*
              $fck = M('fck');
              $UserID = $_POST['UserID'];
              if (!empty($UserID)) {
              import("@.ORG.KuoZhan");  //导入扩展类
              $KuoZhan = new KuoZhan();
              if ($KuoZhan->is_utf8($UserID) == false) {
              $UserID = iconv('GB2312', 'UTF-8', $UserID);
              }
              unset($KuoZhan);
              $where['nickname'] = array('like', "%" . $UserID . "%");
              $where['user_id'] = array('like', "%" . $UserID . "%");
              $where['_logic'] = 'or';
              $map['_complex'] = $where;
              $UserID = urlencode($UserID);
              }
              //$map['is_del'] = array('eq',0);
              $map['is_agent'] = array('gt', 0);
              if (method_exists($this, '_filter')) {
              $this->_filter($map);
              }
              $field = '*';
              //=====================分页开始==============================================
              import("@.ORG.ZQPage");  //导入分页类
              $count = $fck->where($map)->count(); //总页数
              $listrows = C('ONE_PAGE_RE'); //每页显示的记录数
              $page_where = 'UserID=' . $UserID; //分页条件
              $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
              //===============(总页数,每页显示记录数,css样式 0-9)
              $show = $Page->show(); //分页变量
              $this->assign('page', $show); //分页变量输出到模板
              $list = $fck->where($map)->field($field)->order('id desc')->page($Page->getPage() . ',' . $listrows)->select();
              $this->assign('list', $list); //数据输出到模板
              //=================================================

              $Agent_Us_Name = C('Agent_Us_Name');
              $Aname = explode("|", $Agent_Us_Name);
              $this->assign('Aname', $Aname);
             */
            $this->display('adminAgents');
            return;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    public function adminAgentslist()
    {
        $fck = M('fck');
        $map = array();
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $params = $this->getParams();

        $qvalue = trim($params['qvalue']);
        if (!empty($qvalue)) {
            $where['nickname'] = array('like', "%" . $qvalue . "%");
            $where['user_id'] = array('like', "%" . $qvalue . "%");
            $where['_logic'] = 'or';
            $map['_complex'] = $where;
        }
        if (!empty($params['s_time']) && empty($params['e_time'])) {
            $map['idt'] = array('between', array(strtotime($params['s_time']), time()));
        }
        if (!empty($params['s_time']) && !empty($params['e_time'])) {
            $map['idt'] = array('between', array(strtotime($params['s_time']), strtotime($params['e_time'])));
        }
        $map['new_agent'] = array('eq', 1);
        //      $map['_string'] = "shop_id=" . $id . " or re_id=" . $id . "";
        //查询字段
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $fck->where($map)->count(); //总页数
        $listrows = $params['limit']; //每页显示的记录数
        $nowPage = $params['offset'] / $params['limit'] + 1;
        $page_where = ''; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
        //===============(总页数,每页显示记录数,css样式 0-9)
        // $show = $Page->show(); //分页变量
        // $this->assign('page', $show); //分页变量输出到模板
        $list = $fck->where($map)->field($field)->order($params['sort'] . ' ' . $params['order'])->page($Page->getPage() . ',' . $listrows)->select();
        //    $this->assign('list', $list); //数据输出到模板

        show_list_json(40000, $list, $count, $nowPage);
    }

    public function adminAgentsOk()
    {
        $this->_Admin_checkUser();
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') {

            $this->display('adminAgentsok');
            return;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    public function adminAgentsoklist()
    {
        $fck = M('fck');
        $map = array();
        $params = $this->getParams();

        $qvalue = trim($params['qvalue']);
        if (!empty($qvalue)) {
            $where['nickname'] = array('like', "%" . $qvalue . "%");
            $where['user_id'] = array('like', "%" . $qvalue . "%");
            $where['_logic'] = 'or';
            $map['_complex'] = $where;
        }
        if (!empty($params['s_time']) && empty($params['e_time'])) {
            $map['adt'] = array('between', array(strtotime($params['s_time']), time()));
        }
        if (!empty($params['s_time']) && !empty($params['e_time'])) {
            $map['adt'] = array('between', array(strtotime($params['s_time']), strtotime($params['e_time'])));
        }
        $map['is_agent'] = array('gt', 0);
        //      $map['_string'] = "shop_id=" . $id . " or re_id=" . $id . "";
        //查询字段
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $fck->where($map)->count(); //总页数
        $listrows = $params['limit']; //每页显示的记录数
        $nowPage = $params['offset'] / $params['limit'] + 1;
        $page_where = 'UserID=' . $UserID; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $fck->where($map)->field($field)->order($params['sort'] . ' ' . $params['order'])->page($Page->getPage() . ',' . $listrows)->select();
        //    $this->assign('list', $list); //数据输出到模板
        show_list_json(40000, $list, $count, $nowPage);
    }

    public function adminAgentsShow()
    {
        //查看详细信息
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') {
            $fck = M('fck');
            $ID = (int)$_GET['Sid'];
            $where = array();
            $where['id'] = $ID;
            $srs = $fck->where($where)->field('user_id,verify')->find();
            $this->assign('srs', $srs);
            unset($fck, $where, $srs);
            $this->display('adminAgentsShow');
            return;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    /**
     * 删除专卖店申请
     */
    public function delAgent()
    {
        $XGid = $_REQUEST['uid'];
        if ($XGid) {
            $fck = M('fck');
            $params = $this->getParams();
            $content = $_POST['content'];
            $userid = trim($XGid);
            $where['id'] = $userid;
            //$rs=$fck->where($where)->find();
            $fck_rs = $fck->where($where)->field('id,is_agent,is_pay,user_id,user_name,agent_max,is_agent,new_agent,verify,shoplx,re_id,re_name')->find();
            $data = 0;
            if ($fck_rs['new_agent'] == 2 || $fck_rs['is_agent'] == 2) {
                $code = 10000;
                $msg = "审核通过的不能删除";
                $this->ajaxReturn($data, $msg, $code);
            }
            if ($fck_rs['new_agent'] == 1) {
                $r = $fck->query("update __TABLE__ set verify='',is_agent=0,new_agent=0,shoplx=0,shop_a='',shop_b='',shop_c='',shop_d='',idt=0 where id=" . $fck_rs['id']);

                $code = 40000;
                $msg = "删除成功";

            } else {
                $code = 10000;
                $msg = "这个会员没有申请，无法删除.";
            }

            $this->ajaxReturn($data, $msg, $code);
        }
        $this->ajaxReturn($data, $msg, $code);
    }

    /**
     * 专卖店审核
     */
    public function toExamineAgents()
    {
        $XGid = $_REQUEST['uid'];
        if ($XGid) {
            $_SESSION['UrlPTPass'] = 'MyssGuanXiGua';
            $fck = M('fck');
            $params = $this->getParams();
            $content = $_POST['content'];
            $userid = trim($XGid);
            $where['id'] = $userid;
            $fck_rs = $fck->where($where)->field('id,is_agent,is_pay,user_id,user_name,agent_max,is_agent,new_agent,verify,shoplx,re_id,re_name')->find();

            if ($fck_rs) {
                if ($fck_rs['is_pay'] == 0) {
                    $this->error('临时代理商不能授权专卖店!');
                    exit;
                }
                if ($fck_rs['new_agent'] != 1) {
                    $this->error('还没申请!');
                    exit;
                }
                if ($fck_rs['shoplx'] < 1) {
                    $this->error('专卖店级别错误!');
                    exit;
                }
                if (empty($fck_rs['verify'])) {
                    $this->error('请输入备注!');
                    exit;
                }

                if ($fck_rs['new_agent'] == 1) {
//                    $nowdate = time();
//                    $result = $fck->query("update __TABLE__ set is_agent=2,new_agent=2,adt={$nowdate} where id=" . $fck_rs['id']);
                    $data['is_agent'] = 2;
                    $data['new_agent'] = 2;
                    $data['adt'] = time();
                    $data['id'] = $fck_rs['id'];
                    $result = $fck->save($data);
                }
                if ($result) {
                    switch ($fck_rs['shoplx']) {
                        case 1:
                            $amount = 50000 * 0.1;
                            break;
                        case 2:
                            $amount = 200000 * 0.1;
                            break;
                        case 3:
                            $amount = 800000 * 0.1;
                            break;
                        case 4:
                            $amount = 3000000 * 0.1;
                            break;
                    }
                    $this->AddBounds($fck_rs['re_id'], $fck_rs['re_name'], 4, 50, $amount, '开通专卖店贡献奖');
                    $msg = '开通成功！';
                    $code = 40000;
                } else {
                    $msg = '开通失败！';
                    $code = 0;
                }
            } else {
                $msg = '会员不存在！';
                $code = 0;
            }
        } else {
            $msg = '参数错误！';
            $code = 0;
        }
        $this->ajaxReturn($data, $msg, $code);
    }

    /**
     * 专卖店进货管理页面
     */
    public function adminApplygouwu()
    {
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua' && $_SESSION['administrator'] == 1) {
            $fck = M('fck');
            $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
            $fck_rs = $fck->where($where)->field('id,is_agent,is_pay,user_id,user_name,agent_max,is_agent')->find();

            $this->assign('fck', $fck_rs[0]);
            $this->display('adminApplygouwu');
            exit;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    /**
     * 进货申请列表，未确认的在前面
     */
    public function adminApplylist()
    {
        $stock = M('stock');
        $map = array();
        $id = $_SESSION[C('USER_AUTH_KEY')];

        $params = $this->getParams();
        $qvalue = trim($params['qvalue']);
        if (!empty($qvalue)) {
            $where['user_id'] = array('like', "%" . $qvalue . "%");
            $map['_complex'] = $where;
        }
        if (!empty($params['s_time']) && empty($params['e_time'])) {
            $map['create_time'] = array('between', array(strtotime($params['s_time']), time()));
        }
        if (!empty($params['s_time']) && !empty($params['e_time'])) {
            $map['create_time'] = array('between', array(strtotime($params['s_time']), strtotime($params['e_time'])));
        }

        //查询字段
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $stock->where($map)->count(); //总页数
        $listrows = $params['limit']; //每页显示的记录数
        $nowPage = $params['offset'] / $params['limit'] + 1;
        $page_where = ''; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $stock->where($map)->field($field)->order('status asc,create_time desc'/* $params['sort'].' '.$params['order'] */)->page($Page->getPage() . ',' . $listrows)->select();

        show_list_json(40000, $list, $count, $nowPage);
    }

    /*
     *
     * 专卖店进货奖金
     */
    public function shopPurchase($orderid, $uid)
    {
        $fck = M('fck');
        $map['id'] = $uid;
        $fck_rs = $fck->where($map)->find();
        //查询该订单买的专卖店区商品
        $total_price = M('order_goods')->query("select sum(price*total) a from __TABLE__ where orderid=" . $orderid . " and cptype=1");
        $total_price = $total_price[0]['a'];
        //社区专卖店进货
        if ($fck_rs['is_agent'] == 2 && $fck_rs['shoplx'] == 1) {
            unset($map);
            $map['shop_a'] = $fck_rs['shop_a'];
            $map['is_agent'] = 2;
            $map['shoplx'] = array('gt', 1);
            //区、市、省级获得提成
            $regional_agent = $fck->where($map)->select();
            if ($regional_agent) {
                foreach ($regional_agent as $val) {
                    switch ($val['shoplx']) {
                        case 2:
                            $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.1, '进货提成');
                            break;
                        case 3:
                            $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.05, '进货提成');
                            break;
                        case 4:
                            $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.05, '进货提成');
                            break;
                    }
                }
            }
        }
        //区(县)级专卖店进货
        if ($fck_rs['is_agent'] == 2 && $fck_rs['shoplx'] == 2) {
            unset($map);
            $map['shop_a'] = $fck_rs['shop_a'];
            $map['is_agent'] = 2;
            $map['shoplx'] = array('gt', 2);
            //市、省级获得提成
            $regional_agent = $fck->where($map)->select();
            if ($regional_agent) {
                foreach ($regional_agent as $val) {
                    switch ($val['shoplx']) {
                        case 3:
                            $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.05, '进货提成');
                            break;
                        case 4:
                            $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.05, '进货提成');
                            break;
                    }
                }
            }
        }
        //市级专卖店进货
        if ($fck_rs['is_agent'] == 2 && $fck_rs['shoplx'] == 3) {
            unset($map);
            $map['shop_a'] = $fck_rs['shop_a'];
            $map['is_agent'] = 2;
            $map['shoplx'] = 4;
            //省级获得提成
            $regional_agent = $fck->where($map)->find();
            if ($regional_agent) {
                $this->AddBounds($regional_agent['id'], $regional_agent['user_id'], 9, 50, $total_price * 0.05, '进货提成');
            }
        }
    }

    /*
     * 再次进货审核
     */
//    public function adminApplyAC()
//    {
//        $data = '';
//        $uid = $_POST['uid'];
//
//        $fck = M('fck');
//        $stock = M('stock');
//        $map['id'] = $uid;
//        $apply_data = $stock->where($map)->find();
//        if ($apply_data['status'] == 2) {
//            $msg = '已确认通过';
//            $code = 0;
//        }
//        unset($map);
//        $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
//        $auditor = $fck->where($where)->field('id,re_id,re_name,is_agent,is_pay,user_id,user_name,agent_max,is_agent')->find();
//        $total_price = $apply_data['order_amount'];
//
//        if (empty($apply_data)) {
//            $msg = '数据错误！';
//            $code = 0;
//        } else {
//            $apply_data['status'] = 2;
//            $apply_data['check_time'] = time();
//            $apply_data['check_uid'] = $auditor['user_id'];
//            $result = $stock->save($apply_data);
//            if ($result) {
//                //计算奖金
//                $where['id'] = $apply_data['uid'];
//                $fck_rs = $fck->where($where)->find();
//                //专卖店差额奖金
//                if ($fck_rs['is_agent'] == 2 && $fck_rs['shoplx'] == 1) {
//                    unset($map);
//                    $map['shop_a'] = $fck_rs['shop_a'];
//
//                    $map['is_agent'] = 2;
//                    $map['shoplx'] = array('gt', 1);
//                    //区、市、省级获得提成
//                    $regional_agent = $fck->where($map)->select();
//                    if ($regional_agent) {
//                        foreach ($regional_agent as $val) {
//                            if ($val['shoplx'] == 4 && $val['shop_a'] == $fck_rs['shop_a']) {
//                                $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.05, '进货提成');
//                            }
//                            if ($val['shoplx'] == 3 && $val['shop_b'] == $fck_rs['shop_b']) {
//                                $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.05, '进货提成');
//                            }
//                            if ($val['shoplx'] == 2 && $val['shop_c'] == $fck_rs['shop_c']) {
//                                $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.1, '进货提成');
//                            }
//                        }
//                    }
//                }
//                //区(县)级专卖店进货
//                if ($fck_rs['is_agent'] == 2 && $fck_rs['shoplx'] == 2) {
//                    unset($map);
//                    $map['shop_a'] = $fck_rs['shop_a'];
//                    $map['is_agent'] = 2;
//                    $map['shoplx'] = array('gt', 2);
//                    //市、省级获得提成
//                    $regional_agent = $fck->where($map)->select();
//                    if ($regional_agent) {
//                        foreach ($regional_agent as $val) {
//                            if ($val['shoplx'] == 4 && $val['shop_a'] == $fck_rs['shop_a']) {
//                                $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.05, '进货提成');
//                            }
//                            if ($val['shoplx'] == 3 && $val['shop_b'] == $fck_rs['shop_b']) {
//                                $this->AddBounds($val['id'], $val['user_id'], 9, 50, $total_price * 0.05, '进货提成');
//                            }
//                        }
//                    }
//                }
//                //市级专卖店进货
//                if ($fck_rs['is_agent'] == 2 && $fck_rs['shoplx'] == 3) {
//                    unset($map);
//                    $map['shop_a'] = $fck_rs['shop_a'];
//                    $map['is_agent'] = 2;
//                    $map['shoplx'] = 4;
//                    //省级获得提成
//                    $regional_agent = $fck->where($map)->find();
//                    if ($regional_agent) {
//                        $this->AddBounds($regional_agent['id'], $regional_agent['user_id'], 9, 50, $total_price * 0.05, '进货提成');
//                    }
//                }
//                //专卖店差额奖金
//                if (!empty($fck_rs['re_id'])) {
//                    $fee = M('fee');
//                    $fee_rs = $fee->field('s6,s9')->find();
//                    $s6 = explode("|", $fee_rs['s6']);
//                    $s6 = empty($s6[1]) ? 2 : $s6[1];
//
//                    $epoitns = $apply_data['order_amount'] * $s6 / 100.0;
//
//                    $this->AddBounds($fck_rs['re_id'], $fck_rs['re_name'], 4, 42, $epoitns, '特别贡献奖');
//                }
//                $msg = '审核成功！';
//                $code = 40000;
//            } else {
//                $msg = '审核失败！';
//                $code = 0;
//            }
//        }
//        $this->ajaxReturn($data, $msg, $code);
//    }


    /**
     * 再次进货设申请
     */
    public function applygouwu()
    {
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') {
            $fck = M('fck');
            $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
            $fck_rs = $fck->where($where)->find();
            switch ($fck_rs['is_agent']) {
                case 1:
                    $fck_rs['is_agent'] = '审核中';
                    break;
                case 2:
                    $fck_rs['is_agent'] = '已开通';
                    break;
                default :
                    $fck_rs['is_agent'] = '未开通';
            }
            switch ($fck_rs['shoplx']) {
                case 1:
                    $fck_rs['shoplx'] = '社区专卖店';
                    break;
                case 2:
                    $fck_rs['shoplx'] = '县区专卖店';
                    break;
                case 3:
                    $fck_rs['shoplx'] = '地级市专卖店';
                    break;
                case 4:
                    $fck_rs['shoplx'] = '省级专卖店';
                    break;
                default :
                    $fck_rs['shoplx'] = '未开通';
            }
            $this->assign('fck_rs', $fck_rs);
            $this->display('applygouwu');
            exit;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    /**
     * 再次进货设申请处理
     */
    public function applygouwuConfirm()
    {
    	$content = $_POST['content'];
    	$money = trim($_POST['money']);
    	
    	$fck = M('fck');
    	$where['id'] = $_SESSION[C('USER_AUTH_KEY')];
    	$fck_rs = $fck->where($where)->find();
    	
    	$off = 0.8;
    	if ($fck_rs['cpzj'] == 50000) {
    		$off = 0.5;
    	}
    	$realMoney = number_format($money*$off,2,'.','');
    	$this->assign('off', $off);
    	$this->assign('money', $money);
    	$this->assign('realMoney', $realMoney);
    	$this->assign('fck_rs', $fck_rs);
    	$this->assign('content', $content);
    	
    	$this->display('applygouwuconfirm');
    }
    
    public function applygouwuAC()
    {
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') {
            //$this->_checkUser();
            $fck = M('fck');
            $content = $_POST['content'];
            $money = trim($_POST['money']);
            $realmoney =$_POST['realmoney'];
            $off = trim($_POST['off']);

            $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
            $fck_rs = $fck->where($where)->field('id,is_agent,is_pay,user_id,user_name,agent_max,is_agent')->find();

            if ($fck_rs) {
                if ($fck_rs['is_pay'] == 0) {
                    $this->error('临时代理商不能授权专卖店!');
                    exit;
                }
                if ($fck_rs['is_agent'] == 1) {
                    $this->error('上次申请还没通过审核!');
                    exit;
                }
                if ($money < 0) {
                    $this->error('输入金额有误！');
                    exit;
                }
                /*
                if ($money % 1 != 0) {
                    $this->error('请输入100的整数倍');
                    exit;
                }*/
                $stock = M('stock');

                $nowdate = time();
                $data = array('uid' => $where['id'],
                    'ordersn' => '',
                    'order_amount' => $money,
                    'create_time' => $nowdate,
                    'status' => 1,
                    'user_id' => $fck_rs['user_id'],
                    'user_name' => $fck_rs['user_name'],
                    'content' => $content,
                	'off' => $off,
                	'real_cost' => $realmoney,
                    'address_id' => 0
                );

                $stock->add($data);

                //     $result = $stock->query("update __TABLE__ set verify='" . $content . "',is_agent=2,idt=$nowdate,adt={$nowdate} where id=" . $fck_rs['id']);
            }

            $bUrl = __URL__ . '/applygouwulist';
            $this->success('申请成功！', $bUrl, 0);
        } else {
            $this->error('申请出错！');
            exit;
        }

    }

    //审核专卖店(专卖店)申请
    public function adminAgentsAC()
    {
        $this->_Admin_checkUser();
        //处理提交按钮
        $action = $_POST['action'];
        //获取复选框的值
        $XGid = $_POST['tabledb'];
        $fck = M('fck');
        unset($fck);
        if (!isset($XGid) || empty($XGid)) {
            $bUrl = __URL__ . '/adminAgents';
            $this->_box(0, '请选择会员！', $bUrl, 1);
            exit;
        }
        switch ($action) {
            case '确认';
                $this->_adminAgentsConfirm($XGid);
                break;
            case '删除';
                $this->_adminAgentsDel($XGid);
                break;
            default;
                $bUrl = __URL__ . '/adminAgents';
                $this->_box(0, '没有该会员！', $bUrl, 1);
                break;
        }
    }

    public function applygouwulist()
    {
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') {

            $fck = M('fck');
            $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
            $fck_rs = $fck->where($where)->field('id,is_agent,is_pay,user_id,user_name,agent_max,is_agent')->find();

            $this->assign('fck', $fck_rs[0]);
            $this->display('applygouwulist');
            exit;
        } else {
            $this->error('数据错误!');
            exit;
        }
    }

    /**
     * 进货申请记录ajax
     */
    public function Applylist()
    {
        $stock = M('stock');
        $map = array();
        $id = $_SESSION[C('USER_AUTH_KEY')];

        $params = $this->getParams();
        $UserID = $id;
        if (!empty($UserID)) {
            /*
              import("@.ORG.KuoZhan");  //导入扩展类
              $KuoZhan = new KuoZhan();
              if ($KuoZhan->is_utf8($UserID) == false) {
              $UserID = iconv('GB2312', 'UTF-8', $UserID);
              }
              unset($KuoZhan);
              $where['nickname'] = array('like', "%" . $UserID . "%");
              $where['user_id'] = array('like', "%" . $UserID . "%");
              $where['_logic'] = 'or';
              $map['_complex'] = $where;
              $UserID = urlencode($UserID); */
            $map['uid'] = $UserID;
        }
        //    $map['is_agent'] = array('gt', 0);
        //查询字段
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $stock->where($map)->count(); //总页数
        $listrows = $params['limit']; //每页显示的记录数
        $nowPage = $params['offset'] / $params['limit'] + 1;
        $page_where = 'UserID=' . $UserID; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $stock->where($map)->field($field)->order('status asc'/* $params['sort'].' '.$params['order'] */)->page($Page->getPage() . ',' . $listrows)->select();

        show_list_json(40000, $list, $count, $nowPage);
    }

    private function _adminAgentsConfirm($XGid = 0)
    {
        //==========================================确认申请专卖店
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') {
            $fck = D('Fck');
            $where['id'] = array('in', $XGid);
            $where['is_agent'] = 1;
            $rs = $fck->where($where)->field('*')->select();

            $data = array();
            $history = M('history');
            $rewhere = array();
            $nowdate = time();
            $jiesuan = 0;
            foreach ($rs as $rss) {

                $myreid = $rss['re_id'];
                $shoplx = $rss['shoplx'];

                $data['user_id'] = $rss['user_id'];
                $data['uid'] = $rss['uid'];
                $data['action_type'] = '申请成为专卖店';
                $data['pdt'] = $nowdate;
                $data['epoints'] = $rss['agent_no'];
                $data['bz'] = '申请成为专卖店';
                $data['did'] = 0;
                $data['allp'] = 0;
                $history->add($data);

                $fck->query("UPDATE __TABLE__ SET is_agent=2,adt=$nowdate,shoplx=$shoplx,agent_max=0 where id=" . $rss['id']);  //开通
            }
            unset($fck, $where, $rs, $history, $data, $rewhere);
            $bUrl = __URL__ . '/adminAgents';
            $this->_box(1, '确认申请！', $bUrl, 1);
            exit;
        } else {
            $this->error('错误！');
            exit;
        }
    }

    public function adminAgentsCoirmAC()
    {
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') {
            //$this->_checkUser();
            $fck = M('fck');
            $content = $_POST['content'];
            $userid = trim($_POST['userid']);
            $where['user_id'] = $userid;
            //$rs=$fck->where($where)->find();
            $fck_rs = $fck->where($where)->field('id,is_agent,is_pay,user_id,user_name,agent_max,is_agent')->find();

            if ($fck_rs) {
                if ($fck_rs['is_pay'] == 0) {
                    $this->error('临时代理商不能授权专卖店!');
                    exit;
                }
                if ($fck_rs['is_agent'] == 1) {
                    $this->error('上次申请还没通过审核!');
                    exit;
                }
                if ($fck_rs['is_agent'] == 2) {
                    $this->error('该代理商已是专卖店!');
                    exit;
                }
                if (empty($content)) {
                    $this->error('请输入备注!');
                    exit;
                }

                if ($fck_rs['is_agent'] == 0) {
                    $nowdate = time();
                    $result = $fck->query("update __TABLE__ set verify='" . $content . "',is_agent=2,idt=$nowdate,adt={$nowdate} where id=" . $fck_rs['id']);
                }

                $bUrl = __URL__ . '/adminAgents';
                $this->_box(1, '授权成功！', $bUrl, 2);
            } else {
                $this->error('会员不存在！');
                exit;
            }
        } else {
            $this->error('错误！');
            exit;
        }
    }

    private function _adminAgentsDel($XGid = 0)
    {
        //=======================================删除申请专卖店信息
        if ($_SESSION['UrlPTPass'] == 'MyssGuanXiGua') {
            $fck = M('fck');
            $rewhere = array();
            $where['is_agent'] = array('gt', 0);
            $where['id'] = array('in', $XGid);
            $rs = $fck->where($where)->select();
            foreach ($rs as $rss) {
                $fck->query("UPDATE __TABLE__ SET is_agent=0,idt=0,adt=0,new_agent=0,shoplx=0,shop_a='',shop_b='' where id>1 and id = " . $rss['id']);
            }

            unset($fck, $where, $rs, $rewhere);
            $bUrl = __URL__ . '/adminAgents';
            $this->_box('操作成功', '删除申请！', $bUrl, 1);
            exit;
        } else {
            $this->error('错误!');
            exit;
        }
    }

    //专卖店表
    public function financeDaoChu_BD()
    {
        $this->_Admin_checkUser();
        //导出excel
        set_time_limit(0);

        header("Content-Type:   application/vnd.ms-excel");
        header("Content-Disposition:   attachment;   filename=Member-Agent.xls");
        header("Pragma:   no-cache");
        header("Content-Type:text/html; charset=utf-8");
        header("Expires:   0");

        $fck = M('fck');  //奖金表

        $map = array();
        $map['id'] = array('gt', 0);
        $map['is_agent'] = array('gt', 0);
        $field = '*';
        $list = $fck->where($map)->field($field)->order('idt asc,adt asc')->select();

        $title = "专卖店表 导出时间:" . date("Y-m-d   H:i:s");

        echo '<table   border="1"   cellspacing="2"   cellpadding="2"   width="50%"   align="center">';
        //   输出标题
        echo '<tr   bgcolor="#cccccc"><td   colspan="9"   align="center">' . $title . '</td></tr>';
        //   输出字段名
        echo '<tr  align=center>';
        echo "<td>序号</td>";
        echo "<td>会员编号</td>";
        echo "<td>姓名</td>";
        echo "<td>联系电话</td>";
        echo "<td>申请时间</td>";
        echo "<td>确认时间</td>";
        echo "<td>类型</td>";
        echo "<td>专卖店区域</td>";
        echo "<td>剩余电子币</td>";
        echo '</tr>';
        //   输出内容
        //		dump($list);exit;
        $i = 0;
        foreach ($list as $row) {
            $i++;
            $num = strlen($i);
            if ($num == 1) {
                $num = '000' . $i;
            } elseif ($num == 2) {
                $num = '00' . $i;
            } elseif ($num == 3) {
                $num = '0' . $i;
            } else {
                $num = $i;
            }
            if ($row['shoplx'] == 1) {
                $nnn = '专卖店';
            } elseif ($row['shoplx'] == 2) {
                $nnn = '县/区代理商';
            } else {
                $nnn = '市级代理商';
            }
            echo '<tr align=center>';
            echo '<td>' . chr(28) . $num . '</td>';
            echo "<td>" . $row['user_id'] . "</td>";
            echo "<td>" . $row['user_name'] . "</td>";
            echo "<td>" . $row['user_tel'] . "</td>";
            echo "<td>" . date("Y-m-d H:i:s", $row['idt']) . "</td>";
            echo "<td>" . date("Y-m-d H:i:s", $row['adt']) . "</td>";
            echo "<td>" . $nnn . "</td>";
            echo "<td>" . $row['shop_a'] . " / " . $row['shop_b'] . "</td>";
            echo "<td>" . $row['agent_cash'] . "</td>";
            echo '</tr>';
        }
        echo '</table>';
    }
    /*
     * 省市区三级联动
     */
    /**
     * 查询
     */
    public function getList($where)
    {
        $regionModel = M('Region');
        $list = $regionModel->where($where)->select();
        return $list;
    }

    /**
     * 省市区三级联动ajax（获取市）
     */
    public function regionAjaxGetCitys()
    {
        $where['area_parent_id'] = $_GET['provinceId'];
        $list = $this->getList($where);

        $data = array('city' => $list);
        header("Content-type: application/json");
        exit(json_encode($data));
    }

    /**
     * 省市区三级联动ajax(获取地区)
     */
    public function regionAjaxGetdDistrict()
    {
        $where['area_parent_id'] = $_GET['cityId'];
        $list = $this->getList($where);
        $data = array('district' => $list);
        header("Content-type: application/json");
        exit(json_encode($data));
    }

    public function leader_bonus()
    {
        $log = M('leader_bonus_log');
        $log_info = $log->query('select * from xt_leader_bonus_log order by endtime desc');
        $this->assign('log_info', $log_info);
        
        $year = date('Y', time());
        $new_m = date('m', time());
        for ($m = 1; $m <= 12; $m++) {
            $time = strtotime($year . '-' . $m);
            $starttime = strtotime('-1 month', $time);
            $starttime = date('Y-m', $starttime);
            $endtime = date('Y-m-d H:i:00', $time);
            $time_arr[] = array('starttime' => $starttime, 'endtime' => $endtime);
        }
        $this->assign('time_arr', $time_arr);
        $this->display('leader_bonus');
    }
    
    public function leader_bonus_list()
    {
    	$stock = M('leader_bonus_log');
 //   	$log_info = $log->query('select * from xt_leader_bonus_log order by endtime desc');
  //  	$this->assign('log_info', $log_info);
    	$params = $this->getParams();
    	$map = array();
    	$year = date('Y', time());
    	$new_m = date('m', time());
    	for ($m = 1; $m <= 12; $m++) {    		
    		$time = strtotime($year . '-' . $m);
    		$starttime = strtotime('-1 month', $time);
    		$starttime = date('Y-m-d H:i:01', $starttime);
    		$endtime = date('Y-m-d H:i:00', $time);
    		$time_arr[] = array('starttime' => $starttime, 'endtime' => $endtime);
    	}
	    //    $map['is_agent'] = array('gt', 0);
	    //查询字段
	    $field = '*';
	    //=====================分页开始==============================================
	    import("@.ORG.ZQPage");  //导入分页类
	    $count = $stock->where($map)->count(); //总页数
	    $listrows = $params['limit']; //每页显示的记录数
	    $nowPage = $params['offset'] / $params['limit'] + 1;
	    $page_where = ''; //分页条件
	    $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
	    //===============(总页数,每页显示记录数,css样式 0-9)
	    $show = $Page->show(); //分页变量
	    $this->assign('page', $show); //分页变量输出到模板
	    $list = $stock->where($map)->field($field)->order('starttime desc')->page($Page->getPage() . ',' . $listrows)->select();
	    
	    show_list_json(40000, $list, $count, $nowPage);
    }
    
    //领导奖计算
    /**
     * 第一遍先计算是否合格 { 在报单时即时计算  }
     * 
     * 第二遍计算合格的紧缩业绩 { 在月度结算时计算 }
     * { 根据路径计算，推荐路径 }
     * 
     * 第三部计算等级级数 { 根据路径计算 直接推荐合格人数 }
     * 
     * 第四部计算奖金
     * { }
     */
    
    public function leader_bonus_add()
    {
        $log = M('leader_bonus_log');
        $fee = M('fee');
        
        $fee_rs = $fee->find();
        $fee_name = explode('|', $fee_rs['prizename']);
        $fck = M('FckModel:fck');
        $leader_achievement = M('leader_achievement');
        $time = $_POST['time'];
        
        //当前时间转换为时间戳
        $time = strtotime($time);
        //当前时间格式化为年月
        $month = date('Y-m', $time);
        //当前时间转换为只有年月时间戳
        $time = strtotime($month);
        $starttime = strtotime('-1 month', $time);
        $month = date('Y-m', $starttime);
        $starttime = date('Y-m-d H:i:01', $starttime);
        $starttime = strtotime($starttime);
        $endtime = $time;
        $log_info = $log->where(array('starttime' => $starttime, 'endtime' => $endtime))->find();
        if (!empty($log_info)) {
            $msg = '该时间段已发放过领导奖不能重复发放！';
            $code = 0;
            $this->ajaxReturn('', $msg, $code);
        }
        if (time() < $endtime) {
            $msg = '未到发放时间请重新选择！';
            $code = 0;
            $this->ajaxReturn('', $msg, $code);
        }
        /*
        $llog = $log->add(array( 'starttime' => $starttime, 'endtime' => $endtime, 'createtime' => time()));
        if(is_array($llog)){
        	$msg = '出错！';
        	$code = 0;
        	$this->ajaxReturn('', $msg, $code);
        }*/ 
    
        $all_achievement = 0;
        $all_bonus = 0;
        
        $leader_bonus_log = M('leader_bonus_log');
        
        //领导奖分发
        //@todo
        $history = M('history');
        $leader_month = M('leader_month');
        $all_achievement = 0;
        $all_bonus = 0;
        
        $logid = $log->add(array('achievement' => $all_achievement, 'bonus' => $all_bonus, 'starttime' => $starttime, 'endtime' => $endtime,'month'=>$month, 'createtime' => time()));
        
        $hege_users = $leader_month->query("select distinct user_id from xt_leader_month where is_pass=1 and  month='{$month}' group by user_id");
        foreach ($hege_users as $hu) {
        	$downNum = $this->getDownLeve($hu['down_pass']);
        	$re_user = array($hu['user_id']);
        	
        	$taxNum = 0.0;//奖金数
        	
        	//循环下级业绩奖金
        	for ($x=1; $x<=$downNum; $x++){
        		$achievement = 0;
        		$re_id = implode(",", $re_user);
        		$users = $fck->query("select f.id,f.user_id,sum(l.archieve) total from xt_fck f,xt_leader_month l where l.source_id=f.id and re_id in ({$re_id}) and month='{$month}'  GROUP BY f.id,f.user_id");
        		$re_user = array();
        		
        		//获取费率
        		$tax =  $this->getBoundTax($x);
        		
        		foreach ($users as $u){
        			$re_user[] = $u['id'];
        			
        			$achievement += (int)$u['total'];
        			
        			$bounds = $u['total'] * $tax;
        			//写入领导将数据
        			$now = time();
        			//	$leader_bonus_log->add(array('achievement'=>$achievement,'bonus'=>$taxNum,'starttime' => $starttime, 'endtime' => $endtime,'month'=>$month,'createtime'=>$now));
        			$fck->add_history($hu['user_id'], time(), $fee_name[4], $bounds,$u['user_id'], $logid);
        			
        		}
        		
        		$taxNum += (float)$achievement * $tax;
        		$all_achievement += $achievement;

        	}
        	
        	$all_bonus += $taxNum;
        	
        	
        	//$this->add_history($ID, time(), $fee_name[6], $user_data['cpzj'] * $fee_db[0] / 100, $uid);
        }
        
   //     $log->add(array('achievement' => $all_achievement, 'bonus' => $all_bonus, 'starttime' => $starttime, 'endtime' => $endtime,'month'=>$month, 'createtime' => time()));
        $log->save(array('achievement' => $all_achievement, 'bonus' => $all_bonus),array('where'=>array('id'=>$logid)));
        
        //以下为旧算法，注释，新算法在上面
 //     $hege_users = $fck->query('select * from xt_fck where level="市级代理商" and is_hege = 1');//u_level
 /*
        $hege_users = $fck->query('select * from xt_fck where u_level>=5  and is_hege = 1');
        foreach ($hege_users as $hu) {
            $achievement_sum = 0;
            $i = 1;
            $down_path = $hu['down_path'];
            $hege_count = $fck->query('select count(*) co from xt_fck where id in (' . $hu['down_path'] . ') and is_hege = 1');
            if ($hege_count[0]['co'] >= 6) {
                $can = 9;
            } elseif ($hege_count[0]['co'] >= 4) {
                $can = 7;
            } elseif ($hege_count[0]['co'] >= 3) {
                $can = 5;
            } elseif ($hege_count[0]['co'] >= 2) {
                $can = 4;
            } elseif ($hege_count[0]['co'] >= 1) {
                $can = 3;
            } else {
                $can = 0;
            }
            while (!empty($down_path) && $i <= $can) {
                if ($i == 1) {
                    $rate = 5;
                } elseif ($i >= 2 && $i <= 8) {
                    $rate = 2;
                } elseif ($i == 9) {
                    $rate = 1;
                }
                //本级的下级业绩
                $leader_achievement_sum = $leader_achievement->query('select sum(achievement) ac from xt_leader_achievement where uid in (' . $down_path . ') and createtime>=' . $starttime . ' and createtime<=' . $endtime);
                $leader_achievement_sum = $leader_achievement_sum[0]['ac'];
                $achievement_sum = $achievement_sum + $leader_achievement_sum;
                $all_achievement += $achievement_sum;
                if ($achievement_sum >= 20000) {
                	$fck->query('update xt_fck set b6 = b6+' . ($achievement_sum * $rate / 100) . ' where id = ' . $hu['id'],$llog);
                    $this->add_history($hu['id'], time(), $fee_name[4], $achievement_sum * $rate / 100, $hu);
                    $all_bonus += $achievement_sum * $rate / 100;
                    $achievement_sum = 0;
                    $i++;
                }
                //本级的下级user
                $down_user = $fck->query('select * from xt_fck where id in(' . $down_path . ')');
                $down_path = '';
                //遍历求下级的所有下级
                foreach ($down_user as $du) {
                    if (!empty($du['down_path'])) {
                        $down_path = $down_path . $du['down_path'] . ',';
                    }
                }

                $down_path = rtrim($down_path, ',');

                //if无下层且业绩不到20000结算
                if (empty($down_path) && $achievement_sum < 20000 && $achievement_sum > 0) {
                    $fck->query('update xt_fck set b6 = b6+' . ($achievement_sum * $rate / 100) . ' where id = ' . $hu['id']);
                    $this->add_history($hu['id'], time(), $fee_name[4], $achievement_sum * $rate / 100, $hu,$llog);
                }
            }
        }
     //   $ld = $log->where(array(''))->select();
        $log->add(array('achievement' => $all_achievement, 'bonus' => $all_bonus, 'starttime' => $starttime, 'endtime' => $endtime, 'createtime' => time()));
		*/
        
        $msg = '操作成功！';
        $code = 40000;

        $this->ajaxReturn('', $msg, $code);
    }

    public function getDownLeve($passNum){
    	$DownNum = 0;
    	if($passNum ==1){
    		$DownNum = 3;
    	}else if($passNum ==2){
    		$DownNum = 4;
    	}else if($passNum ==3){
    		$DownNum = 5;
    	}else if($passNum ==4||$passNum ==5){
    		$DownNum = 7;
    	}else if($passNum >=6){
    		$DownNum = 9;
    	}
    	return 9;
    	
    }
    
    public function getBoundTax($passNum){
    	$DownNum = 0;
    	if($passNum ==1){
    		$DownNum = 0.05;
    	}else if($passNum >=2 && $passNum <= 8){
    		$DownNum = 0.02;
    	}
    	else if($passNum ==9){
    		$DownNum = 0.01;
    	}
    	
    	return $DownNum;
    	
    }
    
    public  function leader_bonus_detail(){
    	
    	$did = $_GET['bonus_id'];
    	$this->assign('did', $did);
    	
    	$this->display('leader_bonus_detail');
    }
    
    public  function leader_bonus_detail_list(){
    	
    	$stock = M('history');
    	//   	$log_info = $log->query('select * from xt_leader_bonus_log order by endtime desc');
    	//  	$this->assign('log_info', $log_info);
    	$params = $this->getParams();
    	$map = array();
    	
    	$map['did'] = $params['did'];
    	//查询字段
    	$field = '*';
    	//=====================分页开始==============================================
    	import("@.ORG.ZQPage");  //导入分页类
    	$count = $stock->where($map)->count(); //总页数
    	$listrows = $params['limit']; //每页显示的记录数
    	$nowPage = $params['offset'] / $params['limit'] + 1;
    	$page_where = ''; //分页条件
    	$Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where, $nowPage);
    	//===============(总页数,每页显示记录数,css样式 0-9)
    	$show = $Page->show(); //分页变量
    	$this->assign('page', $show); //分页变量输出到模板
    	
    	$Model = new Model(); 
    	$sql = 'select a.id,a.take_home,a.user_id,b.name,b.nickname,a.pdt,a.act_pdt from xt_history as a, xt_fck as b where a.uid=b.id and a.did='.$map['did'].' order by b.id asc,a.id asc limit '.$params['offset'].','.$listrows; 
    	$list = $Model->query($sql);
    	
   // 	$list = $stock->where($map)->field($field)->order('user_id asc')->page($Page->getPage() . ',' . $listrows)->select();
    	
    	show_list_json(40000, $list, $count, $nowPage);
    }
    
//    public function text()
//    {
//        $fck = M('fck');
//        for ($i = 1; $i <= 200; $i++) {
//            //第五个为第四个的下级
//            if ($i % 5 == 0||$i==1) {
//                $last = $fck->query('select * from xt_fck order by id desc limit 1');
//                $re_id = $last[0]['id'];
//                $re_name = $last[0]['user_id'];
//            }
//            $data = array(
//                'bind_account'=>333,
//                'new_login_time'=>0,
//                'last_login_time'=>1541837260,
//                'login_count'=>0,
//                'verify'=>0,
//                'email'=>'574368565@qq.com',
//                'create_time'=>0,
//                'status'=>1,
//                'type_id'=>0,
//                'info'=>'信息',
//                'name'=>'名称',
//                'user_id'=>rand(10000000,99999999),
//                'user_name'=>'123',
//                'password'=>'c4ca4238a0b923820dcc509a6f75849b',
//                'pwd1'=>1,
//                'passopen'=>'c81e728d9d4c2f636f067f89cc14862c',
//                'pwd2'=>2,
//                'passopentwo'=>'eccbc87e4b5ce2fe28308fd9f2a7baf3',
//                'pwd3'=>3,
//                'nickname'=>'张源',
//                'qq'=>574368565,
//                'bank_name'=>'招商银行',
//                'bank_card'=>'123',
//                'bank_province'=>'山东',
//                'bank_city'=>'莱州',
//                'bank_address'=>'地址',
//                'user_code'=>123,
//                'user_address'=>'莱州',
//                'user_tel'=>13256968084,
//                'rdt'=>'1541837260',
//                're_id'=>$re_id,
//                're_name'=>$re_name,
//                'cpzj'=>20000,
//                'f4'=>5,
//                'wenti_dan'=>123,
//                'seller_rate'=>5,
//                'level'=>'社区代理商'
//            );
//            $fck->add($data);
//            $zuihou = $fck->query('select * from xt_fck order by id desc limit 1');
//            $this->toExamine($zuihou[0]['id']);
//
//        }
//    }

}

?>
