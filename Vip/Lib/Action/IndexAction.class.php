<?php

class IndexAction extends CommonAction
{

    // 框架首页
    public function index()
    {
        ob_clean();
        $this->_checkUser();
        $this->_Config_name(); //调用参数
        C('SHOW_RUN_TIME', false); // 运行时间显示
        C('SHOW_PAGE_TRACE', false);
        $fck = D('Fck');

        $id = $_SESSION[C('USER_AUTH_KEY')];
        $this->assign('uid', $id);
        $field = '*';
        $fck_rs = $this->getUserInfo();

        $this->assign('fck_rs', $fck_rs);
        $HYJJ = "";
        $this->_levelConfirm($HYJJ, 1);
        $this->assign('voo', $HYJJ); //会员级别

        //收入奖金
        $income = $this->bonus();
        $this->assign('income', $income);
        unset($map);

        //团队人数
        $map['_string'] = 'FIND_IN_SET(' . $fck_rs["id"] . ', re_path)';
        $map['is_pay'] = array('neq', 0);
        $tem_num = $fck->where($map)->count('id');
        $this->assign('tem_num', $tem_num);

        unset($map);
        //会员直推人数
        $map['re_id'] = $fck_rs['id'];
        $map['is_pay'] = array('neq', 0);
        $zhitui = $fck->where($map)->count();
        $this->assign('zhitui', $zhitui);

        $fck->emptyTime();
        $fck->emptyMonthTime();
        $fck->getLevel();

        $ydate = strtotime(date('Y-m-d')); //当天时间
        $end_date = $ydate + (24 * 3600); //当天结束时间

        $fee_rs = M('fee')->field('s2,s10,i4,str29')->find();
        $fee_i4 = $fee_rs['i4'];
        $gg = $fee_rs['str29'];
        $this->assign('gg', $gg);
        $this->assign('fee_i4', $fee_i4);

        $voo = explode("|", $fee_rs['s10']);
        $this->assign('voo', $voo);
        $map = array();
        $map['s_uid'] = $id;   //会员ID
        $map['s_read'] = 0;     // 0 为未读
        $info_count = M('msg')->where($map)->count(); //总记录数
        $this->assign('info_count', $info_count);

        $arss = $this->_cheakPrem();
        $this->assign('arss', $arss);

        $Guzhi = A("Guzhi");
        $Guzhi->stock_past_due();
        $this->sav_bouns($id);

        $form = M('form');
        $field = '*';

        $newslist = $form->field($field)->limit(0, 3)->order('baile desc,id desc')->select();
        $this->assign('newslist', $newslist);

        //@新增的
        $fck_rs1 = $this->getUserInfo();
        $Model = new Model();
        $sql = "select pdg, 
            sum(case bz when '拓展' then take_home  ELSE 0 END) as `tzh`,
            sum(CASE bz WHEN '级差' THEN take_home  ELSE 0 END) as `jch`,
            sum(CASE bz WHEN '合作' THEN take_home  ELSE 0 END) as `hzh`,
            sum(CASE bz WHEN '管理' THEN take_home  ELSE 0 END) as `gli`,
            sum(CASE bz WHEN '店补' THEN take_home  ELSE 0 END) as `dbu`,
            sum(CASE bz WHEN '领导' THEN take_home  ELSE 0 END) as `ldao`,
            sum(take_home) take_home
             from   
                (select FROM_UNIXTIME(pdt, '%Y-%m-%d') as pdg,sum(`take_home`) as `take_home`,bz
                from xt_history  where uid={$fck_rs['id']}
                group by FROM_UNIXTIME(pdt, '%Y-%m-%d'),bz )
             ss
            group by pdg;";
            $list = $Model->query($sql);
        $this->assign('slist', $list);

        $fee = M('fee');
        $fee_rs = $fee->find();
        $change_rale = json_decode($fee_rs['change_rale'], true);
        $this->assign('change_rale', $change_rale);

        $this->display('index');
    }

    public function index0()
    {
        ob_clean();
        $this->_checkUser();
        $this->_Config_name(); //调用参数
        C('SHOW_RUN_TIME', false); // 运行时间显示
        C('SHOW_PAGE_TRACE', false);
        $fck = D('Fck');

        $id = $_SESSION[C('USER_AUTH_KEY')];
        $field = '*';
        $fck_rs = $fck->field($field)->find($id);

        $HYJJ = "";
        $this->_levelConfirm($HYJJ, 1);
        $this->assign('voo', $HYJJ); //会员级别

        $this->assign('fck_rs', $fck_rs);

        $fck->emptyTime();
        $fck->emptyMonthTime();
        $fck->getLevel();

        $ydate = strtotime(date('Y-m-d')); //当天时间
        $end_date = $ydate + (24 * 3600); //当天结束时间

        $fee_rs = M('fee')->field('s2,i4,str29')->find();
        $fee_i4 = $fee_rs['i4'];
        $gg = $fee_rs['str29'];
        $this->assign('gg', $gg);
        $this->assign('fee_i4', $fee_i4);

        $map = array();
        $map['s_uid'] = $id;   //会员ID
        $map['s_read'] = 0;     // 0 为未读
        $info_count = M('msg')->where($map)->count(); //总记录数
        $this->assign('info_count', $info_count);

        $arss = $this->_cheakPrem();
        $this->assign('arss', $arss);

        $Guzhi = A("Guzhi");
        $Guzhi->stock_past_due();

        //		$this->aotu_clearings();

        $this->display('index0');
    }

    //每日自动结算
    public function aotu_clearings()
    {
        $fck = D('Fck');
        $fee = M('fee');
        $nowday = strtotime(date('Y-m-d'));
        $nowweek = date("w");
        if ($nowweek == 0) {
            $nowweek = 7;
        }
        $kou_w = $nowweek - 1;
        $weekday = $nowday - $kou_w * 24 * 3600;

        $now_dtime = strtotime(date("Y-m-d"));
        if (empty($_SESSION['auto_cl_ok']) || $_SESSION['auto_cl_ok'] != $now_dtime) {
            $js_c = $fee->where('id=1 and f_time<' . $weekday)->count();
            if ($js_c > 0) {
                //经理分红
                $fck->jl_fenghong();
            }
            $_SESSION['auto_cl_ok'] = $now_dtime;
        }
        unset($fck, $fee);
    }

    public function change()
    {
        $fck = D('Fck');
        $fee = M('fee');
        $fee_res = $fee->find();
        $change_rale = json_decode($fee_res['change_rale'], true);
        $user = $fck->where(array('id' => $_POST['id']))->find();
        $type = $_POST['type'];
        $amount = $_POST['amount'];
        $id = $_POST['id'];
        if ($type == 'credit') {
            if ($user['credit'] < $amount) {
                echo json_encode(array('status' => 0, 'msg' => '积分余额不足'));
                return;
            }
            $fck->where('id=' . $id)->setDec('credit', $amount);
            $fck->where('id=' . $id)->setInc('agent_xf', $amount / $change_rale['credit']);
            echo json_encode(array('status' => 1, 'msg' => '操作成功'));
            return;
        }
        if ($type == 'bonus') {
            if ($user['bonus'] < $amount) {
                echo json_encode(array('status' => 0, 'msg' => '奖金余额不足'));
                return;
            }
            $fck->where('id=' . $id)->setDec('bonus', $amount);
            $fck->where('id=' . $id)->setInc('agent_xf', $amount / $change_rale['bonus']);
            echo json_encode(array('status' => 1, 'msg' => '操作成功'));
            return;
        }
        if ($type == 'electron') {
            if ($user['agent_xf'] < $amount) {
                echo json_encode(array('status' => 0, 'msg' => '电子币余额不足'));
                return;
            }
            $fck->where('id=' . $id)->setDec('agent_xf', $amount);
            $fck->where('id=' . $id)->setInc('credit', $amount / $change_rale['electron']);
            echo json_encode(array('status' => 1, 'msg' => '操作成功'));
            return;
        }
    }

    public function recharge()
    {
        $recharge = M('recharge');
        $data['uid'] = $_SESSION[C('USER_AUTH_KEY')];
        $data['amount'] = $_POST['amount'];
        $data['remark'] = $_POST['remark'];
        $data['createtime'] = time();
        $res = $recharge->add($data);
        if ($res) {
            echo json_encode(array('status' => 1, 'msg' => '提交成功，请等待管理员审核到账'));
            return;
        }
        echo json_encode(array('status' => 0, 'msg' => '提交失败，请尝试重新提交'));
        return;
    }

}

?>
