<?php

class GouwuAction extends CommonAction {

    function _initialize() {
        $this->_inject_check(0); //调用过滤函数
        //    $this->_checkUser();
        $this->check_us_gq();
        header("Content-Type:text/html; charset=utf-8");
        //读出商品分类缓存 
        $file_path = "cate_menu.txt";
        $menu_list = file_get_contents($file_path);
        $menu_array = json_decode($menu_list, true);
        $children = $menu_array['b'];
        $goods_data = $menu_array['a'];
        $this->assign('children', $children);
        $this->assign('goods_data', $goods_data); //数据输出到模板
        $sum = $this->cart_number();
        $this->assign('sum', $sum);
        $fck_rs = $this->getUserInfo();
        $this->assign('fck_rs', $fck_rs);
    }

    //二级验证
    public function Cody() {
        //$this->_checkUser();
        $UrlID = (int) $_GET['c_id'];
        if (empty($UrlID)) {
            $this->error('二级密码错误!');
            exit;
        }
        if (!empty($_SESSION['user_pwd2'])) {
            $url = __URL__ . "/codys/Urlsz/$UrlID";
            $this->_boxx($url);
            exit;
        }
        $cody = M('cody');
        $list = $cody->where("c_id=$UrlID")->field('c_id')->find();
        if (!empty($list)) {
            $this->assign('vo', $list);
            $this->display('../Public/cody');
            exit;
        } else {
            $this->error('二级密码错误!');
            exit;
        }
    }

    //二级验证后调转页面
    public function Codys() {
        $Urlsz = $_POST['Urlsz'];
        if (empty($_SESSION['user_pwd2'])) {
            $pass = $_POST['oldpassword'];
            $fck = M('fck');
            if (!$fck->autoCheckToken($_POST)) {
                $this->error('页面过期请刷新页面!');
                exit();
            }
            if (empty($pass)) {
                $this->error('二级密码错误!');
                exit();
            }
            $where = array();
            $where['id'] = $_SESSION[C('USER_AUTH_KEY')];
            $where['passopen'] = md5($pass);
            $list = $fck->where($where)->field('id')->find();
            if ($list == false) {
                $this->error('二级密码错误!');
                exit();
            }
            $_SESSION['user_pwd2'] = 1;
        } else {
            $Urlsz = $_GET['Urlsz'];
        }
        switch ($Urlsz) {
            case 1;
                $_SESSION['UrlszUserpass'] = 'MyssGuanChanPin';
                $bUrl = __URL__ . '/pro_index';
                $this->_boxx($bUrl);
                break;
            case 2;
                $_SESSION['UrlszUserpass'] = 'MyssWuliuList';
                $bUrl = __URL__ . '/adminLogistics';
                $this->_boxx($bUrl);
                break;
            case 3;
                $_SESSION['UrlszUserpass'] = 'ACmilan';
                $bUrl = __URL__ . '/Buycp';
                $this->_boxx($bUrl);
                break;
            case 4;
                $_SESSION['UrlszUserpass'] = 'manlian'; //求购股票
                $bUrl = __URL__ . '/BuycpInfo';
                $this->_boxx($bUrl);
                break;
            case 5:
                $_SESSION['UrlszUserpass'] = 'MyssGuanChanPin';
                $bUrl = __URL__ . '/cptype_index'; //商品分类管理
                $this->_boxx($bUrl);
                break;
            default;
                $this->error('二级密码错误!');
                break;
        }
    }

    //显示产品信息
    public function Cpcontent() {
        //       parent::_initialize();

        $fck = M('fck');
        $product = M('product');
        $PID = (int) $_GET['id'];
        if (empty($PID)) {
            $this->error('错误!');
            exit;
        }
        $map['id'] = $_SESSION[C('USER_AUTH_KEY')];
        $f_rs = $fck->where($map)->find();

        $where = array();
        $where['id'] = $PID;
        $where['yc_cp'] = array('eq', 0);
        $prs = $product->where($where)->field('*')->find();
        if ($prs) {
            $this->assign('prs', $prs);
            $w_money = $prs['a_money'];
            $cc[$prs['id']] = $w_money;
            $this->assign('cc', $cc);

            $canbuy = false;
            if ($prs['cptype'] == 1) {
                if ($f_rs['is_agent'] > 1) {
                    $canbuy = true;
                }
            } else {
                $canbuy = true;
                if($prs['cptype'] == 5){
                   $canbuy = false; 
                }
             }
            $this->assign('canbuy', $canbuy);
            $this->assign('f_rs', $f_rs);
            $this->display('Cpcontent');
        }
    }

    public function Buycp() { //购买产品页
        $cp = M('product');
        $fck = M('fck');
        $map['id'] = $_SESSION[C('USER_AUTH_KEY')];
        $f_rs = $fck->where($map)->find();

        $where = array();
        $ss_type = (int) $_REQUEST['tp'];
        if ($ss_type > 0) {
            $where['cptype'] = array('eq', $ss_type);
        }
        $this->assign('tp', $ss_type);

        $where['yc_cp'] = array('eq', 0);

        $cptype = M('cptype');
        $tplist = $cptype->where('status=0')->order('id asc')->select();
        $this->assign('tplist', $tplist);

        $order = 'id asc';
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $cp->where($where)->count(); //总页数
        $listrows = 20; //每页显示的记录数
        $page_where = 'tp=' . $ss_type; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $cp->where($where)->field($field)->order('id asc')->page($Page->getPage() . ',' . $listrows)->select();
        //=================================================
        foreach ($list as $voo) {
            $w_money = $voo['a_money'];
            $cc[$voo['id']] = $w_money;
        }
        $this->assign('cc', $cc);
        $this->assign('list', $list); //数据输出到模板

        $this->assign('f_rs', $f_rs);
        $this->display('Buycp');
    }

    public function shopCar() {
        $pora = M('product');
        $fck = M('fck');

        $map['id'] = $_SESSION[C('USER_AUTH_KEY')];
        $f_rs = $fck->where($map)->find();
        $agent_xf = $f_rs['agent_xf'];

        $id = $_REQUEST['id'];

        $arr = $_SESSION["shopping"];
        if (empty($arr)) {
            $url = U('Shop/index');
            $this->_box(0, '您的购物车里没有商品！', $url, 1);
            exit;
        }
        $rs = explode('|', $arr);
        $path = ',';
        foreach ($rs as $vo) {
            $str = explode(',', $vo);
            $path .= $str[0] . ',';
            $ids[$str[0]] = $str[1];
        }

        $where['id'] = array('in', '0' . $path . '0');
        $list = $pora->where($where)->select();
        foreach ($list as $lvo) {
            $w_money = $lvo['a_money'];
            //物品总价

            $ep[$lvo['id']] = $ids[$lvo['id']] * $w_money;
            $num[$lvo['id']] = $ids[$lvo['id']];

            //所有商品总价
            $eps += $ids[$lvo['id']] * $w_money;
            $sum += $ids[$lvo['id']];

            $cc[$lvo['id']] = $w_money;
        }

        $bza = $_SESSION["shopping_bz"];
        $blrs = explode("|", $bza);
        $bzz = array();
        foreach ($blrs as $vvv) {
            $vava = explode(",", $vvv);
            $bzz[$vava[0]] = $vava[1];
        }
        $this->assign('agent_xf', $agent_xf);
        $this->assign('bzz', $bzz);
        $this->assign('cc', $cc);
        $this->assign('list', $list);
        $this->assign('path', $path);
        $this->assign('ids', $ids);
        $this->assign('num', $num);
        $this->assign('sum', $sum);
        $this->assign('eps', $eps);
        $this->assign('ep', $ep);

        $this->display('shopCar');
    }

    public function delBuyList() {
        $ID = $_REQUEST['id'];
        $shopping_id = '';
        $arr = $_SESSION["shopping"];

        $rs = explode('|', $arr);
        $path = ',';
        foreach ($rs as $key => $vo) {
            $str = explode(',', $vo);
            if ($str[0] == $ID) {
                unset($rs[$key]);
            } else {
                if (empty($shopping_id)) {
                    $shopping_id = $vo;
                } else {
                    $shopping_id .= '|' . $vo;
                }
            }
        }
        $_SESSION["shopping"] = $shopping_id;
        $this->success("删除成功！", U('Shop/index'));
    }

    public function reset() {
        //清空购物车
        $_SESSION["shopping"] = array();
        $_SESSION["shopping_bz"] = array();
        $this->success("清空完成！", U('Shop/index'));
    }

    public function chang() {
        $ID = $_GET['DID'];
        $nums = $_GET['nums'];
        $arr = $_SESSION["shopping"];
        $rs = explode('|', $arr);
        $shopping_id = '';
        foreach ($rs as $key => $vo) {
            $str = explode(',', $vo);
            if ($str[0] == $ID) {
                $str[1] = $nums;
            }
            $s_id = $str[0] . ',' . $str[1];
            if (empty($shopping_id)) {
                $shopping_id = $s_id;
            } else {
                $shopping_id .= '|' . $s_id;
            }
        }
        $_SESSION["shopping"] = $shopping_id;
    }

    public function chang_bz() {
        $ID = $_GET['DID'];
        $nums = trim($_GET['bzz']);

        if (!empty($nums)) {
            import("@.ORG.KuoZhan");  //导入扩展类
            $KuoZhan = new KuoZhan();
            if ($KuoZhan->is_utf8($nums) == false) {
                $nums = iconv('GB2312', 'UTF-8', $nums);
            }
            unset($KuoZhan);
        }
        if (empty($_SESSION["shopping_bz"])) {
            $_SESSION["shopping_bz"] = $ID . "," . $nums;
        }
        $arr = $_SESSION["shopping_bz"];

        $rs = explode('|', $arr);
        $shopping_id = '';
        $tong = 0;
        foreach ($rs as $key => $vo) {
            $str = explode(',', $vo);
            if ($str[0] == $ID) {
                $tong = 1;
                $str[1] = $nums;
            }
            $s_id = $str[0] . ',' . $str[1];
            if (empty($shopping_id)) {
                $shopping_id = $s_id;
            } else {
                $shopping_id .= '|' . $s_id;
            }
        }
        if ($tong == 0) {
            $shopping_id .= "|" . $ID . "," . $nums;
        }
        $_SESSION["shopping_bz"] = $shopping_id;
    }

    public function ShoppingListAdd() {

        $address = M('address');

        $id = $_SESSION[C('USER_AUTH_KEY')];
//        $aList = $address->where('uid=' . $id)->select();
//        $this->assign('aList', $aList);

        $fck = M('fck');
        $fck_rs = $fck->where('id=' . $id)->find();
        $this->assign('fck_rs', $fck_rs);

        $pora = M('product');
        $arr = $_SESSION["shopping"];
        $rs = explode('|', $arr);
        $path = ',';
        foreach ($rs as $vo) {
            $str = explode(',', $vo);
            $ids[$str[0]] = $str[1];
            $path .= $str[0] . ',';
        }

        $where['id'] = array('in', '0' . $path . '0');
        $list = $pora->where($where)->select();

        foreach ($list as $lvo) {
            $w_money = $lvo['a_money'];
            //物品总价
            $ep[$lvo['id']] = $ids[$lvo['id']] * $w_money;

            //所有商品总价
            $eps += $ids[$lvo['id']] * $w_money;
            $sum += $ids[$lvo['id']];
            if ($lvo['cptype'] == 1 && $fck_rs['is_agent'] == 2) {
                $cash += $ids[$lvo['id']] * $w_money;
//              $shopping_money = 0;
            } else {
                $cash += $ids[$lvo['id']] * $w_money * 0.2;
                $shopping_money += $ids[$lvo['id']] * $w_money * 0.8;
            }

            $cc[$lvo['id']] = $w_money;
        }
        $bza = $_SESSION["shopping_bz"];
        $blrs = explode("|", $bza);
        $bzz = array();
        foreach ($blrs as $vvv) {
            $vava = explode(",", $vvv);
            $bzz[$vava[0]] = $vava[1];
        }
        $this->assign('bzz', $bzz);

        $this->assign('cc', $cc);

        $this->assign('list', $list);
        $this->assign('path', $path);
        $this->assign('ids', $ids);
        $this->assign('sum', $sum);
        $this->assign('eps', $eps);
        $this->assign('ep', $ep);
        $this->assign('uid', $id);
        if (!$shopping_money) {
            $shopping_money = 0;
        }
        $this->assign('shopping_money', $shopping_money);
        $this->assign('cash', $cash);

        $this->display('ShoppingListAdd');
    }

    public function addAddress() {
        $address = M('address');
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $did = $_POST['ID'];

        $name = $_POST['s_name'];
        $are = $_POST['s_address'];
        $tel = $_POST['s_tel'];

        $data['uid'] = $id;
        $data['name'] = $name;
        $data['address'] = $are;
        $data['tel'] = $tel;
        $data['moren'] = 0;

        if (empty($did)) {
            $result = $address->add($data);
        } else {
            $result = $address->where('id=' . $did)->save($data);
        }

        if ($result) {
            $url = __URL__ . '/ShoppingListAdd';
            $this->_box(0, '添加成功！', $url, 1);
            exit;
        } else {
            $this->error('添加失败');
        }
    }

    public function moren() {
        $address = M('address');
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $id = $_GET['ID'];
        $rs = $address->where('id=' . $id)->setField('moren', 1);
        $rs2 = $address->where('id !=' . $id . ' and moren=1')->setField('moren', 0);
        if ($rs && $rs2) {
            echo $id;
        } else {
            echo '0';
        }
    }

    public function addadr() {
        $address = M('address');
        $id = $_GET['ID'];
        $rs = $address->where('id=' . $id)->find();
        $this->assign('rs', $rs);
        $this->assign('did', $id);
        $this->display('addadr');
    }

    public function delAdr() {
        $address = M('address');
        $id = $_GET['ID'];
        $rs = $address->where('id=' . $id)->delete();
        if ($rs) {
            $url = __URL__ . '/ShoppingListAdd';
            $this->_box(1, '删除地址成功！', $url, 1);
            exit;
        } else {
            $this->error('删除失败');
        }
    }

    function beifen() {
        $pora = M('product');
        $address = M('address');
        $fck = D('Fck');

//        $zflx = $_POST['zflx'];
        $prices = $_POST['prices'];

        $arr = $_SESSION["shopping"];
        if (empty($arr)) {
            $this->error("您的购物车里面没有商品！");
            exit;
        }
        $rs = explode('|', $arr);
        $path = ',';
        foreach ($rs as $vo) {
            $str = explode(',', $vo);
            $p_rs = $pora->where('id=' . $str[0] . '')->find();
            if (!$p_rs) {
                $this->error("您所购买的产品暂时没货！");
                exit;
            }
        }
        $rs = explode('|', $arr);
        $path = ',';
        foreach ($rs as $vo) {
            $str = explode(',', $vo);
            $path .= $str[0] . ',';
            $ids[$str[0]] = $str[1];
        }

//        $fck_rs = $fck->where('id=' . $Id)->find();
//
//        $pw = md5(trim($_POST['Password']));
//        if ($fck_rs['passopen'] != $pw) {
//            $this->error('二级密码输入错误!!');
//            exit;
//        }
//		$USerID =trim($_POST['UserID']) ;
//		$fck_rs1 = $fck->where("user_id='$USerID' and  >= 1")->field('id,user_id')->find();
//
//		if(!$fck_rs1){
//			$this->error('专卖店不存在!!');
//			exit;
//		}

        $aid = $_POST['adid'];
        $ars = $address->where('id=' . $aid)->find();
        if (!$ars) {
            $this->error('请您填写收货地址!!');
            exit;
        }
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $order = M('order');
        $post_data = $_POST;
        $data = array();
        $data['uid'] = $post_data['uid'];
        $data['order_amount'] = $post_data['prices'];
        $data['address_id'] = $post_data['adid'];
        $data['total'] = $post_data['nums'];
        $data['status'] = 0;
        $data['create_time'] = time();
        $data['ordersn'] = 'HS' . time() . rand(1000, 9999);
        $res = $order->add($data);
        if ($res) {
            $where = array();
            $where['id'] = array('in', '0' . $path . '0');
            $prs = $pora->where($where)->select();
            $order_goods = M('order_goods');
            foreach ($prs as $vo) {
                $w_money = $vo['a_money'];
                $gwd['goodsid'] = $vo['id'];
                $gwd['price'] = $w_money;
                $gwd['total'] = $ids[$vo['id']];
                $gwd['cprice'] = $ids[$vo['id']] * $w_money;
                $gwd['orderid'] = $res;
                if (!empty($vo['countid'])) {
                    $gwd['countid'] = $vo['countid'];
                }
                $order_goods->add($gwd);
            }
            $_SESSION["shopping"] = array();
            $_SESSION["shopping_bz"] = array();
            $url = U('Gouwu/pay');
            $this->_box(1, '订单提交成功', $url, 1);
        } else {
            $this->error('订单提交失败');
        }
    }

    public function ShopingSave() {
        $id = (int) $_SESSION[C('USER_AUTH_KEY')];
        $pora = M('product');
        $address = M('address');
        $fck = D('Fck');
        $zflx = $_POST['zflx'];
        $prices = $_POST['prices'];
        $shopping_money = $_POST['shopping_money'];
        $cash = $_POST['cash'];
        $nums = $_POST['nums'];
        $arr = $_SESSION["shopping"];

        $this->error("您的购物车里面没有商品！");
        exit;
        
        if (empty($arr)) {
            $this->error("您的购物车里面没有商品！");
            exit;
        }

        $rs = explode('|', $arr);
        $path = ',';
        foreach ($rs as $vo) {
            $str = explode(',', $vo);
            $p_rs = $pora->where('id=' . $str[0] . '')->find();
            if (!$p_rs) {
                $this->error("您所购买的产品暂时没货！");
                exit;
            }
        }
        $rs = explode('|', $arr);
        $path = ',';
        foreach ($rs as $vo) {
            $str = explode(',', $vo);
            $path .= $str[0] . ',';
            $ids[$str[0]] = $str[1];
        }

        $fck_rs = $fck->where('id=' . $id)->find();
        $where['uid'] = $fck_rs['shop_id'];
        $ress_id = $address->where($where)->getField('id');

        if ($zflx == 0) {
            if ($fck_rs['agent_xf'] < $shopping_money) {
                $this->error("您的购物币余额不足！");
            }
            if ($fck_rs['agent_cf'] < $cash) {
                $surplus = $cash - $fck_rs['agent_cf'];
                if ($surplus > $fck_rs['agent_cash']) {
                    $this->error("您的奖金与现金不足请充值");
                }
            }
        } else {
            $this->error("暂不支持该支付方式");
        }

        $data = array();
        $data['uid'] = $id;
        $data['user_id'] = $fck_rs['user_id'];
        $data['ordersn'] = 'DR' . time() . rand(1111, 9999);
        $data['address_id'] = $ress_id; //专卖店收货地址ID
        $data['order_amount'] = $prices;
        $data['create_time'] = time();
        $data['status'] = 1; //订单状态 0/1/2/3/4:无效/下单/确认付款/发货/确认收货
        $data['shopping_money'] = $shopping_money; //购物币 
        $data['cash'] = $cash; //现金
        $data['is_pay'] = 0;
        $data['total'] = $nums;
        $order = M('order');
        $orderid = $order->add($data);

        if ($orderid) {
            $gwd = array();

            $where = array();
            $where['id'] = array('in', '0' . $path . '0');
            $prs = $pora->where($where)->select();
            /*
              foreach ($prs as $vo) {
              $w_money = $vo['a_money'];
              $gwd['did'] = $vo['id'];
              $gwd['money'] = $w_money;
              $gwd['shu'] = $ids[$vo['id']];
              $gwd['cprice'] = $ids[$vo['id']] * $w_money;
              $gwd['sh_money'] = $gwd['cprice'] * 0.8;
              $gwd['cash'] = $gwd['cprice'] * 0.2;
              $gwd['pd_date'] = date('Y-m');
              if (!empty($vo['countid'])) {
              $gwd['countid'] = $vo['countid'];
              }
              $gouwu->add($gwd);
              }
             */
            $gouwu = M('order_goods');
            $gwd['orderid'] = $orderid;
            foreach ($prs as $vo) {
                $w_money = $vo['a_money'];
                $gwd['goodsid'] = $vo['id'];
                $gwd['total'] = $ids[$vo['id']];
                $gwd['price'] = $ids[$vo['id']] * $w_money;
                $gwd['cptype'] = $vo['cptype'];
                $gouwu->add($gwd);
            }
//            if ($zflx == 0) {
            //开启事务
//            $fck->startTrans();
//            $rs = $fck->where('id=' . $id)->setDec('agent_xf', $shopping_money);
//            if ($rs) {
//                $this->deduct_voucher($shopping_money, $id);
//                if ($fck_rs['agent_cf'] < $cash) {
//                    $surplus = $cash - $fck_rs['agent_cf'];
//                    $fck->query("update __TABLE__ set agent_cf=0 where id=" . $id);
//                    $fck->query("update __TABLE__ set agent_cash=agent_cash-" . $surplus . " where id=" . $id);
//                } else {
//                    $fck->query("update __TABLE__ set agent_cf=agent_cf-" . $cash . " where id=" . $id);
//                }
//                $fck->commit();
//            } else {
//                $fck->rollback();
//                $this->error("支付失败");
//            }
            $_SESSION["shopping"] = '';
            $_SESSION["shopping_bz"] = '';
            $url = __URL__ . '/BuycpInfo/';
            $this->_box(1, '购买成功！', $url, 1);
            exit;
        } else {
            $this->error("购买失败！");
        }
    }

    public function BuycpInfo() {//购买信息
        $this->display('BuycpInfo');
    }

    function BuycpInfoJson() {

        $order = M('order');
        $id = $_SESSION[C('USER_AUTH_KEY')];
        $map['uid'] = $id;
        $params = $this->getParams();

        $UserID = $params['qvalue'];
        if (!empty($UserID)) {
            $map['user_id'] = array('like', "%" . $UserID . "%");
        }

        //查询字段
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $order->where($map)->count(); //总页数
        $listrows = $params['limit']; //每页显示的记录数
        $nowPage = $params['offset'] / $params['limit'] + 1;

        $Page = new ZQPage($count, $listrows, 1, 0, 3, '', $nowPage);
        //===============(总页数,每页显示记录数,css样式 0-9)


        $list = $order->where($map)->field($field)->order('id desc')->page($Page->getPage() . ',' . $listrows)->select();

        show_list_json(40000, $list, $count, $nowPage);
    }

    public function BuycpInfoAC() {
        //处理提交按钮
        $action = $_POST['action'];
        //获取复选框的值
        $XGid = $_POST['tabledb'];
        if (!isset($XGid) || empty($XGid)) {
            $bUrl = __URL__ . '/adminLogistics';
            $this->_box(0, '请选择货物！', $bUrl, 1);
            exit;
        }
        switch ($action) {
            case '确定收货';
                $this->_BuycpInfoDone($XGid);
                break;
            case '申请退货';
                $this->_BuycpInfoCancel($XGid);
                break;
            default;
                $bUrl = __URL__ . '/adminLogistics';
                $this->_box(0, '没有该货物！', $bUrl, 1);
                break;
        }
    }

    private function _BuycpInfoDone($XGid) {
        //确定收货
        if ($_SESSION['UrlszUserpass'] == 'manlian') {
            $shopping = M('gouwu');

            $where1 = array();
            $where1['id'] = array('in', $XGid);
            $where1['isfh'] = array('eq', 0);

            $where = array();
            $where['id'] = array('in', $XGid);
            $where['ispay'] = array('eq', 0);

            $rs = $shopping->where($where1)->select();
            if ($rs) {
                $valuearray1 = array(
                    'isfh' => '1',
                    'fhdt' => mktime()
                );

                $valuearray = array(
                    'ispay' => '1',
                    'okdt' => mktime()
                );

                $shopping->where($where1)->setField($valuearray1);
                $shopping->where($where)->setField($valuearray);
                unset($shopping, $where1, $where);

                $bUrl = __URL__ . '/BuycpInfo';
                $this->_box(1, '确认收货成功！', $bUrl, 1);
                exit;
            } else {
                $bUrl = __URL__ . '/BuycpInfo';
                $this->_box(1, '确认收货失败！', $bUrl, 1);
                exit;
            }
        } else {
            $this->error('错误!');
            exit;
        }
    }

    private function _BuycpInfoCancel($XGid) {
        //申请退货
        if ($_SESSION['UrlszUserpass'] == 'manlian') {
            $shopping = M('gouwu');

            $where = array();
            $where['id'] = array('in', $XGid);
            $where['ispay'] = array('eq', 0);

            $valuearray = array(
                'ispay' => '2',
                'okdt' => mktime()
            );

            $shopping->where($where)->setField($valuearray);
            unset($shopping, $where);

            $bUrl = __URL__ . '/BuycpInfo';
            $this->_box(1, '申请退货成功！', $bUrl, 1);
            exit;
        } else {
            $this->error('错误!');
            exit;
        }
    }

    //产品表查询
    public function pro_index() {
        $this->_Admin_checkUser();
//		if ($_SESSION['UrlszUserpass'] == 'MyssGuanChanPin'){
        $product = M('product');
        $title = $_REQUEST['stitle'];
        $map = array();
        if (strlen($title) > 0) {
            $map['name'] = array('like', '%' . $title . '%');
        }
        $map['id'] = array('gt', 0);
        $orderBy = 'create_time desc,id desc';
        $field = '*';
        //=====================分页开始==============================================
        import("@.ORG.ZQPage");  //导入分页类
        $count = $product->where($map)->count(); //总页数
        $listrows = C('ONE_PAGE_RE'); //每页显示的记录数
        $listrows = 10; //每页显示的记录数
        $page_where = 'stitle=' . $title; //分页条件
        $Page = new ZQPage($count, $listrows, 1, 0, 3, $page_where);
        //===============(总页数,每页显示记录数,css样式 0-9)
        $show = $Page->show(); //分页变量
        $this->assign('page', $show); //分页变量输出到模板
        $list = $product->where($map)->field($field)->order($orderBy)->page($Page->getPage() . ',' . $listrows)->select();
        $this->assign('list', $list); //数据输出到模板
        //=================================================

        $this->display();
//		}else{
//            $this->error('错误!');
//        }
    }

    //产品表显示修改
    public function pro_edit() {
        $this->_Admin_checkUser();
        $EDid = $_GET['EDid'];
        $field = '*';
        $product = M('product');
        $where = array();
        $where['id'] = $EDid;
        $rs = $product->where($where)->field($field)->find();
        if ($rs) {
            $this->assign('rs', $rs);
            $this->us_fckeditor('content', $rs['content'], 400, "96%");

//            $cptype = M('cptype');
//            $list = $cptype->where('status=0')->order('id asc')->select();
//            $this->assign('list', $list);
            /*
             * 分类select
             */
            $mode = M('shop_category');
            $map = array();
            $map['id'] = array('gt', 0);
            $orderBy = 'id asc';
            $field = '*';
            $goods_data = $mode->where($map)->field($field)->order($orderBy)->select();
            $tmp = tree($goods_data, 0, 0, '--');
            $select .= "<select name='cptype' id='cptype' class='form-control'>";
            foreach ($tmp as $v) {
                $select .= '<option  value=' . $v['id'] . '>' . $v['html'] . $v['name'] . '</option>';
            }
            $select .= '</select>';

            $this->assign('select', $select);
            $this->display();
        } else {
            $this->error('没有该信息！');
            exit;
        }
    }

    //产品删除
    function pro_del() {

        $delid = $_GET['delid'];
        $product = M('product');
        $data['id'] = $delid;
        $data['is_del'] = 1;
        $rs = $product->save($data);

        if (!$rs) {
            $this->error('删除失败！');
            exit;
        }
        $bUrl = U('Shop/manager');
        $this->_box(1, '操作成功', $bUrl, 1);
        exit;
    }

    //产品表修改保存
    public function pro_edit_save() {
        $this->_Admin_checkUser();
        $product = M('product');
        $data = array();
        //h 函数转换成安全html
        $money = trim($_POST['money']);
        $a_money = $_POST['a_money'];
//        $b_money = $_POST['b_money'];
        $content = stripslashes($_POST['content']);
        $name = trim($_POST['name']);
        $cid = trim($_POST['cid']);
        $img = $_POST['img'];
        $ctime = trim($_POST['ctime']);
        $ccname = $_POST['ccname'];
        $xhname = $_POST['xhname'];
        $countid = $_POST['countid'];
        $cptype = trim($_POST['cptype']);
        $isreg = trim($_POST['isreg']);
        $stock = trim($_POST['stock']);
        $cptype = (int) $cptype;
        $ctime = strtotime($ctime);
        if (empty($name)) {
            $this->error('标题不能为空!');
            exit;
        }
        if (empty($cid)) {
            $this->error('商品编号不能为空!');
            exit;
        }
//        dump($isreg);
// 		if (empty($countid)){
// 			$this->error('结算账号不能为空!');
// 			exit;
// 		}
//		if (empty($xhname)){
//			$this->error('商品型号不能为空!');
//			exit;
//		}
        if (empty($money) || !is_numeric($money) || empty($a_money) || !is_numeric($a_money)) {
            $this->error('价格不能为空!');
            exit;
        }
        if ($money <= 0 || $a_money <= 0) {
            $this->error('输入的价格有误!');
            exit;
        }

        if (!empty($ctime)) {
            $data['create_time'] = $ctime;
        }
        if ($stock < 1 || empty($stock)) {
            $this->error('输入的库存有误!');
            exit;
        }
        $data['cid'] = $cid;
        $data['ccname'] = $ccname;
        $data['xhname'] = $xhname;
        $data['money'] = $money;
        $data['a_money'] = $a_money;
        $data['b_money'] = $b_money;
        $data['name'] = $name;
        $data['content'] = $content;
        $data['cptype'] = $cptype;
        $data['is_reg'] = $isreg;
        $data['stock'] = $stock;
        $data['img'] = $img;
// 		$data['countid'] = $countid;
        $data['id'] = $_POST['id'];

        $rs = $product->save($data);
        if (!$rs) {
            $this->error('编辑失败！');
            exit;
        }
        $bUrl = U('Shop/manager');
        $this->_box(1, '操作成功', $bUrl, 1);
        exit;
    }

    function pro_add() {
        $mode = M('shop_category');
        $map = array();
        $map['id'] = array('gt', 0);
        $orderBy = 'id asc';
        $field = '*';
        $goods_data = $mode->where($map)->field($field)->order($orderBy)->select();
        $tmp = tree($goods_data, 0, 0, '--');
        $select .= "<select name='cptype' id='rank' class='form-control'>";
        foreach ($tmp as $v) {
            $select .= '<option value=' . $v['id'] . '>' . $v['html'] . $v['name'] . '</option>';
        }
        $select .= '</select>';

        $this->assign('select', $select);
        $this->us_fckeditor('content', "", 400, "96%");
        $this->display('pro_add');
    }

    //产品表操作（启用禁用删除）
    public function pro_zz() {
        $this->_Admin_checkUser();
        //处理提交按钮
        $action = $_POST['action'];
        //获取复选框的值
        $PTid = $_POST["checkbox"];
        if ($action == '添加') {

            $cptype = M('cptype');
            $list = $cptype->where('status=0')->order('id asc')->select();
            $this->assign('list', $list);

            $this->us_fckeditor('content', "", 400, "96%");

            $this->display('pro_add');
            exit;
        }
        $product = M('product');
        switch ($action) {
            case '删除';
                $wherea = array();
                $wherea['id'] = array('in', $PTid);
                $rs = $product->where($wherea)->delete();
                if ($rs) {
                    $bUrl = __URL__ . '/pro_index';
                    $this->_box(1, '操作成功', $bUrl, 1);
                    exit;
                } else {
                    $bUrl = __URL__ . '/pro_index';
                    $this->_box(0, '操作失败', $bUrl, 1);
                }
                break;
            case '屏蔽产品';
                $wherea = array();
                $wherea['id'] = array('in', $PTid);
                $rs = $product->where($wherea)->setField('yc_cp', 1);
                if ($rs) {
                    $bUrl = __URL__ . '/pro_index';
                    $this->_box(1, '屏蔽产品成功', $bUrl, 1);
                    exit;
                } else {
                    $bUrl = __URL__ . '/pro_index';
                    $this->_box(0, '屏蔽产品失败', $bUrl, 1);
                }
                break;
            case '解除屏蔽';
                $wherea = array();
                $wherea['id'] = array('in', $PTid);
                $rs = $product->where($wherea)->setField('yc_cp', 0);
                if ($rs) {
                    $bUrl = __URL__ . '/pro_index';
                    $this->_box(1, '解除屏蔽成功', $bUrl, 1);
                    exit;
                } else {
                    $bUrl = __URL__ . '/pro_index';
                    $this->_box(0, '解除屏蔽失败', $bUrl, 1);
                }
                break;
            default;
                $bUrl = __URL__ . '/pro_index';
                $this->_box(0, '操作失败', $bUrl, 1);
                break;
        }
    }

    //产品表添加保存
    public function pro_inserts() {
        $this->_Admin_checkUser();
        $product = M('product');

        $data = array();
        //h 函数转换成安全html
        $content = trim($_POST['content']);
        $name = trim($_POST['name']);
        $cid = trim($_POST['cid']);
        $image = trim($_POST['img']);
        $money = $_POST['money'];
        $a_money = $_POST['a_money'];
//        $b_money = $_POST['b_money'];
        $ccname = $_POST['ccname'];
        $xhname = $_POST['xhname'];
        $countid = $_POST['countid'];
        $cptype = trim($_POST['cptype']);
        $isreg = trim($_POST['isreg']);
        $stock = trim($_POST['stock']);


        if (empty($name)) {
            $this->error('商品名称不能为空!');
            exit;
        }
        if (empty($cid)) {
            $this->error('商品编号不能为空!');
            exit;
        }
// 		if (empty($countid)){
// 			$this->error('商品结算账号不能为空!');
// 			exit;
// 		}
//		if (empty($xhname)){
//			$this->error('商品型号不能为空!');
//			exit;
//		}
        if (empty($money) || !is_numeric($money) || empty($a_money) || !is_numeric($a_money)) {
            $this->error('价格不能为空!');
            exit;
        }
        if ($money <= 0 || $a_money <= 0) {
            $this->error('输入的价格有误!');
            exit;
        }
        if ($stock < 1 || empty($stock)) {
            $this->error('输入的库存有误');
            exit;
        }

        $data['name'] = $name;
        $data['cid'] = $cid;
        $data['content'] = stripslashes($content);
        $data['img'] = $image;
        $data['create_time'] = mktime();
        $data['money'] = $money;
        $data['a_money'] = $a_money;
        $data['stock'] = $stock;
//        $data['b_money'] = $b_money;
        $data['ccname'] = $ccname;
        $data['xhname'] = $xhname;
// 		$data['countid'] = $countid;
        $data['cptype'] = $cptype;
        $data['is_reg'] = $isreg;
        $form_rs = $product->add($data);
        if (!$form_rs) {
            $this->error('添加失败');
            exit;
        }
        $bUrl = U('Shop/manager');
        $this->_box(1, '操作成功', $bUrl, 1);
        exit;
    }

    public function cptype_index() {
        $this->_Admin_checkUser();
        if ($_SESSION['UrlszUserpass'] == 'MyssGuanChanPin') {
            $product = M('shop_category');
            $map = array();
            $map['id'] = array('gt', 0);
            $orderBy = 'id asc';
            $field = '*';
            $children = array();
            $list = $product->where($map)->field($field)->order($orderBy)->select();
            foreach ($list as $index => $row) {
                if (!empty($row['parentid'])) {
                    $children[$row['parentid']][] = $row;
                    unset($list[$index]);
                }
            }
            $this->assign('children', $children);
            $this->assign('list', $list); //数据输出到模板
            //=================================================

            $this->display();
        } else {
            $this->error('错误!');
        }
    }

    public function cptype_edit() {
        $this->_Admin_checkUser();
        $EDid = $_GET['id'];
        $field = '*';
        $product = M('shop_category');
        $where = array();
        $where['id'] = $EDid;
        $rs = $product->where($where)->field($field)->find();
        if ($rs) {
            $this->assign('rs', $rs);

            $pid = $rs['parentid'];
            if ($pid > 0) {
                $field = '*';
                $product = M('shop_category');
                $where = array();
                $where['id'] = $pid;
                $rs = $product->where($where)->field($field)->find();
                $this->assign('parent', $rs);
                $this->assign('parentid', $pid);
            }

            $this->display();
        } else {
            $this->error('没有该信息！');
            exit;
        }
    }

    public function cptype_edit_save() {
        $this->_Admin_checkUser();
        $cptype = M('shop_category');
        $name = trim($_POST['name']);
        if (empty($name)) {
            $this->error('分类名不能为空!');
            exit;
        }
        $data = array();
        $data['name'] = $name;
        $data['id'] = $_POST['id'];
        $data['displayorder'] = $_POST['displayorder'];
        $data['description'] = $_POST['description'];
        $rs = $cptype->save($data);
        if (!$rs) {
            $this->error('编辑失败！');
            exit;
        }
        //更新成功从数据库取数据写到文件缓存
        $mode = M('shop_category');
        $map = array();
        $map['id'] = array('gt', 0);
        $orderBy = 'id asc';
        $field = '*';
        $children = array();
        $goods_data = $mode->where($map)->field($field)->order($orderBy)->select();
        foreach ($goods_data as $index => $row) {
            if (!empty($row['parentid'])) {
                $children[$row['parentid']][] = $row;
                unset($goods_data[$index]);
            }
        }
        $a = array('a' => $goods_data, 'b' => $children);
        file_put_contents('cate_menu.txt', json_encode($a));
        $bUrl = __URL__ . '/cptype_index';
        $this->_box(1, '操作成功', $bUrl, 1);
        exit;
    }

    public function cptype_add() {
        $this->_Admin_checkUser();
        $pid = intval($_GET['parentid']);
        if ($pid > 0) {
            $field = '*';
            $product = M('shop_category');
            $where = array();
            $where['id'] = $pid;
            $rs = $product->where($where)->field($field)->find();
            $this->assign('parent', $rs);
        }
        $this->assign('parentid', $pid);
        $this->display();
    }

    public function cptype_del() {
        $this->_Admin_checkUser();
        $EDid = $_GET['id'];
        $field = '*';
        $product = M('shop_category');
        $where = array();
        $where['id'] = $EDid;
        $rs = $product->where($where)->delete();
        $bUrl = __URL__ . '/cptype_index';
        if ($rs) {
            //删除成功从数据库取数据写到文件缓存
            $mode = M('shop_category');
            $map = array();
            $map['id'] = array('gt', 0);
            $orderBy = 'id asc';
            $field = '*';
            $children = array();
            $goods_data = $mode->where($map)->field($field)->order($orderBy)->select();
            foreach ($goods_data as $index => $row) {
                if (!empty($row['parentid'])) {
                    $children[$row['parentid']][] = $row;
                    unset($goods_data[$index]);
                }
            }
            $a = array('a' => $goods_data, 'b' => $children);
            file_put_contents('cate_menu.txt', json_encode($a));

            $this->success('操作完成', $bUrl, 0);
        } else {
            $this->error('没有该信息！', $bUrl, 0);
            exit;
        }
    }

    //处理
    public function cptype_zz() {
        $this->_Admin_checkUser();
        //处理提交按钮
        $action = $_POST['action'];
        //获取复选框的值
        $PTid = $_POST["checkbox"];
        if ($action == '添加') {
            $this->display('cptype_add');
            exit;
        }
        $product = M('cptype');
        switch ($action) {
            case '删除';
                $wherea = array();
                $wherea['id'] = array('in', $PTid);
                $rs = $product->where($wherea)->delete();
                if ($rs) {
                    $bUrl = __URL__ . '/cptype_index';
                    $this->_box(1, '操作成功', $bUrl, 1);
                    exit;
                } else {
                    $bUrl = __URL__ . '/cptype_index';
                    $this->_box(0, '操作失败', $bUrl, 1);
                }
                break;
            default;
                $bUrl = __URL__ . '/cptype_index';
                $this->_box(0, '操作失败', $bUrl, 1);
                break;
        }
    }

    //产品分类加保存
    public function cptype_inserts() {
        $this->_Admin_checkUser();
        $product = M('shop_category');
        $name = trim($_POST['name']);
        $parentid = $_POST['parentid'];
        $enabled = $_POST['enabled'];
        $description = trim($_POST['description']);
        $displayorder = trim($_POST['displayorder']);

        if (empty($name)) {
            $this->error('分类名不能为空!');
            exit;
        }
        if ($parentid == null) {
            $this->error('请选择上级分类');
            exit;
        }
        if (empty($enabled)) {
            $this->error('请选择是否显示');
            exit;
        }
        $data = array();
        $data['name'] = $name;
        $data['parentid'] = $parentid;
        $data['description'] = $description;
        $data['enabled'] = $enabled;
        $data['displayorder'] = $displayorder;
        $form_rs = $product->add($data);
        if (!$form_rs) {
            $this->error('添加失败');
            exit;
        }
//添加成功从数据库取数据写到文件缓存
        $mode = M('shop_category');
        $map = array();
        $map['id'] = array('gt', 0);
        $orderBy = 'id asc';
        $field = '*';
        $children = array();
        $goods_data = $mode->where($map)->field($field)->order($orderBy)->select();
        foreach ($goods_data as $index => $row) {
            if (!empty($row['parentid'])) {
                $children[$row['parentid']][] = $row;
                unset($goods_data[$index]);
            }
        }
        $a = array('a' => $goods_data, 'b' => $children);
        file_put_contents('cate_menu.txt', json_encode($a));
        $bUrl = __URL__ . '/cptype_index';
        //      $this->_box(1, '操作成功', $bUrl, 1);
        $this->success('操作完成', $bUrl, 0);
        exit;
    }

    public function adminLogistics() {
        $this->_Admin_checkUser(); //后台权限检测
        //物流管理
        if ($_SESSION['UrlszUserpass'] == 'MyssWuliuList') {

            $title = '物流管理';
            $this->assign('title', $title);
            $this->display('adminLogistics');
        } else {
            $this->error('错误!');
            exit;
        }
    }

    public function adminLogisticsajax() {
        $this->_Admin_checkUser(); //后台权限检测
        //物流管理
        if ($_SESSION['UrlszUserpass'] == 'MyssWuliuList') {
            $order = M('order');

            $map = array();
            $params = $this->getParams();
            $map['status'] = array('neq', 0);

            $UserID = $params['qvalue'];
            if (!empty($UserID)) {
                $map['user_id'] = array('like', "%" . $UserID . "%");
            }

            //查询字段
            $field = '*';
            //=====================分页开始==============================================
            import("@.ORG.ZQPage");  //导入分页类
            $count = $order->where($map)->count(); //总页数
            $listrows = $params['limit']; //每页显示的记录数
            $nowPage = $params['offset'] / $params['limit'] + 1;

            $Page = new ZQPage($count, $listrows, 1, 0, 3, '', $nowPage);
            //===============(总页数,每页显示记录数,css样式 0-9)


            $list = $order->where($map)->field($field)->order('id desc')->page($Page->getPage() . ',' . $listrows)->select();

            show_list_json(40000, $list, $count, $nowPage);
        } else {
            $this->error('错误!');
            exit;
        }
    }

    public function adminLogisticsAC() {
        //处理提交按钮
        $action = $_POST['action'];
        //获取复选框的值
        $XGid = $_POST['tabledb'];
        if (!isset($XGid) || empty($XGid)) {
            $bUrl = __URL__ . '/adminLogistics';
            $this->_box(0, '请选择货物！', $bUrl, 1);
            exit;
        }
        switch ($action) {
            case '确认发货';
                $this->_adminLogisticsOK($XGid);
                break;
            case '确定收货';
                $this->_adminLogisticsDone($XGid);
                break;
            case '删除';
                $this->_adminLogisticsDel($XGid);
                break;
            default;
                $bUrl = __URL__ . '/adminLogistics';
                $this->_box(0, '没有该货物！', $bUrl, 1);
                break;
        }
    }

    private function _adminLogisticsOK($XGid) {
        //确定发货
        if ($_SESSION['UrlszUserpass'] == 'MyssWuliuList') {
            $shopping = M('gouwu');
            $where = array();
            $where['id'] = array('in', $XGid);
            $where['isfh'] = array('eq', 0);

            $valuearray = array(
                'isfh' => '1',
                'fhdt' => mktime()
            );
            $shopping->where($where)->setField($valuearray);
            unset($shopping, $where);

            $bUrl = __URL__ . '/adminLogistics';
            $this->_box(1, '发货成功！', $bUrl, 1);
            exit;
        } else {
            $this->error('错误!');
        }
    }

    private function _adminLogisticsDone($XGid) {
        //确定收货
        if ($_SESSION['UrlszUserpass'] == 'MyssWuliuList') {
            $shopping = M('gouwu');

            $where1 = array();
            $where1['id'] = array('in', $XGid);
            $where1['isfh'] = array('eq', 0);

            $where = array();
            $where['id'] = array('in', $XGid);
            $where['ispay'] = array('eq', 0);



            $valuearray1 = array(
                'isfh' => '1',
                'fhdt' => mktime()
            );

            $valuearray = array(
                'ispay' => '1',
                'okdt' => mktime()
            );

            $shopping->where($where1)->setField($valuearray1);
            $shopping->where($where)->setField($valuearray);
            unset($shopping, $where1, $where);

            $bUrl = __URL__ . '/adminLogistics';
            $this->_box(1, '确认收货成功！', $bUrl, 1);
            exit;
        } else {
            $this->error('错误!');
            exit;
        }
    }

    private function _adminLogisticsDel($XGid) {
        //删除
        if ($_SESSION['UrlszUserpass'] == 'MyssWuliuList') {
            $shopping = M('gouwu');
            $where = array();
            $where['id'] = array('in', $XGid);
            $shopping->where($where)->delete();
            unset($shopping, $where);

            $bUrl = __URL__ . '/adminLogistics';
            $this->_box(1, '删除成功！', $bUrl, 1);
            exit;
        } else {
            $this->error('错误!');
        }
    }

//支付页面
    function pay() {
        $this->display();
    }

    public function payAC() {
        $this->ajaxReturn($data);
    }

    /**
     * 上传图片
     * * */
    public function upload_fengcai_pp() {
        $this->_Admin_checkUser(); //后台权限检测
        if (!empty($_FILES)) {
            //如果有文件上传 上传附件
            $this->_upload_fengcai_pp();
        }
    }

    protected function _upload_fengcai_pp() {
        header("content-type:text/html;charset=utf-8");
        $this->_Admin_checkUser(); //后台权限检测
        // 文件上传处理函数
        //载入文件上传类
        import("@.ORG.UploadFile");
        $upload = new UploadFile();

        //设置上传文件大小
        $upload->maxSize = 1048576 * 2; // TODO 50M   3M 3292200 1M 1048576
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg');

        //设置附件上传目录
        $upload->savePath = './Public/Uploads/image/';

        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;

        //设置需要生成缩略图的文件前缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '800';

        //设置缩略图最大高度
        $upload->thumbMaxHeight = '600';

        //设置上传文件规则
//		$upload->saveRule = uniqid;
        $upload->saveRule = date("Y") . date("m") . date("d") . date("H") . date("i") . date("s") . rand(1, 100);

        //删除原图
        $upload->thumbRemoveOrigin = true;

        if (!$upload->upload()) {
            //捕获上传异常
            $error_p = $upload->getErrorMsg();
            echo "<script>alert('" . $error_p . "');history.back();</script>";
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            $U_path = $uploadList[0]['savepath'];
            $U_nname = $uploadList[0]['savename'];
//            $U_inpath = (str_replace('./Public/', '__PUBLIC__/', $U_path)) . $U_nname;
//            $U_inpath = $U_path . $U_nname;
            $U_inpath = (str_replace('./Public/', '/Public/', $U_path)) . $U_nname;

            echo "<script>window.parent.form1.img.value='" . $U_inpath . "';</script>";
            echo "<span style='font-size:12px;'>上传完成！</span>";
            exit;
        }
    }

    /*
     * 确认付款
     */

    function payment() {
        $data = array();
        $dids = $_POST['dids'];
        $fck = M('fck');
        if (!$dids) {
            $msg = '请选择订单';
            $code = 0;
            $this->ajaxReturn($data, $msg, $code);
        }
        $order = M('order');
        $where = array();
        $where['is_pay'] = 0;
        $where['id'] = array('in', $dids);
        $order_info = $order->where($where)->select();
        if (!$order_info) {
            $msg = '该订单已审核';
            $code = 0;
            $this->ajaxReturn($data, $msg, $code);
        }
        foreach ($order_info as $k => $vo) {
            $map['id'] = $vo['uid'];
            $fck_rs = $fck->where($map)->find();
            $shopping_money = $vo['shopping_money'];
            if ($fck_rs['agent_xf'] < $shopping_money) {
                $msg .= $fck_rs['user_id'] . '购物币不足' . ',';
            } else {
                //判断奖金是否小于要扣的现金
                if ($fck_rs['agent_cf'] < $vo['cash']) {
                    $surplus = $vo['cash'] - $fck_rs['agent_cf'];
                    if ($surplus > $fck_rs['agent_cash']) {
                        $msg .= $fck_rs['user_id'] . '奖金与现金不足' . ',';
                    } else {
                        $fck->startTrans();
                        $fck->where('id=' . $vo['uid'])->setDec('agent_xf', $shopping_money);
                        $this->deduct_voucher($shopping_money, $vo['uid']);

                        $fck->execute("update __TABLE__ set agent_cf=0 where id=" . $vo['uid']);
                        $fck->execute("update __TABLE__ set agent_cash=agent_cash-" . $surplus . " where id=" . $vo['uid']);
                        $save_map['id'] = $vo['id'];
                        $save_map['is_pay'] = 1;
                        $save_map['pay_time'] = time();
                        $order->save($save_map);
                        $fck->commit();
                        $msg .= $fck_rs['user_id'] . '通过' . ',';
                    }
                } else {
                    $fck->startTrans();
                    $fck->where('id=' . $vo['uid'])->setDec('agent_xf', $shopping_money);

                    $this->deduct_voucher($shopping_money, $vo['uid']);
                    $fck->execute("update __TABLE__ set agent_cf=agent_cf-" . $vo['cash'] . " where id=" . $vo['uid']);
                    $save_map['id'] = $vo['id'];
                    $save_map['is_pay'] = 1;
                    $save_map['pay_time'] = time();
                    $order->save($save_map);
                    $fck->commit();
                    $msg .= $fck_rs['user_id'] . '通过' . ',';
                }
            }
        }
        $code = 40000;
        $this->ajaxReturn($data, $msg, $code);
    }

     /*
     * 发货处理
     */

    function deliverGoods() {
        $id = $_POST['sn'];

        $sn_number = trim($_POST['sn_number']);
        $data = '';
        if (!$sn_number) {

            $msg = '请填写订单号';
            $code = 0;
        } else {
            $order_data['id'] = $id;
            $order_data['is_delive'] = 1;
            $order_data['delive_time'] = time();
            $order_data['logi_no'] = $sn_number;
            $res = M('order')->save($order_data);
        }
        if ($res) {

            $msg = '发货成功';
            $code = 40000;
        } else {

            $msg = '发货失败';
            $code = 0;
        }
        $this->ajaxReturn($data, $msg, $code);
    }

    /*
     * 查看详情
     */

    function orderDetails() {
        $id = $_GET['orderid'];
        $map['id'] = $id;
        $list = M('order')->where($map)->find();
        $where['id'] = $list['address_id'];
        $list['address_id'] = M('address')->where($where)->find();
        if ($list) {
            $glist = M('order_goods')->query('select * from xt_order_goods g join xt_product p on g.goodsid=p.id where g.orderid=' . $id);
            $list['create_time'] = date('Y-m-d H:i:s', $list['create_time']);
            foreach ($glist as $k => $vo) {
                switch ($vo['cptype']) {
                    case 1:
                        $cptype = '会员资格区';
                        break;
                    case 2:
                        $cptype = '复消区';
                        break;
                    case 3:
                        $cptype = '会员购物区';
                        break;
                    case 4:
                        $cptype = '促销区';
                        break;
                    default :
                        $cptype = '未知';
                }

                $glist[$k]['cptype'] = $cptype;
            }

            $this->assign('glist', $glist);
        }

        $this->assign('list', $list);
        $this->display();
    }

}

?>
