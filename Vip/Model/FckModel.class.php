<?php
class FckModel extends CommonModel {
	//数据库名称

   public function xiangJiao($Pid=0,$DanShu=1,$fnum=0){
        //========================================== 往上统计单数
		$fnum++;
        $where = array();
        $where['id'] = $Pid;
        $field = 'treeplace,father_id';
        $vo = $this ->where($where)->field($field)->find();
        if ($vo){
            $Fid = $vo['father_id'];
            $TPe = $vo['treeplace'];
            $table = $this->tablePrefix.'fck';
            if ($TPe == 0 && $Fid > 0){
                $this->execute("update ". $table ." Set `l`=l+$DanShu, `shangqi_l`=shangqi_l+$DanShu  where `id`=".$Fid);
            }elseif($TPe == 1 && $Fid > 0){
                $this->execute("update ". $table ." Set `r`=r+$DanShu, `shangqi_r`=shangqi_r+$DanShu  where `id`=".$Fid);
            }elseif($TPe == 2 && $Fid > 0){
                $this->execute("update ". $table ." Set `lr`=lr+$DanShu, `shangqi_lr`=shangqi_lr+$DanShu  where `id`=".$Fid);
            }
            if ($Fid > 0) $this->xiangJiao($Fid,$DanShu,$fnum);
        }
        unset($where,$field,$vo);
    }
    
	
	
	public function shangjiaTJ($ppath,$treep=0){
		$where = "id in (0".$ppath."0)";
		$lirs = $this->where($where)->order('p_level desc')->field('id,treeplace')->select();
		foreach($lirs as $lrs){
			$myid = $lrs['id'];
			$mytp = $lrs['treeplace'];
			if($treep==0){
				$this->execute("update __TABLE__ Set `re_nums_l`=re_nums_l+1,`re_nums_b`=re_nums_b+1 where `id`=".$myid);
			}else{
				$this->execute("update __TABLE__ Set `re_nums_r`=re_nums_r+1,`re_nums_b`=re_nums_b+1 where `id`=".$myid);
			}
			$treep = $mytp;
		}
		unset($lirs,$lrs,$where);
    }

//	public function xiangJiao($Pid=0,$DanShu=1,$plv=0,$op=1){
//        //========================================== 往上统计单数【有层碰奖】
//
//        $peng = M ('peng');
//        $where = array();
//        $where['id'] = $Pid;
//        $field = 'treeplace,father_id,p_level';
//        $vo = $this ->where($where)->field($field)->find();
//        if ($vo){
//            $Fid = $vo['father_id'];
//            $TPe = $vo['treeplace'];
//            $table = $this->tablePrefix .'fck';
//			$dt	= strtotime(date("Y-m-d"));//现在的时间
//            if ($TPe == 0 && $Fid > 0){
//            	$p_rs = $peng ->where("uid=$Fid and ceng = $op") ->find();
//            	if($p_rs){
//            		$peng->execute("UPDATE __TABLE__ SET `l`=l+{$DanShu}  WHERE uid=$Fid and ceng = $op");
//            	}else{
//            		$peng->execute("INSERT INTO __TABLE__ (uid,ceng,l) VALUES ($Fid	,$op,$DanShu) ");
//            	}
//
//                $this->execute("UPDATE ". $table ." SET `l`=l+{$DanShu}, `benqi_l`=benqi_l+{$DanShu}  WHERE `id`=".$Fid);
//            }elseif($TPe == 1 && $Fid > 0){
//            	$p_rs = $peng ->where("uid=$Fid and ceng = $op") ->find();
//            	if($p_rs){
//            		$peng->execute("UPDATE __TABLE__ SET `r`=r+{$DanShu}  WHERE uid=$Fid and ceng = $op");
//            	}else{
//            		$peng->execute("INSERT INTO __TABLE__ (uid,ceng,r) VALUES ($Fid,$op,$DanShu) ");
//            	}
//                $this->execute("UPDATE ". $table ." SET `r`=r+{$DanShu}, `benqi_r`=benqi_r+{$DanShu}  WHERE `id`=".$Fid);
//            }
//            $op++;-+*
//            if ($Fid > 0) $this->xiangJiao($Fid,$DanShu,$plv,$op);
//        }
//        unset($where,$field,$vo);
//    }

    public function addencAdd($ID=0,$inUserID=0,$money=0,$name=null,$UID=0,$time=0,$acttime=0,$bz=""){
        //添加 到数据表
        if ($UID > 0) {
            $where = array();
            $where['id'] = $UID;
            $frs = $this->where($where)->field('nickname')->find();
            $name_two = $name;
            $name = $frs['nickname'] . ' 开通会员 ' . $inUserID ;
            $inUserID = $frs['nickname'];
        }else{
            $name_two = $name;
        }

        $data = array();
        $history = M ('history');

        $data['user_id']		= $inUserID;
        $data['uid']			= $ID;
        $data['action_type']	= $name;
        if($time >0){
        	$data['pdt']		= $time;
        }else{
        	$data['pdt']		= mktime();
        }
        $data['epoints']		= $money;
        if(!empty($bz)){
        	$data['bz']			= $bz;
        }else{
        	$data['bz']			= $name;
        }
        $data['did']			= 0;
        $data['type']			= 1;
        $data['allp']			= 0;
        if($acttime>0){
        	$data['act_pdt']	= $acttime;
        }
        $result = $history ->add($data);
        unset($data,$history);
    }

    public function huikuiAdd($ID=0,$tz=0,$zk,$money=0,$nowdate=null){
        //添加 到数据表

        $data                   = array();
        $huikui                = M ('huikui');
        $data['uid']            = $ID;
        $data['touzi']    = $tz;
        $data['zhuangkuang']            = $zk;
        $data['hk']        = $money;
        $data['time_hk']             = $nowdate;
        $huikui ->add($data);
        unset($data,$huikui);
    }


    //对碰1：1
    public function touch1to1(&$Encash,$xL=0,$xR=0,&$NumS=0){
        $xL = floor($xL);
        $xR = floor($xR);

        if ($xL > 0 && $xR > 0){
            if ($xL > $xR){
                $NumS = $xR;
                $xL = $xL - $NumS;
                $xR = $xR - $NumS;
                $Encash['0'] = $Encash['0'] + $NumS;
                $Encash['1'] = $Encash['1'] + $NumS;
            }
            if ($xL < $xR){
                $NumS = $xL;
                $xL   = $xL - $NumS;
                $xR   = $xR - $NumS;
                $Encash['0'] = $Encash['0'] + $NumS;
                $Encash['1'] = $Encash['1'] + $NumS;
            }
            if ($xL == $xR){
                $NumS = $xL;
                $xL   = 0;
                $xR   = 0;
                $Encash['0'] = $Encash['0'] + $NumS;
                $Encash['1'] = $Encash['1'] + $NumS;
            }
            $Encash['2'] = $NumS;
        }else{
            $NumS = 0;
            $Encash['0'] = 0;
            $Encash['1'] = 0;
        }
    }
	
	//对碰N:M
    public function touchNtoM(&$Encash,$xL=0,$xR=0,&$NumS=0,$N=1,$M=1){
        $xL = floor($xL);
        $xR = floor($xR);
        $N = floor($N);
        $M = floor($M);
		if($N<$M){
			$T=$N;
			$N=$M;
			$M=$T;
		}
		
		while(($xL>=$N && $xR>=$M) || ($xL>=$M && $xR>=$N)){
			$NumS++;
			if($xL>=$xR){
				$Encash['0'] = $Encash['0'] + $N;
				$Encash['1'] = $Encash['1'] + $M;
				$xL=$xL-$N;
				$xR=$xR-$M;
			}else{
				$Encash['0'] = $Encash['0'] + $M;
				$Encash['1'] = $Encash['1'] + $N;
				$xL=$xL-$M;
				$xR=$xR-$N;
			}
		}
		$Encash['2'] = $NumS;
    }

    //计算奖金
    public function getusjj($uid,$type=0){
    	$mrs = $this->where('id='.$uid)->find();
    	if($mrs){
			$this->duipeng($mrs['p_path']);
						
    		if($type==1){
    			//报单奖
    			$this->baodanfei($mrs['shop_id'],$mrs['user_id'],$mrs['cpzj']);
    		}
    	}
    	$this->getLevel();
		unset($mrs);
    }
	
	//直推奖
    public function tuijj($ID=0,$inUserID=0,$sulevel=0){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s3')->find(1);
    	$s3 = explode("|",$fee_rs['s3']);

    	$where = array();
    	$where['id'] = $ID;
    	$where['is_fenh'] = array('eq',0);
		$field = 'id,user_id,re_path,u_level';
    	$frs = $this->where($where)->field($field)->find();
		if ($frs){
			$myid = $frs['id'];
            $myusid = $frs['user_id'];
            $repath = $frs['re_path'];
			$prii = $s3[$sulevel-1];
			$money_count = $prii;
			if($money_count>0){
        		$this->rw_bonus($myid,$inUserID,1,$money_count);
			}
		}
		unset($fee,$fee_rs,$frs,$where);
    }
	
 	//对碰奖
    public function duipeng($ppath){
    	$fee = M ('fee');
    	$fee_rs = $fee->field('s3,s6,s9,s10,str1,str5,str9')->find(1);
    	$s3 = explode("|",$fee_rs['s3']);		//各级对碰比例
    	$s6 = explode("|",$fee_rs['s6']);		//封顶提升
    	$s9 = explode("|",$fee_rs['s9']);		//会员级别费用
    	$s10 = explode("|",$fee_rs['s10']);		//各级对碰比例
    	$str1 = explode("|",$fee_rs['str1']);		//封顶
    	$str9 = explode("|",$fee_rs['str9']);		//封顶提升开关
    	$one_mm = $s9[0];
    	$fck_array = 'is_pay>=1 and (shangqi_l>0 or shangqi_r>0) and id in (0'.$ppath.'0)';
    	$field = 'id,user_id,shangqi_l,shangqi_r,benqi_l,benqi_r,is_fenh,p_path,re_nums,nickname,u_level,re_id,day_feng,re_path,re_level,p_level,peng_num,n_pai';
    	$frs = $this->where($fck_array)->field($field)->select();
    	//BenQiL  BenQiR  ShangQiL  ShangQiR
    	foreach ($frs as $vo){
    		$L = 0;
    		$R = 0;
			$yejiM=0;
			$yejiL=$vo['l'];
			$yejiR=$vo['r'];
			if($yejiL>$yejiR){$yejiM=$yejiR;}
			if($yejiL<=$yejiR){$yejiM=$yejiL;}
			
    		$L = $vo['shangqi_l'];
    		$R = $vo['shangqi_r'];
    		$sq_l = $vo['shangqi_l'];
    		$sq_r = $vo['shangqi_r'];
    		$Encash    = array();
    		$NumS      = 0;//碰数
    		$money     = 0;//对碰奖金额
    		$Ls        = 0;//左剩余
    		$Rs        = 0;//右剩余
    		$this->touchNtoM($Encash, $L, $R, $NumS,1,1);
    		$Ls = $L - $Encash['0'];
    		$Rs = $R - $Encash['1'];
    		$myid = $vo['id'];
    		$myusid = $vo['user_id'];
            $myulv = $vo['u_level'];
    		$ss = $myulv-1;
    		$feng = $vo['day_feng'];
    		$is_fenh = $vo['is_fenh'];
    		$reid = $vo['re_id'];
    		$repath = $vo['re_path'];
    		$ul = $s3[$ss];
    		$money = $NumS *$ul;//对碰奖 奖金
			
			$fdbonusb2=$str1[$ss];
    		if($money>$fdbonusb2){
    			$money = $fdbonusb2;
    		}
    		if($feng>=$fdbonusb2){
    			$money=0;
    		}else{
    			$jfeng=$feng+$money;
    			if ($jfeng>$fdbonusb2){
    				$money=$fdbonusb2-$feng;
    			}
    		}
    		$result = $this->query('UPDATE __TABLE__ SET `shangqi_l`='. $Ls .',`shangqi_r`='. $Rs .' where `id`='. $vo['id'].' and shangqi_l='.$sq_l.' and shangqi_r='.$sq_r);
    		$money_count = $money;
    		if($money_count>0&&$is_fenh==0){
    			$this->rw_bonus($myid,$myusid,1,$money_count);
				//领导奖  
				$this->lingdaojiang($repath,$myusid,$money_count);
    		}
    	}
    	unset($fee,$fee_rs,$frs,$vo);
    }
  
 //层奖b1
    public function shuangcengpeng($ppath,$inUserID=0,$p_level=0){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s4,s9')->find(1);
    	$s4 = $fee_rs['s4']/100; //代数
		$s9 = explode("|",$fee_rs['s9']);
		
    	$lirs = $this->where('id in (0'.$ppath.'0) and is_fenh=0')->field('id,p_level,get_ceng,cpzj')->order('p_level desc')->limit(5)->select();

    	foreach ($lirs as $lrs){
    		$myid = $lrs['id'];
    		$mycpzj = $lrs['cpzj'];
    		$myplevel = $lrs['p_level'];
    		$get_ceng = $lrs['get_ceng'];
			
			$xdlevel=$p_level - $myplevel;
			
    		$lnn = 0;
    		$rnn = 0;
			$lrenn=0;
			$rrenn=0;
			$mrenn=0;
    		$l_rs = $this->where('father_id='.$myid)->field('id')->order('treeplace asc')->select();
    		$i=0;
    		foreach($l_rs as $voo){
    			$l_id = $voo['id'];
    			$lnn = $this->where('(p_path like "%,'.$l_id.',%" or id='.$l_id.') and is_pay>0 and p_level='.$p_level)->count();
    			if($lnn > 0){
					if($i==0){
						$lfs=$this->where('(p_path like "%,'.$l_id.',%" or id='.$l_id.') and is_pay>0 and p_level='.$p_level)->field('u_level')->order('u_level desc')->find();
						$lrenn=$lfs['u_level'];
					}
					if($i==1){
						$rfs=$this->where('(p_path like "%,'.$l_id.',%" or id='.$l_id.') and is_pay>0 and p_level='.$p_level)->field('u_level')->order('u_level desc')->find();
						$rrenn=$rfs['u_level'];
					}
    				$i++;
    			}
				
    		}
    		unset($l_rs);
    		
    		if($i >=2 && $get_ceng<$xdlevel){
				if($lrenn>$rrenn){
					$mrenn=$rrenn;
				}else{
					$mrenn=$lrenn;
				}
				$sss=$mrenn-1;
				$money=$s9[$sss];
			
				$this->execute("update __TABLE__ set get_ceng=".$xdlevel." where get_ceng<".$xdlevel." and id=".$myid);
				$money_count = bcmul($s4,$money,2);
				if($money_count>$mycpzj){$money_count=$mycpzj;}
				
    			if($money_count>0){
    				$this->rw_bonus($myid,$inUserID,1,$money_count);
					//互助奖
					$this->Dailijiang($myid,$inUserID,$money_count);
    			}
    		}
    	}
    	unset($lirs,$lrs);
    }
    
   public function getReid($id){
   		$rs = $this->where('id='.$id)->field('id,re_nums,is_fenh')->find();
   		return array('re_id'=>$rs['id'],'re_nums'=>$rs['re_nums'],'is_fenh'=>$rs['is_fenh']); 
   }
    
	//劳务奖b3
    public function guanglij($repath,$inUserID=0,$u_level=0,$tplce){
  
    	$fee = M('fee');
    	$fee_rs = $fee->field('str9')->find(1);
    	$str9 = explode("|",$fee_rs['str9']); //代数
    
    	$lirs = $this->where('id in (0'.$repath.'0)')->field('id,u_level,treeplace,is_fenh')->order('p_level desc')->select();
    	
    	$i = 1;
		foreach($lirs as $lrs){
			$myid = $lrs['id'];		
			$is_fenh = $lrs['is_fenh'];		
			$sss = $u_level-1;
			$myccc = $str9[$sss];
			
			$money_count = $myccc;
			
			//echo($money_count."*".$tplce);
					
			//dump($money_count);
			
			if($money_count>0&&$is_fenh==0&&$tplce>0){
				$this->rw_bonus($myid,$inUserID,3,$money_count);
			}
			
			$tplce= $lrs['treeplace'];
			$i++;
		}
        unset($fee,$fee_rs,$s15,$lirs,$lrs);
    }
    
    //升级
    public function getLevel(){
    	$mm = 30000;
    	
    	//一星董事
    	$this->execute("update __TABLE__ set get_level=1 where get_level=0 and L>=".$mm." and R>=".$mm);//小区业绩达7万
    	
    	//二星董事
    	$list = $this->where('is_pay>0 and get_level=1')->field('id')->order('re_level asc')->select();
   
    	foreach($list as $voo){
    		$rs = $this->where('re_id='.$voo['id'])->field('id')->select();
    		
    		$xx1 = 0;
    
    		foreach ($rs as $vo){	
    			//二星
    			$where = "((re_path like '%,".$vo['id'].",%') or id=".$vo['id'].") and get_level>=1";
    			$rs_count = $this->where($where)->count();
    	
    			if($rs_count){
    				$xx1 +=1;
    			}
    			if($xx1 >= 1){
    				break;
    			}
    		}
			    	
    		if($xx1 >= 1){
    			$this->execute("update __TABLE__ set get_level=2 where get_level=1 and id =".$voo['id']);
    			$xx1 = 0;
    		}		
    	}
    	unset($list,$voo,$rs,$vo,$where,$rs_count,$xx1);
   
    	//三星董事
    	$list = $this->where('is_pay>0 and get_level=2')->field('id')->order('re_level asc')->select();
    	foreach($list as $voo){
    		$rs = $this->where('re_id='.$voo['id'])->field('id')->select();
    	
    		$xx2 = 0;
    		foreach ($rs as $vo){
    			//二星
    			$where = "((re_path like '%,".$vo['id'].",%') or id=".$vo['id'].") and get_level>=1";
    			$rs_count = $this->where($where)->count();
    			if($rs_count){
    				$xx2 +=1;
    				if($xx2 >= 2){
    					break;
    				}
    			}
    		}
    		if($xx2 >= 2){
    			$this->execute("update __TABLE__ set get_level=3 where get_level=2 and id=".$voo['id']);
    		}
    	}
    	unset($list,$voo,$rs,$vo,$where,$rs_count,$xx2);
		
    	//股东
    	$list = $this->where('is_pay>0 and get_level=3')->field('id')->order('re_level asc')->select();
    	foreach($list as $voo){
    		$rs = $this->where('re_id='.$voo['id'])->field('id')->select();
    	
    		$xx3 = 0;
    		foreach ($rs as $vo){
    			//二星
    			$where = "((re_path like '%,".$vo['id'].",%') or id=".$vo['id'].") and get_level>=1";
    			$rs_count = $this->where($where)->count();
    			if($rs_count){
    				$xx3 +=1;
    				if($xx3 >= 2){
    					break;
    				}
    			}
    		}
    		if($xx3 >= 1){
    			$this->execute("update __TABLE__ set get_level=4 where get_level=3 and id=".$voo['id']);
    		}
    	}
    	unset($list,$voo,$rs,$vo,$where,$rs_count,$xx3);
    	
    }
	
//分公司
    public function SubCompanyBonus($uid,$ppath,$inUserID=0){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s1')->find(1);
    	$s1 = explode("|",$fee_rs['s1']);
    	
        //往上找最近的代理商
    	$lirs = $this->where('id in (0'.$ppath.'0) and get_level>0')->field('id,is_fenh,get_level')->order('p_level desc')->select();
    	$i = 1;
		$myid=0;
		$jdlevel=0;
    	foreach($lirs as $lrs){
    		$money_count = 0;
			$myid = $lrs['id'];
    		$is_fenh = $lrs['is_fenh'];
            $get_level = $lrs['get_level'];
			$mygtlevel=$get_level-1;
            
			$money_count = $s1[$mygtlevel];
			
			if($money_count>0&&$is_fenh==0&&$get_level>$jdlevel){
				$jdlevel=$get_level;
				$this->rw_bonus($myid,$inUserID,4,$money_count);
			}
    		
    		$i++;
    	}
    	unset($lirs,$lrs);
    	unset($fee,$fee_rs);
    }  
	
//代理奖
    public function Dailijiang($uid,$inUserID=0,$money){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s6,str9')->find(1);
    	$s6 = explode("|",$fee_rs['s6']);
    	$str9 = explode("|",$fee_rs['str9']);
				
		$where = "is_pay>0 and id=".$uid;
		$rs = $this->where($where)->find();
		if($rs){
			$money_count = 0;
			$myid = $rs['id'];
			$mis_fenh = $rs['is_fenh'];
			$prii = 0;
			
			$re_nums1 = $this->where('is_pay>0 and re_id='.$myid)->count();
			if($re_nums1>0){$prii=$str9[0]/100;}
			$re_nums2 = $this->where('is_pay>0 and u_level>1 and re_id='.$myid)->count();
			if($re_nums2>2){$prii=$str9[1]/100;}
			$re_nums3 = $this->where('is_pay>0 and u_level>2 and re_id='.$myid)->count();
			if($re_nums3>4){$prii=$str9[2]/100;}
			$re_nums4 = $this->where('is_pay>0 and u_level>3 and re_id='.$myid)->count();
			if($re_nums4>9){$prii=$str9[3]/100;}
			
			$money_count = bcmul($prii,$money,2);
			
			if($money_count>0 && $mis_fenh==0){
				$this->rw_bonus($myid,$inUserID,3,$money_count);
			}
		}
		
    }   

	//股东分红
    public function Gudongfenhong(){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s7,a_money')->find(1);
    	$s7 = explode("|",$fee_rs['s7']);
    	$money = $fee_rs['a_money'];
		    	

		$prii = $s7[0]/100;
		$b5_money = bcmul($prii,$money,2);
		$where = "is_pay>0 and u_level>=3";
    	$rs_count = $this->where($where)->count();
		if($rs_count>0){
			$rs=$this->where($where)->select();
			foreach ($rs as $vo){
				$money_count = 0;
				$myid = $vo['id'];
				$inUserID = $vo['user_id'];
				$mis_fenh = $vo['is_fenh'];
				$money_count=$b5_money/$rs_count;
				if($money_count>0&&$mis_fenh==0){
					$this->rw_bonus($myid,$inUserID,4,$money_count);
				}
			}
		}
		unset($where1,$rs_count,$rs);
		
		$prii = $s7[1]/100;
		$b51_money = bcmul($prii,$money,2);
		$where1 = "is_pay>0 and u_level>=4";
		$rs_count1 = $this->where($where1)->count();
		if($rs_count1>0){
			$rs=$this->where($where1)->select();
			foreach ($rs as $vo){
				$money_count = 0;
				$myid = $vo['id'];
				$inUserID = $vo['user_id'];
				$mis_fenh = $vo['is_fenh'];
				$money_count=$b51_money/$rs_count1;
				if($money_count>0&&$mis_fenh==0){
					$this->rw_bonus($myid,$inUserID,4,$money_count);
				}
			}
		}
		unset($where1,$rs_count1,$rs);
		
		$fee->execute("update __TABLE__ set `a_money`=0 where id=1");
    }   
    
//领导奖
    public function lingdaojiang($ppath,$inUserID=0,$money=0){
    	$fee = M('fee');
    	$fee_rs = $fee->field('str9')->find(1);
    	$str9 = explode("|",$fee_rs['str9']);
		$scc = count($str9);
    	
    	$lirs = $this->where('id in (0'.$ppath.'0)')->field('id,is_fenh,u_level')->order('p_level desc')->limit($scc)->select();
    	$i = 1;
    	foreach($lirs as $lrs){
    		$money_count = 0;
    		$myid = $lrs['id'];
    		$is_fenh = $lrs['is_fenh'];
            $u_level = $lrs['u_level'];
			
			$prii = $str9[$i-1]/100;
			$money_count = bcmul($prii, $money,2);
			
    		if($money_count>0&&$is_fenh==0){
    			$this->rw_bonus($myid,$inUserID,2,$money_count);
    		}
    		$i++;
    	}
    	unset($lirs,$lrs);
    	unset($fee,$fee_rs);
    }
	
//消费分红
    public function xiaofeijiang(){
    	$fee = M('fee');
		$gouwu = M('gouwu');
		$bonus = M('bonus');
    	$fee_rs = $fee->field('s4')->find(1);
    	$s4 = explode("|",$fee_rs['s4']);
		
		$vors = $this->where("re_money>=100")->field('id,user_id,re_path,re_money')->order('id asc')->select();
    	foreach($vors as $vo){
			$ppath = $vo['re_path'];
			$money = $vo['re_money'];
			$inUserID = $vo['user_id'];
			//给上5代
			$lirs = $this->where('id in (0'.$ppath.'0)')->field('id,is_fenh,u_level,re_money')->order('re_level desc')->select();
			$i = 1;
			foreach($lirs as $lrs){
				$money_count = 0;
				$myid = $lrs['id'];
				$is_fenh = $lrs['is_fenh'];
				$u_level = $lrs['u_level'];
				$xfmoney = $lrs['re_money'];
				
				if($xfmoney>=100 && $i<11){	
					$prii = $s4[$i-1]/100;
					$money_count = bcmul($prii, $money,2);
				}
				
				if($money_count>0 && $is_fenh==0){
					$this->rw_bonus($myid,$inUserID,3,$money_count);
					$i++;
				}
			}
    		unset($lirs,$lrs);
		}
		unset($vors,$vo);
    	unset($fee,$fee_rs);
    }
    
	//见点奖B网
    public function jiandianjiang_b($ppath,$inUserID=0){
    	$fee = M('fee');
    	$fck2 = M('fck2');
    	$fee_rs = $fee->field('str8,str9')->find(1);
    	$one_m = $fee_rs['str8'];
    	$s15 = explode("|",$fee_rs['str9']);
    	$scc = count($s15);
    	$max_c = 0;
    	for($i=0;$i<$scc;$i++){
    		if($s15[$i]>$max_c){
    			$max_c = $s15[$i];
    		}
    	}
    
    	$lirs = $fck2->where('id in (0'.$ppath.'0)')->field('id,fck_id,u_level')->order('p_level desc')->limit($max_c)->select();
    	$i = 1;
    	foreach($lirs as $lrs){
    		$myid = $lrs['fck_id'];
    		$myulv = $lrs['u_level'];
    		$sss = $myulv-1;
    		$myccc = $s15[$sss];
    		$money_count = 0;
    		if($myccc>=$i){
    			$money_count = $one_m;
    		}
    		$mrs = $this->where('id='.$myid)->field('id,is_fenh')->find();
    		if($mrs){
    			$is_fenh = $mrs['is_fenh'];
    			if($money_count>0&&$is_fenh==0){
    				$this->rw_bonus($myid,$inUserID,7,$money_count);
    			}
    		}
    		unset($mrs);
    		$i++;
    	}
    	unset($fee,$fee_rs,$s15,$lirs,$lrs);
    }
	
	//B网公排
    public function gongpaixt_Two_big_B(){
    	$b_fck2 = M ('fck2');
    	$field = 'id,user_id,p_level,p_path,u_pai';
    	$re_rs = $b_fck2 ->where('is_pay>0')->order('p_level asc,u_pai+0 asc')->field($field)->select();
    	foreach($re_rs as $vo){
    		$faid=$vo['id'];
    		$count = $b_fck2->where("is_pay>0 and father_id=".$faid)->count();
    		if ( is_numeric($count) == false){
    			$count = 0;
    		}
    		if ($count<2){
    			$father_id=$vo['id'];
    			$father_name=$vo['user_id'];
    			$TreePlace=$count;
    			$p_level=$vo['p_level']+1;
    			$p_path=$vo['p_path'].$vo['id'].',';
    			$u_pai=$vo['u_pai']*2+$TreePlace;
    
    			$arry=array();
    			$arry['father_id']=$father_id;
    			$arry['father_name']=$father_name;
    			$arry['treeplace']=$TreePlace;
    			$arry['p_level']=$p_level;
    			$arry['p_path']=$p_path;
    			$arry['u_pai']=$u_pai;
    			return $arry;
    			break;
    		}
    	}
    }
	
	//B网见点奖
	public function jiandianjiang_bb($ppath,$inUserID,$ulevel=1){
		$fee = M('fee');
    	$fee_rs = $fee->field('a_money')->find(1);
    	$a_money = $fee_rs['a_money'];
		
		$fck = M ('fck');
		$fck2 = M ('fck2');
		$field = 'id,fck_id,re_num,user_id,p_level,p_path,u_pai';
		//echo $ppath;
		$lirs = $fck2 ->where('id in (0'.$ppath.'0)')->order('p_level desc')->field($field)->limit(5)->select();
		$i = 1;
		foreach($lirs as $lrs){
			$money_count = 0;
			$pthid = $lrs['id'];
			$pthplevel = $lrs['p_level'];
			$myplevel=$pthplevel+5;
    		$faid=$lrs['fck_id'];
    		$renumid=$lrs['re_num'];
			$money_count=$a_money;
			//echo $renumid."*";
    		if($money_count>0 && $renumid>0){
				if($i<6){
    				$this->rw_bonus($renumid,$inUserID,1,$money_count);
				}
				if($i==5){
				$count_five= $fck2 ->where('is_pay>0 and p_path like "%,'.$pthid.',%" and p_level='.$myplevel)->count();
					if($count_five==32){
						$this->execute("UPDATE __TABLE__ SET tx_num=tx_num+1  WHERE id=".$renumid);
						$fck2->execute("UPDATE __TABLE__ SET is_out=1  WHERE id=".$pthid);
					}
				}
    		}
    		$i++;
    	}
	}
	
    //报单费
    public function baodanfei($uid,$inUserID,$cpzj=0){
    	$bonus = M ('bonus');
		$fee = M('fee');
    	$fee_rs = $fee->field('s14')->find();
		$s14 = $fee_rs['s14']/100;
		$money_count = 0;
		$frs = $this->where('id='.$uid.' and is_pay>0 and is_fenh=0')->field('id,user_id,re_path,u_level')->find();
		if($frs){
			$myid = $frs['id'];
			$myusid = $frs['user_id'];
			$ulevel = $frs['u_level'];
	
			$money_count = bcmul($cpzj,$s14,2);
			if($money_count>0){
        		$this->rw_bonus($myid,$inUserID,4,$money_count);
			}
		}
	    unset($bonus,$fee,$fee_rs,$frs,$s14);
    }

	//销售奖
    public function jiandianjiang($ppath,$inUserID=0,$cek){

        $fee = M('fee');
        $fee_rs = $fee->field('s4')->find(1);
        $s4 = explode("|",$fee_rs['s4']);

    
        $lirs = $this->where('id in (0'.$ppath.'0)')->field('id,is_fenh,f4')->order('p_level desc')->limit($s4[0])->select();
        $i = 0;
        foreach($lirs as $lrs){
            $money_count = 0;
            $myid = $lrs['id'];
            $is_fenh = $lrs['is_fenh'];
            $myccc = $s4[1]*$cek;
            $money_count = $myccc;
            if($money_count>0&&$is_fenh==0){
                $this->rw_bonus($myid,$inUserID,1,$money_count);
            }
            $i++;
        }
        unset($fee,$fee_rs,$s4,$lirs,$lrs);
    }
	
    //层奖和对碰奖的日封顶
    public function zfd_jj($uid,$money=0){
    		$fee = M('fee');
    	    $fee_rs = $fee->field('str1')->find();
    		$str1 = explode("|",$fee_rs['str1']);//分红奖封顶
    		
    	
    		$rs = $this->where('id='.$uid)->field('u_level,day_feng')->find();
    		if($rs){
    			$day_feng = $rs['day_feng'];
    			$feng = $str1[$rs['u_level']-1];
    			if($money > $feng){
    				$money = $feng;
    			}
    	
    			if($day_feng >= $feng){
    				$money = 0;
    			}else{
    				$tt_money = $money + $day_feng;
    				if( $tt_money > $feng){
    					$money = $feng-$day_feng;
    				}
    			}
    		}
    	
    		return $money;
    	}
    	

    	

	//各种扣税
    public function rw_bonus($myid,$inUserID=0,$bnum=0,$money_count=0,$u_level=0){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s5,s9,str4,str5')->find();
    	$s9 = explode('|',$fee_rs['s9']);
    
    	$s5 = $fee_rs['s5']/100;  //进入综合管理费
    	$str5 = $fee_rs['str5'];  //网络管理费
    	$str4 = $fee_rs['str4']/100;  //进入扣税
    	
    	$money_ka = 0;
    	$money_kb = 0;
    	$money_kc = 0;
    	$money_kd = 0;
		
		//$money_kc = $money_count*$str4;// 进入扣税
    	$money_kd = bcmul($s5,$money_count,2);//进入综合管理费
				
		
    	$usqla = "";
    	//网络费
		$mrs = $this->where('id='.$myid.' and wlf=0')->field('id,user_id,zjj,wlf,wlf_money')->find();
    	if($mrs){
    		$uuid = $mrs['id'];
    		$inUserID_did = $mrs['user_id'];
    		$zjj = $mrs['zjj'];
			if($zjj>=3000){
				$money_kc = bcmul($str4,$money_count,2);// 进入扣税
			}
			$last_n = $money_count-$money_kc-$money_kd;//第一次剩余
			
			if($zjj>=500){
				$wlf = $mrs['wlf'];
				$wlf_money = $mrs['wlf_money'];
				$all_mm = $wlf_money+$last_n;
				$k_mon = 0;
				if($all_mm>=$str5){
					$k_mon = $str5-$wlf_money;
					$this->execute("UPDATE __TABLE__ SET wlf=".time()." where `id`=".$uuid." and wlf=0");
				}else{
					$k_mon = $last_n;
				}
				if($k_mon<0){
					$k_mon = 0;
				}
				if($k_mon>0){
					$money_kb = $k_mon;
					$usqla .= ",wlf_money=wlf_money+".$money_kb;
				}
			}elseif($zjj+$last_n>500){
				$nlast_n = $zjj+$last_n-500;
				$wlf = $mrs['wlf'];
				$wlf_money = $mrs['wlf_money'];
				$all_mm = $wlf_money+$nlast_n;
				$k_mon = 0;
				if($all_mm>=$str5){
					$k_mon = $str5-$wlf_money;
					$this->execute("UPDATE __TABLE__ SET wlf=".time()." where `id`=".$uuid." and wlf=0");
				}else{
					$k_mon = $nlast_n;
				}
				if($k_mon<0){
					$k_mon = 0;
				}
				if($k_mon>0){
					$money_kb = $k_mon;
					$usqla .= ",wlf_money=wlf_money+".$money_kb;
				}
			
			}
    	}
    	unset($mrs);
		
		$last_m = $money_count-$money_kb-$money_kc-$money_kd;//剩余，此值写入现金账户
	
    	$bonus = M('bonus');
    	$bid = $this->_getTimeTableList($myid);
    	$inbb = "b".$bnum;
    	
     	if($bnum==1){
     		$usqla = ",day_feng=day_feng+".$money_count.""; 
     	}
    	
    	$usqlc = "agent_use=agent_use+".$last_m.",agent_cf=agent_cf+".$money_kc; //agent_cf重消奖
    	
    	$bonus->execute("UPDATE __TABLE__ SET b0=b0+".$last_m.",b5=b5-".$money_kc.",b6=b6-".$money_kd.",b7=b7-".$money_kb.",".$inbb."=".$inbb."+".$money_count."  WHERE id={$bid}"); //加到记录表
    	$this->execute("update __TABLE__ set ".$usqlc.",zjj=zjj+".$money_count.",xy_money=xy_money+".$money_count.$usqla." where id=".$myid);//加到fck
		
    	unset($bonus);

    	if($money_count>0){
    		$this->addencAdd($myid,$inUserID,$money_count,$bnum);
    	}
    	if($money_kb>0){
    		$this->addencAdd($myid,$inUserID_did,$money_kc,7);
    	}
    	if($money_kc>0){
    		$this->addencAdd($myid,$inUserID_did,-$money_kc,5);
    	}
    	if($money_kd>0){
    		$this->addencAdd($myid,$inUserID_did,-$money_kd,6);
    	}
    	
    	if($last_m>0){
    		$this->addencAdd($myid,$inUserID,$last_m,25,0,0,0,"进入奖金账户");
    	}
    	unset($fee,$fee_rs,$s9,$mrs);
    }
    
    //重复消费月封顶
    public function month_fd($uid,$money=0){
    	$fee = M('fee');
    	$fee_rs = $fee->field('s5')->find();
    	$s5 =$fee_rs['s5'];//封顶
    	 
    	$rs = $this->where('id='.$uid)->field('u_level,re_money')->find();
    	if($rs){
    		$month_feng = $rs['re_money'];
    		
    		if($money > $s5){
    			$money = $s5;
    		}
    		 
    		if($month_feng >= $s5){
    			$money = 0;
    		}else{
    			$tt_money = $money + $month_feng;
    			if( $tt_money > $s5){
    				$money = $s5-$month_feng;
    			}
    		}
    	}
    	 
    	return $money;
    }
    
    
    
    
    //分红添加记录
    public function add_xf($one_prices=0,$cj_ss=0){
		$fenhong = M('fenhong');
		$data = array();
// 		$data['uid'] = 1;
// 		$data['user_id'] = $cj_ss;
		$data['f_num'] = $cj_ss;
		$data['f_money'] = $one_prices;
		$data['pdt'] = mktime();
		$fenhong->add($data);
		unset($fenhong,$data);
    }

	//日封顶
    public function ap_rifengding(){

    	$fee = M('fee');
    	$fee_rs = $fee->field('s7')->find();
    	$s7 = explode("|",$fee_rs['s7']);

    	$where=array();
    	$where['b8'] = array('gt',0);
    	$mrs=$this->where($where)->field('id,b8,day_feng,get_level')->select();
    	foreach($mrs as $vo){
    		$day_feng = $vo['day_feng'];
    		$ss = $vo['get_level'];
    		$bbb = $vo['b8'];
    		$fedd = $s7[$ss];//封顶
			$get_money = $bbb;
    		$all_money = $bbb+$day_feng;
    		$fdok = 0;
    		if($all_money>=$fedd){
    			$fdok = 1;
    			$get_money = $fedd-$day_feng;
    		}
    		if($get_money<0){
    			$get_money = 0;
    		}
    		if($get_money>=0){
    			$this->query("UPDATE __TABLE__ SET `b8`=".$get_money.",day_feng=day_feng+".$get_money." where `id`=".$vo['id']);
    		}
    		if($get_money>0){
    			if($fdok==1){
    				$this->query("UPDATE __TABLE__ SET x_num=x_num+1 where `id`=".$vo['id']);
    			}
    		}
    	}
    	unset($fee,$fee_rs,$s7,$where,$mrs);
    }

	//总封顶
    public function ap_zongfengding(){

    	$fee = M('fee');
    	$fee_rs = $fee->field('s15')->find();
    	$s15 = $fee_rs['s15'];

    	$where=array();
    	$where['b0'] = array('gt',0);
    	$where['_string'] = 'b0+zjj>'.$s15;
    	$mrs=$this->where($where)->field('id,b0,zjj')->select();
    	foreach($mrs as $vo){
    		$zjj = $vo['zjj'];
    		$bbb = $vo['b0'];
    		$get_money = $s15-$zjj;

    		if($get_money>0){
    			$this->query("UPDATE __TABLE__ SET `b0`=".$get_money." where `id`=".$vo['id']);
    		}
    	}
    	unset($mrs);
    }

	//奖金大汇总（包括扣税等）
    public function quanhuizong(){

    	$this->execute('UPDATE __TABLE__ SET `b0`=b1+b2+b3+b4+b5+b6+b7+b8');

    	$this->execute('UPDATE __TABLE__ SET `b0`=0,b1=0,b2=0,b3=0,b4=0,b5=0,b6=0,b7=0,b8=0,b9=0,b10=0 where is_fenh=1');

    }


    //清空时间
	public function emptyTime(){

		$nowdate = strtotime(date('Y-m-d'));

		$this->query("UPDATE `xt_fck` SET `day_feng`=0,_times=".$nowdate." WHERE _times !=".$nowdate."");

	}
	
	
	//清空月封顶
	public function emptyMonthTime(){  //zyq_date 记录当前月
	
		$nowmonth = date('m');
	
		$this->query("UPDATE `xt_fck` SET zyq_date=".$nowmonth." WHERE zyq_date !=".$nowmonth."");
	
	}

	public function gongpaixtsmall($uid){
		$fck = M ('fck');
		$mouid=$uid;
		$field = 'id,user_id,p_level,p_path,u_pai';
		$where = 'is_pay>0 and (p_path like "%,'.$mouid.',%" or id='.$mouid.')';
	
		$re_rs = $fck ->where($where)->order('p_level asc,u_pai asc')->field($field)->select();
		$fck_where = array();
		foreach($re_rs as $vo){
			$faid=$vo['id'];
			$fck_where['is_pay']   = array('egt',0);
			$fck_where['father_id']   = $faid;
			$count = $fck->where($fck_where)->count();
			if ( is_numeric($count) == false){
				$count = 0;
			}
			if ($count<2){
				$father_id=$vo['id'];
				$father_name=$vo['user_id'];
				$TreePlace=$count;
				$p_level=$vo['p_level']+1;
				$p_path=$vo['p_path'].$vo['id'].',';
				$u_pai=$vo['u_pai']*2+$TreePlace;
	
				$arry=array();
				$arry['father_id']=$father_id;
				$arry['father_name']=$father_name;
				$arry['treeplace']=$TreePlace;
				$arry['p_level']=$p_level;
				$arry['p_path']=$p_path;
				$arry['u_pai']=$u_pai;
				return $arry;
				break;
			}
		}
	}
    public function bobifengding(){

		$fee = M ('fee');
		$bonus = M ('bonus');
		$fee_rs = M ('fee') -> find();
    	$table = $this->tablePrefix .'fck';
    	$z_money = 0;//总支出
        $z_money = $this->where('is_pay = 1')->sum('b2');
        $times = M ('times');
        $trs = $times->order('id desc')->field('shangqi')->find();
        if ($trs){
            $benqi = $trs['shangqi'];
        }else{
            $benqi = strtotime(date('Y-m-d'));
        }
        $zsr_money = 0;//总收入
        $zsr_money = $this->where('pdt>='. $benqi .' and is_pay=1')->sum('cpzj');
        $bl = $z_money / $zsr_money ;
        $fbl = $fee_rs['s11'] / 100;
        if ($bl > $fbl){
            //$bl = $fbl;
            //$xbl = $bl - $fbl;
            $z_o1=$zsr_money*$fbl;
            $z_o2=$z_o1/$z_money;
            $this->query("UPDATE ". $table ." SET `b2`=b2*{$z_o2} where `is_pay`>=1 ");
        }



    }


	public  function _getTimeTableList($uid)
    {
    	$times = M ('times');
    	$bonus = M ('bonus');
    	$boid = 0;
    	$nowdate = strtotime(date('Y-m-d'))+3600*24-1;
    	$settime_two['benqi'] = $nowdate;
    	$settime_two['type']  = 0;
    	$trs = $times->where($settime_two)->find();
    	if (!$trs){
    		$rs3 = $times->where('type=0')->order('id desc')->find();
    		if ($rs3){
    			$data['shangqi']  = $rs3['benqi'];
    			$data['benqi']    = $nowdate;
    			$data['is_count'] = 0;
    			$data['type']     = 0;
    		}else{
    			$data['shangqi']  = strtotime('2010-01-01');
    			$data['benqi']    = $nowdate;
    			$data['is_count'] = 0;
    			$data['type']     = 0;
    		}
    		$shangqi = $data['shangqi'];
    		$benqi   = $data['benqi'];
    		unset($rs3);
    		$boid = $times->add($data);
    		unset($data);
    	}else{
    		$shangqi = $trs['shangqi'];
    		$benqi   = $trs['benqi'];
    		$boid = $trs['id'];
    	}
    	$_SESSION['BONUSDID'] = $boid;
    	$brs = $bonus->where("uid={$uid} AND did={$boid}")->find();
    	if ($brs){
    		$bid = $brs['id'];
    	}else{
    		$frs = $this->where("id={$uid}")->field('id,user_id')->find();
    		$data = array();
    		$data['did'] = $boid;
    		$data['uid'] = $frs['id'];
    		$data['user_id'] = $frs['user_id'];
    		$data['e_date'] = $benqi;
    		$data['s_date'] = $shangqi;
    		$bid = $bonus->add($data);
    	}
    	return $bid;
    }
	
	//判断进入B网
    public function pd_into_websiteb($uid){
		//$fck = D ('fck');
		$fck=new FckModel('fck');
    	$fck2 = M ('fck2');
    	$where = "is_pay>0 and is_lock=0 and is_bb>=0 and id=".$uid;
    	$lrs = $fck->where($where)->field('id,user_id,re_id,user_name,nickname,u_level')->find();
    	if($lrs){
    		$myid = $lrs['id'];
    		$result = $fck->execute("update __TABLE__ set is_bb=is_bb+1 where id=".$myid." and is_bb>=0");
    		if($result){
    			$data=array();
    			$data['fck_id'] = $lrs['id'];
    			$data['re_num'] = $lrs['re_id'];
    			$data['user_id'] = $lrs['user_id'];
    			$data['user_name'] = $lrs['user_name'];
    			$data['nickname'] = $lrs['nickname'];
    			$data['u_level'] = $lrs['u_level'];
    			$data['ceng'] = 0;
    
    			$farr = $fck->gongpaixt_Two_big_B();
    			$data['father_id']		= $farr['father_id'];
    			$data['father_name']	= $farr['father_name'];
    			$data['treeplace']		= $farr['treeplace'];
    			$data['p_level']		= $farr['p_level'];
    			$data['p_path']			= $farr['p_path'];
    			$data['u_pai']			= $farr['u_pai'];
    			$data['is_pay']			= 1;
    			$data['pdt']			= time();
    			$ress = $fck2->add($data);  // 添加
    			$ppath = $data['p_path'];
    			$inUserID = $data['user_id'];
    			$ulevel = $data['u_level'];
    			unset($data,$farr);
    			if($ress){
    				//b网见点
    				$fck->jiandianjiang_bb($ppath,$inUserID,$ulevel);
    			}
    		}
    	}
    	unset($fck2,$lrs,$where,$fck);
    }
	
}
?>