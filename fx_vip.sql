-- MySQL dump 10.16  Distrib 10.1.16-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: fx_vip
-- ------------------------------------------------------
-- Server version	10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fx_gouwuquan`
--

DROP TABLE IF EXISTS `fx_gouwuquan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fx_gouwuquan` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `orderid` int(11) unsigned DEFAULT NULL,
  `num` decimal(12,2) NOT NULL DEFAULT '0.00',
  `createtime` int(11) unsigned NOT NULL,
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `expiretime` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fx_gouwuquan`
--

LOCK TABLES `fx_gouwuquan` WRITE;
/*!40000 ALTER TABLE `fx_gouwuquan` DISABLE KEYS */;
/*!40000 ALTER TABLE `fx_gouwuquan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_access`
--

DROP TABLE IF EXISTS `xt_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `pid` smallint(6) NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `groupId` (`role_id`) USING BTREE,
  KEY `nodeId` (`node_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_access`
--

LOCK TABLES `xt_access` WRITE;
/*!40000 ALTER TABLE `xt_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_address`
--

DROP TABLE IF EXISTS `xt_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `moren` int(11) NOT NULL COMMENT '是否为默认地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_address`
--

LOCK TABLES `xt_address` WRITE;
/*!40000 ALTER TABLE `xt_address` DISABLE KEYS */;
INSERT INTO `xt_address` VALUES (1,2,'张源','山东烟台','13256968084',0),(73,467,'1','1','1',0),(74,468,'1','1','1',0),(75,469,'1','1','1',0);
/*!40000 ALTER TABLE `xt_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_blist`
--

DROP TABLE IF EXISTS `xt_blist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_blist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `rdt` int(11) NOT NULL,
  `nums` int(11) NOT NULL,
  `cj_nums` int(11) NOT NULL,
  `cj_time` int(11) NOT NULL,
  `is_cj` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `prices` decimal(12,2) NOT NULL,
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_blist`
--

LOCK TABLES `xt_blist` WRITE;
/*!40000 ALTER TABLE `xt_blist` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_blist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_bonus`
--

DROP TABLE IF EXISTS `xt_bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_bonus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `did` int(11) NOT NULL,
  `s_date` int(11) NOT NULL,
  `e_date` int(11) NOT NULL,
  `b0` decimal(12,2) NOT NULL,
  `b1` decimal(12,2) NOT NULL,
  `b2` decimal(12,2) NOT NULL,
  `b3` decimal(12,2) NOT NULL,
  `b4` decimal(12,2) NOT NULL,
  `b5` decimal(12,2) NOT NULL,
  `b6` decimal(12,2) NOT NULL,
  `b7` decimal(12,2) NOT NULL,
  `b8` decimal(12,2) NOT NULL,
  `b9` decimal(12,2) NOT NULL,
  `b11` decimal(12,2) NOT NULL,
  `b12` decimal(12,2) NOT NULL,
  `b10` decimal(12,2) NOT NULL,
  `encash_l` int(11) NOT NULL,
  `encash_r` int(11) NOT NULL,
  `encash` int(11) NOT NULL,
  `is_count_b` int(11) NOT NULL,
  `is_count_c` int(11) NOT NULL,
  `is_pay` int(11) NOT NULL,
  `u_level` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  `additional` varchar(50) NOT NULL COMMENT '额外奖',
  `encourage` varchar(50) NOT NULL COMMENT '阶段鼓励奖',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=239 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_bonus`
--

LOCK TABLES `xt_bonus` WRITE;
/*!40000 ALTER TABLE `xt_bonus` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_bonus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_card`
--

DROP TABLE IF EXISTS `xt_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL DEFAULT '0',
  `buser_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `card_no` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `card_pw` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `c_time` int(11) NOT NULL DEFAULT '0',
  `f_time` int(11) NOT NULL DEFAULT '0',
  `l_time` int(11) NOT NULL DEFAULT '0',
  `b_time` int(11) NOT NULL DEFAULT '0',
  `is_sell` int(3) NOT NULL DEFAULT '0',
  `is_use` int(3) NOT NULL DEFAULT '0',
  `money` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_card`
--

LOCK TABLES `xt_card` WRITE;
/*!40000 ALTER TABLE `xt_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_cash`
--

DROP TABLE IF EXISTS `xt_cash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bid` int(11) NOT NULL DEFAULT '0',
  `b_user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `rdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `epoint` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `x1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sellbz` text COLLATE utf8_unicode_ci NOT NULL,
  `s_type` smallint(3) NOT NULL DEFAULT '0',
  `is_buy` int(11) NOT NULL DEFAULT '0',
  `bdt` int(11) NOT NULL DEFAULT '0',
  `ldt` int(11) NOT NULL DEFAULT '0',
  `okdt` int(11) NOT NULL DEFAULT '0',
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `is_sh` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_cash`
--

LOCK TABLES `xt_cash` WRITE;
/*!40000 ALTER TABLE `xt_cash` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_cash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_chongzhi`
--

DROP TABLE IF EXISTS `xt_chongzhi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_chongzhi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_bin NOT NULL,
  `epoint` decimal(12,2) NOT NULL,
  `huikuan` decimal(12,2) NOT NULL,
  `zhuanghao` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rdt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL DEFAULT '0',
  `is_pay` smallint(2) NOT NULL,
  `stype` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_chongzhi`
--

LOCK TABLES `xt_chongzhi` WRITE;
/*!40000 ALTER TABLE `xt_chongzhi` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_chongzhi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_clearing_log`
--

DROP TABLE IF EXISTS `xt_clearing_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_clearing_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `createtime` int(11) unsigned DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `userid` varchar(80) DEFAULT NULL,
  `creat_date` varchar(24) CHARACTER SET utf8 NOT NULL DEFAULT '0000-00' COMMENT '结算年月',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_clearing_log`
--

LOCK TABLES `xt_clearing_log` WRITE;
/*!40000 ALTER TABLE `xt_clearing_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_clearing_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_cody`
--

DROP TABLE IF EXISTS `xt_cody`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_cody` (
  `c_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `cody_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_cody`
--

LOCK TABLES `xt_cody` WRITE;
/*!40000 ALTER TABLE `xt_cody` DISABLE KEYS */;
INSERT INTO `xt_cody` VALUES (1,'profile'),(2,'password'),(3,'Jj_FA'),(4,'4'),(5,'5'),(6,'6'),(7,'7'),(8,'8'),(9,'9'),(10,'10'),(11,'11'),(12,'12'),(13,'13'),(14,'14'),(15,'15'),(16,'16'),(17,'17'),(18,'18'),(19,'19'),(20,'20'),(21,'21'),(22,'22'),(23,'23'),(24,'24'),(25,'25'),(26,'26'),(27,'27'),(28,'28'),(29,'29'),(30,'30');
/*!40000 ALTER TABLE `xt_cody` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_cptype`
--

DROP TABLE IF EXISTS `xt_cptype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_cptype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tpname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `b_id` int(11) NOT NULL DEFAULT '0',
  `s_id` int(11) NOT NULL DEFAULT '0',
  `t_pai` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_cptype`
--

LOCK TABLES `xt_cptype` WRITE;
/*!40000 ALTER TABLE `xt_cptype` DISABLE KEYS */;
INSERT INTO `xt_cptype` VALUES (1,'家用电器',0,0,0,0),(2,'食品',0,0,0,0),(3,'生活用品',0,0,0,0);
/*!40000 ALTER TABLE `xt_cptype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_deduct_voucher`
--

DROP TABLE IF EXISTS `xt_deduct_voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_deduct_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deduct_val` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '扣除值',
  `deduct_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '扣除时间',
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员id',
  `type` tinyint(3) unsigned DEFAULT NULL COMMENT '1为下级注册扣除，2为月结扣除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物币自动扣除表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_deduct_voucher`
--

LOCK TABLES `xt_deduct_voucher` WRITE;
/*!40000 ALTER TABLE `xt_deduct_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_deduct_voucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fck`
--

DROP TABLE IF EXISTS `xt_fck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fck` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(64) DEFAULT NULL,
  `bind_account` varchar(50) DEFAULT NULL,
  `new_login_time` int(11) NOT NULL DEFAULT '0',
  `new_login_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_login_time` int(11) unsigned DEFAULT '0',
  `last_login_ip` varchar(40) DEFAULT NULL,
  `login_count` mediumint(8) unsigned DEFAULT '0',
  `verify` varchar(32) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `type_id` tinyint(2) unsigned DEFAULT '0',
  `info` text,
  `name` varchar(25) DEFAULT NULL,
  `dept_id` smallint(3) DEFAULT NULL,
  `user_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '用户编号',
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '银行开户名',
  `password` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '一级密码',
  `pwd1` varchar(50) DEFAULT NULL COMMENT '一级密码不加密',
  `passopen` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '二级密码',
  `pwd2` varchar(50) DEFAULT NULL COMMENT '二级密码不加密',
  `passopentwo` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '三级密码',
  `pwd3` varchar(50) DEFAULT NULL COMMENT '三级密码不加密',
  `nickname` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '昵称',
  `qq` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'QQ',
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行',
  `bank_card` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '银行卡号',
  `bank_province` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行所在省',
  `bank_city` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '开户银行所在城市',
  `bank_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '支行地址',
  `user_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '身份证',
  `user_address` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '联系地址',
  `user_post` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '联系方式',
  `user_tel` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '电话',
  `user_phone` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '手机',
  `rdt` int(11) NOT NULL COMMENT '注册时间',
  `treeplace` int(11) DEFAULT NULL COMMENT '区分左(中)右',
  `father_id` int(11) NOT NULL COMMENT '父节点',
  `father_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '父名',
  `re_id` int(11) NOT NULL COMMENT '推荐ID',
  `re_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '推荐人名称',
  `is_pay` int(11) NOT NULL COMMENT '是否开通(0,1)',
  `is_lock` int(11) NOT NULL COMMENT '是否锁定(0,1)',
  `is_lock_ok` int(3) NOT NULL DEFAULT '0',
  `shoplx` int(11) NOT NULL COMMENT '报单中心ID',
  `is_agent` int(11) NOT NULL DEFAULT '0' COMMENT '1为社区专卖店，2为县级，3为地市，4为省级',
  `agent_max` decimal(12,2) NOT NULL COMMENT '申请报单总金额',
  `agent_use` decimal(12,2) NOT NULL COMMENT '奖金币',
  `agent_cash` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '充值现金',
  `agent_kt` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '赠送币',
  `agent_xf` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '购物币/电子币',
  `agent_cf` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '可提现奖金',
  `agent_gp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gp_num` int(11) NOT NULL DEFAULT '0',
  `agent_lock` decimal(12,2) NOT NULL DEFAULT '0.00',
  `live_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_in_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `p_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `no_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `ok_out_gupiao` int(11) NOT NULL DEFAULT '0',
  `yuan_gupiao` int(11) NOT NULL DEFAULT '0',
  `all_gupiao` int(11) NOT NULL DEFAULT '0',
  `tx_num` int(3) NOT NULL DEFAULT '0',
  `lssq` decimal(12,2) NOT NULL COMMENT '级差奖',
  `zsq` decimal(12,2) NOT NULL,
  `adt` int(11) NOT NULL COMMENT '申请成报单中心时间',
  `l` int(11) NOT NULL COMMENT '左边总人数',
  `r` int(11) NOT NULL COMMENT '右边总人数',
  `benqi_l` int(11) NOT NULL COMMENT '本期左区新增',
  `benqi_r` int(11) NOT NULL COMMENT '本期右区新增',
  `shangqi_l` int(11) NOT NULL COMMENT '上期左区剩余',
  `shangqi_r` int(11) NOT NULL COMMENT '上期右区剩余',
  `peng_num` int(11) NOT NULL DEFAULT '0',
  `u_level` int(11) NOT NULL COMMENT '等级(会员级别)',
  `is_boss` int(11) NOT NULL COMMENT '管理人为1,其它为0',
  `idt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL COMMENT '开通时间',
  `re_level` int(11) NOT NULL COMMENT '相对于推的代数',
  `p_level` int(11) NOT NULL COMMENT '绝对层数',
  `re_path` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '推荐的路径',
  `all_re_id` text CHARACTER SET swe7 COMMENT '自己推荐的所有人',
  `left_path` text,
  `team_path` text,
  `down_path` text,
  `p_path` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '自已的路径',
  `tp_path` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_del` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL COMMENT '隶属报单ID',
  `shop_name` varchar(50) NOT NULL,
  `b0` decimal(12,2) NOT NULL COMMENT '每期总资金',
  `b1` decimal(12,2) NOT NULL COMMENT '奖1',
  `b2` decimal(12,2) NOT NULL COMMENT '奖2',
  `b3` decimal(12,2) NOT NULL COMMENT '奖3',
  `b4` decimal(12,2) NOT NULL COMMENT '奖4',
  `b5` decimal(12,2) NOT NULL COMMENT '奖5',
  `b6` decimal(12,2) NOT NULL COMMENT '奖6',
  `b7` decimal(12,2) NOT NULL COMMENT '奖7',
  `b8` decimal(12,2) NOT NULL COMMENT '奖8',
  `b9` decimal(12,2) NOT NULL COMMENT '奖9',
  `b12` decimal(12,2) NOT NULL COMMENT '奖12',
  `b11` decimal(12,2) NOT NULL COMMENT '奖11',
  `b10` decimal(12,2) NOT NULL COMMENT '奖10',
  `wlf` int(11) NOT NULL COMMENT '网络费',
  `wlf_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `cpzj` decimal(12,2) NOT NULL COMMENT '注册金额',
  `zjj` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '总奖金',
  `re_money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '推荐总注册金额',
  `cz_epoint` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '冲值总金额',
  `lr` int(11) NOT NULL COMMENT '中间总单数',
  `shangqi_lr` int(11) NOT NULL COMMENT '中间上期剩余单数',
  `benqi_lr` int(11) NOT NULL COMMENT '中间本期单数',
  `user_type` varchar(200) NOT NULL COMMENT '多线登录限制',
  `re_peat_money` decimal(12,2) NOT NULL COMMENT 'x',
  `re_nums` smallint(4) NOT NULL DEFAULT '0' COMMENT 'x',
  `re_nums_b` int(11) NOT NULL DEFAULT '0',
  `re_nums_l` int(11) NOT NULL DEFAULT '0',
  `re_nums_r` int(11) NOT NULL DEFAULT '0',
  `duipeng` decimal(12,2) NOT NULL,
  `_times` int(11) NOT NULL,
  `fanli` int(11) NOT NULL,
  `fanli_time` int(11) NOT NULL,
  `fanli_num` int(11) NOT NULL,
  `fanli_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_fenh` smallint(2) NOT NULL,
  `open` smallint(2) NOT NULL,
  `f4` int(11) NOT NULL DEFAULT '0',
  `new_agent` smallint(1) NOT NULL DEFAULT '0' COMMENT '//是否新服务中心',
  `day_feng` decimal(12,2) NOT NULL DEFAULT '0.00',
  `get_date` int(11) DEFAULT '0',
  `get_numb` int(11) DEFAULT '0',
  `is_jb` int(11) DEFAULT '0',
  `sq_jb` int(11) DEFAULT '0',
  `jb_sdate` int(11) DEFAULT '0',
  `jb_idate` int(11) DEFAULT '0',
  `man_ceng` int(11) NOT NULL DEFAULT '0' COMMENT '//满层数',
  `prem` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '//权限',
  `wang_j` smallint(1) NOT NULL DEFAULT '0' COMMENT '//结构图',
  `wang_t` smallint(1) NOT NULL DEFAULT '0' COMMENT '//推荐图',
  `get_level` int(11) NOT NULL DEFAULT '0',
  `is_xf` smallint(11) NOT NULL DEFAULT '0',
  `xf_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_zy` int(11) NOT NULL DEFAULT '0',
  `zyi_date` int(11) NOT NULL DEFAULT '0',
  `zyq_date` int(11) NOT NULL DEFAULT '0',
  `mon_get` decimal(12,2) NOT NULL DEFAULT '0.00',
  `xy_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `xx_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `down_num` int(11) NOT NULL DEFAULT '0',
  `u_pai` int(11) NOT NULL DEFAULT '0',
  `n_pai` int(11) NOT NULL DEFAULT '0',
  `ok_pay` int(11) NOT NULL DEFAULT '0',
  `wenti` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `wenti_dan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `is_tj` int(11) NOT NULL DEFAULT '0',
  `re_f4` int(11) NOT NULL DEFAULT '0',
  `is_aa` int(3) NOT NULL DEFAULT '0',
  `is_bb` int(3) NOT NULL DEFAULT '0',
  `us_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `x_pai` int(11) NOT NULL DEFAULT '0',
  `x_out` int(3) NOT NULL DEFAULT '0',
  `x_num` int(11) NOT NULL DEFAULT '0',
  `is_lockqd` int(11) NOT NULL COMMENT '是否关闭签到',
  `is_lockfh` int(11) NOT NULL COMMENT '是否关闭分红',
  `seller_rate` int(11) NOT NULL DEFAULT '5',
  `lang` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '语言',
  `countrys` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '国家',
  `get_ceng` int(11) NOT NULL DEFAULT '0',
  `gouwuquan` decimal(12,2) DEFAULT NULL COMMENT '???',
  `settlement_time` int(11) DEFAULT NULL COMMENT '结算时间',
  `achievement` decimal(20,0) unsigned DEFAULT '0' COMMENT '总业绩',
  `options` decimal(12,2) unsigned DEFAULT '0.00' COMMENT '??',
  `star` tinyint(2) NOT NULL DEFAULT '0' COMMENT '董事星级',
  `shop_a` int(10) DEFAULT NULL,
  `shop_b` int(10) DEFAULT NULL,
  `shop_c` int(10) DEFAULT NULL,
  `shop_d` varchar(256) DEFAULT NULL,
  `level` varchar(200) DEFAULT NULL COMMENT '这里特指会员等级',
  `more_level` varchar(200) DEFAULT NULL COMMENT '更高级别 省代 全国代',
  `is_hege` int(5) DEFAULT NULL COMMENT '这里特指是否合格 不合格为0',
  `has_hege` int(5) DEFAULT NULL COMMENT '曾经合格过',
  `sj_time` char(12) DEFAULT NULL COMMENT '升级为市级代理的时间',
  `hege_time` char(12) DEFAULT NULL COMMENT '升级为合格市级代理的时间',
  `jia_achievement` int(11) unsigned DEFAULT '0' COMMENT '不合格市代增加的业绩',
  `bonus` decimal(20,2) DEFAULT '0.00' COMMENT '总奖金new',
  `credit` decimal(20,2) DEFAULT '0.00' COMMENT '积分new',
  `shopping` decimal(20,2) DEFAULT '0.00' COMMENT '购物币new',
  `is_shoper` int(5) DEFAULT '0' COMMENT '是否具有保单权限',
  `shoper_id` int(20) DEFAULT NULL COMMENT '保单人id',
  `hege_num` int(11) DEFAULT '0' COMMENT '推荐的合格市代数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=470 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fck`
--

LOCK TABLES `xt_fck` WRITE;
/*!40000 ALTER TABLE `xt_fck` DISABLE KEYS */;
INSERT INTO `xt_fck` VALUES (1,'admin','0',1544925867,'127.0.0.1',1544882490,'127.0.0.1',0,'1','132321231132@163.com','',0,0,1,1,'0','100000',0,'100000','公司','c4ca4238a0b923820dcc509a6f75849b','1','c4ca4238a0b923820dcc509a6f75849b','1','c4ca4238a0b923820dcc509a6f75849b','1','','','工商银行','8889992','','','444','4444','51111','QQ126163@QQ.com','','0',1295884800,0,0,'0',0,'0',1,0,1,1,2,0.00,0.00,0.00,0.00,100000.00,13680.00,0.00,0,1.00,10000000,0,0,0,0,0,0,0,0,13.00,0.00,1317024831,0,0,0,0,0,0,0,7,2,0,0,0,0,'',NULL,NULL,NULL,'','','',0,2108,'0',0.00,6000.00,7000.00,0.00,400.00,1800.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0.00,12000.00,0.00,0.00,0.00,0,0,0,'8c5c4fb8b95fd32d851865e33031397d',0.00,0,0,0,0,0.00,1544889600,0,1544630400,0,0.00,0,0,3,0,0.00,1544630400,0,1,0,0,1323230473,0,',1,2,3,4,16,17,5,14,6,7,8,15,9,10,11,12,13,',0,0,0,0,0.00,0,0,12,919379.05,0.00,0.00,0,1,1,1,'你爱人叫什么名字？','123',0,0,0,0,'/Public/Uploads/2014010816185380.jpg',1,1,0,1,0,5,'中文','马来西亚',0,NULL,0,5800000,0.00,0,NULL,NULL,NULL,NULL,'全国代理','省级代理',0,1,'1542096000','',0,504625.30,0.00,0.00,0,NULL,0),(467,NULL,NULL,0,'',1544692800,'',0,'0','1',NULL,0,NULL,1,0,'信息','名称',NULL,'6693939','1','c4ca4238a0b923820dcc509a6f75849b','1','c81e728d9d4c2f636f067f89cc14862c','2',NULL,NULL,'2','','农业银行','1','请选择','请选择','','1','','','1','',1544692800,0,0,'',1,'100000',1,0,0,0,0,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0.00,0,0,0,0,0,0,0,0,0,4.00,0.00,0,0,0,0,0,0,0,0,2,0,0,0,0,0,'1',NULL,'1',NULL,NULL,'','0',0,0,'',0.00,0.00,0.00,472.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0.00,50000.00,0.00,0.00,0.00,0,0,0,'',0.00,0,0,0,0,0.00,1544889600,0,0,0,0.00,0,0,5,0,0.00,0,0,0,0,0,0,0,'',0,0,0,0,0.00,0,0,12,0.00,0.00,0.00,0,0,0,0,'','xx',0,0,0,0,'',0,0,0,0,0,5,'','',0,NULL,NULL,20000,0.00,0,NULL,NULL,NULL,NULL,'社区代理商',NULL,NULL,NULL,NULL,NULL,0,472.00,0.00,0.00,1,1,0),(468,NULL,NULL,1544871523,'127.0.0.1',0,'',0,'0','1',NULL,0,NULL,1,0,'信息','名称',NULL,'5842224','1','c4ca4238a0b923820dcc509a6f75849b','1','c81e728d9d4c2f636f067f89cc14862c','2',NULL,NULL,'q','','农业银行','1','请选择','请选择','','1','','','1','',1544692866,0,0,'',1,'100000',1,0,0,0,0,0.00,0.00,0.00,0.00,0.00,3736.80,0.00,0,0.00,0,0,0,0,0,0,0,0,0,4.00,0.00,0,0,0,0,0,0,0,0,2,0,0,0,0,0,'1',NULL,'1,467',NULL,NULL,'','0',0,0,'',0.00,3000.00,800.00,352.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0.00,20000.00,0.00,0.00,0.00,0,0,0,'1f0bd516ad8131c643a18bcdd68480d3',0.00,0,0,0,0,0.00,1544889600,0,0,0,0.00,0,0,3,0,0.00,0,0,0,0,0,0,0,'',0,0,0,0,0.00,0,0,12,0.00,0.00,0.00,0,0,0,0,'','xx',0,0,0,0,'',0,0,0,0,0,5,'','',0,NULL,NULL,20000,0.00,0,NULL,NULL,NULL,NULL,'VIP',NULL,NULL,NULL,NULL,NULL,0,4152.00,0.00,0.00,0,1,0),(469,NULL,NULL,1544880818,'127.0.0.1',1544874411,'127.0.0.1',0,'0','1',NULL,0,NULL,1,0,'信息','名称',NULL,'5496429','1','c4ca4238a0b923820dcc509a6f75849b','1','c81e728d9d4c2f636f067f89cc14862c','2',NULL,NULL,'s','','农业银行','1','请选择','请选择','','1','','','1','',1544871602,0,0,'',468,'5842224',1,0,0,0,0,0.00,0.00,0.00,0.00,0.00,57.60,0.00,0,0.00,0,0,0,0,0,0,0,0,0,4.00,0.00,0,0,0,0,0,0,0,0,2,0,0,0,0,0,'1,468',NULL,'468',NULL,NULL,'','0',0,0,'',0.00,0.00,0.00,64.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0.00,20000.00,0.00,0.00,0.00,0,0,0,'e5539f4f21118bd54ccc541ea9b25860',0.00,0,0,0,0,0.00,1544889600,0,0,0,0.00,0,0,3,0,0.00,0,0,0,0,0,0,0,'',0,0,0,0,0.00,0,0,12,0.00,0.00,0.00,0,0,0,0,'','xx',0,0,0,0,'',0,0,0,0,0,5,'','',0,NULL,NULL,0,0.00,0,NULL,NULL,NULL,NULL,'VIP',NULL,NULL,NULL,NULL,NULL,0,64.00,0.00,0.00,0,1,0);
/*!40000 ALTER TABLE `xt_fck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fck2`
--

DROP TABLE IF EXISTS `xt_fck2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fck2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ceng` int(11) DEFAULT '0',
  `numb` int(11) DEFAULT '0',
  `fend` int(11) DEFAULT '0',
  `jishu` int(11) NOT NULL DEFAULT '0' COMMENT '//����',
  `fck_id` int(11) DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nickname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_pay` int(11) NOT NULL DEFAULT '1',
  `u_level` int(11) DEFAULT '0',
  `re_num` int(11) NOT NULL DEFAULT '0',
  `pdt` int(11) NOT NULL,
  `treeplace` int(11) DEFAULT '0',
  `father_id` int(11) DEFAULT '0',
  `father_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `p_level` int(11) DEFAULT '0',
  `p_path` text CHARACTER SET utf8 COLLATE utf8_bin,
  `u_pai` varchar(200) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0' COMMENT '//�ж��Ƿ�Ϊ����',
  `is_out` smallint(1) NOT NULL DEFAULT '0' COMMENT '//�ж��Ƿ��Ѿ�����',
  `is_yinc` smallint(1) NOT NULL DEFAULT '0' COMMENT '//����',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fck2`
--

LOCK TABLES `xt_fck2` WRITE;
/*!40000 ALTER TABLE `xt_fck2` DISABLE KEYS */;
INSERT INTO `xt_fck2` VALUES (1,0,0,0,0,1,'100000','','',1,1,0,1322031843,0,0,'',0,',','1',1,0,0);
/*!40000 ALTER TABLE `xt_fck2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fck_shop`
--

DROP TABLE IF EXISTS `xt_fck_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fck_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `create_time` int(11) NOT NULL,
  `is_pay` smallint(1) NOT NULL DEFAULT '0',
  `pdt` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  `num` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `p_dt` int(11) NOT NULL COMMENT '退货时间',
  `p_is_pay` smallint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `out_type` smallint(2) NOT NULL DEFAULT '0' COMMENT '0为未评论，1为已评论',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2533 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fck_shop`
--

LOCK TABLES `xt_fck_shop` WRITE;
/*!40000 ALTER TABLE `xt_fck_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_fck_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fee`
--

DROP TABLE IF EXISTS `xt_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `i1` int(12) DEFAULT '0',
  `i2` int(12) DEFAULT '0',
  `i3` int(12) DEFAULT '0',
  `i4` int(12) DEFAULT '0',
  `i5` int(12) DEFAULT '0',
  `i6` int(12) DEFAULT '0',
  `i7` int(12) DEFAULT '0',
  `i8` int(12) DEFAULT '0',
  `i9` int(12) DEFAULT '0',
  `i10` int(12) DEFAULT '0',
  `s1` varchar(200) DEFAULT NULL,
  `s2` varchar(200) DEFAULT NULL,
  `s3` varchar(200) DEFAULT NULL,
  `s4` varchar(200) DEFAULT NULL,
  `s5` varchar(200) DEFAULT NULL,
  `s6` varchar(200) DEFAULT NULL,
  `s7` varchar(200) DEFAULT NULL,
  `s8` varchar(200) DEFAULT NULL,
  `s9` varchar(200) DEFAULT NULL,
  `s10` varchar(200) DEFAULT NULL,
  `s11` varchar(200) DEFAULT NULL,
  `s12` varchar(200) DEFAULT NULL,
  `s13` varchar(200) DEFAULT NULL,
  `s14` varchar(200) DEFAULT NULL,
  `s15` varchar(200) DEFAULT NULL,
  `s16` varchar(200) DEFAULT NULL,
  `s17` varchar(200) DEFAULT NULL,
  `s18` varchar(200) DEFAULT NULL,
  `s19` varchar(400) DEFAULT NULL,
  `s20` varchar(200) DEFAULT NULL,
  `str1` varchar(200) DEFAULT NULL,
  `str2` varchar(200) DEFAULT NULL,
  `str3` varchar(200) DEFAULT NULL,
  `str4` varchar(200) DEFAULT NULL,
  `str5` varchar(200) DEFAULT NULL,
  `str6` varchar(200) DEFAULT NULL,
  `str7` varchar(200) DEFAULT NULL,
  `str8` varchar(200) DEFAULT NULL,
  `str9` varchar(200) DEFAULT NULL,
  `str10` varchar(200) DEFAULT NULL,
  `str11` varchar(200) DEFAULT NULL,
  `str12` varchar(200) DEFAULT NULL,
  `str13` varchar(200) DEFAULT NULL,
  `str14` varchar(200) DEFAULT NULL,
  `str15` varchar(200) DEFAULT NULL,
  `str16` varchar(200) DEFAULT NULL,
  `str17` varchar(200) DEFAULT NULL,
  `str18` varchar(200) DEFAULT NULL,
  `str19` varchar(400) DEFAULT NULL,
  `str20` varchar(200) DEFAULT NULL,
  `str21` varchar(200) DEFAULT NULL,
  `str22` varchar(200) DEFAULT NULL,
  `str23` varchar(200) DEFAULT NULL,
  `str24` varchar(200) DEFAULT NULL,
  `str25` varchar(200) DEFAULT NULL,
  `str26` varchar(200) DEFAULT NULL,
  `str27` varchar(200) DEFAULT NULL,
  `str28` varchar(200) DEFAULT NULL,
  `str29` varchar(200) DEFAULT NULL,
  `str30` varchar(200) DEFAULT NULL,
  `str99` text NOT NULL,
  `us_num` int(11) NOT NULL DEFAULT '0',
  `create_time` int(11) DEFAULT NULL COMMENT '清空数据时间截',
  `f_time` int(11) NOT NULL DEFAULT '0',
  `a_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `b_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `ff_num` int(11) NOT NULL DEFAULT '0',
  `gp_one` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '当前价格',
  `gp_open` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '开盘价格',
  `gp_close` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '关盘价格',
  `gp_kg` int(3) NOT NULL DEFAULT '0' COMMENT '开关',
  `gp_cnum` int(11) NOT NULL DEFAULT '0' COMMENT '拆股次数',
  `gp_perc` varchar(50) NOT NULL COMMENT '手续费',
  `gp_inm` varchar(50) NOT NULL COMMENT '交易分账',
  `gp_inn` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gp_cgbl` varchar(50) NOT NULL COMMENT '拆比例',
  `gp_fxnum` int(11) NOT NULL DEFAULT '0',
  `gp_senum` int(11) NOT NULL DEFAULT '0',
  `prize1` varchar(200) DEFAULT NULL COMMENT '极差奖',
  `prize2` varchar(200) DEFAULT NULL COMMENT '拓展奖',
  `prize3` varchar(200) DEFAULT NULL COMMENT '管理奖',
  `prize4` varchar(200) DEFAULT NULL COMMENT '合作奖',
  `prize5` varchar(200) DEFAULT NULL COMMENT '领导奖',
  `prize6` varchar(200) DEFAULT NULL COMMENT '福利分红',
  `prize7` varchar(200) DEFAULT NULL COMMENT '店补',
  `prize8` varchar(200) DEFAULT NULL COMMENT '二次消费',
  `prizename` varchar(400) DEFAULT NULL COMMENT '奖项名称',
  `change_rale` text COMMENT '币间转换比例',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fee`
--

LOCK TABLES `xt_fee` WRITE;
/*!40000 ALTER TABLE `xt_fee` DISABLE KEYS */;
INSERT INTO `xt_fee` VALUES (1,0,1,0,0,0,0,0,0,0,0,'2','1|3|5|10','100|100|100|100','15|5','10','10|2','2|2|1','100','2000|20000|50000','200000|800000|1800000|5800000','10','50','40','4','1|2|3|4|5|6|7|8|9','100','','分享奖|业绩奖|复销奖|福利奖|服务费|特别贡献奖|扣税|管理费|网络管理费','会员|VIP|社区代理商','区级代理商|市级代理商|省级代理|全国代理','300|900|1500|3000','10','50','6','10','11223344','123123|456789|123456789|987654321','zh_rm_ge_g_ws_ws_wws8111','5|3|1','7','6','对碰奖百分比','见单奖','领导奖','领导奖封顶','C网见单金额 1级|2级','80089701','48zxYEVxfiC5k3el8dwQ49trHJ6hqvbpoP3n2nbu3tUdxv5ylUo1qxLUphGoiNgKgwruZRJvccxkFqTvnhVwgAYQd8Yu6MmFnkWbdlzulSzoaBpZnvbaSKD6y1r0TyPp','开户银行','','/A181/Public/Uploads/media/qqcjtzjtyxgs_baofeng.flv','/A136/Public/Images/02.jpg','/A136/Public/Images/03.jpg','中文|英文|马来西亚|泰语','中国|美国|马来西亚|泰国','','欢迎您使用本系统，系统暂时关闭结算中，请您耐心等待。','恭喜您注册成功，请联系公司客服！\r\n客服电话：4009918867\r\n联系电话：15653867267\r\n客服QQ：1803396384','农业银行|工商银行|建设银行|财付通','','你爱人叫什么名字？|你家养有什么宠物？|你的生日是多少？|你最喜欢什么？|你的出生地是？',4,1544692749,1544371200,54440.00,2000.00,1,0.10,0.10,0.10,0,0,'5','50','10','1:1',300000,0,'2|4|4|7|9|11|13','15','50','8|6|4','5|2|2|2|2|2|2|2|1','2','3','','级差|拓展|管理|合作|领导|福利分红|店补|二次消费','{\"credit\":\"2\",\"bonus\":\"5\",\"electron\":\"2\",\"recharge\":\"2\"}');
/*!40000 ALTER TABLE `xt_fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_fenhong`
--

DROP TABLE IF EXISTS `xt_fenhong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_fenhong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `f_num` decimal(12,2) NOT NULL DEFAULT '0.00',
  `f_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `pdt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_fenhong`
--

LOCK TABLES `xt_fenhong` WRITE;
/*!40000 ALTER TABLE `xt_fenhong` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_fenhong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_form`
--

DROP TABLE IF EXISTS `xt_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `e_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `e_content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `baile` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_form`
--

LOCK TABLES `xt_form` WRITE;
/*!40000 ALTER TABLE `xt_form` DISABLE KEYS */;
INSERT INTO `xt_form` VALUES (75,'测试新闻','<p>&nbsp;啦啦啦啦阿里</p>','','',1,1541992487,0,1,0,'1');
/*!40000 ALTER TABLE `xt_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_form_class`
--

DROP TABLE IF EXISTS `xt_form_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_form_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `baile` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_form_class`
--

LOCK TABLES `xt_form_class` WRITE;
/*!40000 ALTER TABLE `xt_form_class` DISABLE KEYS */;
INSERT INTO `xt_form_class` VALUES (1,'新闻公告',0);
/*!40000 ALTER TABLE `xt_form_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_game`
--

DROP TABLE IF EXISTS `xt_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_game` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `g_money` decimal(12,2) NOT NULL,
  `used_money` decimal(12,2) NOT NULL,
  `get_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_game`
--

LOCK TABLES `xt_game` WRITE;
/*!40000 ALTER TABLE `xt_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_gouwu`
--

DROP TABLE IF EXISTS `xt_gouwu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_gouwu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `lx` int(11) NOT NULL,
  `ispay` smallint(2) NOT NULL,
  `pdt` int(11) NOT NULL,
  `pd_date` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '购买日期（年月）',
  `money` decimal(12,2) DEFAULT NULL COMMENT '单价',
  `shu` int(11) NOT NULL DEFAULT '0' COMMENT '件数',
  `sh_money` decimal(12,2) unsigned DEFAULT NULL COMMENT '所需购物币',
  `cash` decimal(12,2) unsigned DEFAULT NULL COMMENT '现金',
  `cprice` decimal(12,2) DEFAULT NULL COMMENT '总价',
  `pvzhi` decimal(12,2) NOT NULL,
  `guquan` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `s_type` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `us_tel` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `isfh` int(11) NOT NULL DEFAULT '0',
  `fhdt` int(11) NOT NULL DEFAULT '0',
  `okdt` int(11) NOT NULL DEFAULT '0',
  `ccxhbz` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `countid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ok_pay` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_gouwu`
--

LOCK TABLES `xt_gouwu` WRITE;
/*!40000 ALTER TABLE `xt_gouwu` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_gouwu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_gp`
--

DROP TABLE IF EXISTS `xt_gp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_gp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gp_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '股票名称',
  `danhao` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '股票单号',
  `opening` decimal(12,2) NOT NULL COMMENT '开盘价',
  `closing` decimal(12,2) NOT NULL COMMENT '收盘价',
  `today` decimal(12,2) NOT NULL COMMENT '当前报价',
  `most_g` decimal(12,2) NOT NULL COMMENT '最高价',
  `most_d` decimal(12,2) NOT NULL COMMENT '最低价',
  `up` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '涨幅',
  `down` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '跌幅',
  `gp_quantity` int(11) NOT NULL DEFAULT '0' COMMENT '股票数量',
  `cgp_num` int(11) NOT NULL DEFAULT '0',
  `gp_zongji` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '总价',
  `turnover` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '成交量',
  `f_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '发布时间',
  `status` int(11) NOT NULL COMMENT '状态（0关闭1开启）',
  `pao_num` int(11) NOT NULL DEFAULT '0',
  `ca_numb` int(11) NOT NULL DEFAULT '0',
  `all_sell` int(11) NOT NULL DEFAULT '0',
  `day_sell` int(11) NOT NULL DEFAULT '0',
  `yt_sellnum` int(11) NOT NULL DEFAULT '0',
  `buy_num` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `fx_num` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_gp`
--

LOCK TABLES `xt_gp` WRITE;
/*!40000 ALTER TABLE `xt_gp` DISABLE KEYS */;
INSERT INTO `xt_gp` VALUES (1,'基金','GP1308185648',0.10,0.10,0.10,0.10,0.10,NULL,'',0,212672,0.00,'0','1544925867',1,0,0,192672,0,0,0,0,0);
/*!40000 ALTER TABLE `xt_gp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_gp_sell`
--

DROP TABLE IF EXISTS `xt_gp_sell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_gp_sell` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `sNun` int(11) NOT NULL DEFAULT '0',
  `ispay` int(2) NOT NULL DEFAULT '0',
  `eDate` int(11) NOT NULL DEFAULT '0',
  `sell_mm` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sell_ln` int(11) NOT NULL DEFAULT '0',
  `sell_mon` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `sell_date` int(11) NOT NULL DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_gp_sell`
--

LOCK TABLES `xt_gp_sell` WRITE;
/*!40000 ALTER TABLE `xt_gp_sell` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_gp_sell` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_gupiao`
--

DROP TABLE IF EXISTS `xt_gupiao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_gupiao` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `pid` int(12) NOT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `one_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sNun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `used_num` int(12) NOT NULL,
  `lnum` int(12) NOT NULL,
  `ispay` int(2) NOT NULL,
  `status` smallint(2) NOT NULL,
  `eDate` int(11) NOT NULL,
  `type` smallint(2) NOT NULL,
  `is_en` smallint(1) NOT NULL DEFAULT '0',
  `sell_mon` int(11) NOT NULL DEFAULT '0',
  `sell_num` int(11) NOT NULL DEFAULT '0',
  `sell_date` int(11) NOT NULL DEFAULT '0',
  `is_over` smallint(1) NOT NULL DEFAULT '0',
  `bz` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `buy_s` decimal(12,2) NOT NULL DEFAULT '0.00',
  `buy_a` decimal(12,2) NOT NULL DEFAULT '0.00',
  `buy_nn` int(11) NOT NULL DEFAULT '0',
  `sell_g` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_cancel` int(11) NOT NULL DEFAULT '0',
  `spid` int(11) NOT NULL DEFAULT '0',
  `last_s` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_gupiao`
--

LOCK TABLES `xt_gupiao` WRITE;
/*!40000 ALTER TABLE `xt_gupiao` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_gupiao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_hgupiao`
--

DROP TABLE IF EXISTS `xt_hgupiao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_hgupiao` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `pid` int(12) NOT NULL,
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gprice` decimal(12,2) NOT NULL DEFAULT '0.00',
  `one_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `gmp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `pmp` decimal(12,2) NOT NULL DEFAULT '0.00',
  `sNun` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ispay` int(2) NOT NULL,
  `eDate` int(11) NOT NULL,
  `type` smallint(2) NOT NULL,
  `is_en` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_hgupiao`
--

LOCK TABLES `xt_hgupiao` WRITE;
/*!40000 ALTER TABLE `xt_hgupiao` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_hgupiao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_history`
--

DROP TABLE IF EXISTS `xt_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `did` int(11) NOT NULL,
  `user_did` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '40为注册绩效奖金标识',
  `pdt` int(11) NOT NULL,
  `allp` decimal(12,2) NOT NULL,
  `bz` text NOT NULL,
  `type` smallint(1) NOT NULL DEFAULT '0' COMMENT '充值0明细1，2为注册绩效奖，3分红，4为特别贡献奖',
  `act_pdt` int(11) NOT NULL DEFAULT '0',
  `epoints` decimal(12,2) DEFAULT NULL COMMENT '扣费之前奖金',
  `take_home` decimal(12,2) unsigned DEFAULT '0.00' COMMENT '实得奖金',
  `fain_fee` decimal(12,2) unsigned DEFAULT '0.00' COMMENT '10%维护费',
  `option` decimal(12,2) unsigned DEFAULT '0.00' COMMENT '扣10%期权',
  `is_count` tinyint(4) NOT NULL DEFAULT '0',
  `link` int(11) DEFAULT NULL COMMENT '这里指奖金关系人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19063 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_history`
--

LOCK TABLES `xt_history` WRITE;
/*!40000 ALTER TABLE `xt_history` DISABLE KEYS */;
INSERT INTO `xt_history` VALUES (19046,1,'100000',0,'','',1544692807,0.00,'级差',0,0,2600.00,2340.00,260.00,0.00,1,6693939),(19047,467,'6693939',0,'','',1544692810,0.00,'合作',0,0,208.00,187.20,20.80,0.00,0,100000),(19048,1,'100000',0,'','',1544692810,0.00,'拓展',0,0,3000.00,2700.00,300.00,0.00,1,6693939),(19049,1,'100000',0,'','',1544692810,0.00,'店补',0,0,600.00,540.00,60.00,0.00,1,6693939),(19050,1,'100000',0,'','',1544692875,0.00,'级差',0,0,2600.00,2340.00,260.00,0.00,1,5842224),(19051,468,'5842224',0,'','',1544692875,0.00,'合作',0,0,208.00,187.20,20.80,0.00,1,100000),(19052,467,'6693939',0,'','',1544692875,0.00,'合作',0,0,156.00,140.40,15.60,0.00,0,100000),(19053,1,'100000',0,'','',1544692875,0.00,'拓展',0,0,3000.00,2700.00,300.00,0.00,1,5842224),(19054,1,'100000',0,'','',1544692875,0.00,'店补',0,0,600.00,540.00,60.00,0.00,1,5842224),(19055,468,'5842224',0,'','',1544871623,0.00,'级差',0,0,800.00,720.00,80.00,0.00,1,5496429),(19056,1,'100000',0,'','',1544871623,0.00,'管理',0,0,400.00,360.00,40.00,0.00,1,5842224),(19057,469,'5496429',0,'','',1544871623,0.00,'合作',0,0,64.00,57.60,6.40,0.00,1,5842224),(19058,1,'100000',0,'','',1544871623,0.00,'级差',0,0,1800.00,1620.00,180.00,0.00,1,5842224),(19059,468,'5842224',0,'','',1544871623,0.00,'合作',0,0,144.00,129.60,14.40,0.00,1,100000),(19060,467,'6693939',0,'','',1544871623,0.00,'合作',0,0,108.00,97.20,10.80,0.00,0,100000),(19061,468,'5842224',0,'','',1544871623,0.00,'拓展',0,0,3000.00,2700.00,300.00,0.00,1,5496429),(19062,1,'100000',0,'','',1544871623,0.00,'店补',0,0,600.00,540.00,60.00,0.00,1,5496429);
/*!40000 ALTER TABLE `xt_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_huikui`
--

DROP TABLE IF EXISTS `xt_huikui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_huikui` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `touzi` varchar(255) CHARACTER SET latin1 NOT NULL,
  `zhuangkuang` varchar(255) CHARACTER SET latin1 NOT NULL,
  `hk` decimal(12,2) NOT NULL,
  `time_hk` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_huikui`
--

LOCK TABLES `xt_huikui` WRITE;
/*!40000 ALTER TABLE `xt_huikui` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_huikui` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_img`
--

DROP TABLE IF EXISTS `xt_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `c_time` int(11) NOT NULL DEFAULT '0',
  `small_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_img`
--

LOCK TABLES `xt_img` WRITE;
/*!40000 ALTER TABLE `xt_img` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_item`
--

DROP TABLE IF EXISTS `xt_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `fsize` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `zip_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip_title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_read` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_item`
--

LOCK TABLES `xt_item` WRITE;
/*!40000 ALTER TABLE `xt_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_jia`
--

DROP TABLE IF EXISTS `xt_jia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_jia` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(5) DEFAULT NULL COMMENT '用户id',
  `jia_achievement` int(12) DEFAULT NULL COMMENT '增加的业绩',
  `time` char(15) DEFAULT NULL COMMENT '产生业绩的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3091 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_jia`
--

LOCK TABLES `xt_jia` WRITE;
/*!40000 ALTER TABLE `xt_jia` DISABLE KEYS */;
INSERT INTO `xt_jia` VALUES (1632,225,0,'1542096420'),(1633,226,0,'1542096420'),(1634,227,0,'1542096420'),(1635,228,0,'1542096420'),(3035,228,0,'1542176962'),(3036,1,0,'1542176962'),(3037,228,0,'1542176995'),(3038,228,0,'1542177076'),(3039,1,0,'1542177076'),(3040,228,0,'1542177244'),(3041,1,0,'1542177244'),(3042,228,0,'1542177309'),(3043,1,0,'1542177309'),(3044,228,0,'1542177379'),(3045,1,0,'1542177380'),(3046,228,0,'1542177486'),(3047,228,0,'1542177559'),(3048,228,0,'1542177627'),(3049,1,0,'1542177627'),(3050,228,0,'1542177682'),(3051,228,0,'1542177747'),(3052,228,0,'1542177781'),(3053,1,0,'1542177781'),(3054,228,5600,'1542177813'),(3055,1,5600,'1542177813'),(3056,228,5600,'1542177856'),(3057,1,20000,'1544672308'),(3058,1,20000,'1544672510'),(3059,1,2000,'1544672671'),(3060,440,2000,'1544672829'),(3061,1,2000,'1544672829'),(3062,1,20000,'1544673022'),(3063,442,2000,'1544673090'),(3064,1,2000,'1544673090'),(3065,1,20000,'1544681624'),(3066,1,20000,'1544682172'),(3067,1,20000,'1544682592'),(3068,1,20000,'1544682918'),(3069,1,20000,'1544685748'),(3070,1,20000,'1544686031'),(3071,1,20000,'1544686284'),(3072,1,2000,'1544686374'),(3073,1,20000,'1544686503'),(3074,1,20000,'1544686566'),(3075,1,20000,'1544687128'),(3076,1,20000,'1544687206'),(3077,1,20000,'1544687313'),(3078,1,20000,'1544687454'),(3079,1,20000,'1544687680'),(3080,1,20000,'1544687994'),(3081,1,20000,'1544688338'),(3082,1,20000,'1544688822'),(3083,1,20000,'1544692070'),(3084,1,20000,'1544692146'),(3085,1,20000,'1544692493'),(3086,1,20000,'1544692721'),(3087,1,20000,'1544692810'),(3088,1,20000,'1544692875'),(3089,468,20000,'1544871623'),(3090,1,20000,'1544871623');
/*!40000 ALTER TABLE `xt_jia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_jiadan`
--

DROP TABLE IF EXISTS `xt_jiadan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_jiadan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `adt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `danshu` smallint(5) NOT NULL DEFAULT '0' COMMENT '单数',
  `is_pay` smallint(3) NOT NULL DEFAULT '0',
  `up_level` smallint(2) NOT NULL DEFAULT '0',
  `out_level` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_jiadan`
--

LOCK TABLES `xt_jiadan` WRITE;
/*!40000 ALTER TABLE `xt_jiadan` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_jiadan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_leader_achievement`
--

DROP TABLE IF EXISTS `xt_leader_achievement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_leader_achievement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `achievement` decimal(20,2) DEFAULT NULL COMMENT '业绩',
  `createtime` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=491 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_leader_achievement`
--

LOCK TABLES `xt_leader_achievement` WRITE;
/*!40000 ALTER TABLE `xt_leader_achievement` DISABLE KEYS */;
INSERT INTO `xt_leader_achievement` VALUES (490,469,20000.00,1544871623),(489,468,20000.00,1544692875),(488,467,20000.00,1544692810),(487,466,20000.00,1544692721),(486,465,20000.00,1544692493),(485,464,20000.00,1544692147),(484,463,20000.00,1544692070),(483,462,20000.00,1544688823),(482,461,20000.00,1544688339),(481,460,20000.00,1544687995),(480,459,20000.00,1544687680),(479,458,20000.00,1544687454),(478,457,20000.00,1544687313),(477,456,20000.00,1544687206),(476,455,20000.00,1544687128),(475,454,20000.00,1544686566),(474,453,20000.00,1544686503),(473,452,2000.00,1544686374),(472,451,20000.00,1544686284),(471,450,20000.00,1544686031),(470,449,20000.00,1544685748),(469,448,20000.00,1544682918),(468,447,20000.00,1544682592),(467,446,20000.00,1544682172),(466,445,20000.00,1544681624),(465,443,2000.00,1544673090),(464,442,20000.00,1544673022),(463,441,2000.00,1544672829),(462,440,2000.00,1544672671),(461,439,20000.00,1544672510),(460,438,20000.00,1544672308),(459,437,2000.00,1544260881),(458,436,2000.00,1544259304),(457,435,2000.00,1544252644);
/*!40000 ALTER TABLE `xt_leader_achievement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_leader_bonus_log`
--

DROP TABLE IF EXISTS `xt_leader_bonus_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_leader_bonus_log` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `achievement` decimal(20,2) DEFAULT NULL COMMENT '业绩',
  `bonus` decimal(20,2) DEFAULT NULL COMMENT '奖金',
  `starttime` int(20) DEFAULT NULL COMMENT '开始时间',
  `endtime` int(20) DEFAULT NULL COMMENT '结束时间',
  `createtime` int(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_leader_bonus_log`
--

LOCK TABLES `xt_leader_bonus_log` WRITE;
/*!40000 ALTER TABLE `xt_leader_bonus_log` DISABLE KEYS */;
INSERT INTO `xt_leader_bonus_log` VALUES (3,29600000.00,658200.00,1541001601,1543593600,1542096593),(4,1680000.00,48000.00,1541001601,1543593600,1542160711),(5,1680000.00,48000.00,1541001601,1543593600,1542160786),(6,1680000.00,48000.00,1541001601,1543593600,1542160931),(7,1680000.00,48000.00,1541001601,1543593600,1542160967),(8,1680000.00,48000.00,1541001601,1543593600,1542161065),(9,1680000.00,48000.00,1541001601,1543593600,1542161110);
/*!40000 ALTER TABLE `xt_leader_bonus_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_msg`
--

DROP TABLE IF EXISTS `xt_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_uid` int(11) NOT NULL DEFAULT '0',
  `f_user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `s_uid` int(11) NOT NULL DEFAULT '0',
  `s_user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `f_time` int(11) NOT NULL DEFAULT '0',
  `f_del` smallint(3) NOT NULL DEFAULT '0',
  `s_del` smallint(3) NOT NULL DEFAULT '0',
  `f_read` smallint(3) NOT NULL DEFAULT '0',
  `s_read` smallint(3) NOT NULL DEFAULT '0',
  `reply_content` text CHARACTER SET utf8,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_msg`
--

LOCK TABLES `xt_msg` WRITE;
/*!40000 ALTER TABLE `xt_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_news_a`
--

DROP TABLE IF EXISTS `xt_news_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_news_a` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `n_title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_content` text COLLATE utf8_unicode_ci NOT NULL,
  `n_top` int(11) NOT NULL DEFAULT '0',
  `n_status` tinyint(1) NOT NULL DEFAULT '1',
  `n_create_time` int(11) NOT NULL,
  `n_update_time` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_news_a`
--

LOCK TABLES `xt_news_a` WRITE;
/*!40000 ALTER TABLE `xt_news_a` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_news_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_news_class`
--

DROP TABLE IF EXISTS `xt_news_class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_news_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) NOT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_news_class`
--

LOCK TABLES `xt_news_class` WRITE;
/*!40000 ALTER TABLE `xt_news_class` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_news_class` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_order`
--

DROP TABLE IF EXISTS `xt_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户表id',
  `ordersn` varchar(24) NOT NULL DEFAULT '' COMMENT '订单编号',
  `order_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `cash` decimal(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '需付现金',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '生成订单时间',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '订单状态',
  `address_id` int(11) unsigned DEFAULT NULL COMMENT '地址id',
  `total` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '商品总件数',
  `is_pay` tinyint(4) DEFAULT '0' COMMENT '是否付款',
  `pay_time` int(11) unsigned DEFAULT NULL COMMENT '付款时间',
  `is_delive` tinyint(4) DEFAULT '0',
  `logi_no` varchar(200) DEFAULT NULL COMMENT '物流编号',
  `delive_time` int(11) DEFAULT NULL,
  `shopping_money` decimal(12,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '所需购物币',
  `user_id` int(11) NOT NULL COMMENT '用户编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_order`
--

LOCK TABLES `xt_order` WRITE;
/*!40000 ALTER TABLE `xt_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_order_goods`
--

DROP TABLE IF EXISTS `xt_order_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_order_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `total` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '单件商品总件数',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '商品价格',
  `orderid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '订单id',
  `cptype` tinyint(4) DEFAULT NULL COMMENT '商品分类',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单商品';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_order_goods`
--

LOCK TABLES `xt_order_goods` WRITE;
/*!40000 ALTER TABLE `xt_order_goods` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_order_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_peng`
--

DROP TABLE IF EXISTS `xt_peng`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_peng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL,
  `ceng` int(12) NOT NULL,
  `l` int(12) NOT NULL,
  `r` int(12) NOT NULL,
  `l1` int(12) NOT NULL,
  `r1` int(12) NOT NULL,
  `l2` int(12) NOT NULL,
  `r2` int(12) NOT NULL,
  `l3` int(12) NOT NULL,
  `r3` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_peng`
--

LOCK TABLES `xt_peng` WRITE;
/*!40000 ALTER TABLE `xt_peng` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_peng` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_plan`
--

DROP TABLE IF EXISTS `xt_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '奖励计划',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_plan`
--

LOCK TABLES `xt_plan` WRITE;
/*!40000 ALTER TABLE `xt_plan` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_product`
--

DROP TABLE IF EXISTS `xt_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_product` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `cid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cptype` int(11) NOT NULL DEFAULT '0',
  `ccname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `xhname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `money` decimal(12,2) DEFAULT '0.00',
  `a_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `b_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `create_time` int(11) DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `yc_cp` int(11) NOT NULL DEFAULT '0',
  `countid` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_reg` int(11) NOT NULL DEFAULT '0',
  `edit_time` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `market_price` decimal(8,2) DEFAULT NULL,
  `stock` int(8) DEFAULT NULL,
  `status` tinyint(2) unsigned DEFAULT '1',
  `has_option` tinyint(1) unsigned DEFAULT NULL,
  `weight` decimal(6,2) DEFAULT NULL,
  `goods_sn` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_sn` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buy_num` int(8) DEFAULT NULL,
  `is_bd` int(4) DEFAULT NULL,
  `category_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_del` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_product`
--

LOCK TABLES `xt_product` WRITE;
/*!40000 ALTER TABLE `xt_product` DISABLE KEYS */;
INSERT INTO `xt_product` VALUES (48,'1','测试商品',1,'10*10','Z1028',10.00,20.00,0.00,1541985357,'<p>&nbsp;WDQQWESSDADW</p>','/Public/Uploads/image/2018111209155213.jpg',0,'',0,NULL,NULL,NULL,100,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(49,'2','测试商品',2,'10*10','Z1028',10.00,20.00,0.00,1541985357,'<p>&nbsp;WDQQWESSDADW</p>','/Public/Uploads/image/2018111209155213.jpg',0,'',0,NULL,NULL,NULL,100,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(50,'3','测试商品',3,'10*10','Z1028',10.00,20.00,0.00,1541985357,'<p>&nbsp;WDQQWESSDADW</p>','/Public/Uploads/image/2018111209155213.jpg',0,'',0,NULL,NULL,NULL,100,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(51,'4','测试商品',4,'10*10','Z1028',10.00,20.00,0.00,1541985357,'<p>&nbsp;WDQQWESSDADW</p>','/Public/Uploads/image/2018111209155213.jpg',0,'',0,NULL,NULL,NULL,100,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(52,'5','测试商品',5,'10*10','Z1028',10.00,20.00,0.00,1541985357,'<p>&nbsp;WDQQWESSDADW</p>','/Public/Uploads/image/2018111209155213.jpg',0,'',0,NULL,NULL,NULL,100,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `xt_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_promo`
--

DROP TABLE IF EXISTS `xt_promo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_promo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `u_level` smallint(3) NOT NULL DEFAULT '0' COMMENT '升级前级别',
  `uid` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `up_level` smallint(3) NOT NULL COMMENT '升级后级别',
  `danshu` smallint(2) NOT NULL COMMENT '单数',
  `pdt` int(11) NOT NULL,
  `is_pay` smallint(3) NOT NULL DEFAULT '0',
  `u_bank_name` smallint(2) NOT NULL DEFAULT '0' COMMENT '汇款银行',
  `type` smallint(2) NOT NULL DEFAULT '0' COMMENT '0标示晋级，1标示加单',
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_promo`
--

LOCK TABLES `xt_promo` WRITE;
/*!40000 ALTER TABLE `xt_promo` DISABLE KEYS */;
INSERT INTO `xt_promo` VALUES (37,0.00,0.00,3,1,1544692764,7,0,1544692764,1,0,0,' <font color=red>後台升降級</font>','100000');
/*!40000 ALTER TABLE `xt_promo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_recharge`
--

DROP TABLE IF EXISTS `xt_recharge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `amount` decimal(20,2) NOT NULL COMMENT '金额',
  `remark` text COMMENT '备注',
  `status` int(5) NOT NULL DEFAULT '0' COMMENT '状态 -1拒绝 0申请中 1通过',
  `createtime` int(50) NOT NULL COMMENT '申请时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_recharge`
--

LOCK TABLES `xt_recharge` WRITE;
/*!40000 ALTER TABLE `xt_recharge` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_recharge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_region`
--

DROP TABLE IF EXISTS `xt_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '区域主键',
  `area_name` varchar(16) DEFAULT NULL COMMENT '区域名称',
  `area_code` varchar(128) DEFAULT NULL COMMENT '区域代码',
  `area_short` varchar(32) DEFAULT NULL COMMENT '区域简称',
  `area_is_hot` varchar(1) DEFAULT NULL COMMENT '是否热门(0:否、1:是)',
  `area_sequence` int(11) DEFAULT NULL COMMENT '区域序列',
  `area_parent_id` int(11) DEFAULT NULL COMMENT '上级主键',
  `init_date` datetime DEFAULT NULL COMMENT '初始时间',
  `init_addr` varchar(16) DEFAULT NULL COMMENT '初始地址',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`area_parent_id`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='区域字典';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_region`
--

LOCK TABLES `xt_region` WRITE;
/*!40000 ALTER TABLE `xt_region` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_remit`
--

DROP TABLE IF EXISTS `xt_remit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_remit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `b_uid` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `kh_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `or_time` int(11) NOT NULL DEFAULT '0',
  `orderid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bankid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ok_time` int(11) NOT NULL DEFAULT '0',
  `ok_type` int(11) NOT NULL DEFAULT '0',
  `is_pay` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_remit`
--

LOCK TABLES `xt_remit` WRITE;
/*!40000 ALTER TABLE `xt_remit` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_remit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_shop_category`
--

DROP TABLE IF EXISTS `xt_shop_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_shop_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0' COMMENT '所属帐号',
  `name` varchar(50) DEFAULT NULL COMMENT '分类名称',
  `thumb` varchar(255) DEFAULT NULL COMMENT '分类图片',
  `parentid` int(11) DEFAULT '0' COMMENT '上级分类ID,0为第一级',
  `isrecommand` int(10) DEFAULT '0',
  `description` varchar(500) DEFAULT NULL COMMENT '分类介绍',
  `displayorder` tinyint(3) unsigned DEFAULT '0' COMMENT '排序',
  `enabled` tinyint(1) DEFAULT '1' COMMENT '是否开启',
  `ishome` tinyint(3) DEFAULT '0',
  `advimg` varchar(255) DEFAULT '',
  `advurl` varchar(500) DEFAULT '',
  `level` tinyint(3) DEFAULT NULL,
  `min_commission` decimal(10,2) DEFAULT NULL COMMENT '最低佣金比例',
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_displayorder` (`displayorder`),
  KEY `idx_enabled` (`enabled`),
  KEY `idx_parentid` (`parentid`),
  KEY `idx_isrecommand` (`isrecommand`),
  KEY `idx_ishome` (`ishome`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_shop_category`
--

LOCK TABLES `xt_shop_category` WRITE;
/*!40000 ALTER TABLE `xt_shop_category` DISABLE KEYS */;
INSERT INTO `xt_shop_category` VALUES (1,0,'会员资格区',NULL,0,0,NULL,0,1,0,'','',NULL,NULL),(2,0,'每月复购区',NULL,0,0,NULL,0,1,0,'','',NULL,NULL),(3,0,'会员购物区',NULL,0,0,NULL,0,1,0,'','',NULL,NULL),(4,0,'促销产品区',NULL,0,0,NULL,0,1,0,'','',NULL,NULL),(5,0,'专卖店进货区',NULL,0,0,NULL,0,1,0,'','',NULL,NULL);
/*!40000 ALTER TABLE `xt_shop_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_shouru`
--

DROP TABLE IF EXISTS `xt_shouru`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_shouru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `in_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `in_time` int(11) NOT NULL DEFAULT '0',
  `in_bz` text COLLATE utf8_unicode_ci NOT NULL,
  `in_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1:注册，2：用户升级',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16903 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_shouru`
--

LOCK TABLES `xt_shouru` WRITE;
/*!40000 ALTER TABLE `xt_shouru` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_shouru` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_stock`
--

DROP TABLE IF EXISTS `xt_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员id',
  `ordersn` varchar(24) NOT NULL COMMENT '订单编号',
  `order_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '申请状态0/1/2:无效/申请中/审核过',
  `address_id` int(11) unsigned DEFAULT NULL COMMENT '地址id',
  `check_uid` int(11) DEFAULT NULL COMMENT '审核人id',
  `user_id` varchar(30) DEFAULT NULL COMMENT '用户编号',
  `user_name` varchar(30) DEFAULT NULL COMMENT '姓名',
  `content` text,
  `check_time` int(11) DEFAULT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='专卖店进货记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_stock`
--

LOCK TABLES `xt_stock` WRITE;
/*!40000 ALTER TABLE `xt_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_times`
--

DROP TABLE IF EXISTS `xt_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `benqi` int(11) NOT NULL COMMENT '本期结算日期',
  `benqi_date` varchar(36) DEFAULT NULL COMMENT '统计年月日',
  `shangqi` int(11) DEFAULT NULL COMMENT '上期结算日期',
  `income` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '收入',
  `pay` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '支出',
  `proportion` float NOT NULL DEFAULT '0' COMMENT '拨比',
  `type` smallint(2) NOT NULL DEFAULT '0' COMMENT '是否已经结算',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=164 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_times`
--

LOCK TABLES `xt_times` WRITE;
/*!40000 ALTER TABLE `xt_times` DISABLE KEYS */;
INSERT INTO `xt_times` VALUES (163,1544693534,'2018-12',NULL,70000.00,11674.80,17,0);
/*!40000 ALTER TABLE `xt_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_tiqu`
--

DROP TABLE IF EXISTS `xt_tiqu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_tiqu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `rdt` int(11) NOT NULL,
  `money` decimal(12,2) NOT NULL,
  `money_two` decimal(12,2) NOT NULL,
  `epoint` decimal(12,2) NOT NULL,
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_address` varchar(200) NOT NULL,
  `user_tel` varchar(200) NOT NULL,
  `x1` varchar(50) DEFAULT NULL,
  `x2` varchar(50) DEFAULT NULL,
  `x3` varchar(50) DEFAULT NULL,
  `x4` varchar(50) DEFAULT NULL,
  `t_type` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_tiqu`
--

LOCK TABLES `xt_tiqu` WRITE;
/*!40000 ALTER TABLE `xt_tiqu` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_tiqu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_ulevel`
--

DROP TABLE IF EXISTS `xt_ulevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_ulevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) DEFAULT NULL,
  `u_level` smallint(3) DEFAULT '0' COMMENT '升级前级别',
  `uid` int(11) DEFAULT NULL,
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `up_level` smallint(3) DEFAULT NULL COMMENT '升级后级别',
  `danshu` smallint(2) DEFAULT NULL COMMENT '单数',
  `pdt` int(11) DEFAULT NULL,
  `is_pay` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_ulevel`
--

LOCK TABLES `xt_ulevel` WRITE;
/*!40000 ALTER TABLE `xt_ulevel` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_ulevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_voucher`
--

DROP TABLE IF EXISTS `xt_voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `coupon_value` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '券值',
  `caert_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `expiry_date` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '有效期，单位月',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物券';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_voucher`
--

LOCK TABLES `xt_voucher` WRITE;
/*!40000 ALTER TABLE `xt_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_voucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_xfhistory`
--

DROP TABLE IF EXISTS `xt_xfhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_xfhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `did` int(11) NOT NULL,
  `d_user_id` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pdt` int(11) NOT NULL,
  `epoints` decimal(12,2) NOT NULL,
  `allp` decimal(12,2) NOT NULL,
  `bz` text COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(1) NOT NULL COMMENT '充值0明细1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_xfhistory`
--

LOCK TABLES `xt_xfhistory` WRITE;
/*!40000 ALTER TABLE `xt_xfhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_xfhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_xiaof`
--

DROP TABLE IF EXISTS `xt_xiaof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_xiaof` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `rdt` int(11) NOT NULL,
  `pdt` int(11) NOT NULL DEFAULT '0',
  `money` decimal(12,2) NOT NULL,
  `money_two` int(11) NOT NULL DEFAULT '0',
  `epoint` decimal(12,2) NOT NULL,
  `fh_money` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_pay` int(11) NOT NULL,
  `user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bank_card` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `x1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x3` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `x4` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_xiaof`
--

LOCK TABLES `xt_xiaof` WRITE;
/*!40000 ALTER TABLE `xt_xiaof` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_xiaof` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_xml`
--

DROP TABLE IF EXISTS `xt_xml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_xml` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(12,2) NOT NULL,
  `amount` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `x_date` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `is_lock` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_xml`
--

LOCK TABLES `xt_xml` WRITE;
/*!40000 ALTER TABLE `xt_xml` DISABLE KEYS */;
INSERT INTO `xt_xml` VALUES (116,0.10,'0','1525881600',0),(117,0.10,'0','1527091200',0),(118,0.10,'0','1533225600',0),(119,0.10,'0','1533312000',0),(120,0.10,'0','1534608000',0),(121,0.10,'0','1536940800',0),(122,0.10,'0','1537113600',0),(123,0.10,'0','1539360000',0),(124,0.10,'0','1539532800',0),(125,0.10,'0','1539619200',0),(126,0.10,'0','1539705600',0),(127,0.10,'0','1539792000',0),(128,0.10,'0','1539878400',0),(129,0.10,'0','1539964800',0),(130,0.10,'0','1540051200',0),(131,0.10,'0','1540137600',0),(132,0.10,'0','1540224000',0),(133,0.10,'0','1541088000',0),(134,0.10,'0','1541347200',0),(135,0.10,'0','1541433600',0),(136,0.10,'0','1541520000',0),(137,0.10,'0','1541606400',0),(138,0.10,'0','1541692800',0),(139,0.10,'0','1541779200',0),(140,0.10,'0','1541952000',0),(141,0.10,'0','1542038400',0),(142,0.10,'0','1542124800',0),(143,0.10,'0','1542211200',0),(144,0.10,'0','1542297600',0),(145,0.10,'0','1542729600',0),(146,0.10,'0','1542816000',0),(147,0.10,'0','1542988800',0),(148,0.10,'0','1544198400',0),(149,0.10,'0','1544630400',0),(150,0.10,'0','1544803200',0);
/*!40000 ALTER TABLE `xt_xml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `xt_zhuanj`
--

DROP TABLE IF EXISTS `xt_zhuanj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xt_zhuanj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `in_uid` int(11) DEFAULT NULL,
  `out_uid` int(11) DEFAULT NULL,
  `in_userid` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `out_userid` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `epoint` decimal(12,2) DEFAULT NULL,
  `rdt` int(11) DEFAULT NULL,
  `sm` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `xt_zhuanj`
--

LOCK TABLES `xt_zhuanj` WRITE;
/*!40000 ALTER TABLE `xt_zhuanj` DISABLE KEYS */;
/*!40000 ALTER TABLE `xt_zhuanj` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-16 10:55:02
