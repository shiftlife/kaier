﻿<?php

 return array(
     'URL_MODEL'=>1, // 如果你的环境不支持PATHINFO 请设置为3
 	//'URL_PATHINFO_MODEL'=>2,
 	'DB_TYPE'=>'mysql',
 	'DB_HOST'=>'127.0.0.1',
 	'DB_NAME'=>'fx_vip',
 	'DB_USER'=>'root',
 	'DB_PWD'=>'root',
 	'DB_PORT'=>'3306',
 	'DB_PREFIX'=>'xt_',
 	'DB_FIELDS_CACHE'   => true,//设置是否启用数据库字段缓存
 )

// 线下测试数据库
//return array(
//    'URL_MODEL'=>1, // 如果你的环境不支持PATHINFO 请设置为3
//	//'URL_PATHINFO_MODEL'=>2,
//	'DB_TYPE'=>'mysql',
//	'DB_HOST'=>'localhost',
//	'DB_NAME'=>'fx_vip2',
//	'DB_USER'=>'root',
//	'DB_PWD'=>'',
//	'DB_PORT'=>'3306',
//	'DB_PREFIX'=>'xt_',
//	'DB_FIELDS_CACHE'   => false,//设置是否启用数据库字段缓存
//)

?>
